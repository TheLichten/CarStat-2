2.3.1
-Added Search bar to VEHSelector
-Changed in SetupManager the Date Column formate to year.month.day
-Fixed pressing the ExpandAll button in VEHSelector crashing CarStat
-Fixed CarStat being crashed when opening an encrypted mod with non encrypted only mode (default) enabled
-Fixed Wheelbase Display
-Fixed FSR 2016 and 2018 mods breaking CarStat through malformed data
-Fixed more missing data will now be accepted without asking for filler data

2.3.0 - A Question Of Setup
-Added Setup Creation Tap
-Added Notification on Startup if there is a new version, with a download link
-Added Setup Manager (Found in the toolbar under others), which allows you to search, copy, move and delete your setups
-Added Info for Weight Penalties with driver AIDs.
-Added Info on Wheelbase of a car
-Added Info about the file names of the files used for cars
-Added start up parameter -mode to allow you to open CarStat fr a certain game, without overwriting your autostart. Available options: -mode rf2, -mod rf1, -mode gtr2
-Added Button to return to the CarStat Selector (Game selection window)
-Added the Options menu, and moved all options there
-Added Fuel per minute (very rough number)
-Changed the Description of Optimum Oil Temperature
-Made the Open Checked Car(s) button in the Vehicle Selector displaying Loading... while a car is loading.
-Fixed the Engine Graph a bit more by implementing TorqueCurveShift and Overpressure scaling (5th rpmbase value)
-Fixed Extra Engine Friction not refreshing the Labels correctly.
-Fixed a potential bug of the hdv not being found
-Fixed Fahrenheit causing exceptions when creating the brake graphs
-Fixed a lot of mods by adding default values for missing property's
-Fixed DW12 steering lock value being now displayed
-Fixed boost being defined outside of the engine file (DW12)
-Fixed Woochoo's Endurance Carset breaking the engine graph
-Fixed Multiview not deleting the car icons out of the temporary files
-Fixed using different Games in carstat deletes the saved Game Location (at least for rf2 to rf1/GTL, not rf1 and GTL)
-Brought Engine Values for the BT55 a bit closer to reality, but it is still very bad

2.2.2
-Added Semi Auto Gearbox Type to General
-Added if Anti Stall is in the car to General
-Added Tabs (General and Other) to the Engine Tab (to display more Data)
-Added Fuel Consumtion Value (very rough) to the other tab
-Added Engine Inertia to the other tab
-Added Turbo Pressure to the other tab
-Added notification when changing tire tabs from any non master in multiview
-Moved the Include Extra Engine Friction to the other tab
-Moved Pitstop Simulation into the Pitstop tab
-Included special Torque and Power multi of boost mapping (used rarely) in Engine Graph
-Average Lifetime of the Engine now scales with the increase of Boost mapping
-Removed notification when showing upgrades from any non master in multiview
-Fixed Max Power and Max Torque being to low
-Fixed Speedlimiter and Start being shown as unavaible when available
-Fixed Upgrades not showing in multiview
-Fixed Tire Degredation Graph 2 not changing values

2.2.1
-Fixed selecting engine mix crashing CarStat
-Fixed bug that did not uncheck the engine mix
-Fixed Revlimiter being too short (Race Truck)
-Fixed Initial scaling for Engine Graph of Turbo charged Engines
-Fixed EnduRacers Flat6 GTC crashing CarStat

2.2.0 - Turbo!
-Added very basic turbo support (turbo v0.1). It assumes that max pressure is all the time.
-Added Option to show the engine with Turbo-Charger (only turbo charged cars) and without (all).
-Added Option for different Engine-Mixtures
-Added Option for different Boost Mappings
-Added Rev Limiter to Engine Graph
-Added CarIcon to the General Info (also changed the layout of it)
-Added Webhelp into the Help Window (and removed pdf)
-Added About Page will if there is a newer version avaible
-Added Anti-Rollbar and Trackbar adjustments in the cockpit to the general info
-When running multiview, the bar hight in the Tires>General Info is now comparble
-Updated CarStat 2 icon color to a slick blue
-Updated rf2 icon in CarStat Selector
-Optimization of a lot of things (now uses multithreading)
-Fixed wording of which folder to select in the VEHicle Selector
-Full support for left-right tires
-Fixed usf 2000 not loading
-Fixed Tires>General Info bug when deselting compounds
-Fixed Potential Crash when opening a new vehicle with multiview of 3 or more already open
-Fixed Crash when opening a new car when not being on the General Tab
-Bodged Crash when opening A1GP mod (still throwing exceptions, because it is a farily broken mod, at the moment at least)

2.1.1
-Added display about the avaibility of tire pressure change while changing tires
-Tire Pressure Adjustment time is now correctly ordered to changing pressure without tires
-Fixed Tire Pressure Adjustments on the fly being avaible while changing all tires.
-Fixed Bug that caused certain files not to be found (for example Machine's StockCar 2016 mod)
-Fixed File search so it is not stoping search when the path does not contain rFactor 2, but instead 5 folders up max

2.1.0 - The Return
-Added Pitstop Simulation being simulated for all cars opened in Multiview
-Added options to simulate all car adjustments in the Pitstop Simulation
-Added that options in the Pitstop Simulation will not be avaible if not avaible in the car
-Added checkbox to disable the function lock in Pitstop Simulation
-Closing one of the child windows of Multiview will no longer quit the programm, only when closing the main window
-New Error Box for PitStop Simulation
-Updated About Window
-Changed quickbms version (Even though it is not need, it was time to update)
-Fixed Crash on Unit Change when no car is loaded
-Fixed Radical Weight Distribution display
-Fixed Fuel Tank Capacity
-Fixed Fuel calculation in Pitstop Simulation (Now limited to positiv numbers and lower than fuel tank capacity)
-Fixed Windows overlaying over everything
-Fixed Tire Graphs for cars with same tires all around (Dissenter, sc2015-RoadCourse), and added Bodge for Right-Left Tires (Oval)
-Fixed Tire Rim size calculation for the same reasons
-Fixed Tire Wear Graph for EnduRacers P2 cars
-Fixed Mark Corp Group C Porsch not loading because of Missing Engine File
-Fixed Program crash when switching between upgrades
-Fixed Display and Pitstop Simulation of parallel refueling and tire change
-Fixed Bug when, even though cars are checked, cars will not be loaded (F1 1988)
-Fixed Bug were values that are avaible are displayed as not avaible (Damage Repair Mark Corp Group C cars)

2.0e (Never released beta)
-Fixed Engine Oil Temperatur reading (Caused Labels not to load)
-Fixed detect rFactor 2 install