﻿namespace CommonLibs
{
    partial class VehSelector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VehSelector));
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.btnSelectrFactor = new System.Windows.Forms.Button();
            this.btnCollapse = new System.Windows.Forms.Button();
            this.checkRemember = new System.Windows.Forms.CheckBox();
            this.btnOpenVEH = new System.Windows.Forms.Button();
            this.btnExpand = new System.Windows.Forms.Button();
            this.lblLoading = new System.Windows.Forms.Label();
            this.checkSave = new System.Windows.Forms.CheckBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.listSelected = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(12, 12);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(391, 280);
            this.treeView1.TabIndex = 0;
            this.treeView1.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(treeView1_NodeMouseDoubleClick);
            // 
            // btnSelectrFactor
            // 
            this.btnSelectrFactor.Location = new System.Drawing.Point(12, 298);
            this.btnSelectrFactor.Name = "btnSelectrFactor";
            this.btnSelectrFactor.Size = new System.Drawing.Size(153, 28);
            this.btnSelectrFactor.TabIndex = 1;
            this.btnSelectrFactor.Text = "Select rFactor folder";
            this.btnSelectrFactor.UseVisualStyleBackColor = true;
            this.btnSelectrFactor.Click += new System.EventHandler(this.btnSelectrFactor_Click);
            // 
            // btnCollapse
            // 
            this.btnCollapse.Location = new System.Drawing.Point(418, 12);
            this.btnCollapse.Name = "btnCollapse";
            this.btnCollapse.Size = new System.Drawing.Size(105, 23);
            this.btnCollapse.TabIndex = 2;
            this.btnCollapse.Text = "Collapse All";
            this.btnCollapse.UseVisualStyleBackColor = true;
            this.btnCollapse.Click += new System.EventHandler(this.btnCollapse_Click);
            // 
            // checkRemember
            // 
            this.checkRemember.AutoSize = true;
            this.checkRemember.Location = new System.Drawing.Point(12, 332);
            this.checkRemember.Name = "checkRemember";
            this.checkRemember.Size = new System.Drawing.Size(153, 17);
            this.checkRemember.TabIndex = 3;
            this.checkRemember.Text = "Remember rFactor location";
            this.checkRemember.UseVisualStyleBackColor = true;
            this.checkRemember.CheckedChanged += new System.EventHandler(this.checkRemember_CheckedChanged);
            // 
            // btnOpenVEH
            // 
            this.btnOpenVEH.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenVEH.Location = new System.Drawing.Point(418, 194);
            this.btnOpenVEH.Name = "btnOpenVEH";
            this.btnOpenVEH.Size = new System.Drawing.Size(105, 98);
            this.btnOpenVEH.TabIndex = 4;
            this.btnOpenVEH.Text = "Open selected car(s)";
            this.btnOpenVEH.UseVisualStyleBackColor = true;
            this.btnOpenVEH.Click += new System.EventHandler(this.btnOpenVEH_Click);
            // 
            // btnExpand
            // 
            this.btnExpand.Location = new System.Drawing.Point(418, 41);
            this.btnExpand.Name = "btnExpand";
            this.btnExpand.Size = new System.Drawing.Size(105, 23);
            this.btnExpand.TabIndex = 5;
            this.btnExpand.Text = "Expand All";
            this.btnExpand.UseVisualStyleBackColor = true;
            this.btnExpand.Click += new System.EventHandler(this.btnExpand_Click);
            // 
            // lblLoading
            // 
            this.lblLoading.AutoSize = true;
            this.lblLoading.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoading.Location = new System.Drawing.Point(367, 309);
            this.lblLoading.Name = "lblLoading";
            this.lblLoading.Size = new System.Drawing.Size(0, 20);
            this.lblLoading.TabIndex = 7;
            // 
            // checkSave
            // 
            this.checkSave.AutoSize = true;
            this.checkSave.Location = new System.Drawing.Point(386, 309);
            this.checkSave.Name = "checkSave";
            this.checkSave.Size = new System.Drawing.Size(137, 17);
            this.checkSave.TabIndex = 8;
            this.checkSave.Text = "Save VEHicle locations";
            this.toolTip1.SetToolTip(this.checkSave, resources.GetString("checkSave.ToolTip"));
            this.checkSave.UseVisualStyleBackColor = true;
            this.checkSave.CheckedChanged += new System.EventHandler(this.checkSave_CheckedChanged);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(386, 326);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(132, 23);
            this.btnUpdate.TabIndex = 9;
            this.btnUpdate.Text = "Update VEHicle locations";
            this.toolTip1.SetToolTip(this.btnUpdate, "Updates the list of Vehicle locations. This is necessary when\r\nyou use the \"Save\"" +
                    " option and installed a new mod meanwhile.");
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 20000;
            this.toolTip1.InitialDelay = 500;
            this.toolTip1.ReshowDelay = 100;
            // 
            // listSelected
            // 
            this.listSelected.FormattingEnabled = true;
            this.listSelected.Location = new System.Drawing.Point(418, 95);
            this.listSelected.Name = "listSelected";
            this.listSelected.Size = new System.Drawing.Size(105, 95);
            this.listSelected.TabIndex = 10;
            // 
            // VehSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(539, 358);
            this.Controls.Add(this.listSelected);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.checkSave);
            this.Controls.Add(this.lblLoading);
            this.Controls.Add(this.btnExpand);
            this.Controls.Add(this.btnOpenVEH);
            this.Controls.Add(this.checkRemember);
            this.Controls.Add(this.btnCollapse);
            this.Controls.Add(this.btnSelectrFactor);
            this.Controls.Add(this.treeView1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(555, 396);
            this.MinimumSize = new System.Drawing.Size(540, 396);
            this.Name = "VehSelector";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VehSelector";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Button btnSelectrFactor;
        private System.Windows.Forms.Button btnCollapse;
        private System.Windows.Forms.CheckBox checkRemember;
        private System.Windows.Forms.Button btnOpenVEH;
        private System.Windows.Forms.Button btnExpand;
        private System.Windows.Forms.Label lblLoading;
        private System.Windows.Forms.CheckBox checkSave;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ListBox listSelected;
    }
}