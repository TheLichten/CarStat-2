﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Runtime.Serialization.Formatters.Binary;

namespace CommonLibs
{
    public partial class VehSelector : Form
    {
        List<string> vehfiles = new List<string>();
        private string vehselected = null;
        private string rfactorloc = "";
        private bool init = false;
        private string appName = null;
        public VehSelector(string appName)
        {
            this.appName = appName;
            InitializeComponent();
            this.Visible = true;
            init = true;
            try
            {
                RegistryKey MyReg = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\" + appName, true);
                rfactorloc = "" + MyReg.GetValue("Startupdir");
                checkRemember.Checked = true;
                
            }
            catch (NullReferenceException)
            {
                RegistryKey key = Registry.CurrentUser.OpenSubKey("SOFTWARE", true);
                RegistryKey newkey = key.CreateSubKey(appName);
                newkey.SetValue("Startupdir", "");
            }
            try
            {
                RegistryKey MyReg = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\" + appName, true);
                if ((int)MyReg.GetValue("Savefile") == 0)
                    checkSave.Checked = false;
                else
                    checkSave.Checked = true;
            }
            catch (NullReferenceException)
            {
                RegistryKey key = Registry.CurrentUser.OpenSubKey("SOFTWARE", true);
                RegistryKey newkey = key.CreateSubKey(appName);
                newkey.SetValue("Savefile", 0);
            }
            init = false;
            if (rfactorloc != "")
            {
                findVEH(rfactorloc + @"\GameData\Vehicles");
                makeTree();
            }
        }

        private void btnSelectrFactor_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.ShowNewFolderButton = false;
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                rfactorloc = dialog.SelectedPath;
                checkRemember_CheckedChanged(null, null);
                
            }
            treeView1.Nodes.Clear();
            vehfiles.Clear();
            File.Delete(appName + "_locations.dat");
            findVEH(rfactorloc + @"\GameData\Vehicles");
            lblLoading.Text = "Loading...";
            makeTree();
            lblLoading.Text = "";
            checkSave_CheckedChanged(null, null);
        }

        private void makeTree()
        {
            if (!File.Exists(appName + "_locations.dat"))
            {
                for (int i = 0; i < vehfiles.Count; i++)
                {
                    vehfiles[i] = vehfiles[i].Substring(vehfiles[i].IndexOf("Vehicles") + 9);
                }
            }
            TreeNode start = new TreeNode("Cars");
            treeView1.Nodes.Add(start);
            for (int i = 0; i < vehfiles.Count; i++)
            {
                nodeAdd(start, vehfiles[i]);
            }
            start.Expand();
        }

        private TreeNode nodeAdd(TreeNode parent, string text)
        {
            int i = 0;
            bool exists = false;
            foreach (TreeNode noot in parent.Nodes)
            {
                if (noot.Text == (text.Split('\\'))[0])
                {
                    exists = true;
                    break;
                }
                i++;
            }
            TreeNode veh = new TreeNode();
            if (exists)
            {
                if (text.Contains('\\'))
                {
                    veh = nodeAdd(parent.Nodes[i], text.Substring(text.IndexOf('\\') + 1));
                }
            }
            else
            {
                parent.Nodes.Add(new TreeNode((text.Split('\\'))[0]));
                if (text.Contains('\\'))
                {
                    veh = nodeAdd(parent.Nodes[i], text.Substring(text.IndexOf('\\') + 1));
                }
            }
            return parent.Nodes[parent.Nodes.Count-1];
        }


        //Searches in a dir for a file from the outside to the inside
        private void findVEH(string dir)
        {
            
            if (File.Exists(appName + "_locations.dat"))
            {
                Stream stream = File.Open(appName + "_locations.dat", FileMode.Open);
                BinaryFormatter bformatter = new BinaryFormatter();

                vehfiles = (List<string>)bformatter.Deserialize(stream);
                stream.Close();
                return;
            }
            try
            {
                foreach (string f in Directory.GetFiles(dir))
                {
                    if ((f.Split('.')[1]).ToLower() == "veh")
                    {
                        vehfiles.Add(f);
                    }
                }
                foreach (string d in Directory.GetDirectories(dir))
                {
                    findVEH(d);
                }
            }
            catch (System.Exception excpt)
            {
                Console.WriteLine(excpt.Message);
            }
        }

        

        private void checkRemember_CheckedChanged(object sender, EventArgs e)
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey("SOFTWARE", true);
            RegistryKey newkey = key.CreateSubKey(appName);
            if (checkRemember.Checked)
            {
                newkey.SetValue("Startupdir", rfactorloc);
            }
            else
            {
                newkey.SetValue("Startupdir", "");
            }
        }

        private void btnOpenVEH_Click(object sender, EventArgs e)
        {
            if (vehselected != null)
            {
                this.Close();
            }
            else
            {
                string file = treeView1.SelectedNode.FullPath;
                if (file.ToLower().Contains(".veh"))
                {
                    vehselected = rfactorloc + @"\GameData\Vehicles\" + file.Substring(file.IndexOf("Start\\") + 6);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Not a valid selection, please select a *.veh file.");
                }
            }
        }
        private void btnCollapse_Click(object sender, EventArgs e)
        {
            treeView1.CollapseAll();
        }

        private void btnExpand_Click(object sender, EventArgs e)
        {
            treeView1.ExpandAll();
        }

        private void btnManual_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Vehicle files (*.veh)|*.veh";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                vehselected = dialog.FileName;
                btnOpenVEH_Click(null, null);
            }
            else
            {
                MessageBox.Show("No file selected.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void checkSave_CheckedChanged(object sender, EventArgs e)
        {
            if (init)
                return;
            RegistryKey key = Registry.CurrentUser.OpenSubKey("SOFTWARE", true);
            RegistryKey newkey = key.CreateSubKey(appName);
            if (checkSave.Checked)
            {
                Stream stream = File.Open(appName + "_locations.dat", FileMode.Create);
                BinaryFormatter bformatter = new BinaryFormatter();
                bformatter.Serialize(stream, vehfiles);
                stream.Close();
                newkey.SetValue("Savefile", 1);
            }
            else
            {
                File.Delete(appName + "_locations.dat");
                newkey.SetValue("Savefile", 0);
            }
            
            
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            treeView1.Nodes.Clear();
            vehfiles.Clear();
            File.Delete(appName + "_locations.dat");
            findVEH(rfactorloc + @"\GameData\Vehicles");
            lblLoading.Text = "Loading...";
            makeTree();
            lblLoading.Text = "";
            checkSave_CheckedChanged(null, null);
        }

        public string getVehSelected()
        {
            return vehselected;
        }
    }
}
