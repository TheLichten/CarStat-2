﻿using System;
using System.IO;
using System.Collections.Generic;

namespace CommonLibs
{
    public class MiscFunctions
    {
        public MiscFunctions()
        {
        }

        //Makes a List of strings from the given file (memory = faster)
        public List<string> FileToList(string fileloc)
        {
            List<string> filelist = new List<string>();
            StreamReader reader = new StreamReader(fileloc);
            string line;
            while ((line = reader.ReadLine()) != null)
            {
                if (!line.Trim().StartsWith("//") && !line.Equals(""))
                    filelist.Add(line);
            }
            reader.Close();
            return filelist;
        }

        //Makes a List of strings from the given file (memory = faster) without removing anything.
        public List<string> FileToList(string fileloc, bool dontremove)
        {
            List<string> filelist = new List<string>();
            StreamReader reader = new StreamReader(fileloc);
            string line;
            while ((line = reader.ReadLine()) != null)
            {
                if (dontremove || (!line.Trim().StartsWith("//") && !line.Equals("")))
                    filelist.Add(line);
            }
            reader.Close();
            return filelist;
        }

        //Makes a file of a List of strings 
        public void ListToFile(List<string> file, string fileloc)
        {
            StreamWriter writer = new StreamWriter(fileloc);
            for (int i = 0; i < file.Count; i++)
            {
                writer.WriteLine(file[i]);
            }
            writer.Close();
        }

        //Makes a List of strings from the given file but only starts from the first compound
        public List<string> FileToListChop(string fileloc)
        {
            List<string> filelist = new List<string>();
            StreamReader reader = new StreamReader(fileloc);
            string line;
            while (!((line = reader.ReadLine()).Contains("[COMPOUND]")))
            {
            }
            while (line != null)
            {
                if (!line.Trim().StartsWith("//") && !line.Equals(""))
                    filelist.Add(line);
                line = reader.ReadLine();
            }
            reader.Close();
            return filelist;
        }

        //Function to convert seconds to hours:minutes
        public string getTime(double time)
        {
            int hours = (int)time / 3600;
            int minutes = (int)(((time - (hours * 3600)) / 60));
            string timestring = hours + " hours, " + minutes + " minutes";
            return timestring;
        }

        //Calculates the engine power, given a torquenumber at a specific rpm
        public int torqueToHP(double torque, double rpm)
        {
            int power;
            power = (int)(torque * rpm / 9549);
            return power;
        }

        //Calculates the HP, given a unit in kW
        public double kwToHP(double kw)
        {
            return Math.Round(kw * 1.3410, 0);
        }

        //Calculates the Lb, given a unit in Nm
        public double nmToLb(double nm)
        {
            return Math.Round(nm * 0.7375621, 0);
        }

        //Calculates the PSI, given a unit in kpa
        public double kpaToPSI(double kpa)
        {
            return Math.Round(kpa * 0.145037738, 1);
        }

        //Gets the value of the given property in the given file on the given location (= value)
        public string getProperty(List<string> file, string property, int value)
        {
            string prop = null;
            value = value - 1;
            foreach (string line in file)
            {
                if (line.Split('=')[0].Equals(property))
                {
                    prop = line.Split('/')[0];
                    prop = prop.Split('=')[1];
                    prop = prop.Replace("\"", "");
                    prop = prop.Replace("(", "");
                    prop = prop.Replace(")", "");
                    prop = prop.Split(',')[value];
                    prop = prop.Trim();
                    break;
                }
            }
            if ((prop == null) || (prop == ""))
                prop = "n/a";
            return prop;
        }

        //Gets the value of the given property in the given file on the given location (= value) with a start-field
        public string getProperty(List<string> file, string property, int value, string start)
        {
            string prop = null;
            value = value - 1;
            int i;
            for (i = 0; i < file.Count; i++)
            {
                if (file[i].Contains("Name=\"" + start + "\"") || file[i].Contains("Name=" + start))
                {
                    break;
                }
            }
            for (int j = i; j < file.Count; j++)
            {
                string line = file[j];
                if (line.Split('=')[0].Equals(property))
                {
                    prop = line.Split('/')[0];
                    prop = prop.Split('=')[1];
                    prop = prop.Replace("\"", "");
                    prop = prop.Replace("(", "");
                    prop = prop.Replace(")", "");
                    prop = prop.Split(',')[value];
                    prop = prop.Trim();
                    break;
                }

            }
            if ((prop == null) || (prop == ""))
                prop = "n/a";
            return prop;
        }

        //Gets the value of the given property in the given file on the given location (= value) with a start-field
        public string getProperty(List<string> file, string property, int value, string start, bool rear)
        {
            string prop = null;
            value = value - 1;
            int i;
            for (i = 0; i < file.Count; i++)
            {
                if (file[i].Contains("Name=\"" + start + "\"") || file[i].Contains("Name=" + start))
                {
                    break;
                }
            }
            if (rear)
            {
                for (; i < file.Count; i++)
                {
                    if (file[i].ToLower().Contains("rear:"))
                    {
                        break;
                    }
                }
            }
            for (int j = i; j < file.Count; j++)
            {
                string line = file[j];
                if (line.Split('=')[0].Equals(property))
                {
                    prop = line.Split('/')[0];
                    prop = prop.Split('=')[1];
                    prop = prop.Replace("\"", "");
                    prop = prop.Replace("(", "");
                    prop = prop.Replace(")", "");
                    prop = prop.Split(',')[value];
                    prop = prop.Trim();
                    break;
                }

            }
            if ((prop == null) || (prop == ""))
                prop = "n/a";
            return prop;
        }

        //Modifies the value of the given property in the given file with a start-field, replaicing the original value
        public List<string> setProperty(List<string> file, string property, string start, string originalval, string newval, bool rear)
        {
            int i;
            for (i = 0; i < file.Count; i++)
            {
                if (file[i].Contains("Name=\"" + start + "\"") || file[i].Contains("Name=" + start))
                {
                    break;
                }
            }
            if (rear)
            {
                for (; i < file.Count; i++)
                {
                    if (file[i].Contains("REAR:"))
                    {
                        break;
                    }
                }
            }
            for (; i < file.Count; i++)
            {
                string line = file[i];
                if (line.Split('=')[0].Equals(property))
                {
                    file[i] = line.Replace(originalval, newval);
                    break;
                }
            }
            return file;
        }


    }
}
