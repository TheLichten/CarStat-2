

Introducing Setup Developer 3
=============================
Extending upon the original Setup Development tool, the purpose of this setup developer tool is to help you develop your setup over a period of time. It is not a "quick fix" or "cheat sheet". It will require an investment of time and effort, requiring you to navigate back and forth between sections continuously. You may want to focus on specific items each time you sit down to develop your setup. Do not overwhelm yourself by trying to work through this tool all in one session.



System Requirements
===================
Minimum Specifications
----------------------
Operating Systems     	Microsoft Windows 2000, XP, Vista
Processor         	1.3 GHz Intel Pentium III or 100% compatible
Memory           	512 MB RAM
Hard Drive         	25 MB free space
3D Accelerator        	DirectX 8.1 compatible graphics card with 64 MB memory
Sound Card            	DirectX 8.1 compatible  
Input       		Keyboard and Mouse
DirectX Version   	9.0c

Visit http://www.race2play.com to create your FREE Race2Play account today.


Installing Setup Developer 3
============================
You may need to have administrator rights to install Setup Developer 2 on Microsoft Windows 2000/XP/Vista. You need to login using an administrator account for the installation.



Quick Start Guide
=================
� After installing Setup Developer 3, the program can then be launched. Choose the language you wish to use and check for any updates regularly. 
� Within the program, you will be prompted with multiple-choice questions. It is recommended that you read the corresponding articles first, then move onto answering the questions. You will be provided with recommendations after each sequence of questions are answered.  



Maximizing Setup Developer Performance
======================================
� If you are experiencing high CPU usage, you may edit the config.INI (standard text editor will work) and change the "Wait" value. The higher the number (0-9999 seconds) the less CPU usage but also higher delay in the recommendations. 



Adding your own language translations
=====================================
You can add your own language translations to the Setup Developer by editing the TXT and INI files located in the \Lang\Other\ folder. 

� Text does not wrap automatically. To add a line break, use "%CRLF%" (No quotes) 
� Do not edit the text within the square-brackets ([DIALOGS]) or any text before the "=" (Tab1=)

I ask that if you create any translations, please send them in whole to tmcarthur@race2play.com so I can add them to future releases



ADVICE
======
The one item that will benefit your setup time greater than any other item is the one sitting behind the steering wheel: You. If you do not know the track or the proper racing line, no setup changes will help you. You must take the time to adjust "you" before adjusting your car. Only when you can do multiple consistent laps should you begin to alter your setup.

As for the setup changes, you know the rule: Only make one change at a time! Trying to "shortcut" the setup process will not work. Take your time and allow your setup to evolve over time, possibly over an entire season of racing in your favorite league. Practice, setup development and testing can easily add up to 10 times the length of the race itself. 



LEGAL
======
No part of this application may be reproduced, transmitted, transcribed, stored in a retrieval system, in any form or by any means, electronic, mechanical, photocopying, recording, or otherwise, without prior written permission from VindexWorks Inc. All distribution rights are held exclusively by VindexWorks Inc. and may not be distributed, by any means, by any party. The information furnished herein is believed to be accurate and reliable. However, no responsibility is assumed by VindexWorks Inc. for its use.

