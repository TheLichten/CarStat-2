

Introducci�n a la Herramienta de Elaboraci�n de Setup 3
=======================================================
El prop�sito de esta nueva herramienta de elaboraci�n de setup, ampliaci�n de la herramienta de elaboraci�n de setup original, es ayudarle a crear su setup a lo largo del tiempo. No se trata de una "hoja de trucos" o de "ajuste r�pido". Le requerir� que invierta tiempo y esfuerzo, necesitando retroceder y avanzar entre las distintas secciones continuamente. Es aconsejable centrarse en pocos par�metros cada vez que se dedique a elaborar su setup. No se sobrecargue intentando configurar todos los par�metros en una sola sesi�n.



Requerimientos del Sistema
==========================
Especificaciones M�nimas
------------------------
Sistema Operativo     	Microsoft Windows 2000, XP, Vista
Procesador         	1.3 GHz Intel Pentium III o 100% compatible
Memoria           	512 MB RAM
Disco Duro         	Espacio libre 25 MB
Acelerador 3D 		Tarjeta gr�fica compatible DirectX 8.1 con 64 MB de memoria
Tarjeta de sonido	Compatible DirectX 8.1
Entrada de datos       	Teclado y rat�n
Versi�n DirectX 	9.0c

Visite http://www.race2play.com para crear su cuenta GRATUITA de Race2Play.


Instalaci�n de la Herramienta de Elaboraci�n de Setup 3
=======================================================
Es necesario que disponga de permisos de administrador para instalar la Herramienta de Elaboraci�n de Setup 2 en Microsoft Windows 2000/XP/Vista. Necesita acceder al sistema con su cuenta de administrador para la instalaci�n.



Gu�a r�pida de ejecuci�n
========================
� Tras instalar la Herramienta de Elaboraci�n de Setup 3 puede ejecutar el programa. Elija el idioma que desee utilizar y compruebe si hay actualizaciones peri�dicamente.
� Ya en el programa, se le presentar�n preguntas con posibilidad de elegir entre m�ltiples respuestas. Es recomendable que lea los art�culos correspondientes antes de responder a las preguntas. Tras responder a cada grupo de preguntas, se le presentar�n las recomendaciones para elaborar su setup.



Rendimiento de la Herramienta de Elaboraci�n de Setup
=====================================================
� Si tiene problemas de uso elevado de CPU, puede editar el fichero config.INI (servir� cualquier editor de texto est�ndar) y cambiar el valor del par�metro "Wait". A mayor valor (0 - 9999 seconds) menos uso de CPU, pero tambi�n aumentar� el retraso en la presentaci�n de las recomendaciones una vez responda a las preguntas.



A�adir una traducci�n en su propio idioma
=========================================
Puede a�adir traducciones en su propio idioma a la Herramienta de Elaboraci�n de Setup editando los ficheros TXT y INI situados en la carpeta \Lang\Other\ 

� El texto no cambia de l�nea autom�ticamente. Para a�adir un salto de l�nea, utilice %CRLF% 
� No modifique el texto entre corchetes ([DIALOGS]) o cualquier texto que preceda al signo "=" (Tab1=)

Le ruego que, si realiza alguna traducci�n, me la env�e completa a tmcarthur@race2play.com para que pueda incluirla en actualizaciones futuras.



RECOMENDACI�N
=============
El par�metro que m�s beneficiar� su tiempo de preparaci�n de setup, sobre cualquier otro, es el par�metro que est� sentado tras el volante: Usted. Si no conoce la pista o la trazada correcta, ninguna modificaci�n en el setup le ser� de ayuda. Debe dedicar el tiempo necesario para ajustar el par�metro "usted" antes de ajustar su coche. S�lo cuando consiga dar vueltas consistentes una tras otra deber� comenzar a ajustar su setup.

En cuanto a los cambios en el setup, ya conoce la regla: �haga s�lo un cambio cada vez! Intentar atajar el proceso de preparar el setup haciendo varios cambios a la vez no funcionar�. T�mese su tiempo y deje que su setup evolucione con el tiempo, probablemente durante toda una temporada de carreras de su competici�n favorita. Las pr�cticas, la elaboraci�n del setup y las pruebas pueden llevar f�cilmente 10 veces la duraci�n de la propia carrera.



NOTA LEGAL
==========
No est� permitida la reproducci�n, transmisi�n, transcripci�n ni almacenamiento en un sistema de recuperaci�n de cualquier tipo o mediante cualquier medio electr�nico, mec�nico, fotocopia, grabaci�n o cualquier otra forma, de ninguna parte de esta publicaci�n sin autorizaci�n expresa por escrito de VindexWorks Inc. Todos los derechos de distribuci�n son propiedad en exclusiva de VindexWorks Inc. y no se pueden distribuir, de ninguna manera, por terceras partes. Entendemos que la informaci�n contenida aqu� es fiable y correcta. Sin embargo, VindexWorks Inc. no asume ninguna responsabilidad derivada de su uso.