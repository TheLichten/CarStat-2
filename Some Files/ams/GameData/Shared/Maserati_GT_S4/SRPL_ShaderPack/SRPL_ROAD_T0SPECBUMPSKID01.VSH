
float4x4 viewProjMatrix : register (c0);
float4x4 worldMatrix : register (c4);
float4 worldEye : register (c8);
float4 fogData : register (c9);
float4 dirLight : register (c14);

int4 numOmniLight : register (i0); // (count, init, stride, 0)
float4 omniPosRad[64]   : register (c72);
float4 omniRGBDelta[64] : register (c136);


struct VS_OUTPUT
{
   float4 Pos    : POSITION;
   float4 Diff   : COLOR0;
   float  Fog    : FOG;
   float2 Tex    : TEXCOORD0; // base
   float4 Spec   : TEXCOORD1; // spec and track map
   float4 Normal : TEXCOORD2; // world space vertex normal + day factor
   float3 EyeVec : TEXCOORD3; // world space eye vec

   float4 Omni   : COLOR1;
};

  VS_OUTPUT SRPL_ROAD_T0SPECBUMPSKID01 (float4 inPos : POSITION, float4 inNorm : NORMAL, float4 inColor : COLOR0, float2 inTex0 : TEXCOORD0, float2 inSpec : TEXCOORD1, float2 inSpec2 : TEXCOORD3)
{
  VS_OUTPUT Out;
  
  // Transform vertex position into view space:
  Out.Pos = mul (viewProjMatrix, inPos);

  float3 worldNorm = mul (worldMatrix, inNorm.xyz);
  if (inNorm.y < 0.8)
  {
    worldNorm = mul (worldMatrix, float3(0.0, 1.0, 0.0));
  }
  Out.Normal.xyz = worldNorm;
  Out.Normal.w = pow(saturate(normalize(dirLight.xyz).y * 1.6 + 0.1), 1.0);
  
  float3 worldPos = mul (worldMatrix, inPos);
  float3 eyeVec = (worldEye.xyz + worldPos.xyz);
  Out.EyeVec = eyeVec;
  
  Out.Fog = (fogData[1] - (length (eyeVec.xyz) * fogData[0]));

  // Compute omnilights
  Out.Omni = float4(0,0,0,0);
  for (int i=0; i<numOmniLight.x; i++)
  {
    float intens;
    float3 omniToVert = omniPosRad[i].xyz - worldPos.xyz;
    float mag = dot (omniToVert.xyz, omniToVert.xyz);
    // check if vert in range
    if ( mag < omniPosRad[i].w) // using square distances
      if ((intens = dot (worldNorm, normalize (omniToVert))) > 0) // check backface
        Out.Omni.rgb += (sqrt(intens) * omniRGBDelta[i].rgb * (1.0F - (sqrt(mag) * omniRGBDelta[i].w)));
  }
  Out.Omni.a = 1.0F;    
  //Out.Omni.rgb *= 0.5F;

  // Propagate color and texture coordinates:
  Out.Diff = inColor;
  Out.Tex.xy = inTex0;
  Out.Spec.xy = inPos.xz;
  Out.Spec.zw = inSpec2;

  return (Out);
}

