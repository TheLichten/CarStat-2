// Light flare effect shader description file
// ------------------------------------------
// 
// Created by Siim Annuk
// Date: June 28, 2009

ShaderName=L2LightFlare
{
  ShaderDesc="Light Flare"
  ShaderLongDesc="Light Flare. DX9"
  ShaderDowngrade=L1CmapSpecAddT0
  ShaderLevel=(2) // DX9
  {
    Pass=(0)
    {
      VertexShader=vs20_blinnLightFlare
      {
        File=vs20_LightFlare.vsh 
        Language=HLSL

        // Normal glow size and multiplier
        Define=(GLOWSIZE, 1.5)
        Define=(GLOWMULTIPLIER, 3)

        VertexDecl=PShadeDecl
        ShaderConstants=Default
      }
      PixelShader=ps20_blinnLightFlare
      {
        File=ps20_LightFlare.psh
        Language=HLSL
        StageState=(0, DiffuseMap, Modulate)
        SamplerState=(0, Wrap, Wrap, Wrap)
      }
    }
  }
}

ShaderName=L2LightFlareHeadlights
{
  ShaderDesc="Light Flare - Headlights"
  ShaderLongDesc="Light Flare. Suitable for car headlights. DX9"
  ShaderDowngrade=L1CmapSpecAddT0
  ShaderLevel=(2) // DX9
  {
    Pass=(0)
    {
      VertexShader=vs20_blinnLightFlareHeadlights
      {
        File=vs20_LightFlare.vsh 
        Language=HLSL
        
        // Has flash effect
        Define=(HIGHFLASH, 1)
        Define=(FLASHSIZE, 1.01)
        Define=(FLASHMULTIPLIER, 200)
	
        // Normal glow size and multiplier
        Define=(GLOWSIZE, 1.35)
        Define=(GLOWMULTIPLIER, 2)

        VertexDecl=PShadeDecl
        ShaderConstants=Default
      }
      PixelShader=ps20_blinnLightFlareHeadlights
      {
        File=ps20_LightFlare.psh
        Language=HLSL

        // Has flash effect
        Define=(HIGHFLASH, 1)

        StageState=(0, DiffuseMap, Modulate)
        SamplerState=(0, Wrap, Wrap, Wrap)
      }
    }
  }
}

