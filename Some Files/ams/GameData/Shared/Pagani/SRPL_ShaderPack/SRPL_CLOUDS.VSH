#define USEFOG

float4x4 viewProjMatrix : register (c0);
float4x4 worldMatrix : register (c4);
float4 worldEye : register (c8);
float4 fogData : register (c9);
float4 dirLight : register (c14);
float4 worldLight    : register (c2);
int4 numOmniLight : register (i0); // (count, init, stride, 0)
//int4 numSpotLight : register (i1); // (count, init, stride, 0)
float4x4 viewMatrix : register (c16);

float4 omniPosRad[64]   : register (c72);
float4 omniRGBDelta[64] : register (c136);

float4 spotPosRad[64]   : register (c72);
float4 spotDirCone[64]  : register (c136);
float4 spotRGBDelta[64] : register (c200);

struct VS_OUTPUT
{
   float4 Pos    : POSITION;
   float4 Diff   : COLOR0;
   float  Fog    : FOG;
   float4 Texc0  : TEXCOORD0;
   float4 Texc1  : TEXCOORD1;
   float4 Texc2  : TEXCOORD2;
   float4 EyeVec : TEXCOORD3; // world space eye vec
};


  VS_OUTPUT SRPL_CLOUDS (float4 inPos : POSITION, float4 inNorm : NORMAL, float4 inColor : COLOR0, float2 inTex0 : TEXCOORD0)
{
  VS_OUTPUT Out;

  Out.EyeVec.w = (inPos.y - 200.0) / 50.0;


float4 inPosOff = inPos;
inPosOff.x *= -1.0;


 inPosOff.y /= 2.0;
 inPosOff.y -= length(inPos.xz) / 20.0;

float movementScale = 0.35;

inPosOff.w = movementScale;

  // Transform vertex position into view space:
  Out.Pos = mul (viewProjMatrix, inPosOff);

  float3 worldPos = mul (worldMatrix, inPosOff);
  float3 eyeVec = (worldEye.xyz + worldPos);
  Out.EyeVec.xyz = eyeVec;  

  // override fog
  Out.Fog = 1.0;

  // Propagate colo
  Out.Diff.rgb = inColor.rgb;

  Out.Diff.a = max(0.75 - length(inPos) / 2500.0, 0.0);

 
  float2 offset = mul(worldMatrix, inPosOff).xz / movementScale + worldEye.xz * 0.8; //ten fragment tez wykomentowuje po przeniesienu /gmt do instance=skyboxi    + worldEye.xz * 0.8;
  offset *= 0.00025 * movementScale;

////////////////////////////////////////
float2 vec1 = normalize(dirLight.rb);

float angle;

  if (vec1.x == 0)
  {
    if (vec1.y > 0)
    {
      angle = 0.0;
    }
    else
    {
      angle = 3.1415926535;
    }
  }

  if (vec1.y == 0)
  {
    if (vec1.x > 0)
    {
      angle = 0.5 * 3.1415926535;
    }
    else
    {
      angle = 1.5 * 3.1415926535;
    }
  }

  if (vec1.y > 0)
  {
    angle = atan(vec1.x / vec1.y);
  }
  else
  {
    angle = 3.1415926535 + atan(vec1.x / vec1.y);
  }
  float animation = angle / 6.2832;
////////////////////////////////////////
  animation *= 20.0;

  Out.Texc0.xy = offset + float2(1.0, 2.0) * animation;
  Out.Texc0.zw = offset * 2.0 + float2(2.0, 1.0) * animation;
  Out.Texc1.xy = offset * 4.0 + float2(1.0, 2.0) * animation;
  Out.Texc1.zw = offset * 8.0 + float2(2.0, 1.0) * animation;
  Out.Texc2.xy = offset * 16.0 + float2(1.0, 2.0) * animation;
  Out.Texc2.zw = offset * 32.0 + float2(2.0, 1.0) * animation;

  return (Out);
}

