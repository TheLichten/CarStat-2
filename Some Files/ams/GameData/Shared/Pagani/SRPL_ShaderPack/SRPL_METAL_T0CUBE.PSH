float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 specularColor : register (c3); //power in c3.a

struct PS_INPUT
{
   float4 SideVec : COLOR0;
   float  Fog     : FOG;
#ifdef OMNISCALE
   float4 Omni   : COLOR1;
#endif
   float2 Tex     : TEXCOORD0; // base
   float2 Spec    : TEXCOORD1; // spec
   float4 Normal  : TEXCOORD2;
   float3 EyeVec  : TEXCOORD3;
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

sampler2D sTex0 : register (s0);
samplerCUBE sCube : register (s1);

PS_OUTPUT MAINFUNCTION (PS_INPUT Input) 
{
  PS_OUTPUT Out;

  // Sample the textures
  float4 tex = tex2D (sTex0, Input.Tex.xy) * DIFFUSESCALE;
#ifdef ALPHATEST
  if (tex.a < ALPHATEST)
  {
    discard;
  }
#endif

  float3 normal = normalize (Input.Normal.xyz);
  float3 normEyeVec = normalize (Input.EyeVec);

  float3 halfway = normalize (worldLight - normEyeVec); 
  halfway.x = dot (halfway, normal);
  
  half4 specular;
  specular.rgb = saturate (pow (halfway.x, SPECULARPOWER1)) * specularColor * SPECULARSCALE1;
  specular.a = 0;

  half4 specular2;
  specular2 = saturate (pow (halfway.x, SPECULARPOWER2)) * float4(1.0, 0.9, 0.75, 0.0) * SPECULARSCALE2;

  // Compute diffuse color
  half4 diffuse = saturate(dot (worldLight, normal)) * diffuseColor + ambientColor * AMBIENTSCALE;
#ifdef OMNISCALE
  diffuse = diffuse + Input.Omni * OMNISCALE;
#endif
  diffuse = saturate (diffuse);

  // Compute cube map vector [E - 2(E dot N)N], then sample the cube map
  float3 cubeCoord;
  cubeCoord = reflect(normEyeVec, normal);
  float4 cube = texCUBE (sCube, cubeCoord);


  //add more contrast to cubemap
  cube.rgb *= 1.2;

  cube.a = 1.0;

  // Calculate fresnel effect for better reflections
  float dotEyeVecNormal = abs(dot(normEyeVec, normal));

  // Use the specular alpha for reflection amount and add fresnel to that
  float fresnel = FRESNELBASE + FRESNELSCALE * pow(1 - dotEyeVecNormal, FRESNELPOWER);

  //less reflection during night
  float day = (diffuseColor.r + diffuseColor.g + diffuseColor.b) / 3.0;
  fresnel *= day;

  // Final color
  Out.Color = (tex * diffuse + specular * float4(day, day, day, 1.0)) * (1.0) + ((cube + specular2) * fresnel);
  Out.Color.a = tex.a + ALPHABIAS;

  return (Out);
}
