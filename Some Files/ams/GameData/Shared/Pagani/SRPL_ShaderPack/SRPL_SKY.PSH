float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 dirLight : register (c14);
float4 specularColor : register (c3); //power in c3.a
//float4 blendPct      : register (c4); //base/cube blend pct in c4.a

struct PS_INPUT
{
   float4 Color  : COLOR0;
   float3 Tex    : TEXCOORD0; // base
   float4 Normal : TEXCOORD2;
   float3 EyeVec : TEXCOORD3;
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

sampler2D sTex0 : register (s0);
sampler2D sTex1 : register (s1);


PS_OUTPUT SRPL_SKY (PS_INPUT Input) 
{
  PS_OUTPUT Out;

  float3 LightVec = normalize(worldLight.xyz);
  float3 EyeVec = normalize(Input.EyeVec);
  float day1 = diffuseColor.r + diffuseColor.g + diffuseColor.b;
//  float2 day2 = max(float2(day1, day1) - float2(1.4, 2.4), float2(0.0, 0.0)) / float2(3.2, 1.2);
//  float2 day2 = max(float2(day1, day1) - float2(0.6, 1.6), float2(0.0, 0.0)) / float2(4.8, 2.8);
  float2 day2 = max(float2(day1, day1) - float2(0.2, 1.6), float2(0.0, 0.0)) / float2(5.6, 2.8);
  float horizon = pow(1.0 - EyeVec.y, 5.0);
  float horizon2 = pow(horizon, 2.0);
  float4 sunPow = pow(float4(1.0, 1.0, 1.0, 1.0) * max(dot(LightVec, EyeVec), 0.0),
                      float4(1.0, 2.0, 50.0, 5000 * day2.y));


  // Sample the textures
  float4 tex = tex2D (sTex0, Input.Tex.xy) * 0.5;
  float4 tex2 = tex2D (sTex1, Input.Tex.xy);


  float4 atmo = tex2 * 2.0;                                             //high clouds texture
  float4x4 colorMatrix = {1.0, 0.5, 0.8, 25.0,
                          0.8, 0.5, 0.6, 20.0,
                          0.5, 0.4, 0.4, 15.0,
                          0.0, 0.0, 0.0, 0.0};
  atmo += mul(colorMatrix, float4(horizon2, sunPow.y, sunPow.z, sunPow.w * day2.y));
  atmo.rgb *= day2.x;

  float dusk = sin(day2.x * 6.3);
  atmo *= (float4(1.0, 1.0, 1.0, 1.0) - float4(0.0, 0.2, 0.4, 0.0) * dusk); //red sky
  atmo += float4(0.6, 0.4, 0.2, 0.0) * sunPow.x * horizon * dusk;   //red horizon around sun

  // gamma correction
  atmo.rgb = atmo.rgb / (0.3 + day2.x);
  atmo.rgb = float3(1.0, 1.0, 1.0) - pow(float3(2.0, 2.0, 2.0), atmo.rgb * -3.0) * 1.2;

  // Final color
  atmo.rgb = max(atmo.rgb, float3(0.0, 0.0, 0.0)) + tex.rgb / (0.25 + day2.x * 7.25);

  Out.Color.rgb = atmo.rgb;
  Out.Color.a = 1.0;

  return (Out);
}
