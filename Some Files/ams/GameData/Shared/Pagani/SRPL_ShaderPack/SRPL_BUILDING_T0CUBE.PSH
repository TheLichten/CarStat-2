float specpow(float val1, float val2)
{
  return pow(val1, val2);
}

float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 specularColor : register (c3); //power in c3.a
float4 blendPct      : register (c4); //base/cube blend pct in c4.a

struct PS_INPUT
{
   float4 Color  : COLOR0;
//#ifdef OMNISCALE
//   float4 Omni   : COLOR1;
//#endif
   float  Fog    : FOG;

   float2 Tex    : TEXCOORD0; // base
   float4 Normal : TEXCOORD2;
   float3 EyeVec : TEXCOORD3;
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

sampler2D sTex : register (s0);
samplerCUBE sCube : register (s1);


PS_OUTPUT MAINFUNCTION (PS_INPUT Input)
{
  PS_OUTPUT Out;
  
  // sample the textures
  float4 tex = tex2D (sTex, Input.Tex.xy);              // sample base tex

#ifdef ALPHATEST
  if (tex.a < ALPHATEST)
  {
    discard;
  }
#endif

  tex.rgb *= DIFFUSESCALE;

  float3 normal = normalize (Input.Normal.xyz);
  float3 normEyeVec = normalize (Input.EyeVec);
  float3 normLightVec = normalize (worldLight.xyz);
  float dotEyeVecNormal = abs(dot(normEyeVec, normal));
  
  // compute specular color 
  float3 halfway = normalize (worldLight.xyz - normEyeVec); 
  halfway.x = dot (halfway, normal);
  
  float specular;
  specular = saturate (specpow (halfway.x, SPECULARPOWER));
  specular += saturate (pow (halfway.x, SPECULARPOWER2)) * 0.6;
  specular += saturate (pow (halfway.x, SPECULARPOWER3)) * 0.2;

  // compute cube map vector [E - 2(E dot N)N], then sample the cube map
  float4 cube = texCUBE (sCube, reflect (normEyeVec, normal));
  
  // Calculate fresnel effect for better reflections
  float fresnel = min(1.0, FRESNELBASE + FRESNELSCALE * pow(1.0 - dotEyeVecNormal, FRESNELPOWER));

  //less reflection during night
  float day = (diffuseColor.r + diffuseColor.g + diffuseColor.b) / 3.0;

cube.rgb = cube.rgb * 1.6;
cube.a = fresnel;
cube.rgba = lerp(cube.rgba, float4(1.0, 0.9, 0.75, 0.75) * SPECULARSCALE, specular);
cube.rgb *= day;
cube.a *= (1.0 - tex.a);

tex.rgb = tex.rgb * (saturate(dot(normLightVec, normal)) * DIFFUSESCALE * diffuseColor + ambientColor * AMBIENTSCALE);

Out.Color = lerp(float4(tex.r, tex.g, tex.b, tex.a), cube, cube.a);

  return (Out);
}
