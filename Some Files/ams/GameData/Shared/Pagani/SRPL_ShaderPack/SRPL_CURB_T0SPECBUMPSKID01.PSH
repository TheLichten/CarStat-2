float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 specularColor : register (c3); //power in c3.a
//float4 blendPct      : register (c4); //base/cube blend pct in c4.a

struct PS_INPUT
{
   float4 Color  : COLOR0;
   float  Fog    : FOG;
   float2 Tex    : TEXCOORD0; // base
   float4 Spec   : TEXCOORD1; // spec and track map
   float4 Normal : TEXCOORD2;
   float3 EyeVec : TEXCOORD3;

   float3 Basis1 : TEXCOORD4; // tangent space eye vec
   float3 Basis2 : TEXCOORD5; // tangent space eye vec
   float3 Basis3 : TEXCOORD6; // tangent space eye vec

   float4 Omni   : COLOR1;
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

sampler2D sTex0 : register (s0);
sampler2D sSpec : register (s1);
sampler2D sBump : register (s2);
sampler2D sMap  : register (s3);

PS_OUTPUT SRPL_CURB_T0SPECBUMPSKID01 (PS_INPUT Input) 
{
  PS_OUTPUT Out;

  // Fresnel parameters
  float fresnelBias = 0.8;
  float fresnelScale = 0.6;
  float fresnelPower = 4.0;

  // Sample the textures
  float4 tex = tex2D (sTex0, Input.Tex.xy);
  float4 spec = tex2D (sSpec, Input.Spec.xy);
  float4 bump = (tex2D (sBump, Input.Spec.xy) - 0.5) * 2.0;
  float2 skidCoord = Input.Spec.zw;
skidCoord.x = saturate(skidCoord.x);
  float4 trackMap = tex2D (sMap, skidCoord);
trackMap = lerp(trackMap, float4(0.35, 0.0, 0.51, 1.0), Input.Color.a);

tex.rgb *= (1.0 - trackMap.r * 0.99);

//bump.z += trackMap.g * 10.0;
//spec = lerp(spec, float4(0.5, 0.5, 0.5, 1.0), trackMap.g);

  float3 normal = normalize (Input.Normal.xyz);

  float3 viewBump;
//  viewBump.x = dot (Input.Basis1, bump);
//  viewBump.y = dot (Input.Basis2, bump);
//  viewBump.z = dot (Input.Basis3, bump);
  viewBump = bump.xzy + normal;

  float3 bumpNormal = normalize (viewBump);
  float3 normEyeVec = normalize (Input.EyeVec);
  float3 lightVec = normalize(worldLight);

  float3 reflected = reflect(normEyeVec, bumpNormal);
  float dotReflect = max(0.0, dot(reflected, lightVec));
  
  half4 specular;
  specular.rgb = pow(dotReflect, 3.0) * float3(0.5, 0.45, 0.4);
  specular.rgb += pow(dotReflect, 100.0) * float3(0.4, 0.35, 0.3);
  specular.a = 0;

  // Compute diffuse color
  half4 diffuse = (saturate (dot (lightVec, bumpNormal))) * diffuseColor + ambientColor;
diffuse += Input.Omni;

  // Calculate fresnel effect for better reflections
  float dotEyeVecNormal = abs(dot(normEyeVec, bumpNormal));

  // Use the specular alpha for reflection amount and add fresnel to that
  float fresnel = fresnelBias  + fresnelScale * pow(1 - dotEyeVecNormal, fresnelPower);
//  float fresnel2 = fresnelBias2  + fresnelScale2 * pow(1 - dotEyeVecNormal, fresnelPower2);

  //less reflection during night
  float day = diffuseColor.r; //(diffuseColor.r + diffuseColor.g + diffuseColor.b) / 3.0;
  fresnel *= day; // * trackMap.b;
  
  // Final color
  Out.Color.rgb = (tex.rgb * diffuse.rgb)
     + (specular.rgb + float3(0.3, 0.35, 0.4)) * spec.rgb * diffuseColor.rgb * fresnel;
  Out.Color.a = tex.a;

  return (Out);
}
