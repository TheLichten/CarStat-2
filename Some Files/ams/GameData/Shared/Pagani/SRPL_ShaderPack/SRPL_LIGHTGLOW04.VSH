// Vertex shader for light flares 
// ------------------------------
// This shader makes the quad face the camera and change its size according to the camera vs quad normal.
// This shader should only be applied to quads (square consisting of 2 triangles) that face forward. 
//
// Created by Siim Annuk
// Date: June 28, 2009

#define USEFOG

float4x4 viewProjMatrix : register (c0);
float4x4 worldMatrix : register (c4);
float4x4 viewMatrix : register (c16);
float4x4 invViewMatrix : register (c20);
float4 worldEye: register (c8);
float4 fogData : register (c9);
float4 texMod : register (c11); // tex0.u=x, tex0.v=y, tex1.u=z, tex1.v=w
float4 specularColor : register (c15); // power in c15.a

struct VS_OUTPUT
{
   float4 Pos  : POSITION;
   float4 Diff : COLOR0;
#ifdef USEFOG
   float  Fog  : FOG;
#endif   
   float3 Tex  : TEXCOORD0;
};

VS_OUTPUT SRPL_LIGHTGLOW04 (float4 inPos : POSITION, float4 inNorm : NORMAL, float4 inColor : COLOR0, float2 inTex0 : TEXCOORD0, float2 inTex1 : TEXCOORD1, float2 inTex2 : TEXCOORD2, float2 inTex3 : TEXCOORD3)  
{
  VS_OUTPUT Out;

  float2 cornerOff = float2(-1.0f, -1.0f);
  if (inColor.a < 0.5f)
  {
    if (inColor.a > 0.25f)
    {
      cornerOff.x = 1.0f;
    }
  }
  else
  {
    cornerOff.y = 1.0f;
    if (inColor.a < 0.75f)
    {
      cornerOff.x = 1.0f;
    }
  }

  float4 centerPos;
  centerPos.xy = inTex0;
  centerPos.z = inTex1.x;
  centerPos.w = 1.0f;
  
  float3 eyeVector = normalize(mul(worldMatrix, centerPos) + worldEye);

  float3 primaryNormal;
  primaryNormal.x = sin(inTex2.x) * cos(inTex2.y);
  primaryNormal.z = cos(inTex2.x) * cos(inTex2.y);
  primaryNormal.y = sin(inTex2.y);
  primaryNormal = mul (worldMatrix, primaryNormal.xyz);

  float primaryDot = dot(-eyeVector, primaryNormal);

  float glow1 = pow(max(primaryDot, 0.0f), inColor.b * 100.0f);
  float radius = inTex1.y;
  radius += radius * (inColor.r * 100.0f * glow1);
  
  float3 cornerOff2;
  float4x4 worldViewMatrix = mul(viewMatrix, worldMatrix);
  cornerOff2 = worldViewMatrix[0] * cornerOff.x + worldViewMatrix[1] * cornerOff.y;

  centerPos.xyz -= eyeVector * 0.5f;
  centerPos.xyz += cornerOff2 * radius;
  
  float alpha = 1.0f;
  if (inTex3.y > inTex3.x)
  {
    alpha = saturate(((primaryDot * 0.5f + 0.5f) - inTex3.x) / (inTex3.y - inTex3.x));
  }

  Out.Pos = mul (viewProjMatrix, centerPos);

#ifdef USEFOG
  Out.Fog.x = 100000.0f;
#endif
  
  Out.Tex.z = alpha * inColor.g * 2.0;
  Out.Tex.xy = cornerOff * float2(0.5f, 0.5f) + float2(0.5f, 0.5f);
  Out.Diff = float4(1.0f, 1.0f, 1.0f, 1.0f);

  return (Out);
}








