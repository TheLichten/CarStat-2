float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 specularColor : register (c3); //power in c3.a

struct PS_INPUT
{
   float4 Color  : COLOR0;
#ifdef OMNISCALE
   float4 Omni   : COLOR1;
#endif
   float  Fog    : FOG;

   float2 Tex0    : TEXCOORD0; // base0
   float3 Normal  : TEXCOORD1; // 
   float3 EyeVec  : TEXCOORD2; // world space eye vec
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

sampler2D sTex0: register (s0);
sampler2D sTex1: register (s1);
 
PS_OUTPUT SRPL_GRASSBLADES_T0MIX (PS_INPUT Input)
{
  PS_OUTPUT Out;
  
  float4 tex0 = tex2D (sTex0, Input.Tex0.xy);        // sample base tex
  if (tex0.a < 0.5)
  {
    discard;
  }
  float4 tex1 = tex2D (sTex1, Input.Tex0.xy);        // sample mix  tex

  float3 normEyeVec = normalize (Input.EyeVec);
  float3 normal = normalize (Input.Normal);
  float3 lightVec = normalize(worldLight); 

  float lightBehind = pow(max(0.0, dot(lightVec.xz, normEyeVec.xz) * 0.7 + 0.3), 2.0);
//  float lightInFront = 0.65 - dot(lightVec.xz, normEyeVec.xz) * 0.3;

  lightBehind *= tex1.a;

  float lightThrough = 0.0; //lightBehind * pow(max(0.0, dot(lightVec, normEyeVec)), 2.0);


//  tex0.rgb *= lightInFront;
  tex0 = lerp(tex0, tex1, lightBehind);  //back lighting

  // compute diffuse color
  half4 diffuse = max(dot(lightVec, normal), 0.0) * diffuseColor * DIFFUSESCALE + ambientColor * AMBIENTSCALE;
#ifdef OMNISCALE
  diffuse = diffuse + Input.Omni * OMNISCALE;
#endif

  tex0.rgb = tex0.rgb * saturate(diffuse.rgb);

//  tex0.rgb = lerp(tex0.rgb, tex1.rgb, lightThrough * 0.3);  //subsurface scattering

//gray-out the texture
//  tex0.rgb = float3(0.5, 0.5, 0.5) * (tex0.r + tex0.g + tex0.b) + tex0.rgb * 0.5;

  Out.Color.rgb = tex0.rgb * Input.Color.rgb;
  Out.Color.a = tex0.a + ALPHABIAS;

//Out.Color.rgba = tex.rgba;

  return (Out);
}
