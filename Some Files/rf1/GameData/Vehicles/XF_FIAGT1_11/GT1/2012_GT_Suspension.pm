//////////////////////////////////////////////////////////////////////////
//
// Conventions:
//
// +x = left
// +z = rear
// +y = up
// +pitch = nose up
// +yaw = nose right
// +roll = right
//
// [BODY]  - a rigid mass with mass and inertial properties
// [JOINT] - a ball joint constraining an offset of one body to an
//           offset of another body (eliminates 3 DOF)
// [HINGE] - a constraint restricting the relative rotations of two
//           bodies to be around a single axis (eliminates 2 DOF).
// [BAR]   - a constraint holding an offset of one body from an offset of
//           another body at a fixed distance (eliminates 1 DOF).
// [JOINT&HINGE] - both the joint and hinge constraints, forming the
//           conventional definition of a hinge (eliminates 5 DOF).
//
//////////////////////////////////////////////////////////////////////////

// Body including all rigidly attached parts (wings, barge boards, etc.)
[BODY]
name=body mass=(0.0) inertia=(0.0,0.0,0.0)
pos=(0.0,0.0,0.0) ori=(0.0,0.0,0.0)

// Front spindles
[BODY]
name=fl_spindle mass=(15.5) inertia=(0.0275,0.0260,0.0245)
pos=(0.873,0.0,-1.470) ori=(0.0,0.0,0.0)

[BODY]
name=fr_spindle mass=(15.5) inertia=(0.0275,0.0260,0.0245)
pos=(-0.873,0.0,-1.470) ori=(0.0,0.0,0.0)

// Front wheels
[BODY]
name=fl_wheel mass=(20.0) inertia=(1.292,0.724,0.724)
pos=(0.873,0.0,-1.470) ori=(0.0,0.0,0.0)

[BODY]
name=fr_wheel mass=(20.0) inertia=(1.292,0.724,0.724)
pos=(-0.873,0.0,-1.470) ori=(0.0,0.0,0.0)

// Rear spindles
[BODY]
name=rl_spindle mass=(17.0) inertia=(0.0275,0.0260,0.0245)
pos=(0.854,0.0,1.24) ori=(0.0,0.0,0.0)

[BODY]
name=rr_spindle mass=(17.0) inertia=(0.0275,0.0260,0.0245)
pos=(-0.854,0.0,1.24) ori=(0.0,0.0,0.0)

// Rear wheels (includes half of rear-axle)
[BODY]
name=rl_wheel mass=(25.0) inertia=(1.446,0.875,0.875)
pos=(0.854,0.0,1.24) ori=(0.0,0.0,0.0)

[BODY]
name=rr_wheel mass=(25.0) inertia=(1.446,0.875,0.875)
pos=(-0.854,0.0,1.24) ori=(0.0,0.0,0.0)

// Fuel in tank is not rigidly attached - it is attached with springs and
// dampers to simulate movement.  Properties are defined in the HDV file.

[BODY]
name=fuel_tank mass=(1.0) inertia=(1.0,1.0,1.0)
pos=(0.0,0.0,0.0) ori=(0.0,0.0,0.0)

// Driver's head is not rigidly attached, and it does NOT affect the vehicle
// physics.  Position is from the eyepoint defined in the VEH file, while
// other properties are defined in the head physics file.

[BODY]
name=driver_head mass=(5.0) inertia=(0.02,0.02,0.02)
pos=(0.0,0.0,0.0) ori=(0.0,0.0,0.0)


//////////////////////////////////////////////////////////////////////////
//
// Constraints
//
//////////////////////////////////////////////////////////////////////////

// Front wheel and spindle connections
[JOINT&HINGE]
posbody=fl_wheel negbody=fl_spindle pos=fl_wheel axis=(-0.873,0.0,0.0)

[JOINT&HINGE]
posbody=fr_wheel negbody=fr_spindle pos=fr_wheel axis=(0.873,0.0,0.0)

// Front left suspension (2 A-arms + 1 steering arm = 5 links)
[BAR] // forward upper arm
name=fl_fore_upper posbody=body negbody=fl_spindle pos=(0.460,0.130,-1.457) neg=(0.763,0.160,-1.457)

[BAR] // rearward upper arm
posbody=body negbody=fl_spindle pos=(0.460,0.130,-1.257) neg=(0.763,0.160,-1.457)

[BAR] // forward lower arm
posbody=body negbody=fl_spindle pos=(0.267,-0.168,-1.457) neg=(0.800,-0.168,-1.457)

[BAR] // rearward lower arm
name=fl_fore_lower posbody=body negbody=fl_spindle pos=(0.267,-0.168,-1.207) neg=(0.800,-0.168,-1.457)

[BAR] // steering arm (must be named for identification)
name=fl_steering posbody=body negbody=fl_spindle pos=(0.320, -0.005,-1.625) neg=(0.880, 0.015 ,-1.625)

// Front right suspension (2 A-arms + 1 steering arm = 5 links)
[BAR] // forward upper arm (used in steering lock calculation)
name=fr_fore_upper posbody=body negbody=fr_spindle pos=(-0.460,0.130,-1.457) neg=(-0.763,0.160,-1.457)

[BAR] // rearward upper arm
posbody=body negbody=fr_spindle pos=(-0.460,0.130,-1.257) neg=(-0.763,0.160,-1.457)

[BAR] // forward lower arm
name=fr_fore_lower posbody=body negbody=fr_spindle pos=(-0.267,-0.168,-1.457) neg=(-0.800,-0.168,-1.457)

[BAR] // rearward lower arm
posbody=body negbody=fr_spindle pos=(-0.267,-0.168,-1.207) neg=(-0.800,-0.168,-1.457)

[BAR] // steering arm (must be named for identification)
name=fr_steering posbody=body negbody=fr_spindle pos=(-0.320, -0.005,-1.625) neg=(-0.800, 0.015,-1.625)

// Rear left suspension (2 A-arms + 1 straight link = 5 links)
[BAR] // forward upper arm
posbody=body negbody=rl_spindle pos=(0.543,0.160,0.94) neg=(0.795,0.188,1.24)

[BAR] // rearward upper arm
posbody=body negbody=rl_spindle pos=(0.543,0.160,1.24) neg=(0.795,0.188,1.24)

[BAR] // forward lower arm
posbody=body negbody=rl_spindle pos=(0.363,-0.152,0.94) neg=(0.823,-0.180,1.24)

[BAR] // rearward lower arm
posbody=body negbody=rl_spindle pos=(0.363,-0.152,1.24) neg=(0.823,-0.180,1.24)

[BAR] // straight link
posbody=body negbody=rl_spindle pos=(0.420,-0.000,1.41) neg=(0.800,0.015,1.41)

// Rear right suspension (2 A-arms + 1 straight link = 5 links)
[BAR] // forward upper arm
posbody=body negbody=rr_spindle pos=(-0.543,0.160,0.94) neg=(-0.795,0.188,1.24)

[BAR] // rearward upper arm
posbody=body negbody=rr_spindle pos=(-0.543,0.160,1.24) neg=(-0.795,0.188,1.24)

[BAR] // forward lower arm
posbody=body negbody=rr_spindle pos=(-0.363,-0.152,0.94) neg=(-0.823,-0.180,1.24)

[BAR] // rearward lower arm
posbody=body negbody=rr_spindle pos=(-0.363,-0.152,1.24) neg=(-0.823,-0.180,1.24)

[BAR] // straight link
posbody=body negbody=rr_spindle pos=(-0.420,-0.00,1.41) neg=(-0.800,0.015,1.41)

// Rear spindle and wheel connections
[JOINT&HINGE]
posbody=rl_wheel negbody=rl_spindle pos=rl_wheel axis=(-0.854,0.0,0.0)

[JOINT&HINGE]
posbody=rr_wheel negbody=rr_spindle pos=rr_wheel axis=(0.854,0.0,0.0)




