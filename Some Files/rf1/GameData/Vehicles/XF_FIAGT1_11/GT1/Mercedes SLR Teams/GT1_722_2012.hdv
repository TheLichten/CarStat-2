// Maserati MC12 FIA GT1 2010
// It is pointed to by one or more *.car files.
//
// Any range has the following values: (minimum, step size, number of steps)
// Any setting refers to the step from 0 to <number of steps - 1>.
//
// Everything is in SI units (kg, m, kPa, N, etc.), except:
// Engine speed is measured in RPM.
// Angles are measured in degrees.
//
// +x = left
// +y = up
// +z = rear
//
// Pushrod connections are adjusted from the values found in this file
// based on the graphical location of the wheels.  If the graphical location
// does not match the physical location (found in a .sp file), then all
// suspension joints (including the pushrods) are adjusted to match the
// graphical locations.  It should be also noted that suspension joints
// are also adjusted after setting the camber and toe-in.
//
// Aerodynamic variables:
// Lift variables creating downforce are negative numbers
// zmena default setupu

[GENERAL]
Rules=0// what rules to apply to garage setups (0=none, 1=stock car)
GarageDisplayFlags=7// how settings are displayed in garage (add): 1=rear wing, 2=radiator, 4=more gear info
FeelerFlags=0// how collision feelers are generated (add): 1=box influence 2=reduce wall-jumping 4=allow adjustment hack 8=top directions
Mass=1020// all mass except fuel (1130kg car + 80kg driver)
Inertia=(1475.5,1640.0,255.0)// all inertia except fuel
FuelTankPos=(0.0,0.200,-1.280)// location of tank affects center of gravity (very close and even slightly ahead of CG in F1)
FuelTankMotion=(560.0,0.7)// simple model of fuel movement in tank (spring rate per kg, critical damping ratio)
Notes=""
Symmetric=1
DamageFile=2012_GT_DamageRFGT// file to find physical and graphical damage info
CGHeight=0.245// height of body mass (excluding fuel) above reference plane
CGRightRange=(0.50,0.000,0)// fraction of weight on left tires
CGRightSetting=0
CGRearRange=(0.54,0.005,0)
CGRearSetting=0
GraphicalOffset=(0.0, -0.140, 0.0)// does not affect physics!
Undertray00=(0.95,-0.00,-1.895)// offset from center of car, and between axles// LF (Left front corner of splitter) (forward is negative, right is negative)
Undertray01=(-0.95,-0.00,-1.895)// offset from center of car, and between axles// RF (Right front corner of splitter)
Undertray02=(0.65,-0.00,0.900)// offset from center of car, and between axles // LR (Left rear corner of diffuser)
Undertray03=(-0.65,-0.00,0.900)// offset from center of car, and between axles // RR (Right rear corner of diffuser)
Undertray04=(0.55,-0.00,-2.280)// offset from center of car, and between axles// Left center front nose
Undertray05=(-0.55,-0.00,-2.280)// offset from center of car, and between axles// Right center front nose
Undertray06=(0.95,-0.00,0.000)// offset from center of car, and between axles //  Center of Left rocker panel
Undertray07=(-0.95,-0.00,0.000)// offset from center of car, and between axles //  Center of right rocker panel
Undertray08=(0.35,-0.00,-1.000)// offset from center of car, and between axles//  Floor LF corner
Undertray09=(-0.35,-0.00,-1.000)// offset from center of car, and between axles//  Floor RF corner
Undertray10=(0.00,-0.00,-2.345)// offset from center of car, and between axles //  Dead center on splitter
Undertray11=(-0.00,-0.00,0.000)// offset from center of car, and between axles //  Dead center on floor bottom
UndertrayParams=(262500.0,11600.0,4.0)// undertray spring rate, damper rate, and coefficient of friction
TireBrand=2012_GT_Tires.tbc// must appear before tire compound setting (references *.tyr file)
FrontTireCompoundSetting=0// Front compound index within brand
RearTireCompoundSetting=0// Rear compound index within brand
FuelRange=(1.0,1.0,150)
FuelSetting=10
NumPitstopsRange=(0,1,4)
NumPitstopsSetting=3
Pitstop1Range=(1.0,1.0,150)
Pitstop1Setting=50
Pitstop2Range=(1.0,1.0,150)
Pitstop2Setting=50
Pitstop3Range=(1.0,1.0,150)
Pitstop3Setting=50
AIMinPassesPerTick=2// minimum passes per tick (can use more accurate spring/damper/torque values, but takes more CPU)
AIRotationThreshold=0.12// rotation threshold (rads/sec) to temporarily increment passes per tick
AIEvenSuspension=0.0// averages out spring and damper rates to improve stability (0.0 - 1.0)
AISpringRate=1.0// spring rate adjustment for AI physics (improves stability)
AIDamperSlow=1.2// contribution of average slow damper into simple AI damper
AIDamperFast=0.4// contribution of average fast damper into simple AI damper
AIDownforceZArm=0.150// hard-coded center-of-pressure offset from vehicle CG
AIDownforceBias=0.0// bias between setup and hard-coded value (0.0-1.0)
AITorqueStab=(1.25,1.25,1.25)// torque adjustment to keep AI stable
//ADD V95
FeelerFlags=15                  // how collision feelers are generated (add): 1=box influence 2=reduce wall-jumping 4=allow adjustment hack 8=top directions
FeelerOffset=(0.0, 0.0, 0.0)    // offset from cg to use when generating feelers
FeelersAtCGHeight=1                       // whether corner and side feelers are automatically adjusted to CG height
FeelerFrontLeft=(0.863,0.384,-1.720)      // front-left corner collision feeler
FeelerFrontRight=(-0.863,0.384,-1.720)    // front-right corner collision feeler
FeelerRearLeft=(0.815,0.384,1.700)        // rear-left corner collision feeler
FeelerRearRight=(-0.815,0.384,1.700)      // rear-right corner collision feeler
FeelerFront=(0.064,0.384,-1.900)          // front side collision feeler
FeelerRear=(0.064,0.384,1.720)            // rear side collision feeler
FeelerRight=(-0.933,0.384,-0.247)         // right side collision feeler
FeelerLeft=(0.933,0.384,-0.247)           // left side collision feeler
FeelerTopFrontLeft=(-0.478,1.540,-0.298)  // top front-left collision feeler
FeelerTopFrontRight=(0.478,1.540,-0.298)  // top front-right collision feeler
FeelerTopRearLeft=(-0.652,1.511,1.323)    // top rear-left collision feeler
FeelerTopRearRight=(0.652,1.511,1.323)    // top rear-right collision feeler
FeelerBottom=(0.064,0.249,-0.247)         // bottom feeler

[PITMENU]
StopGo=1// Whether stop/go pit menu item is available (highly recommended); default=1
Fuel=1// Whether fuel pit menu item is available (recommended); default=1
AllTires=0// Option for changing all tires (all other tire choices should be 0); default=0
FrontRearTires=(1,1)// Option for changing front tires, rear tires (all other conflicting tire choices should be 0); default=(1,1)
LeftRightTires=(0,0)// Option for changing left tires, right tires (all other conflicting tire choices should be 0); default=(0,0)
IndividualTires=(0,0,0,0)// Option for changing individual tire FL, FR, RL, RR (all other conflicting tire choices should be 0); default=(0,0,0,0)
FenderFlare=(0,0)// Options for changing left fender flare, right fender flare; default=(0,0)
FrontWing=1// Front wing adjustment (front wing repair is covered under Damage); default=1
RearWing=1// Rear wing adjustment (rear wing repair is covered under Damage); default=0
Driver=1// Driver change; default=1
Wedge=0// Wedge adjustment; default=0
Radiator=1// Radiator or grille tape adjustment; default=0
TrackBar=0// Track bar adjustment; default=0
Pressure=(1,1,1,1)// Tire pressure adjustment FL, FR, RL, RR; default=(0,0,0,0)
SpringRubber=(1,1,1,1)// Spring rubber adjustment FL, FR, RL, RR; default=(0,0,0,0)
Damage=2// Number of options to fix damage (0=none, 1=bodywork, 2=bodywork+suspension); default=1
StopGoSimultaneous=0// Whether stop/go penalties can be served during a regular pit stop (time is added at end); default=0
PressureOnTheFly=1// Whether tire pressures can be adjusted WITHOUT changing tires; default=0
DamagedTiresOnly=0// Tire change restrictions: 0=any tire can be changed 1=only damaged tires can be changed; default=0
CompoundRestrictions=0// Whether tire compounds have restrictions: 0=unrestricted 1=one dry compound from qualifying on, 2=front/rear compounds must match, 3=both; default=0
Preparation=(70.0,25.0,0.5,4.5)// When crew gives up after request, crew prep time, delay multiplier for how much more time was needed to prep, max delay; default=(150.0,25.0,0.5,4.5)
FuelTime=(10.0,2.0,1.0,0.5,1.0)// Fuel fill rate (L/s), random delay, nozzle insertion time, nozzle removal time, concurrent fuel filling (0.0=separate, 1.0=concurrent); default=(12.0,2.0,1.0,0.5,1.0)
TireTime=(6.0,8.0,3.0,1.0)// Time to change two tires, time to change four tires, random delay on any tire, concurrent tire changes (0.0=separate, 1.0=concurrent); default=(5.5,5.5,2.0,1.0)
FenderFlareTime=3.5// Time to adjust fender flare; default=3.5
FrontWingTime=(4.0,20.0)// Time to adjust front wing, time to replace front wing; default=(8.0,8.0)
RearWingTime=(3.0,15.0)// Time to adjust rear wing, time to replace rear wing; default=(8.0,33.0)
DriverTime=(10.0,3.0,5.0,1.0)// Time to change driver, random delay, extra delay if vehicle is damaged, concurrent driver changes (0.0=separate, 1.0=concurrent); default=(11.0,1.5,4.0,1.0)
WedgeTime=3.5// Time to adjust wedge; default=3.5
RadiatorTime=3.0// Time to adjust radiator/grille tape; default=5.0
TrackBarTime=3.0// Time to adjust track bar; default=3.5
PressureTime=4.5// Time to adjust tire pressure WITHOUT changing tire; default=2.5
SpringRubberTime=3.0// Time to adjust spring rubber; default=3.0
DamageTime=(7.0,2.0,40.0,1.0)// Time to fix aero damage, random delay, fix suspension including broken off wheels, concurrent damage fixing (0.0=separate, 1.0=concurrent); default=(8.5,1.0,90.0,1.0)

[FRONTWING]
FWRange=(4.0,1.0,1)// front wing range
FWSetting=1// front wing setting
FWMaxHeight=(0.10)// maximum height to take account of for downforce
FWDragParams=(0.007,0.0098,0.0004)// base drag and 1st and 2nd order with setting
FWLiftParams=(-0.260,-0.110,0.00003)// base lift and 1st and 2nd order with setting
FWLiftHeight=(0.335)// effect of current height on lift coefficient
FWLiftSideways=(0.0)// dropoff in downforce with yaw (0.0 = none, 1.0 = max)
FWLeft=(-0.005,0.0,0.0)// aero forces from moving left
FWRight=(0.005,0.0,0.0)// aero forces from moving right
FWUp=(0.0,-0.05,-0.001)// aero forces from moving up
FWDown=(0.0,0.025,0.001)// aero forces from moving down
FWAft=(0.0,0.02,-0.02)// aero forces from moving rearwards
FWFore=(0.0,0.0,0.0)// aero forces from moving forwards (recomputed from settings)
FWRot=(0.05,0.025,0.075)// aero torque from rotating
FWCenter=(0.00,0.00,-0.80)// center of front wing forces (offset from center of front axle in ref plane)

[REARWING]
RWRange=(11.0,1.0,12)// rear wing range
RWSetting=6// rear wing setting
RWDragParams=(0.00001,0.00001,0.0005)// base drag and 1st and 2nd order with setting
RWLiftParams=(-0.360,-0.030,0.00002)// base lift and 1st and 2nd order with setting
RWLiftSideways=(0.3)// dropoff in downforce with yaw (0.0 = none, 1.0 = max)
RWPeakYaw=(12.0,0.90)// angle of peak, multiplier at peak
RWLeft=(-0.005,0.0,0.0)// aero forces from moving left
RWRight=(0.005,0.0,0.0)// aero forces from moving right
RWUp=(0.0,-0.09,-0.002)// aero forces from moving up
RWDown=(0.0,0.045,0.002)// aero forces from moving down
RWAft=(0.0,0.03,-0.4)// aero forces from moving rearwards
RWFore=(0.0,0.0,0.0)// aero forces from moving forwards (recomputed from settings)
RWRot=(0.08,0.05,0.11)// aero torque from rotating
RWCenter=(0.00,0.60,1.50)// center of rear wing forces (offset from center of rear axle at ref plane)

[BODYAERO]
BodyDragBase=(0.290)// base drag
BodyDragHeightAvg=(0.22)// drag increase with average ride height
BodyDragHeightDiff=(0.90)// drag increase with front/rear ride height difference
BodyMaxHeight=(0.20)// maximum ride height that affects drag/lift
BodyLeft=(-0.6,0.0,0.0)// aero forces from moving left
BodyRight=(0.6,0.0,0.0)// aero forces from moving right
BodyUp=(0.0,-0.9,0.0)// aero forces from moving up
BodyDown=(0.0,0.9,0.0)// aero forces from moving down
BodyAft=(0.0,0.5,-0.8)// aero forces from moving rearwards
BodyFore=(0.0,-0.240,0.435)// aero forces from moving forwards (lift value important!)
BodyRot=(5.00,3.75,2.50)// aero torque from rotating
BodyCenter=(0.0,0.50,-1.300)// center of body aero forces (offset from center of rear axle at ref plane)
RadiatorRange=(0.0,1.0,7)// radiator range
RadiatorSetting=3// radiator setting
RadiatorDrag=(0.002)// effect of radiator setting on drag
RadiatorLift=(0.0015)// effect of radiator setting on lift
BrakeDuctRange=(0.0,1.0,7)// brake duct range
BrakeDuctSetting=5// brake duct setting
BrakeDuctDrag=(0.006)//  effect of brake duct setting on drag
BrakeDuctLift=(0.0)// effect of brake duct setting on lift

[DIFFUSER]
DiffuserBase=(-0.320,-2.33,43.0)// base lift and 1st/2nd order with rear ride height
DiffuserFrontHeight=(0.000)// 1st order with front ride height
DiffuserRake=(0.005,-5.0,150.0)// Optimum rake (rear - front ride height), 1st order with current difference from opt, 2nd order
DiffuserLimits=(0.010,0.100,0.055)// Min ride height before stalling begins (0.0 to disable), max rear ride height for computations, max rake difference for computations
DiffuserStall=(0.0,1.0)// function to compute stall ride height (0.0=minimum, 1.0=average), downforce lost when bottoming out (0.0=none,1.0=complete stall)
DiffuserSideways=(0.3)// dropoff with yaw (0.0 = none, 1.0 = max)
DiffuserCenter=(0.0,0.00,-1.30)// center of diffuser forces (offset from center of rear axle at ref plane)

[SUSPENSION]
PhysicalModelFile=2012_GT_Suspension.pm
ApplySlowToFastDampers=0// whether to apply slow damper settings to fast damper settings
CorrectedInnerSuspHeight=0.250// instead of moving inner susp height relative with ride height, use this offset (set to -1 for original behavior)
AdjustSuspRates=1// adjust suspension rates due to motion ratio ( 0=enable, 1=disable)
AlignWheels=1// correct for minor graphical offsets
FrontWheelTrack=0.0// if non-zero, forces the front wheels to be specified track width
RearWheelTrack=0.0// if non-zero, forces the rear wheels to be specified track width
LeftWheelBase=0.0// if non-zero, forces the left side to use specified wheelbase
RightWheelBase=0.0// if non-zero, forces the right side to use specified wheelbase
SpringBasedAntiSway=1// 0=diameter-based, 1=spring-based
FrontAntiSwayBase=0.0
FrontAntiSwayRange=(10000.0,10000.0,25)
FrontAntiSwaySetting=7
FrontAntiSwayRate=(1.36e11,4.0)// not applicable with spring-based antisway
AllowNoAntiSway=1// Whether first setting gets overridden to mean no antisway bar
RearAntiSwayBase=0.0
RearAntiSwayRange=(5000.0,5000.0,20)
RearAntiSwaySetting=10
RearAntiSwayRate=(1.36e11,4.0)// not applicable with spring-based antisway
FrontToeInRange=(-2.0,0.1,41)
FrontToeInSetting=19
RearToeInRange=(-2.0,0.1,41)
RearToeInSetting=21
LeftCasterRange=(-0.0,0.25,29)// front-left caster
LeftCasterSetting=28
RightCasterRange=(-0.0,0.25,29)// front-right caster
RightCasterSetting=28

[CONTROLS]
SteeringFFBMult=1.1
UpshiftAlgorithm=(0.97,0.0)//  percentage of the rev limit to upshift at.  If the 2nd value is non-zero, then we will use it as the exact RPM to upshift at.
DownshiftAlgorithm=(0.9,0.85,0.6)// percentage of "optimum" downshift point in high gears, percentage of "optimum" downshift point in low gears, oval adjustment.
SteerLockRange=(5.0,0.5,37)
SteerLockSetting=28
RearBrakeRange=(0.200,0.005,121)
RearBrakeSetting=48
BrakePressureRange=(0.80,0.01,21)
BrakePressureSetting=20
HandbrakePressRange=(0.00,0.05,21)//
HandbrakePressSetting=0
AutoUpshiftGripThresh=0.63// auto upshift waits until all driven wheels have this much grip (reasonable range: 0.4-0.9)
AutoDownshiftGripThresh=0.63// auto downshift waits until all driven wheels have this much grip (reasonable range: 0.4-0.9)
TractionControlGrip=(1.00,0.20)// average driven wheel grip multiplied by 1st number, then added to 2nd
TractionControlLevel=(0.30,0.90)// effect of grip on throttle for low TC and high TC
ABS4Wheel=1// 0 = old-style single brake pulse, 1 = more effective 4-wheel ABS
ABSGrip=(1.00,0.20)// grip multiplied by 1st number and added to 2nd
ABSLevel=(0.30,0.90)// effect of grip on brakes for low ABS and high ABS

[ENGINE]
SpeedLimiter=1// Whether a pitlane speed limiter is available
Normal=2012_GT1_Engine.ini
RestrictorPlate=2012_GT1_Engine.ini

[DRIVELINE]
ClutchEngageRate=0.8//Auto clutch gradual engagement rate from neutral to 1st gear.
ClutchInertia=0.0148
ClutchTorque=1400.0
ClutchWear=0.0
ClutchFriction=30.00
BaulkTorque=1250.0
Semiautomatic=1
UpshiftDelay=0.10
UpshiftClutchTime=0.05
DownshiftDelay=0.10
DownshiftClutchTime=0.20
DownshiftBlipThrottle=0.00
WheelDrive=REAR// which wheels are driven: REAR, FOUR (even torque split), or FRONT
GearFile=2012_GT_Ratios.ini// Must come before final/reverse/gear settings!
FinalDriveSetting=2// indexed into GearFile list
ReverseSetting=0
ForwardGears=6
Gear1Setting=0
Gear2Setting=3
Gear3Setting=7
Gear4Setting=13
Gear5Setting=21
Gear6Setting=25
DiffPumpTorque=00.0// at 100% pump diff setting, the torque redirected per wheelspeed difference in radians/sec (roughly 1.2kph)
DiffPumpRange=(0.0,0.0,0)
DiffPumpSetting=0
DiffPowerRange=(0.3,0.1,6)// fraction of power-side input torque transferred through diff
DiffPowerSetting=0/ (not implemented for four-wheel drive)
DiffCoastRange=(0.3,0.1,6)// fraction of coast-side input torque transferred through diff
DiffCoastSetting=0// (not implemented for four-wheel drive)
DiffPreloadRange=(85.0,0.0,1)// preload torque that must be overcome to have wheelspeed difference
DiffPreloadSetting=1// (not implemented for four-wheel drive)

[FRONTLEFT]
BumpTravel=-0.030// travel to bumpstop with zero packers and zero ride height
ReboundTravel=-0.120// these two numbers assume front ride height is 30cm to 90cm with 10cm leeway
BumpStopSpring=125000.0// initial spring rate of bumpstop
BumpStopRisingSpring=1.25e7// rising spring rate of bumpstop (multiplied by deflection squared)
BumpStopDamper=1050.0// initial damping rate of bumpstop
BumpStopRisingDamper=2.19e5// rising damper rate of bumpstop (multiplied by deflection squared)
BumpStage2=0.090// speed where damper bump moves from slow to fast
ReboundStage2=-0.090// speed where damper rebound moves from slow to fast
FrictionTorque=9.50// Newton-meters of friction between spindle and wheel
SpinInertia=1.422// inertia in pitch direction including any axle
CGOffsetX=0.000// x-offset from graphical center to physical center (NOT IMPLEMENTED)
PushrodSpindle=(-0.150,-0.150,0.000)// relative to spindle
PushrodBody=(-0.350,0.320,0.000)// spring/damper connection to body (relative to wheel center)
CamberRange=(-4.5,0.1,66)
CamberSetting=5
PressureRange=(120.0,1.0,106)
PressureSetting=71
PackerRange=(0.000,0.001,41)
PackerSetting=0
SpringMult=.56// take into account suspension motion if spring is not attached to spindle (affects physics but not garage display)
SpringRange=(200000.0,10000.0,20)
SpringSetting=15
RideHeightRange=(0.050,0.005,8)
RideHeightSetting=0
DamperMult=.56// take into account suspension motion if damper is not attached to spindle (affects physics but not garage display)
SlowBumpRange=(3000.0,300.0,11)
SlowBumpSetting=3
FastBumpRange=(1500.0,200.0,11)
FastBumpSetting=2
SlowReboundRange=(8000.0,300.0,16)
SlowReboundSetting=9
FastReboundRange=(4000.0,300.0,16)
FastReboundSetting=7
BrakeDiscRange=(0.038,0.000,0)// disc thickness
BrakeDiscSetting=0
BrakePadRange=(0,1,5)// pad type (not implemented)
BrakePadSetting=2
BrakeDiscInertia=0.80// inertia per meter of thickness
BrakeResponseCurve=(-200,250,850,1350)
BrakeWearRate=2.5e-13// meters of wear per second at optimum temperature
BrakeFailure=(2.5e-002,9.0e-04)// average and variation in disc thickness at failure
BrakeTorque=2950.0// maximum brake torque at zero wear and optimum temp
BrakeHeating=0.00068// heat added linearly with brake torque
BrakeCooling=(1.90e-002,0.9e-004)// minimum brake cooling rate (static and per unit velocity)
BrakeDuctCooling=0.55e-004// brake cooling rate per brake duct setting

[FRONTRIGHT]
BumpTravel=-0.030// travel to bumpstop with zero packers and zero ride height
ReboundTravel=-0.120// these two numbers assume front ride height is 30cm to 90cm with 10cm leeway
BumpStopSpring=125000.0// initial spring rate of bumpstop
BumpStopRisingSpring=1.25e7// rising spring rate of bumpstop (multiplied by deflection squared)
BumpStopDamper=1050.0// initial damping rate of bumpstop
BumpStopRisingDamper=2.19e5// rising damper rate of bumpstop (multiplied by deflection squared)
BumpStage2=0.090// speed where damper bump moves from slow to fast
ReboundStage2=-0.090// speed where damper rebound moves from slow to fast
FrictionTorque=9.50// Newton-meters of friction between spindle and wheel
SpinInertia=1.422// inertia in pitch direction including any axle
CGOffsetX=0.000// x-offset from graphical center to physical center (NOT IMPLEMENTED)
PushrodSpindle=(0.150,-0.150,0.000)// relative to spindle
PushrodBody=(0.350,0.320,0.000)// spring/damper connection to body (relative to wheel center)
CamberRange=(-4.5,0.1,66)
CamberSetting=5
PressureRange=(120.0,1.0,106)
PressureSetting=71
PackerRange=(0.000,0.001,41)
PackerSetting=0
SpringMult=.56// take into account suspension motion if spring is not attached to spindle (affects physics but not garage display)
SpringRange=(200000.0,10000.0,20)
SpringSetting=15
RideHeightRange=(0.050,0.005,8)
RideHeightSetting=0
DamperMult=.56// take into account suspension motion if damper is not attached to spindle (affects physics but not garage display)
SlowBumpRange=(3000.0,300.0,11)
SlowBumpSetting=3
FastBumpRange=(1500.0,200.0,11)
FastBumpSetting=2
SlowReboundRange=(8000.0,300.0,16)
SlowReboundSetting=9
FastReboundRange=(4000.0,300.0,16)
FastReboundSetting=7
BrakeDiscRange=(0.038,0.000,0)// disc thickness
BrakeDiscSetting=0
BrakePadRange=(0,1,5)// pad type (not implemented)
BrakePadSetting=2
BrakeDiscInertia=0.80// inertia per meter of thickness
BrakeResponseCurve=(-200,250,850,1350)
BrakeWearRate=2.5e-13// meters of wear per second at optimum temperature
BrakeFailure=(2.5e-002,9.0e-04)// average and variation in disc thickness at failure
BrakeTorque=2950.0// maximum brake torque at zero wear and optimum temp
BrakeHeating=0.00068// heat added linearly with brake torque
BrakeCooling=(1.90e-002,0.9e-004)// minimum brake cooling rate (static and per unit velocity)
BrakeDuctCooling=0.55e-004// brake cooling rate per brake duct setting

[REARLEFT]
BumpTravel=-0.030// travel to bumpstop with zero packers and zero ride height
ReboundTravel=-0.140// these two numbers assume rear ride height is 40cm to 100cm with 10cm leeway
BumpStopSpring=125000.0// initial spring rate of bumpstop
BumpStopRisingSpring=1.25e7// rising spring rate of bumpstop (multiplied by deflection squared)
BumpStopDamper=1050.0// initial damping rate of bumpstop
BumpStopRisingDamper=2.19e5// rising damper rate of bumpstop (multiplied by deflection squared)
BumpStage2=0.090// speed where damper bump moves from slow to fast
ReboundStage2=-0.090// speed where damper rebound moves from slow to fast
FrictionTorque=12.50// Newton-meters of friction between spindle and wheel
SpinInertia=1.850// inertia in pitch direction including any axle
CGOffsetX=-0.030// x-offset from graphical center to physical center (NOT IMPLEMENTED)
PushrodSpindle=(-0.200,-0.150,0.000)// relative to spindle
PushrodBody=(-0.300,0.320,0.000)// spring/damper connection to body (relative to wheel center)
CamberRange=(-4.5,0.1,66)
CamberSetting=20
PressureRange=(120.0,1.0,106)
PressureSetting=63
PackerRange=(0.000,0.001,61)
PackerSetting=0
SpringMult=.60// take into account suspension motion if spring is not attached to spindle (affects physics but not garage display)
SpringRange=(200000.0,10000.0,20)
SpringSetting=15
RideHeightRange=(0.060,0.005,8)
RideHeightSetting=2
DamperMult=.60// take into account suspension motion if damper is not attached to spindle (affects physics but not garage display)
SlowBumpRange=(2000.0,300.0,11)
SlowBumpSetting=2
FastBumpRange=(1000.0,200.0,11)
FastBumpSetting=1
SlowReboundRange=(6000.0,300.0,16)
SlowReboundSetting=11
FastReboundRange=(3000.0,300.0,16)
FastReboundSetting=9
BrakeDiscRange=(0.035,0.000,0)// disc thickness
BrakeDiscSetting=0
BrakePadRange=(0,1,5)// pad type (not implemented)
BrakePadSetting=2
BrakeDiscInertia=0.70// inertia per meter of thickness
BrakeResponseCurve=(-200,250,850,1350)
BrakeWearRate=2.5e-13// meters of wear per second at optimum temperature
BrakeFailure=(2.4e-002,9.0e-04)// average and variation in disc thickness at failure
BrakeTorque=2600.0// maximum brake torque at zero wear and optimum temp
BrakeHeating=0.00082// heat added linearly with brake torque
BrakeCooling=(1.70e-002,0.60e-004)// minimum brake cooling rate (static and per unit velocity)
BrakeDuctCooling=0.42e-004// brake cooling rate per brake duct setting

[REARRIGHT]
BumpTravel=-0.030// travel to bumpstop with zero packers and zero ride height
ReboundTravel=-0.140// these two numbers assume rear ride height is 40cm to 100cm with 10cm leeway
BumpStopSpring=125000.0// initial spring rate of bumpstop
BumpStopRisingSpring=1.25e7// rising spring rate of bumpstop (multiplied by deflection squared)
BumpStopDamper=1050.0// initial damping rate of bumpstop
BumpStopRisingDamper=2.19e5// rising damper rate of bumpstop (multiplied by deflection squared)
BumpStage2=0.090// speed where damper bump moves from slow to fast
ReboundStage2=-0.090// speed where damper rebound moves from slow to fast
FrictionTorque=12.50// Newton-meters of friction between spindle and wheel
SpinInertia=1.850// inertia in pitch direction including any axle
CGOffsetX=-0.030// x-offset from graphical center to physical center (NOT IMPLEMENTED)
PushrodSpindle=(0.200,-0.150,0.000)// relative to spindle
PushrodBody=(0.300,0.320,0.000)// spring/damper connection to body (relative to wheel center)
CamberRange=(-4.5,0.1,66)
CamberSetting=20
PressureRange=(120.0,1.0,106)
PressureSetting=63
PackerRange=(0.000,0.001,61)
PackerSetting=0
SpringMult=.60// take into account suspension motion if spring is not attached to spindle (affects physics but not garage display)
SpringRange=(200000.0,10000.0,20)
SpringSetting=15
RideHeightRange=(0.060,0.005,8)
RideHeightSetting=2
DamperMult=.60// take into account suspension motion if damper is not attached to spindle (affects physics but not garage display)
SlowBumpRange=(2000.0,300.0,11)
SlowBumpSetting=2
FastBumpRange=(1000.0,200.0,11)
FastBumpSetting=1
SlowReboundRange=(6000.0,300.0,16)
SlowReboundSetting=11
FastReboundRange=(3000.0,300.0,16)
FastReboundSetting=9
BrakeDiscRange=(0.035,0.000,0)// disc thickness
BrakeDiscSetting=0
BrakePadRange=(0,1,5)// pad type (not implemented)
BrakePadSetting=2
BrakeDiscInertia=0.70// inertia per meter of thickness
BrakeResponseCurve=(-200,250,850,1350)
BrakeWearRate=2.5e-13// meters of wear per second at optimum temperature
BrakeFailure=(2.4e-002,9.0e-04)// average and variation in disc thickness at failure
BrakeTorque=2600.0// maximum brake torque at zero wear and optimum temp
BrakeHeating=0.00082// heat added linearly with brake torque
BrakeCooling=(1.70e-002,0.60e-004)// minimum brake cooling rate (static and per unit velocity)
BrakeDuctCooling=0.42e-004// brake cooling rate per brake duct setting
