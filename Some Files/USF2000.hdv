﻿// USF2000 DP08 2016 spec. Van Diemen / Élan / Mazda / Cooper Tires
[GENERAL]
Rules=0                      // what rules to apply to garage setups (0=none, 1=stock car)
GarageDisplayFlags=1         // how settings are displayed in garage (add): 1=rear downforce value (vs. angle), 2=radiator (vs. grille tape), 4=more gear info, 8+16=front downforce/wing/splitter/air dam, 32+64=rear downforce/wing/spoiler, 128=flip ups (vs. fender flare)
Mass=530.25
//Inertia=(726.01,767.57,86.87) // (673.67,709.16,89.27)
FuelTankForceDistrib=(0.447:Front_Subbody:(0,0,-0.0650875),0.553:Rear_Subbody:(0,0,0.0650875))
FuelTankPosFull=(0.0, 0.288,-1.1435)
FuelTankPosEmpty=(0.0, 0.038,-1.1435)
FuelTankMotion=(754.0,0.6)
Notes=""
//Symmetric=1
DamageFile=USF2000_Damage    // .ini file to find physical and graphical damage info
CGHeightRange=(0.27,0,1)
CGHeightSetting=0
CGRightRange=(0.5, 0.0, 1)
CGRightSetting=0
CGRearRange=(0.5413, 0.0, 1)
CGRearSetting=0
WedgeRange=(0.0, 0.0, 1)
WedgeSetting=0
WedgeSpecial=(0, "N/A", "",)
WedgePushrod=0.0
GraphicalOffset=(0.0, 0.0, 0.0)
Undertray00=( 0.25, 0.0,-1.285) // corner offsets from center of wheels in reference plane
Undertray01=(-0.25, 0.0,-1.285) // the height of the first 4 undertray points (00-03) are used in the diffuser calculations
Undertray02=( 0.20, 0.0, 1.285)
Undertray03=(-0.20, 0.0, 1.285)
Undertray04=( 0.26, 0.0,-1.855) // wooden floor
Undertray05=(-0.26, 0.0,-1.855) // wooden floor
Undertray06=( 0.26, 0.0, 0.245) // wooden floor
Undertray07=(-0.26, 0.0, 0.245) // wooden floor
Undertray08=( 0.625, 0.0225,-2.115) // front edge of the front wing
Undertray09=(-0.625, 0.0225,-2.115) // front edge of the front wing
Undertray10=( 0.47, 0.01, 1.7225) // rear edge of the diffuser
Undertray11=(-0.47, 0.01, 1.7225) // rear edge of the diffuser
UndertrayParams=(280000,5000,0.7) // undertray spring rate, damper rate, and coefficient of friction
//TireBrand=USF2000_Cooper
FrontTireCompoundSetting=0
RearTireCompoundSetting=0
FuelRange=(0.5, 0.5, 53)
FuelSetting=40
NumPitstopsRange=(0, 1, 1)
NumPitstopsSetting=0
Pitstop1Range=(0.5, 0.5, 53)
Pitstop1Setting=24
Pitstop2Range=(0.5, 0.5, 53)
Pitstop2Setting=24
Pitstop3Range=(0.5, 0.5, 53)
Pitstop3Setting=24
AIMinPassesPerTick=7           // minimum passes per tick (can use more accurate spring/damper/torque values, but takes more CPU)
AINegSuspForceMult=0.01        // 0.0 means negative suspension forces are not allowed, 1.0 completely allows them
AICornerRates=(0.33,0.33,0.108,0.108) // spring rate adjustment for AI physics
AIBumpstop=(1.0,0.5,1.0,0.4) // AI bumpstop (<spring mult>,<rising spring mult>,<damper mult>,<rising damper mult>)
AIDamping=(1.0,1.0,1.0,1.0)    // contribution of average slow damper into simple AI damper
AIDownforceZArm=0.0            // hard-coded center-of-pressure offset from vehicle CG
AIDownforceBias=0.0            // bias between setup and hard-coded value (0.0-1.0)
AIFuelMult=-1.0                // PLR file override for AI fuel usage - only positive value will override, see PLR for default
AIPerfUsage=(-1.0, -1.0, -1.0) // PLR file overrides for (brake power usage, brake grip usage, corner grip usage) used by AI to estimate performance - only positive values will override, see PLR for defaults
AITableParams=(-1.0, -1.0)     // PLR file overrides for (max load, min radius) used when computing performance estimate tables - only positive values will override, see PLR for defaults


[PITMENU]
StopGo=1                       // Whether stop/go pit menu item is available (highly recommended); default=1
Fuel=1                         // Whether fuel pit menu item is available (recommended); default=1
AllTires=0                     // Option for changing all tires (all other tire choices should be 0); default=0
FrontRearTires=(0,0)           // Option for changing front tires, rear tires (all other conflicting tire choices should be 0); default=(1,1)
LeftRightTires=(0,0)           // Option for changing left tires, right tires (all other conflicting tire choices should be 0); default=(0,0)
IndividualTires=(1,1,1,1)      // Option for changing individual tire FL, FR, RL, RR (all other conflicting tire choices should be 0); default=(0,0,0,0)
FenderFlare=(0,0)              // Options for changing left fender flare, right fender flare; default=(0,0)
FrontWing=1                    // Front wing adjustment (front wing repair is covered under Damage); default=1
RearWing=1                     // Rear wing adjustment (rear wing repair is covered under Damage); default=0
Driver=1                       // Driver change; default=1
Wedge=0                        // Wedge adjustment; default=0
Radiator=0                     // Radiator or grille tape adjustment; default=0
TrackBar=0                     // Track bar adjustment; default=0
Pressure=(1,1,1,1)             // Tire pressure adjustment FL, FR, RL, RR; default=(0,0,0,0)
SpringRubber=(0,0,0,0)         // Spring rubber adjustment FL, FR, RL, RR; default=(0,0,0,0)
Damage=2                       // Number of options to fix damage (0=none, 1=bodywork, 2=bodywork+suspension); default=1
StopGoSimultaneous=0           // Whether stop/go penalties can be served during a regular pit stop (time is added at end); default=0
PressureOnTheFly=1             // Whether tire pressures can be adjusted WITHOUT changing tires; default=0
DamagedTiresOnly=0             // Tire change restrictions: 0=any tire can be changed 1=only damaged tires can be changed; default=0
CompoundRestrictions=2         // Whether tire compounds have restrictions: 0=unrestricted 1=one dry compound from qualifying on, 2=front/rear compounds must match, 3=both; default=0
Preparation=(150,30,0.5,6.0)   // When crew gives up after request, crew prep time, delay multiplier for how much more time was needed to prep, max delay; default=(150.0,25.0,0.5,4.5)
FuelTime=(0.5,2.0,1.2,0.7,0.0) // Fuel fill rate (L/s), random delay, nozzle insertion time, nozzle removal time, concurrent fuel filling (0.0=separate, 1.0=concurrent); default=(12.0,2.0,1.0,0.5,1.0)
TireTime=(15.0,32.0,7.0,1.0)   // Time to change two tires, time to change four tires, random delay on any tire, concurrent tire changes (0.0=separate, 1.0=concurrent); default=(5.5,5.5,2.0,1.0)
FenderFlareTime=0.0            // Time to adjust fender flare; default=3.5
FrontWingTime=(15,50)          // Time to adjust front wing, time to replace front wing; default=(8.0,8.0)
RearWingTime=(60,90)           // Time to adjust rear wing, time to replace rear wing; default=(8.0,33.0)
DriverTime=(30,6.0,4.0,0.0)    // Time to change driver, random delay, extra delay if vehicle is damaged, concurrent driver changes (0.0=separate, 1.0=concurrent); default=(11.0,1.5,4.0,1.0)
WedgeTime=0.0                  // Time to adjust wedge; default=3.5
RadiatorTime=10.0               // Time to adjust radiator/grille tape; default=5.0
TrackBarTime=0.0               // Time to adjust track bar; default=3.5
PressureTime=10.0               // Time to adjust tire pressure WITHOUT changing tire; default=2.5
SpringRubberTime=3.5           // Time to adjust spring rubber; default=3.0
DamageTime=(20.0,5.0,300,1.0) // Time to fix aero damage, random delay, fix suspension including broken off wheels, concurrent damage fixing (0.0=separate, 1.0=concurrent); default=(8.5,1.0,90.0,1.0)

[PLUGIN]                       // Whether certain sensors are available as telemetry outputs
AerodynamicSensor=1            // Aerodynamic force sensors
EngineSensor=1
SuspensionSensor=1
TireForceSensor=1
TireTemperatureSensor=1

[AIDPENALTIES]
TC=(0,0.018859,0.0282885)              // Weight penalties for using different levels of aids.
ABS=(0,0.018859,0.0282885)              // First value is typically with the aid off so it should be 0.0.
Stability=(0,0.0094295,0.0094295)       // Penalties should only be applied to aids that the
Autoshift=(0,0.0,0.0,0.0) //  vehicle would not be allowed to run with.
Steering=(0,0.0,0.0,0.0)   // Penalties should typically only be used if the aid improves
Braking=(0,0.0,0.0)         // laptimes for a decent driver.
Invulnerable=(0,0.0)         // Values are fractions of the total vehicle mass,
Opposite=(0,0.0)              //  and are modeled as extra weight in the fuel tank.
SpinRecovery=(0,0.0)          // Do not use negative values.
AutoPit=(0,0.0)
AutoLift=(0,0.0)
AutoBlip=(0,0.0094295)

[FRONTWING]
FWForceDistrib=(1:Front_Subbody)
FWRange=(5,2.555556,10)
//FWSetting=5
FWMaxHeight=(0.5)
FWDragParams=(0.05,0,1.57e-5)
FWLiftParams=(-0.275,-0.0106,2.12e-5)
FWDraftLiftMult=1.1
FWLiftHeightPlus=(0,1.22,0)
FWNegRakeDrag=(0,-0.0274,5)
FWPosRakeDrag=(0,-0.05896,5)
FWNegRakeLift=(0,-0.07123,5)
FWPosRakeLift=(0,0.1533,5)
FWLiftSideways=0.443
FWLiftPeakYaw=(0,1)
FWLeft=(-0.079326,0,0)
FWRight=(0.079326,0,0)
FWUp=(0,-0.327,0)
FWDown=(0,0.327,0)
FWAft=(0,0.04,-0.04)
FWFore=(0,0,0)
FWRot=(0.051351,0.065,0.15561)
FWCenter=(0,0.04,-0.49)

[REARWING]
RWForceDistrib=(1:Rear_Subbody)
RWRange=(4,1,21)
//RWSetting=11
RWDragParams=(0.04,0.00311,-1.74e-5)
RWLiftParams=(-0.03,-0.016,1.0e-4)
RWDraftLiftMult=1.02
RWLiftSideways=1.0
RWLiftPeakYaw=(0,1)
RWNegRakeDrag=(0,-0.01,5)
RWPosRakeDrag=(0,-0.04,5)
RWNegRakeLift=(0,-0.035,5)
RWPosRakeLift=(0,0.07,5)
RWLeft=(-0.089154,0,0)
RWRight=(0.089154,0,0)
RWUp=(0,-0.14742,0)
RWDown=(0,0.14742,0)
RWAft=(0,0.08,-0.08)
RWFore=(0,0,0)
RWRot=(0.009214,0.01416,0.034791)
RWCenter=(0,0.92,0.705)

[BODYAERO]
BodyForceDistrib=(0.4319:Front_Subbody,0.5681:Rear_Subbody)
BodyDragBase=(0.266)
BodyDragHeightAvg=(0.173)
BodyDragHeightDiff=(-0.01)
BodyMaxHeight=(0.2)
DraftBalanceMult=1
BodyDraftLiftMult=1
RadiatorDraftFract=1.5
BodyNegRakeDrag=(0,0,5)
BodyPosRakeDrag=(0,0,5)
BodyNegRakeLift=(0,0,5)
BodyPosRakeLift=(0,0,5)
BodyLeft=(-1.12788,0,0)
BodyRight=(1.12788,0,0)
BodyUp=(0,-2.49678,0)
BodyDown=(0,2.746458,0)
BodyAft=(0,0.2,-0.85)
BodyFore=(0,0.12,0.12)
BodyRot=(2.634103,1.189913,1.279849)
BodyCenter=(0,0.34,-1.11)
RadiatorRange=(1.0, -0.1, 11)
//RadiatorSetting=0
RadiatorSpecial=(0,"","No tape",)
RadiatorSpecial=(1,"","10%",)
RadiatorSpecial=(2,"","20%",)
RadiatorSpecial=(3,"","30%",)
RadiatorSpecial=(4,"","40%",)
RadiatorSpecial=(5,"","50%",)
RadiatorSpecial=(6,"","60%",)
RadiatorSpecial=(7,"","70%",)
RadiatorSpecial=(8,"","80%",)
RadiatorSpecial=(9,"","90%",)
RadiatorSpecial=(10,"","100%",)
RadiatorCaption="RADIATOR TAPE"
RadiatorDrag=(0.0002)
RadiatorLift=(0.0040)
BrakeDuctSpecial=(0, "N/A", "",)
BaseDropoffLeadFollow=(0.08,0.15)
LeadingExponent=2.2
FollowingExponent=1.7
VehicleWidth=1.89
SideEffect=0.9
SideLeadingExponent=2
SideFollowingExponent=10
RoadModifierMults=(0.4,0.7)

[DIFFUSER]
DiffuserForceDistrib=(1:Rear_Subbody)
DiffuserBasePlus=(-0.21,0.01,1,10)
DiffuserFrontHeightPlus=(0,0,0,0.1)
DiffuserRake=(0.01,-0.5,2.5)
DiffuserLimits=(0,0.1,0.07)
DiffuserStall=(0,0.5)
DiffuserDraftLiftMult=1
DiffuserSideways=1.0
DiffuserPeakYaw=(0,1)
DiffuserOffsetZ=(0,0)
DiffuserCenter=(0,0,0)

[SUSPENSION]
//UltraChassis=USF2000_Chassis.ini
ModelWheelsIncludeAllTireMass=1
CorrectedInnerSuspHeightAll=(0.2343,0.2343,0.2339,0.2339)
ApplySlowToFastDampers=1
LimitFastDampers=1
AdjustSuspRates=0
AlignWheels=1
CenterWheelsOnBodyX=0
ChassisAdj00Special=(0, "N/A", "",)
ChassisAdj01Special=(0, "N/A", "",)
ChassisAdj02Special=(0, "N/A", "",)
ChassisAdj03Special=(0, "N/A", "",)
ChassisAdj04Special=(0, "N/A", "",)
ChassisAdj05Special=(0, "N/A", "",)
FrontAntiSwayParams=(1, 0, 1)
FrontAntiSwayBase=0.0
FrontAntiSwayRange=(140000, 20000, 5)
//FrontAntiSwaySetting=2
FrontAntiSwaySpecial=(0,"1"," (soft)","138000.0,FrontRollDamping=276.0")
FrontAntiSwaySpecial=(1,"2","","145000.0,FrontRollDamping=290.0")
FrontAntiSwaySpecial=(2,"3","","160000.0,FrontRollDamping=320.0")
FrontAntiSwaySpecial=(3,"4","","195000.0,FrontRollDamping=390.0")
FrontAntiSwaySpecial=(4,"5"," (hard)","220000.0,FrontRollDamping=440.0")
RearAntiSwayParams=(1, 0, 0)
RearAntiSwayBase=0.0
RearAntiSwayRange=(138000, 7500, 5)
//RearAntiSwaySetting=2
RearAntiSwaySpecial=(0,"1"," (soft)","138000.0,FrontRollDamping=276.0")
RearAntiSwaySpecial=(1,"2","","142000.0,FrontRollDamping=284.0")
RearAntiSwaySpecial=(2,"3","","149500.0,FrontRollDamping=299.0")
RearAntiSwaySpecial=(3,"4","","168000.0,FrontRollDamping=336.0")
RearAntiSwaySpecial=(4,"5"," (hard)","172000.0,FrontRollDamping=344.0")
//FrontToeInRange=(-0.694072, 0.086759, 17)
//FrontToeInSetting=4
//RearToeInRange=(0.347033, 0.086759, 5)
//RearToeInSetting=0
//LeftCasterRange=(11.768, 0.1, 1)
//LeftCasterSetting=0
//RightCasterRange=(11.768, 0.1, 1)
//RightCasterSetting=0
LeftTrackBarRange=( 0.0, 0.0, 1)
LeftTrackBarSetting=0
LeftTrackBarSpecial=(0, "N/A", "",)
RightTrackBarRange=(0.0, 0.0, 1)
RightTrackBarSetting=0
RightTrackBarSpecial=(0, "N/A", "",)
Front3rdSlowBumpSpecial=(0, "N/A", "",)
Front3rdFastBumpSpecial=(0, "N/A", "",)
Front3rdSlowReboundSpecial=(0, "N/A", "",)
Front3rdFastReboundSpecial=(0, "N/A", "",)
Front3rdPackerSpecial=(0, "N/A", "",)
Rear3rdSlowBumpSpecial=(0, "N/A", "",)
Rear3rdFastBumpSpecial=(0, "N/A", "",)
Rear3rdSlowReboundSpecial=(0, "N/A", "",)
Rear3rdFastReboundSpecial=(0, "N/A", "",)
Rear3rdPackerSpecial=(0, "N/A", "",)

[CONTROLS]
NominalMaxSteeringTorque=15
TurnsLockToLock=1.333333
SteeringShaftBaseLeft=(0,-0.001035,-0.460586)
SteeringShaftBaseRight=(0,-0.001035,-0.460586)
SteeringShaftAxis=(0.25882,0,-0.96593)
SteeringInnerTable=(0.2206720019086,-0.0098373,-0.458226976):(-0.1443279980914,-0.0098373,-0.458226976)
SteeringInnerTable=(0.1443279980914,-0.0098373,-0.458226976):(-0.2206720019086,-0.0098373,-0.458226976)
SteerLockCaption="WHEEL RANGE (LOCK)"
SteerLockRange=(21.085,0,8)
SteerLockSetting=7
SteerLockSpecial=(0,"180 ","(7.9) deg","TurnsLockToLock=0.5;SteeringFraction=0.375")
SteerLockSpecial=(1,"240 ","(10.5) deg","TurnsLockToLock=0.666667;SteeringFraction=0.5")
SteerLockSpecial=(2,"270 ","(11.9) deg","TurnsLockToLock=0.75;SteeringFraction=0.5625")
SteerLockSpecial=(3,"310 ","(13.6) deg","TurnsLockToLock=0.861111;SteeringFraction=0.645833")
SteerLockSpecial=(4,"360 ","(15.8) deg","TurnsLockToLock=1;SteeringFraction=0.75")
SteerLockSpecial=(5,"380 ","(16.7) deg","TurnsLockToLock=1.055556;SteeringFraction=0.791667")
SteerLockSpecial=(6,"450 ","(19.8) deg","TurnsLockToLock=1.25;SteeringFraction=0.9375")
SteerLockSpecial=(7,"480 ","(21.1) deg","TurnsLockToLock=1.333333;SteeringFraction=1")
SeatRangeLongitudinal=(-0.4,0.16)
SeatRangeVertical=(-0.07,0.04)
RearBrakeRange=(0.4, 0.005, 33)
//RearBrakeSetting=8
BrakePressureRange=(0.5, 0.0125, 41)
BrakePressureSetting=20
BrakePressureSpecial=(0,"40"," kgf (50%)",)
BrakePressureSpecial=(1,"41"," kgf (51.25%)",)
BrakePressureSpecial=(2,"42"," kgf (52.5%)",)
BrakePressureSpecial=(3,"43"," kgf (53.75%)",)
BrakePressureSpecial=(4,"44"," kgf (55%)",)
BrakePressureSpecial=(5,"45"," kgf (56.25%)",)
BrakePressureSpecial=(6,"46"," kgf (57.5%)",)
BrakePressureSpecial=(7,"47"," kgf (58.75%)",)
BrakePressureSpecial=(8,"48"," kgf (60%)",)
BrakePressureSpecial=(9,"49"," kgf (61.25%)",)
BrakePressureSpecial=(10,"50"," kgf (62.5%)",)
BrakePressureSpecial=(11,"51"," kgf (63.75%)",)
BrakePressureSpecial=(12,"52"," kgf (65%)",)
BrakePressureSpecial=(13,"53"," kgf (66.25%)",)
BrakePressureSpecial=(14,"54"," kgf (67.5%)",)
BrakePressureSpecial=(15,"55"," kgf (68.75%)",)
BrakePressureSpecial=(16,"56"," kgf (70%)",)
BrakePressureSpecial=(17,"57"," kgf (71.25%)",)
BrakePressureSpecial=(18,"58"," kgf (72.5%)",)
BrakePressureSpecial=(19,"59"," kgf (73.75%)",)
BrakePressureSpecial=(20,"60"," kgf (75%)",)
BrakePressureSpecial=(21,"61"," kgf (76.25%)",)
BrakePressureSpecial=(22,"62"," kgf (77.5%)",)
BrakePressureSpecial=(23,"63"," kgf (78.75%)",)
BrakePressureSpecial=(24,"64"," kgf (80%)",)
BrakePressureSpecial=(25,"65"," kgf (81.25%)",)
BrakePressureSpecial=(26,"66"," kgf (82.5%)",)
BrakePressureSpecial=(27,"67"," kgf (83.75%)",)
BrakePressureSpecial=(28,"68"," kgf (85%)",)
BrakePressureSpecial=(29,"69"," kgf (86.25%)",)
BrakePressureSpecial=(30,"70"," kgf (87.5%)",)
BrakePressureSpecial=(31,"71"," kgf (88.75%)",)
BrakePressureSpecial=(32,"72"," kgf (90%)",)
BrakePressureSpecial=(33,"73"," kgf (91.25%)",)
BrakePressureSpecial=(34,"74"," kgf (92.5%)",)
BrakePressureSpecial=(35,"75"," kgf (93.75%)",)
BrakePressureSpecial=(36,"76"," kgf (95%)",)
BrakePressureSpecial=(37,"77"," kgf (96.25%)",)
BrakePressureSpecial=(38,"78"," kgf (97.5%)",)
BrakePressureSpecial=(39,"79"," kgf (98.75%)",)
BrakePressureSpecial=(40,"80"," kgf (100%)",)
BrakePressureCaption="MAX PEDAL FORCE"
HandfrontbrakePressRange=(0,0.1,1)
HandfrontbrakePressSetting=0
HandfrontbrakePressSpecial=(0, "N/A", "",)
HandbrakePressRange=(0,0.1,1)
HandbrakePressSetting=0
HandbrakePressSpecial=(0, "N/A", "",)
Handbrake4WDRelease=0.0        // Start disconnecting at half this value, range is 0.0 (disconnect immediately with any handbrake) to 2.0 (default value, which will never even partially disconnect)
UpshiftAlgorithm=(0.992,0.0)   // Fraction of rev limit to auto-upshift, or rpm to shift at (if 0.0, uses rev limit algorithm)
DownshiftAlgorithm=(0.7945,0.7466,0.6) // High gear downshift point, low gear downshift point, oval adjustment
AutoUpshiftGripThresh=0.2     // auto upshift waits until all driven wheels have this much grip (reasonable range: 0.4-0.9)
AutoDownshiftGripThresh=0.2    // auto downshift waits until all driven wheels have this much grip (reasonable range: 0.4-0.9)
TractionControlGrip=(1.4, 0.2) // average driven wheel grip multiplied by 1st number, then added to 2nd
TractionControlLevel=(0.33, 1.0) // effect of grip on throttle for low TC and high TC
ABS4Wheel=1                    // 0 = old-style single brake pulse, 1 = more effective 4-wheel ABS
ABSGrip=(2.0, 0.0)             // grip multiplied by 1st number and added to 2nd
ABSLevel=(0.31, 0.93)          // effect of grip on brakes for low ABS and high ABS
OnboardBrakeBias=1             // whether brake bias is allowed onboard
PitcrewPushForce=750           // force that a pitcrew may use when in pitlane and out of fuel
MarshalPushForce=750           // force that a marshal may use when vehicle is apparently stuck
TCSpecial=(0, "N/A", "",)
ABSSpecial=(0, "N/A", "",)

[ENGINE]
Normal=USF2000_Engine       // engine file
GeneralTorqueMult*=0.96     // friction and non-optimal conditions (engine wear, etc)
GeneralPowerMult*=0.959
GeneralEngineBrakeMult*=1.0
TorqueCurveShift*=1.0

[DRIVELINE] // Hewland JL200 Sequential or LD200 (for National)
EngineTorqueDistrib=(0.18:front_subbody,0.82:rear_subbody)
ClutchTorqueDistrib=(0.09:front_subbody,0.91:rear_subbody)
GearboxTorqueDistrib=(0:front_subbody,1:rear_subbody)
DifferentialTorqueDistrib=(0:Front_Subbody,1:Rear_Subbody)
ClutchEngineRPM=(1500,-1)      // clutch engagement state depends on engine speed range (-1 for second value to disable)
ClutchEngageRate=5.0           // How quickly clutch is engaged with auto-clutch driving aid
AIClutchEngageRate=2.1         // Override for AI only
ClutchInertia=0.009           // Inertia of parts between clutch and transmission
ClutchTorque=240               // Maximum torque that can be transferred through clutch
ClutchWear=0.01                // Unimplemented
ClutchFriction=4.4             // Friction torque of parts between clutch and transmission when in gear (automatically reduced in neutral)
BaulkTorque=500                // Maximum torque transferred through gears while engaging them
AllowManualOverride=1          // Whether to allow manual shift overrides when using auto shifting
SemiAutomatic=2                // Whether throttle and clutch are operated automatically (1 full semi-auto for up and downshifts, 2 upshift only, 3 downshift only)
AntiStallLogic=(-1,1,10)       // User clutch level to deactivate (or special values -1=no anti-stall, 0="soft" anti-stall), max gear, time till ignition cut)
UpshiftDelay=0.12            // Delay in selecting higher gear (low for semi-automatic, higher for manual)
UpshiftClutchTime=0.0        // Time to ease auto-clutch in AFTER upshift
UpshiftLiftThrottle=0.0       // Lift to this throttle fraction while upshifting (if controlled by game not player))
DownshiftDelay=0.12           // Delay in selecting lower gear (low for semi-automatic, higher for manual)
DownshiftClutchTime=0.2      // Time to ease auto-clutch in AFTER downshift (used to be SemiAutoClutchTime)
DownshiftBlipThrottle=0.86     // Amount of throttle used to blip if controlled by game (instead of player)
WheelDrive=REAR                // Which wheels are driven: REAR, FOUR, or FRONT
AllowGearingChanges=0        // Whether to allow gear ratio changes (not relevant if using 'special overrides' below)
AllowFinalDriveChanges=0     // Whether to allow final drive ratio changes (not relevant if using 'special overrides' below)
AllowReverseAndNeutral=(1,1)   // whether to allow reverse (0 or 1), whether to allow neutral (0 or 1)
//Gear6Special=(0," ",,)
//Gear6Caption=" "
//Gear7Special=(0," ",,)
//Gear7Caption=" "
//Gear8Special=(0," ",,)
//Gear8Caption=" "
DiffPumpTorque=170
DiffPumpRange=(0.004,0.0,1)
DiffPumpSetting=0
DiffPowerRange=(0.0,0.1,1)
DiffPowerSetting=0
DiffCoastRange=(0.0,0.1,1)
DiffCoastSetting=0
DiffPreloadRange=(0.0,1,1)
DiffPreloadSetting=0
DiffPreloadSpecial=(0, "0", " Nm",)
RearSplitRange=(1.0,0.1,1)
RearSplitSetting=0
Pump4WDEffect=(   0.0, 0.0, 1.0) // Effect of various diff settings on
Power4WDEffect=(  0.0, 0.0, 1.0) // the center diff, then the front diff,
Coast4WDEffect=(  0.0, 0.0, 1.0) // and then the rear diff. Sorry, no
Preload4WDEffect=(0.0, 0.0, 1.0) // separate settings for each diff.

[FRONTLEFT]
BumpStopTravels=(0.0,-0.1)    // suspension travel (upwards, downwards)
BumpStopSpring=40000            // initial spring rate of bumpstop
BumpStopRisingSpring=1.2e8      // rising spring rate of bumpstop (multiplied by deflection squared)
BumpStopDamper=2000             // initial damping rate of bumpstop
BumpStopRisingDamper=4.0e6      // rising damper rate of bumpstop (multiplied by deflection squared)
BumpStage2=0.02                 // speed where damper bump moves from slow to fast
ReboundStage2=-0.024            // speed where damper rebound moves from slow to fast
FrictionTorque=1.1              // Newton-meters of friction between spindle and wheel
CGOffsetX=0.0                    // x-offset from graphical center to physical center (NOT IMPLEMENTED)
PackerRange=(0.0, 0.001, 1)
PackerSetting=0
PackerSpecial=(0, "N/A", "",)
SpringMult=2.6569
SpringRange=(70050.734, 17512.6835, 7)
SpringSpecial=(0,400,"lb/in",)
SpringSpecial=(1,500,"lb/in",)
SpringSpecial=(2,600,"lb/in",)
SpringSpecial=(3,700,"lb/in",)
SpringSpecial=(4,800,"lb/in",)
SpringSpecial=(5,900,"lb/in",)
SpringSpecial=(6,1000,"lb/in",)
//SpringSetting=2
SpringRubberRange=(0, 1, 1)
SpringRubberSetting=0
RideHeightRange=(0.025, 0.001, 11)
RideHeightSetting=0
DamperMult=2.6569
SlowBumpRange=(3600, 2100, 11)
SlowBumpSetting=4
FastBumpRange=(2350, 30, 13)
FastBumpSetting=5
SlowReboundRange=(2000, 850, 13)
SlowReboundSetting=5
FastReboundRange=(4475, 100, 13)
FastReboundSetting=5
BrakeDiscRange=(0.01, 0.000, 1) // disc thickness
BrakeDiscSetting=0
BrakePadRange=(0, 1, 2)          // pad type (not implemented)
BrakePadSetting=1
BrakeDiscInertia=2.293           // inertia per meter of thickness
BrakeResponseCurve=(-130,265,560,1065) // Cold temperature in Celcius (where brake torque is half optimum), min temp for optimum brake torque, max temp for optimum brake torque, and overheated temperature (where brake torque is half optimum)
BrakeWearRate=1.2e-11            // meters of wear per second at optimum temperature
BrakeFailure=(0.007,3.0e-4)     // average and variation in disc thickness at failure
BrakeTorque=1850
BrakeHeating=0.00127             // heat added linearly with brake torque times wheel speed (at max disc thickness)
BrakeCooling=(0.0189,1.9e-4)     // minimum brake cooling rate (base and per unit velocity) (at max disc thickness)
BrakeGlow=(550,900)              // Temperature range (in Celsius) that brake glow ramps up

[FRONTRIGHT]
BumpStopTravels=(0.0,-0.1)     // suspension travel (upwards, downwards)
BumpStopSpring=40000             // initial spring rate of bumpstop
BumpStopRisingSpring=1.2e8       // rising spring rate of bumpstop (multiplied by deflection squared)
BumpStopDamper=2000              // initial damping rate of bumpstop
BumpStopRisingDamper=4.0e6       // rising damper rate of bumpstop (multiplied by deflection squared)
BumpStage2=0.02                 // speed where damper bump moves from slow to fast
ReboundStage2=-0.024            // speed where damper rebound moves from slow to fast
FrictionTorque=1.1              // Newton-meters of friction between spindle and wheel
CGOffsetX=0.0                    // x-offset from graphical center to physical center (NOT IMPLEMENTED)
PackerRange=(0.0, 0.001, 1)
PackerSetting=0
PackerSpecial=(0, "N/A", "",)
SpringMult=2.6569
SpringRange=(70050.734, 17512.6835, 7)
SpringSpecial=(0,400,"lb/in",)
SpringSpecial=(1,500,"lb/in",)
SpringSpecial=(2,600,"lb/in",)
SpringSpecial=(3,700,"lb/in",)
SpringSpecial=(4,800,"lb/in",)
SpringSpecial=(5,900,"lb/in",)
SpringSpecial=(6,1000,"lb/in",)
//SpringSetting=2
SpringRubberRange=(0, 1, 1)
SpringRubberSetting=0
RideHeightRange=(0.025, 0.001, 11)
RideHeightSetting=0
DamperMult=2.6569
SlowBumpRange=(3600, 2100, 11)
SlowBumpSetting=4
FastBumpRange=(2350, 30, 13)
FastBumpSetting=5
SlowReboundRange=(2000, 850, 13)
SlowReboundSetting=5
FastReboundRange=(4475, 100, 13)
FastReboundSetting=5
BrakeDiscRange=(0.01, 0.000, 1) // disc thickness
BrakeDiscSetting=0
BrakePadRange=(0, 1, 2)          // pad type (not implemented)
BrakePadSetting=1
BrakeDiscInertia=2.293           // inertia per meter of thickness
BrakeResponseCurve=(-130,265,560,1065) // Cold temperature in Celcius (where brake torque is half optimum), min temp for optimum brake torque, max temp for optimum brake torque, and overheated temperature (where brake torque is half optimum)
BrakeWearRate=1.2e-11            // meters of wear per second at optimum temperature
BrakeFailure=(0.007,3.0e-4)     // average and variation in disc thickness at failure
BrakeTorque=1850
BrakeHeating=0.00127             // heat added linearly with brake torque times wheel speed (at max disc thickness)
BrakeCooling=(0.0189,1.9e-4)     // minimum brake cooling rate (base and per unit velocity) (at max disc thickness)
BrakeGlow=(550,900)              // Temperature range (in Celsius) that brake glow ramps up

[REARLEFT]
BumpStopTravels=(0.0,-0.1)     // suspension travel (upwards, downwards)
BumpStopSpring=40000             // initial spring rate of bumpstop
BumpStopRisingSpring=1.20e8      // rising spring rate of bumpstop (multiplied by deflection squared)
BumpStopDamper=2000              // initial damping rate of bumpstop
BumpStopRisingDamper=4.00e6      // rising damper rate of bumpstop (multiplied by deflection squared)
BumpStage2=0.0167                // speed where damper bump moves from slow to fast
ReboundStage2=-0.0381            // speed where damper rebound moves from slow to fast
FrictionTorque=2.9              // Newton-meters of friction between spindle and wheel
CGOffsetX=-0.0                   // x-offset from graphical center to physical center (NOT IMPLEMENTED)
PackerRange=(0.0, 0.001, 1)
PackerSetting=0
PackerSpecial=(0, "N/A", "",)
SpringMult=3.61
SpringRange=(70050.734, 17512.6835, 7)
SpringSpecial=(0,400,"lb/in",)
SpringSpecial=(1,500,"lb/in",)
SpringSpecial=(2,600,"lb/in",)
SpringSpecial=(3,700,"lb/in",)
SpringSpecial=(4,800,"lb/in",)
SpringSpecial=(5,900,"lb/in",)
SpringSpecial=(6,1000,"lb/in",)
//SpringSetting=3
SpringRubberRange=(0, 1, 1)
SpringRubberSetting=0
RideHeightRange=(0.03, 0.001, 16)
RideHeightSetting=10
DamperMult=3.61
SlowBumpRange=(3000, 2700, 11)
SlowBumpSetting=3
FastBumpRange=(2250, 36, 13)
FastBumpSetting=5
SlowReboundRange=(1400, 2700, 13)
SlowReboundSetting=5
FastReboundRange=(5900, 150, 13)
FastReboundSetting=5
BrakeDiscRange=(0.01, 0.000, 1) // disc thickness
BrakeDiscSetting=0
BrakePadRange=(0, 1, 2)          // pad type (not implemented)
BrakePadSetting=1
BrakeDiscInertia=2.293           // inertia per meter of thickness
BrakeResponseCurve=(-130,265,560,1065) // Cold temperature in Celcius (where brake torque is half optimum), min temp for optimum brake torque, max temp for optimum brake torque, and overheated temperature (where brake torque is half optimum)
BrakeWearRate=1.2e-11            // meters of wear per second at optimum temperature
BrakeFailure=(0.007,3.0e-4)     // average and variation in disc thickness at failure
BrakeTorque=1607
BrakeHeating=0.00127             // heat added linearly with brake torque times wheel speed (at max disc thickness)
BrakeCooling=(0.0176,1.47e-4)    // minimum brake cooling rate (base and per unit velocity) (at max disc thickness)
BrakeGlow=(550,900)              // Temperature range (in Celsius) that brake glow ramps up

[REARRIGHT]
BumpStopTravels=(0.0,-0.1)     // suspension travel (upwards, downwards)
BumpStopSpring=40000             // initial spring rate of bumpstop
BumpStopRisingSpring=1.2e8       // rising spring rate of bumpstop (multiplied by deflection squared)
BumpStopDamper=2000              // initial damping rate of bumpstop
BumpStopRisingDamper=4.0e6       // rising damper rate of bumpstop (multiplied by deflection squared)
BumpStage2=0.0167                // speed where damper bump moves from slow to fast
ReboundStage2=-0.0381            // speed where damper rebound moves from slow to fast
FrictionTorque=2.9              // Newton-meters of friction between spindle and wheel
CGOffsetX=0.0                    // x-offset from graphical center to physical center (NOT IMPLEMENTED)
PackerRange=(0.0, 0.001, 1)
PackerSetting=0
PackerSpecial=(0, "N/A", "",)
SpringMult=3.61
SpringRange=(70050.734, 17512.6835, 7)
SpringSpecial=(0,400,"lb/in",)
SpringSpecial=(1,500,"lb/in",)
SpringSpecial=(2,600,"lb/in",)
SpringSpecial=(3,700,"lb/in",)
SpringSpecial=(4,800,"lb/in",)
SpringSpecial=(5,900,"lb/in",)
SpringSpecial=(6,1000,"lb/in",)
//SpringSetting=3
SpringRubberRange=(0, 1, 1)
SpringRubberSetting=0
RideHeightRange=(0.03, 0.001, 16)
RideHeightSetting=10
DamperMult=3.61
SlowBumpRange=(3000, 2700, 11)
SlowBumpSetting=3
FastBumpRange=(2250, 36, 13)
FastBumpSetting=5
SlowReboundRange=(1400, 2700, 13)
SlowReboundSetting=5
FastReboundRange=(5900, 150, 13)
FastReboundSetting=5
BrakeDiscRange=(0.01, 0.000, 1) // disc thickness
BrakeDiscSetting=0
BrakePadRange=(0, 1, 2)          // pad type (not implemented)
BrakePadSetting=1
BrakeDiscInertia=2.293           // inertia per meter of thickness
BrakeResponseCurve=(-130,265,560,1065) // Cold temperature in Celcius (where brake torque is half optimum), min temp for optimum brake torque, max temp for optimum brake torque, and overheated temperature (where brake torque is half optimum)
BrakeWearRate=1.2e-11            // meters of wear per second at optimum temperature
BrakeFailure=(0.007,3.0e-4)     // average and variation in disc thickness at failure
BrakeTorque=1607
BrakeHeating=0.00127             // heat added linearly with brake torque times wheel speed (at max disc thickness)
BrakeCooling=(0.0176,1.47e-4)    // minimum brake cooling rate (base and per unit velocity) (at max disc thickness)
BrakeGlow=(550,900)              // Temperature range (in Celsius) that brake glow ramps up
