﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Misc;


namespace CarStat_2
{
    public abstract class SetLabels
    {
        protected CarStat2 Form;

        public SetLabels(CarStat2 Form)
        {
            this.Form = Form;
        }

        //Populates the Team information labels
        public abstract void setTeamLabels();

        //Setss the comboboxes (Mixture and Boost) up
        public virtual void setEngineComboBoxes()
        {
            //FuelMixture
            Form.cbFuelMixture.Items.Clear();
            Form.cbFuelMixture.Text = "";

            Form.chkEngineMixture.Enabled = false;
            Form.chkEngineMixture.Checked = false;
            Form.cbFuelMixture.Enabled = false;

            //Boost settings
            Form.cbBoostMapping.Items.Clear();
            Form.cbBoostMapping.Text = "";

            Form.chkBoostMapping.Enabled = false;
            Form.chkBoostMapping.Checked = false;
            Form.cbBoostMapping.Enabled = false;

            Form.chkBoostMapping.Visible = false;
            Form.cbBoostMapping.Visible = false;
            Form.chkEngineMixture.Visible = false;
            Form.cbFuelMixture.Visible = false;

            Form.setLabelText("lblAspiration", "Normally aspirated");
            Form.radbAspNaturally.Checked = true;
            Form.radbAspTurbo.Enabled = false;
        }

        //Populates the Engine labels
        public abstract void setEngineLabels();

        //Populates the Tire labels
        public abstract void setTireLabels();

        //Populates the Brakes labels
        public abstract void setBrakesLabels();


        //Populates the General Car labels
        public abstract void setCarLabels();


        //Populates the Pitstops-labels
        public abstract void setPitstopLabels();
    }
}
