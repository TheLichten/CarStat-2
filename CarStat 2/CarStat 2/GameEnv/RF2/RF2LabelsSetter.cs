﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Misc;

namespace CarStat_2.GameEnv.RF2
{
    public class RF2LabelsSetter : SetLabels
    {

        public RF2LabelsSetter(CarStat2 Form) : base(Form)
        {
        }


        //Populates the Team information labels
        public override void setTeamLabels()
        {
            Form.setLabelText("lblNumber", MiscFunctions.getProperty(Form.getCarData.VEHicleData.Values, "Number", 0));
            Form.setLabelText("lblTeam", MiscFunctions.getProperty(Form.getCarData.VEHicleData.Values, "Team", 0));
            Form.setLabelText("lblDescription", MiscFunctions.getProperty(Form.getCarData.VEHicleData.Values, "Description", 0));
            Form.setLabelText("lblDriver", MiscFunctions.getProperty(Form.getCarData.VEHicleData.Values, "Driver", 0));
            Form.setLabelText("lblEngine", MiscFunctions.getProperty(Form.getCarData.VEHicleData.Values, "Engine", 0));
            Form.setLabelText("lblManufacturer", MiscFunctions.getProperty(Form.getCarData.VEHicleData.Values, "Manufacturer", 0));

            Form.setLabelText("lblFullTeam", MiscFunctions.getProperty(Form.getCarData.VEHicleData.Values, "FullTeamName", 0));
            Form.setLabelText("lblFounded", MiscFunctions.getProperty(Form.getCarData.VEHicleData.Values, "TeamFounded", 0));
            Form.setLabelText("lblHQ", MiscFunctions.getProperty(Form.getCarData.VEHicleData.Values, "TeamHeadquarters", 0));
            Form.setLabelText("lblStarts", MiscFunctions.getProperty(Form.getCarData.VEHicleData.Values, "TeamStarts", 0));
            Form.setLabelText("lblPoles", MiscFunctions.getProperty(Form.getCarData.VEHicleData.Values, "TeamPoles", 0));
            Form.setLabelText("lblWins", MiscFunctions.getProperty(Form.getCarData.VEHicleData.Values, "TeamWins", 0));

            if (MiscFunctions.getProperty(Form.getCarData.VEHicleData.Values, "classes", 0) != null)
                Form.setLabelText("lblClass", Form.getCarData.VEHicleData.Values["classes"][0].Replace(",", ""));

            if (MiscFunctions.getProperty(Form.getCarData.VEHicleData.Values, "category", 0) != null)
                Form.setLabelText("lblCategory", Form.getCarData.VEHicleData.Values["category"][0]);
        }

        //Setss the comboboxes (Mixture and Boost) up
        public override void setEngineComboBoxes()
        {
            //FuelMixture
            Form.cbFuelMixture.Items.Clear();
            Form.cbFuelMixture.Text = "";
            if (Form.getCarData.EngineData.Values.ContainsKey("enginemixturerange"))
            {
                string[] engineMix = Form.getCarData.EngineData.Values["enginemixturerange"][0].Split(',');
                string[] engineMixTable = new string[0];

                if (Form.getCarData.EngineData.Values.ContainsKey("enginemixturespecial"))
                {
                    engineMixTable = Form.getCarData.EngineData.Values["enginemixturespecial"];
                }

                int maxMixtures = Int32.Parse(engineMix[2]);
                string[] mixtures = new string[maxMixtures];

                for (int i = 0; i < mixtures.Length; i++)
                {
                    string name = null;
                    foreach (string line in engineMixTable)
                    {
                        if (line.Split(',')[0].Trim() == ("" + i))
                        {
                            name = line.Split(',')[1].Trim();
                        }
                    }

                    if (name != null)
                        name = (i + 1) + "-" + name;
                    else
                        name = (i + 1) + "";

                    mixtures[i] = name;
                }

                Form.cbFuelMixture.Items.AddRange(mixtures);

                Form.chkEngineMixture.Enabled = true;
                Form.cbFuelMixture.Enabled = Form.chkEngineMixture.Checked;
            }
            else
            {
                Form.chkEngineMixture.Enabled = false;
                Form.chkEngineMixture.Checked = false;
                Form.cbFuelMixture.Enabled = false;
            }

            //Boost settings
            Form.cbBoostMapping.Items.Clear();
            Form.cbBoostMapping.Text = "";
            if (Form.getCarData.EngineData.Values.ContainsKey("engineboostrange") && Double.Parse(MiscFunctions.getProperty(Form.getCarData.EngineData.Values, "engineboostrange", 2)) > 1)
            {
                int maxBoost = Int32.Parse(MiscFunctions.getProperty(Form.getCarData.EngineData.Values, "engineboostrange", 2));
                string[] boosts = new string[maxBoost];

                for (int i = 0; i < boosts.Length; i++)
                {
                    boosts[i] = "" + (i + 1);
                }

                Form.cbBoostMapping.Items.AddRange(boosts);

                Form.chkBoostMapping.Enabled = true;
                Form.cbBoostMapping.Enabled = Form.chkBoostMapping.Checked;

            }
            else
            {
                Form.chkBoostMapping.Enabled = false;
                Form.chkBoostMapping.Checked = false;
                Form.cbBoostMapping.Enabled = false;
            }

            switch (MiscFunctions.getTurboAmount(Form.getCarData.EngineData.EngineFile))
            {
                case 0: Form.setLabelText("lblAspiration", "Normally aspirated"); Form.radbAspNaturally.Checked = true; Form.radbAspTurbo.Enabled = false; break;
                case 1: Form.setLabelText("lblAspiration", "Turbocharged"); Form.radbAspTurbo.Enabled = true; Form.radbAspTurbo.Checked = true; Form.btnRescale_Click(null, null); break;
                case 2: Form.setLabelText("lblAspiration", "Twin-turbocharged"); Form.radbAspTurbo.Enabled = true; Form.radbAspTurbo.Checked = true; Form.btnRescale_Click(null, null); break;
            }
        }

        //Populates the Engine labels
        public override void setEngineLabels()
        {
            bool newEngineModel = true;
            //Refernce Airpressure (and "new" Engine Model)
            int standardAirPressure = 101325;
            int referncePressure = standardAirPressure;
            if (Form.getCarData.EngineData.Values.ContainsKey("referenceconditions"))
            {
                referncePressure = (int)Double.Parse(MiscFunctions.getProperty(Form.getCarData.EngineData.Values, "referenceconditions", 0));
            }
            else
            {
                newEngineModel = false;
            }

            //Boost Mapping
            int boostMapping = Form.getBoostMapping();


            //Turbo
            int boostPressure = -1;
            if (MiscFunctions.getTurboAmount(Form.getCarData.EngineData.EngineFile) > 0 && Form.radbAspTurbo.Checked)
            {
                boostPressure = Form.getBoostPressure();
            }

            //Weight
            double weight = Double.NaN;
            if (Form.getCarData.HDVehicleData != null && Form.getCarData.HDVehicleData.Values != null)
            {
                if (!Double.TryParse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "general", "Mass", 0), out weight))
                    weight = Double.NaN;
            }

            //Engine Mixture
            double fuelMixture = Form.getFuelMixture();
            
            if (Form.getMenuPowerChecked == false) // in kW
                Form.setLabelText("lblPower", Math.Round((Form.getCarData.EngineData.Maxpower), 1) + " kW @ " + Form.getCarData.EngineData.Powerrpm + " rpm");
            else
                Form.setLabelText("lblPower", Math.Round(MiscFunctions.kwToHP((Form.getCarData.EngineData.Maxpower)), 1) + " Hp @ " + Form.getCarData.EngineData.Powerrpm + " rpm");
            if (Form.getMenuTorqueChecked == true) //in NM
                Form.setLabelText("lblTorque", Math.Round((Form.getCarData.EngineData.Maxtorque), 1) + " Nm @ " + Form.getCarData.EngineData.Torquerpm + " rpm");
            else
                Form.setLabelText("lblTorque", Math.Round(MiscFunctions.nmToLb((Form.getCarData.EngineData.Maxtorque)), 1) + " ft-lb @ " + Form.getCarData.EngineData.Torquerpm + " rpm");
            if (Form.getMenuPowerChecked == false) // in kW
                Form.setLabelText("lblPowerWeight", Math.Round((Form.getCarData.EngineData.Maxpower) / weight, 2) + " kW/kg");
            else
                Form.setLabelText("lblPowerWeight", Math.Round(MiscFunctions.kwToHP((Form.getCarData.EngineData.Maxpower)) / weight, 2) + " Hp/kg");
            Double defaultrevlimit = Double.Parse(MiscFunctions.getProperty(Form.getCarData.EngineData.Values, "RevLimitRange", 0));
            Double revlimitChange = Double.Parse(MiscFunctions.getProperty(Form.getCarData.EngineData.Values, "RevLimitRange", 1)) * (Double.Parse(MiscFunctions.getProperty(Form.getCarData.EngineData.Values, "RevLimitRange", 2)) - 1);
            Form.setLabelText("lblRevlimit", defaultrevlimit + " - " + (defaultrevlimit + revlimitChange) + " rpm");
            string tempOptimumOilTemp = MiscFunctions.getProperty(Form.getCarData.EngineData.Values, "OptimumOilTemp", 0);
            if (tempOptimumOilTemp == "n/a" || tempOptimumOilTemp == null)
                tempOptimumOilTemp = MiscFunctions.getProperty(Form.getCarData.EngineData.Values, "OilTemps", 1);
            if (Form.getMenuTempChecked == false) //in °C
                Form.setLabelText("lblOptimal", Double.Parse(tempOptimumOilTemp) + " °C");
            else
                Form.setLabelText("lblOptimal", MiscFunctions.CelsiusToFahrenheit(Double.Parse(tempOptimumOilTemp)) + " °F");
            if (Form.getMenuTempChecked == false) //in °C
            {
                Form.setLabelText("lblSafeOil", Double.Parse(MiscFunctions.getProperty(Form.getCarData.EngineData.Values, "LifetimeOilTemp", 0)) + " °C");
                Form.setLabelText("lblWearTemp", Double.Parse(MiscFunctions.getProperty(Form.getCarData.EngineData.Values, "LifetimeOilTemp", 1)) + " °C");
            }
            else
            {
                Form.setLabelText("lblSafeOil", MiscFunctions.CelsiusToFahrenheit(Double.Parse(MiscFunctions.getProperty(Form.getCarData.EngineData.Values, "LifetimeOilTemp", 0))) + " °F");
                Form.setLabelText("lblWearTemp", MiscFunctions.CelsiusToFahrenheit(Double.Parse(MiscFunctions.getProperty(Form.getCarData.EngineData.Values, "LifetimeOilTemp", 1)) - 17.77778) + " °F");
            }
                
            double temp = 0;

            //Boost engine wear
            double engineLifeMulti = 1;
            string boostEffectWear = MiscFunctions.getProperty(Form.getCarData.EngineData.Values, "BoostEffects", 2);
            if (boostMapping != 0 && Double.TryParse(boostEffectWear, out temp))
            {
                if (newEngineModel)
                    engineLifeMulti = 1 - (temp * boostMapping);
                else
                    engineLifeMulti = (100.0 - (temp * boostMapping))/ 100.0;
            }

            string avgLifetime = MiscFunctions.getProperty(Form.getCarData.EngineData.Values, "LifetimeAvg", 0);
            if (Double.TryParse(avgLifetime, out temp))
                Form.setLabelText("lblLifetime", MiscFunctions.getTime(Double.Parse(avgLifetime) * engineLifeMulti));
            else
                Form.setLabelText("lblLifetime", "n/a");

            string varLifetime = MiscFunctions.getProperty(Form.getCarData.EngineData.Values, "LifetimeVar", 0);
            if (Double.TryParse(varLifetime, out temp) == true)
                Form.setLabelText("lblLifetimeVar", MiscFunctions.getTime(Double.Parse(varLifetime)));
            else
                Form.setLabelText("lblLifetimeVar", "n/a");

            Form.setLabelText("lblMaxrpm", Double.Parse(MiscFunctions.getProperty(Form.getCarData.EngineData.Values, "LifetimeEngineRPM", 0)) + " rpm");

            Form.setLabelText("lblWearRPM", Double.Parse(MiscFunctions.getProperty(Form.getCarData.EngineData.Values, "LifetimeEngineRPM", 1)) + " rpm");
            

            Form.setLabelText("lblEngineInertia", MiscFunctions.getProperty(Form.getCarData.EngineData.Values, "EngineInertia", 0));

            if (boostPressure != -1)
                Form.setLabelText("lblTurboPressure", ((double)boostPressure) / 1000.0 + "kpa");
            else
                Form.setLabelText("lblTurboPressure", "n/a");

            string fuelConsumtion = MiscFunctions.getProperty(Form.getCarData.EngineData.Values, "FuelConsumption", 0);
            double airMulti = ((double)standardAirPressure) / ((double)referncePressure);
            if (boostPressure != -1)
                airMulti = ((double)boostPressure) / ((double)referncePressure);

            string boostEffectFuel = MiscFunctions.getProperty(Form.getCarData.EngineData.Values, "BoostEffects", 1);
            double boostFuelMulti = 1;
            if (Double.TryParse(boostEffectFuel, out temp))
                boostFuelMulti = 1 + (temp * boostMapping);


            if (Double.TryParse(fuelConsumtion, out temp))
            {
                double magicMulti = 2 * Math.PI;
                double fuelCons = Double.Parse(fuelConsumtion) * airMulti * fuelMixture * boostFuelMulti;
                Form.setLabelText("lblFuelConsumtion", Math.Round(fuelCons * Math.Pow(10, 5), 3) + "");
                Form.setLabelText("lblFuelMaxPower", Math.Round(fuelCons * Form.getCarData.EngineData.Powerrpm * magicMulti, 3) + "l");
                Form.setLabelText("lblFuelMaxTorque", Math.Round(fuelCons * Form.getCarData.EngineData.Torquerpm * magicMulti, 3) + "l");

                double revlimit = Double.Parse(MiscFunctions.getProperty(Form.getCarData.EngineData.Values, "RevLimitRange", 1))
                * (Double.Parse(MiscFunctions.getProperty(Form.getCarData.EngineData.Values, "RevLimitRange", 2)) - 1)
                + Double.Parse(MiscFunctions.getProperty(Form.getCarData.EngineData.Values, "RevLimitRange", 0));
                Form.setLabelText("lblFuelMaxRPM", Math.Round(fuelCons * revlimit * magicMulti, 3) + "l");

            }
            else
            {
                Form.setLabelText("lblFuelConsumtion", "n/a");
            }
                

        }

        //Populates the Tire labels
        public override void setTireLabels()
        {
            //No more pressure!
            /*
            if(MenuKPA.Checked)
                lblPressure.Text = MiscFunctions.getProperty(data.TyreFile, "OptimumPressure", 1, CheckCompounds1.SelectedItem.ToString()) + " kPa";
            else
                lblPressure.Text = MiscFunctions.kpaToPSI(Double.Parse(MiscFunctions.getProperty(data.TyreFile, "OptimumPressure", 1, CheckCompounds1.SelectedItem.ToString()))) + " psi";
             */
            //label27.Visible = false;
            //lblPressure.Visible = false;

            //Set tire masses:
            string selectedCompound1 = Form.getCheckCompounds1.SelectedItem.ToString();
            string selectedCompound2 = Form.getCheckCompounds2.SelectedItem.ToString();

            string selectedCompound5 = Form.getCheckCompounds5.SelectedItem.ToString();
            string selectedCompound6 = Form.getCheckCompounds6.SelectedItem.ToString();

            Form.setLabelText("lblFrontTireMass", Math.Round(Double.Parse(MiscFunctions.getProperty(Form.getCarData.TireCompounds[selectedCompound1].TGMFile1, "TotalMass", 1)), 3) + " kg");
            Form.setLabelText("lblRearTireMass", Math.Round(Double.Parse(MiscFunctions.getProperty(Form.getCarData.TireCompounds[selectedCompound1].TGMFile2, "TotalMass", 1)), 3) + " kg");

            if (!Form.getCarData.TireCompounds[selectedCompound1].FrontRear)
            {
                Form.labelTire1Mass.Text = Form.labelTire1Mass.Text.Replace("Front", "Right");
                Form.labelTire2Mass.Text = Form.labelTire2Mass.Text.Replace("Rear", "Left");
                Form.labelRim1.Text = Form.labelRim1.Text.Replace("Front", "Right");
                Form.labelRim2.Text = Form.labelRim2.Text.Replace("Rear", "Left");
            }
            else
            {
                Form.labelTire1Mass.Text = Form.labelTire1Mass.Text.Replace("Right", "Front");
                Form.labelTire2Mass.Text = Form.labelTire2Mass.Text.Replace("Left", "Rear");
                Form.labelRim1.Text = Form.labelRim1.Text.Replace("Right", "Front");
                Form.labelRim2.Text = Form.labelRim2.Text.Replace("Left", "Rear");
            }

            //Set tire radii
            double frontRadius = Double.Parse(MiscFunctions.getProperty(Form.getCarData.TyreFile, "Rim", 1, false, selectedCompound1));
            double rearRadius = Double.Parse(MiscFunctions.getProperty(Form.getCarData.TyreFile, "Rim", 1, true, selectedCompound1));

            Form.setLabelText("lblFrontTireRim", 2 * frontRadius + " m - " + Math.Round(2 * MiscFunctions.MetersToInches(frontRadius), 1) + "\"");
            Form.setLabelText("lblRearTireRim", 2 * rearRadius + " m - " + Math.Round(2 * MiscFunctions.MetersToInches(rearRadius), 1) + "\"");

            //Set tire starttemps:
            if (Form.getMenuTempChecked == false) //In °C
                Form.setLabelText("lblTireStartTemp", Double.Parse(MiscFunctions.getProperty(Form.getCarData.TyreFile, "Temperatures", 2, selectedCompound1)) + " °C");
            else
                Form.setLabelText("lblTireStartTemp", MiscFunctions.CelsiusToFahrenheit(Double.Parse(MiscFunctions.getProperty(Form.getCarData.TyreFile, "Temperatures", 2, selectedCompound1))) + " °F");

            //Set tire optimal temps:
            if (Form.getMenuTempChecked == false) //In °C
                Form.setLabelText("lblTireTemp", MiscFunctions.KelvinToCelsius(Double.Parse(MiscFunctions.getProperty(Form.getCarData.TireCompounds[selectedCompound2].TGMFile1, "StaticCurve", 3))) + " °C");
            else
                Form.setLabelText("lblTireTemp", MiscFunctions.CelsiusToFahrenheit(MiscFunctions.KelvinToCelsius(Double.Parse(MiscFunctions.getProperty(Form.getCarData.TireCompounds[selectedCompound2].TGMFile1, "StaticCurve", 3)))) + " °F");

            //Set thermal degradation activation temp:
            double activationTemp = Double.Parse(MiscFunctions.getProperty(Form.getCarData.TireCompounds[selectedCompound5].TGMValues1, "RealTime", "DegradationCurveParameters", 0));
            double activationTemp2 = Double.Parse(MiscFunctions.getProperty(Form.getCarData.TireCompounds[selectedCompound6].TGMValues1, "RealTime", "DegradationCurveParameters", 0));
            if (Form.getMenuTempChecked == false) //In °C
            {
                Form.setLabelText("lblDegActivation", MiscFunctions.KelvinToCelsius(activationTemp) + " °C");
                Form.setLabelText("lblDegActivation2", MiscFunctions.KelvinToCelsius(activationTemp2) + " °C");
            }
            else
            {
                Form.setLabelText("lblDegActivation", MiscFunctions.CelsiusToFahrenheit(MiscFunctions.KelvinToCelsius(activationTemp)) + " °F");
                Form.setLabelText("lblDegActivation2", MiscFunctions.CelsiusToFahrenheit(MiscFunctions.KelvinToCelsius(activationTemp2)) + " °F");

            }

            double degHistStep = Double.Parse(MiscFunctions.getProperty(Form.getCarData.TireCompounds[selectedCompound5].TGMValues1, "RealTime", "DegradationCurveParameters", 1));
            double degHistStep2 = Double.Parse(MiscFunctions.getProperty(Form.getCarData.TireCompounds[selectedCompound6].TGMValues1, "RealTime", "DegradationCurveParameters", 1));

            Form.setLabelText("lblDegHistoryStep", degHistStep + "");
            Form.setLabelText("lblDegHistoryStep2", degHistStep2 + "");

        }

        //Populates the Brakes labels
        public override void setBrakesLabels()
        {
            string temp = MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "FRONTLEFT", "BrakeOptimumTemp", 0);
            if (temp != null && temp != "n/a")
            {
                double frontOptimum = Double.Parse(temp);
                double rearOptimum = Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "REARLEFT", "BrakeOptimumTemp", 0));
                if (Form.getMenuTempChecked == false) //In °C
                {
                    Form.setLabelText("lblFrontBrakes", frontOptimum + " °C");
                    Form.setLabelText("lblRearBrakes", rearOptimum + " °C");
                }
                else
                {
                    Form.setLabelText("lblFrontBrakes", MiscFunctions.CelsiusToFahrenheit(frontOptimum) + " °F");
                    Form.setLabelText("lblRearBrakes", MiscFunctions.CelsiusToFahrenheit(rearOptimum) + " °F");
                }
            }
            else
            {
                if (Form.getMenuTempChecked == false) //In °C
                {
                    Form.setLabelText("lblFrontBrakes", MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "FRONTLEFT", "BrakeResponseCurve", 1) + "-" + MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "FRONTLEFT", "BrakeResponseCurve", 2) + " °C");
                    Form.setLabelText("lblRearBrakes", MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "REARLEFT", "BrakeResponseCurve", 1) + "-" + MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "REARLEFT", "BrakeResponseCurve", 2) + " °C");
                }
                else
                {
                    Form.setLabelText("lblFrontBrakes", MiscFunctions.CelsiusToFahrenheit(Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "FRONTLEFT", "BrakeResponseCurve", 1)))
                                                   + "-" + MiscFunctions.CelsiusToFahrenheit(Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "FRONTLEFT", "BrakeResponseCurve", 2))) + " °F");
                    Form.setLabelText("lblRearBrakes", MiscFunctions.CelsiusToFahrenheit(Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "REARLEFT", "BrakeResponseCurve", 1)))
                                                   + "-" + MiscFunctions.CelsiusToFahrenheit(Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "REARLEFT", "BrakeResponseCurve", 2))) + " °F");
                }
            }
            if (Form.getMenuTorqueChecked == true) //in NM
            {
                Form.setLabelText("lblFrontBrakeTorque", MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "FRONTLEFT", "BrakeTorque", 0) + " Nm");
                Form.setLabelText("lblRearBrakeTorque", MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "REARLEFT", "BrakeTorque", 0) + " Nm");
            }
            else
            {
                Form.setLabelText("lblFrontBrakeTorque", MiscFunctions.nmToLb(Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "FRONTLEFT", "BrakeTorque", 0))) + " ft-lb");
                Form.setLabelText("lblRearBrakeTorque", MiscFunctions.nmToLb(Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "REARLEFT", "BrakeTorque", 0))) + " ft-lb");
            }
            Form.setLabelText("lblFrontBrakeThick", Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "FRONTLEFT", "BrakeDiscRange", 0)) * 1000 + " mm");
            Form.setLabelText("lblRearBrakeThick", Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "REARLEFT", "BrakeDiscRange", 0)) * 1000 + " mm");

            //Calculate the min and max Brake Failure thickness:
            string brakeFailFront = MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "FRONTLEFT", "BrakeFailure", 1);
            string brakeFailRear = MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "REARLEFT", "BrakeFailure", 1);
            if (brakeFailFront != "n/a" && brakeFailFront != null)
            {
                double minThickFront = (Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "FRONTLEFT", "BrakeFailure", 0)) - Double.Parse(brakeFailFront)) * 1000;
                double maxThickFront = (Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "FRONTLEFT", "BrakeFailure", 0)) + Double.Parse(brakeFailFront)) * 1000;
                Form.setLabelText("lblFrontBrakeFail", minThickFront + " - " + maxThickFront + " mm");
            }
            else
            {
                Form.setLabelText("lblFrontBrakeFail", "n/a");
            }

            if (brakeFailRear != "n/a" && brakeFailFront != null)
            {
                double minThickRear = (Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "REARLEFT", "BrakeFailure", 0)) - Double.Parse(brakeFailRear)) * 1000;
                double maxThickRear = (Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "REARLEFT", "BrakeFailure", 0)) + Double.Parse(brakeFailRear)) * 1000;
                Form.setLabelText("lblRearBrakeFail", minThickRear + " - " + maxThickRear + " mm");
            }
            else
            {
                Form.setLabelText("lblRearBrakeFail", "n/a");
            }


            if (MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "CONTROLS", "OnboardBrakeBias", 0) == "1")
                Form.setLabelText("lblBrakeBias", "Available");
            else
                Form.setLabelText("lblBrakeBias", "Unavailable");
        }


        //Populates the General Car labels
        public override void setCarLabels()
        {
            Form.setLabelText("lblWeight", MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "GENERAL", "Mass", 0) + " kg");
            double bla;
            if (Double.TryParse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "GENERAL", "CGHeight", 0), out bla)) //CGHeight is found
                Form.setLabelText("lblCG", Math.Round(Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "GENERAL", "CGHeight", 0)), 3) + " m");
            else  //CGHeight not found, so looking for CGHeightRange
                Form.setLabelText("lblCG", Math.Round(Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "GENERAL", "CGHeightRange", 0)), 3) + " m");
            double rearWeight = Math.Round(double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "GENERAL", "CGRearRange", 0)) * 100, 2);
            Form.setLabelText("lblWeightDistribution", (100 - rearWeight) + " : " + rearWeight + " (F:R)");
            double fuel = Math.Round((double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "GENERAL", "FuelRange", 2)) - 1)
                * double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "GENERAL", "FuelRange", 1))
                + double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "GENERAL", "FuelRange", 0)), 1);
            Form.setLabelText("lblFuel", fuel + " liter");
            if ((MiscFunctions.getProperty(Form.getCarData.EngineData.Values, "SpeedLimiter", 0) == "0") || (MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "GENERAL", "SpeedLimiter", 0) == "0"))
                Form.setLabelText("lblLimiter", "Unavailable");
            else
                Form.setLabelText("lblLimiter", "Available");
            if ((MiscFunctions.getProperty(Form.getCarData.EngineData.Values, "OnboardStarter", 0) == "0") || (MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "GENERAL", "OnboardStarter", 0) == "0"))
                Form.setLabelText("lblStarter", "Unavailable");
            else
                Form.setLabelText("lblStarter", "Available");

            //AntiRoll
            string frontAntiRoll = MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "SUSPENSION", "FrontAntiSwayParams", 2);
            string rearAntiRoll = MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "SUSPENSION", "RearAntiSwayParams", 2);

            if (frontAntiRoll == "1")
            {
                if (rearAntiRoll == "1")
                {
                    Form.setLabelText("lblAntiRoll", "Both");
                }
                else
                {
                    Form.setLabelText("lblAntiRoll", "Front");
                }
            }
            else if (rearAntiRoll == "1")
            {
                Form.setLabelText("lblAntiRoll", "Rear");
            }
            else
            {
                Form.setLabelText("lblAntiRoll", "No");
            }

            if (MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "SUSPENSION", "OnboardRightTrackBarAdjustment", 0) == "1")
                Form.setLabelText("lblTrackbarFly", "Yes");
            else
                Form.setLabelText("lblTrackbarFly", "No");

            Form.setLabelText("lblUpshift", (Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "DRIVELINE", "UpshiftDelay", 0)) + Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "DRIVELINE", "UpshiftClutchTime", 0))) * 1000 + " ms");
            Form.setLabelText("lblDownshift", (Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "DRIVELINE", "DownshiftDelay", 0)) + Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "DRIVELINE", "DownshiftClutchTime", 0))) * 1000 + " ms");
            switch (MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "DRIVELINE", "WheelDrive", 0).ToLower())
            {
                case "rear": Form.setLabelText("lblWheels", "Rear wheels"); break;
                case "front": Form.setLabelText("lblWheels", "Front wheels"); break;
                case "four": Form.setLabelText("lblWheels", "Four wheels"); break;
            }
            Form.setLabelText("lblDrag", MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "BODYAERO", "BodyDragBase", 0));
            double TurnsLockToLock = 0;
            if (Double.TryParse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "CONTROLS", "TurnsLockToLock", 0), out TurnsLockToLock) == true)
                Form.setLabelText("lblWheelRotation", Math.Round(TurnsLockToLock * 360, 2) + "°");
            else if (Form.getCarData.HDVehicleData.Values["controls"].ContainsKey("steerlockspecial"))
            {
                int steeringSettings = Int32.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "CONTROLS", "SteerLockRange", 2));
                TurnsLockToLock = -1;

                for (int i = 0; i < steeringSettings; i++)
                {
                    string[] steeringSpecial = Form.getCarData.HDVehicleData.Values["controls"]["steerlockspecial"][i].Split(',');
                    string turns = "";

                    foreach (string val in steeringSpecial)
                    {
                        if (val.ToLower().Contains("turnslocktolock"))
                        {
                            turns = val.Split('=')[1].Trim();
                            break;
                        }
                    }

                    double currentLock;

                    if (Double.TryParse(turns, out currentLock))
                    {
                        if (currentLock > TurnsLockToLock)
                            TurnsLockToLock = currentLock;
                    }

                }


                if (TurnsLockToLock != -1)
                    Form.setLabelText("lblWheelRotation", Math.Round(TurnsLockToLock * 360, 2) + "°");
                else
                    Form.setLabelText("lblWheelRotation", "n/a");
            }
            else
            {
                Form.setLabelText("lblWheelRotation", "n/a");
            }

            Form.setLabelText("lblNumberGears", MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "DRIVELINE", "ForwardGears", 0));

            string antiStall = MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "DRIVELINE", "AntiStallLogic", 0);
            if (antiStall == "-1" || antiStall == null)
                Form.setLabelText("lblAntiStall", "No");
            else
                Form.setLabelText("lblAntiStall", "Yes");

            string semiAuto = MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "DRIVELINE", "SemiAutomatic", 0);
            if (semiAuto == "1")
                Form.setLabelText("lblSemiAuto", "Full");
            else if (semiAuto == "2")
                Form.setLabelText("lblSemiAuto", "Upshifts Only");
            else if (semiAuto == "3")
                Form.setLabelText("lblSemiAuto", "Downshifts Only");
            else
                Form.setLabelText("lblSemiAuto", "No");

            //AIDPenalties
            double vehWeight = Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "GENERAL", "Mass", 0));

            if (Form.getCarData.HDVehicleData.Values.ContainsKey("AIDPENALTIES".ToLower()))
            {
                Form.setLabelText("lblTCLow", Math.Round(Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "AIDPENALTIES", "TC", 1)) * vehWeight, 3) + " kg");
                Form.setLabelText("lblTCHigh", Math.Round(Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "AIDPENALTIES", "TC", 2)) * vehWeight, 3) + " kg");
                Form.setLabelText("lblABSLow", Math.Round(Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "AIDPENALTIES", "ABS", 1)) * vehWeight, 3) + " kg");
                Form.setLabelText("lblABSHigh", Math.Round(Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "AIDPENALTIES", "ABS", 2)) * vehWeight, 3) + " kg");
                Form.setLabelText("lblSCLow", Math.Round(Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "AIDPENALTIES", "Stability", 1)) * vehWeight, 3) + " kg");
                Form.setLabelText("lblSCHigh", Math.Round(Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "AIDPENALTIES", "Stability", 2)) * vehWeight, 3) + " kg");
                Form.setLabelText("lblAutoShiftOne", Math.Round(Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "AIDPENALTIES", "Autoshift", 1)) * vehWeight, 3) + " kg");
                Form.setLabelText("lblAutoShiftFull", Math.Round(Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "AIDPENALTIES", "Autoshift", 3)) * vehWeight, 3) + " kg");
                Form.setLabelText("lblAutoPit", Math.Round(Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "AIDPENALTIES", "AutoPit", 1)) * vehWeight, 3) + " kg");
                Form.setLabelText("lblAutoLift", Math.Round(Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "AIDPENALTIES", "AutoLift", 1)) * vehWeight, 3) + " kg");
                Form.setLabelText("lblAutoBlip", Math.Round(Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "AIDPENALTIES", "AutoBlip", 1)) * vehWeight, 3) + " kg");
            }
            else
            {
                Form.setLabelText("lblTCLow", Math.Round(0.004 * vehWeight, 3) + " kg");
                Form.setLabelText("lblTCHigh", Math.Round(0.006 * vehWeight, 3) + " kg");
                Form.setLabelText("lblABSLow", Math.Round(0.006 * vehWeight, 3) + " kg");
                Form.setLabelText("lblABSHigh", Math.Round(0.01 * vehWeight, 3) + " kg");
                Form.setLabelText("lblSCLow", Math.Round(0.005 * vehWeight, 3) + " kg");
                Form.setLabelText("lblSCHigh", Math.Round(0.008 * vehWeight, 3) + " kg");
                Form.setLabelText("lblAutoShiftOne", Math.Round(0.001 * vehWeight, 3) + " kg");
                Form.setLabelText("lblAutoShiftFull", Math.Round(0.002 * vehWeight, 3) + " kg");
                Form.setLabelText("lblAutoPit", Math.Round(0.002 * vehWeight, 3) + " kg");
                Form.setLabelText("lblAutoLift", Math.Round(0.001 * vehWeight, 3) + " kg");
                Form.setLabelText("lblAutoBlip", Math.Round(0.004 * vehWeight, 3) + " kg");
            }

            

            //Distance Between Wheels
            double[] carSize = { 0, 0, 0, 0 };
            Double.TryParse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "SUSPENSION", "FrontWheelTrackRange", 0), out carSize[0]);
            Double.TryParse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "SUSPENSION", "RearWheelTrackRange", 0), out carSize[1]);
            Double.TryParse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "SUSPENSION", "RightWheelBase", 0), out carSize[2]);
            Double.TryParse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "SUSPENSION", "LeftWheelBase", 0), out carSize[3]);

            Form.setLabelText("lblFrontTrackWidth", Math.Round(carSize[0],3) + " m");
            Form.setLabelText("lblRearTrackWidth", Math.Round(carSize[1], 3) + " m");
            Form.setLabelText("lblRightWheelBase", Math.Round(carSize[2], 3) + " m");
            Form.setLabelText("lblLeftWheelBase", Math.Round(carSize[3], 3) + " m");

            //Filenames:
            string filename = Form.getCarData.VEHlocation;
            if (filename != null)
                Form.setLabelText("lblVEH", System.IO.Path.GetFileName(filename));
            else
                Form.setLabelText("lblVEH", "");

            string hdv = MiscFunctions.getProperty(Form.getCarData.VEHicleData.Values, "HDVehicle", 0);
            if (hdv != null)
                Form.setLabelText("lblHDV", System.IO.Path.GetFileNameWithoutExtension(hdv) + ".hdv");
            else
                Form.setLabelText("lblHDV", "");

            string tbc = MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "GENERAL", "TireBrand", 0);
            if (tbc != null)
                Form.setLabelText("lblTBC", System.IO.Path.GetFileNameWithoutExtension(tbc) + ".tbc");
            else
                Form.setLabelText("lblTBC", "");

            string engine = MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "ENGINE", "Normal", 0);
            if (engine != null)
                Form.setLabelText("lblEngineINI", System.IO.Path.GetFileNameWithoutExtension(engine) + ".ini");
            else
                Form.setLabelText("lblEngineINI", "");

            string upgrades = MiscFunctions.getProperty(Form.getCarData.VEHicleData.Values, "Upgrades", 0);
            if (upgrades != null)
                Form.setLabelText("lblUpgradesINI", System.IO.Path.GetFileNameWithoutExtension(upgrades) + ".ini");
            else
                Form.setLabelText("lblUpgradesINI", "");

            string chassis = MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "SUSPENSION", "UltraChassis", 0);
            if (chassis != null)
                Form.setLabelText("lblChassisINI", System.IO.Path.GetFileNameWithoutExtension(chassis) + ".ini");
            else
                Form.setLabelText("lblChassisINI", "");
        }


        //Populates the Pitstops-labels
        public override void setPitstopLabels()
        {
            Dictionary<string, string[]> pitmenu = null;
            Form.getCarData.HDVehicleData.Values.TryGetValue("pitmenu", out pitmenu);
            if (pitmenu == null)
            {
                throw new Exception("Pitmenu not contained in the HDV file");
            }

            Form.setLabelText("lblGiveup", MiscFunctions.getProperty(pitmenu, "Preparation", 0) + " secs");
            Form.setLabelText("lblPreparation", MiscFunctions.getProperty(pitmenu, "Preparation", 1) + " secs");
            Form.setLabelText("lblDelayMulti", "x " + MiscFunctions.getProperty(pitmenu, "Preparation", 2));
            Form.setLabelText("lblMaxDelay", MiscFunctions.getProperty(pitmenu, "Preparation", 3) + " secs");

            Form.setLabelText("lblFuelFillRate", MiscFunctions.getProperty(pitmenu, "FuelTime", 0) + " liters/sec");
            Form.setLabelText("lblMaxFuelDelay", MiscFunctions.getProperty(pitmenu, "FuelTime", 1) + " secs");
            Form.setLabelText("lblNozzleInsertion", MiscFunctions.getProperty(pitmenu, "FuelTime", 2) + " secs");
            Form.setLabelText("lblNozzleRemoval", MiscFunctions.getProperty(pitmenu, "FuelTime", 3) + " secs");


            Form.setLabelText("lblChange2tires", MiscFunctions.getProperty(pitmenu, "TireTime", 0) + " secs");
            Form.setLabelText("lblChange4tires", MiscFunctions.getProperty(pitmenu, "TireTime", 1) + " secs");
            Form.setLabelText("lblDelayTirechange", MiscFunctions.getProperty(pitmenu, "TireTime", 2) + " secs");

            //Check if FrontWing adjustment is possible and fill values accordingly:
            if (MiscFunctions.getProperty(pitmenu, "FrontWing", 0) == "1")
                Form.setLabelText("lblFrontWing", MiscFunctions.getProperty(pitmenu, "FrontWingTime", 0) + " secs");
            else
                Form.setLabelText("lblFrontWing", "Not allowed");

            //Check if RearWing adjustment is possible and fill values accordingly:
            if (MiscFunctions.getProperty(pitmenu, "RearWing", 0) == "1")
                Form.setLabelText("lblRearWing", MiscFunctions.getProperty(pitmenu, "RearWingTime", 0) + " secs");
            else
                Form.setLabelText("lblRearWing", "Not allowed");

            //Check if Wedge adjustment is possible and fill values accordingly:
            if (MiscFunctions.getProperty(pitmenu, "Wedge", 0) == "1")
                Form.setLabelText("lblWedge", MiscFunctions.getProperty(pitmenu, "WedgeTime", 0) + " secs");
            else
                Form.setLabelText("lblWedge", "Not allowed");

            //Check if Radiator adjustment is possible and fill values accordingly:
            if (MiscFunctions.getProperty(pitmenu, "Radiator", 0) == "1")
                Form.setLabelText("lblRadiator", MiscFunctions.getProperty(pitmenu, "RadiatorTime", 0) + " secs");
            else
                Form.setLabelText("lblRadiator", "Not allowed");

            //Check if TrackBar adjustment is possible and fill values accordingly:
            if (MiscFunctions.getProperty(pitmenu, "TrackBar", 0) == "1")
                Form.setLabelText("lblTrackbar", MiscFunctions.getProperty(pitmenu, "TrackBarTime", 0) + " secs");
            else
                Form.setLabelText("lblTrackbar", "Not allowed");

            //Check if Tire Pressure adjustment is possible and fill values accordingly:
            if (MiscFunctions.getProperty(pitmenu, "PressureOnTheFly", 0) == "1")
                Form.setLabelText("lblPressureAdjustment", MiscFunctions.getProperty(pitmenu, "PressureTime", 0) + " secs");
            else
                Form.setLabelText("lblPressureAdjustment", "Not allowed");

            //Check if Tire Pressure adjustment is possible and fill values accordingly:
            if (MiscFunctions.getProperty(pitmenu, "Pressure", 0) == "1")
                Form.setLabelText("lblPressureChangePossible", "Yes");
            else
                Form.setLabelText("lblPressureChangePossible", "No");

            //Check if SpringRubber adjustment is possible and fill values accordingly:
            if (MiscFunctions.getProperty(pitmenu, "SpringRubber", 0) == "1")
                Form.setLabelText("lblSpringRubber", MiscFunctions.getProperty(pitmenu, "SpringRubberTime", 0) + " secs");
            else
                Form.setLabelText("lblSpringRubber", "Not allowed");

            //Check if Drivers swaps are possible and fill values accordingly:
            if (MiscFunctions.getProperty(pitmenu, "Driver", 0) == "1")
            {
                Double driverSwap = Double.Parse(MiscFunctions.getProperty(pitmenu, "DriverTime", 0));
                Double driverDelay = Double.Parse(MiscFunctions.getProperty(pitmenu, "DriverTime", 1));
                Form.setLabelText("lblDriverswap", driverSwap + " - " + (driverSwap + driverDelay) + " secs");
                if (MiscFunctions.getProperty(pitmenu, "DriverTime", 3).Replace(".0", "") == "1")
                    Form.setLabelText("lblConcurrentDriver", "Yes");
                else
                    Form.setLabelText("lblConcurrentDriver", "No");
            }
            else
            {
                Form.setLabelText("lblDriverswap", "Not allowed");
                Form.setLabelText("lblConcurrentDriver", "No");
            }

            double fueltime = -1;
            double tiretime = -1;

            Double.TryParse(MiscFunctions.getProperty(pitmenu, "FuelTime", 4), out fueltime);
            Double.TryParse(MiscFunctions.getProperty(pitmenu, "TireTime", 3), out tiretime);

            if (fueltime == 1.0)
                if (tiretime == 1.0)
                    Form.setLabelText("lblConcurrent", "Yes");
                else
                    Form.setLabelText("lblConcurrent", "No");
            else
                if (tiretime == 0.0)
                Form.setLabelText("lblConcurrent", "No");
            else
                Form.setLabelText("lblConcurrent", "No");

            Form.setLabelText("lblDamageAero", MiscFunctions.getProperty(pitmenu, "DamageTime", 0) + " secs");
            Form.setLabelText("lblDamageSusp", MiscFunctions.getProperty(pitmenu, "DamageTime", 2) + " secs");
            Form.setLabelText("lblDamageDelay", MiscFunctions.getProperty(pitmenu, "DamageTime", 1) + " secs");
            if (MiscFunctions.getProperty(pitmenu, "DamageTime", 3) == "1.0")
                Form.setLabelText("lblConcurRepair", "Yes");
            else
                Form.setLabelText("lblConcurRepair", "No");
        }
    }
}
