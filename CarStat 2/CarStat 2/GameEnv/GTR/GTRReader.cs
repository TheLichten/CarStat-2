﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarStat_2.GameEnv.RF1;
using Misc;

namespace CarStat_2.GameEnv.GTR
{
    public class GTRReader : RF1Reader
    {
        public GTRReader(GameEnvironmentFunc gameEnv) : base(gameEnv)
        {
        }

        internal override void readTireFile(bool OnlyTBC)
        {
            base.readTireFile(OnlyTBC, ".tyr");
        }

        protected override void findHDV()
        {
            //Read HDV file:
            string hdvFileName = MiscFunctions.getProperty(gameEnv.Data.VEHicleData.Values, "hdvehicle", 0);
            if (hdvFileName != null && !hdvFileName.ToLower().Contains(".hdc"))
                hdvFileName = hdvFileName + ".hdc";


            string hdvfile = DirSearch(gameEnv.Data.VehicleRoot, hdvFileName);
            if (hdvfile == "encrypted")
            {
                throw new FileNotFoundException("File Encryted");
            }
            gameEnv.Data.HDVlocation = hdvfile;
        }

        internal override void readEngineFile()
        {
            string engineFileName = MiscFunctions.getProperty(gameEnv.Data.HDVehicleData.Values, "engine", "Normal", 0);
            if (Path.GetExtension(engineFileName).ToLower() != ".eng")
                engineFileName += ".eng";
            string engineFileLoc = DirSearch(gameEnv.Data.VehicleRoot, engineFileName);
            if (engineFileLoc == "encrypted")
            {
                throw new FileNotFoundException("File Encryted");
            }
            if (engineFileLoc == null)
                throw new Exception("Engine file not found. Try another vehicle file.");

            readEngineFile(engineFileLoc);
        }

        internal override void readEngineFile(string engineFileLoc)
        {
            gameEnv.Data.EngineFileLocation = engineFileLoc;
            
            gameEnv.Data.EngineData.EngineFile = MiscFunctions.FileToList(gameEnv.Data.EngineFileLocation);

            //Fix Woochoo bug
            gameEnv.Data.EngineData.EngineFile[0] = gameEnv.Data.EngineData.EngineFile[0].Replace(Path.GetFileName(gameEnv.Data.EngineFileLocation), "");

            gameEnv.Data.EngineData.Values = ListToDict(gameEnv.Data.EngineData.EngineFile);
        }

        internal override void readGearRatios()
        {
            //Nothing, gears will not be read
        }
    }
}
