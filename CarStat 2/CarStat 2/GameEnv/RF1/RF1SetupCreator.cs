﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarStat_2;

namespace CarStat_2.GameEnv.RF1
{
    public class RF1SetupCreator : SetupCreator
    {
        public RF1SetupCreator(CarStat2 carStat) : base(carStat)
        {
        }

        //TODO: Future implementation

        public override void build()
        {
            carStat.tabSetupControl.Visible = false;
        }

        public override void refill()
        {
            
        }

        public override void saveSetup(string fileloc)
        {
            
        }

        public override void redrawGraphs(object sender, EventArgs e)
        {
            
        }
    }
}
