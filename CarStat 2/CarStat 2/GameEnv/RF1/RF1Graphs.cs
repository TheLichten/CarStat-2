﻿using Misc;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZedGraph;

namespace CarStat_2.GameEnv.RF1
{
    public class RF1Graphs : Graphs
    {
        double brakeOffsetFront = 0;
        double brakeOffsetRear = 0;

        public RF1Graphs(CarStat2 Form) : base(Form)
        {
        }

        public override void BrakeTempsGraph(bool rearBrake)
        {
            GraphPane myPane;
            if (rearBrake == true) //So read the rear brake
            {
                myPane = Form.getZedRearBrakes.GraphPane;
                Form.getZedRearBrakes.GraphPane.CurveList.Clear();
            }
            else
            {
                myPane = Form.getZedFrontBrakes.GraphPane;
                Form.getZedFrontBrakes.GraphPane.CurveList.Clear();
            }

            string brakeLocation = "FRONTLEFT";
            if (rearBrake == true) //So read the rear brake
            {
                brakeLocation = "REARLEFT";
            }
            double F_lowhalved = 0;
            double F_lowoptimal = 0;
            double F_highoptimal = 0;
            double F_highhalved = 0;

            if (MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, brakeLocation, "BrakeOptimumTemp", 0) != null
                && MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, brakeLocation, "BrakeOptimumTemp", 0) != "n/a")
            {
                double BrakeOptimumTemp = Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, brakeLocation, "BrakeOptimumTemp", 0));
                double BrakeFadeRange = Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, brakeLocation, "BrakeFadeRange", 0));
                F_lowhalved = BrakeOptimumTemp - BrakeFadeRange;
                F_lowoptimal = BrakeOptimumTemp;
                F_highoptimal = BrakeOptimumTemp;
                F_highhalved = BrakeOptimumTemp + BrakeFadeRange;
            }
            else
            {
                F_lowhalved = Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, brakeLocation, "BrakeResponseCurve", 0));
                F_lowoptimal = Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, brakeLocation, "BrakeResponseCurve", 1));
                F_highoptimal = Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, brakeLocation, "BrakeResponseCurve", 2));
                F_highhalved = Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, brakeLocation, "BrakeResponseCurve", 3));
            }

            if (Form.getMenuTempChecked == true) //In °F
            {
                F_lowhalved = MiscFunctions.CelsiusToFahrenheit(F_lowhalved);
                F_lowoptimal = MiscFunctions.CelsiusToFahrenheit(F_lowoptimal);
                F_highoptimal = MiscFunctions.CelsiusToFahrenheit(F_highoptimal);
                F_highhalved = MiscFunctions.CelsiusToFahrenheit(F_highhalved);
            }

            myPane.BarSettings.Type = BarType.Stack;


            // Generate a blue bar with "Curve 2" in the legend
            BarItem myCurve = myPane.AddBar("There", new double[] { F_lowoptimal - F_lowhalved }, null, Color.Blue);
            // Fill the bar with a Blue-white-Blue color gradient for a 3d look
            myCurve.Bar.Fill = new Fill(Color.Red, Color.Green);

            // Generate a green bar with "Curve 3" in the legend
            myCurve = myPane.AddBar("Elsewhere", new double[] { F_highoptimal - F_lowoptimal }, null, Color.Green);
            // Fill the bar with a Green-white-Green color gradient for a 3d look
            myCurve.Bar.Fill = new Fill(Color.Green, Color.Green);
            // Generate a green bar with "Curve 3" in the legend
            myCurve = myPane.AddBar("Elsewhere", new double[] { F_highhalved - F_highoptimal }, null, Color.Green);
            // Fill the bar with a Green-white-Green color gradient for a 3d look
            myCurve.Bar.Fill = new Fill(Color.Green, Color.Red);


            myPane.BarSettings.Base = BarBase.Y;
            myPane.BarSettings.ClusterScaleWidth = 4;

            // Fill the chart background with a color gradient
            myPane.Fill.Color = SystemColors.ButtonFace;
            myPane.Margin.All = 0;
            myPane.Border.IsVisible = false;
            myPane.IsFontsScaled = false;
            myPane.Title.IsVisible = false;
            myPane.Legend.IsVisible = false;
            myPane.YAxis.Scale.Min = 0;
            myPane.YAxis.Scale.Max = 2;
            myPane.YAxis.IsVisible = false;
            myPane.XAxis.Title.IsVisible = false;
            myPane.XAxis.MajorTic.IsOpposite = false;
            myPane.XAxis.MinorTic.IsOpposite = false;
            myPane.YAxis.Type = AxisType.Linear;

            myPane.XAxis.Scale.FontSpec.Size = 12;
            // Calculate the Axis Scale Ranges

            if (rearBrake == true) //So read the rear brake
            {
                brakeOffsetRear = F_lowhalved;
                myPane.XAxis.Scale.Max = F_highhalved - brakeOffsetRear;
                Form.getZedRearBrakes.MasterPane.Border.IsVisible = false;
                Form.getZedRearBrakes.GraphPane.XAxis.ScaleFormatEvent += new Axis.ScaleFormatHandler(XScaleFormatEventRear);
                Form.getZedRearBrakes.AxisChange();
                Form.getZedRearBrakes.Refresh();
            }
            else
            {
                brakeOffsetFront = F_lowhalved;
                myPane.XAxis.Scale.Max = F_highhalved - brakeOffsetFront;
                Form.getZedFrontBrakes.MasterPane.Border.IsVisible = false;
                Form.getZedFrontBrakes.GraphPane.XAxis.ScaleFormatEvent += new Axis.ScaleFormatHandler(XScaleFormatEventFront);
                Form.getZedFrontBrakes.AxisChange();
                Form.getZedFrontBrakes.Refresh();
            }
        }

        public string XScaleFormatEventFront(GraphPane pane, Axis axis, double val, int index)
        {
            if (Form.getMenuTempChecked == false) //In °C
                return (val + brakeOffsetFront).ToString() + "°C";
            else
                return (val + brakeOffsetFront).ToString() + "°F";
        }

        public string XScaleFormatEventRear(GraphPane pane, Axis axis, double val, int index)
        {
            if (Form.getMenuTempChecked == false) //In °C
                return (val + brakeOffsetRear).ToString() + "°C";
            else
                return (val + brakeOffsetRear).ToString() + "°F";
        }

        public override void EngineGraph(bool init)
        {
            Form.getZedEngine.GraphPane.CurveList.Clear();
            GraphPane myPane = Form.getZedEngine.GraphPane;
            myPane.Title.Text = "Torque/Power";
            myPane.XAxis.Title.Text = "RPM";

            if (Form.getMenuTorqueChecked == true)
                myPane.YAxis.Title.Text = "Torque (Nm)";
            else
                myPane.YAxis.Title.Text = "Torque (ft-lb)";
            if (Form.getMenuPowerChecked == true)
                myPane.Y2Axis.Title.Text = "Power (Hp)";
            else
                myPane.Y2Axis.Title.Text = "Power (kW)";

            PointPairList torque = new PointPairList();
            PointPairList power = new PointPairList();
            string line = null;
            string[] split = null;
            //int selectedUpgrade = dropEngine.SelectedIndex;

            Form.getCarData.EngineData.Maxpower = 0;
            Form.getCarData.EngineData.Powerrpm = 0;
            Form.getCarData.EngineData.Maxtorque = 0;
            Form.getCarData.EngineData.Torquerpm = 0;

            try
            {
                for (int i = 0; i < Form.getCarData.EngineData.EngineFile.Count; i++)
                {
                    line = Form.getCarData.EngineData.EngineFile[i];
                    if (line.Contains("RPMTorque") || line.Contains("RPMBase"))
                    {
                        line = line.Substring(line.IndexOf("(") + 1);
                        line = line.Replace(")", "");
                        line = line.Trim();
                        split = line.Split(',');
                        double trek = 0;
                        double rpm = 0;
                        if (split[2].Contains("/"))
                            split[2] = split[2].Substring(0, split[2].IndexOf('/')).Trim();

                        rpm = Double.Parse(split[0]);
                        trek = Double.Parse(split[2]);

                        /*if ((selectedUpgrade != -1) && (data.Upgrades[selectedUpgrade].TorqueMulti != 0))
                            trek = trek * data.Upgrades[selectedUpgrade].TorqueMulti;
                        rpm = Double.Parse(split[0]);
                        if ((selectedUpgrade != -1) && (data.Upgrades[selectedUpgrade].TorqueShift != 0))
                            rpm = rpm * data.Upgrades[selectedUpgrade].TorqueShift;*/
                        //rep.message(rpm + " rpm: \nTorque: " + trek + ", Power: " + torqueToHP(trek, rpm));

                        if (trek < 0)
                            trek = 0;
                        if (Form.getMenuTorqueChecked)
                            torque.Add(rpm, trek);
                        else
                            torque.Add(rpm, MiscFunctions.nmToLb(trek));

                        double pow = MiscFunctions.torqueToHP(trek, rpm);

                        if (Form.getMenuTorqueChecked)
                            torque.Add(rpm, trek);
                        else
                            torque.Add(rpm, MiscFunctions.nmToLb(trek));

                        if (Form.getMenuPowerChecked)
                            power.Add(rpm, MiscFunctions.kwToHP(pow));
                        else
                            power.Add(rpm, pow);

                        if (trek > Form.getCarData.EngineData.Maxtorque)
                        {
                            Form.getCarData.EngineData.Maxtorque = trek;
                            Form.getCarData.EngineData.Torquerpm = rpm;
                        }
                        if (pow > Form.getCarData.EngineData.Maxpower)
                        {
                            Form.getCarData.EngineData.Maxpower = pow;
                            Form.getCarData.EngineData.Powerrpm = rpm;
                        }
                    }
                }

                LineItem myCurve1 = myPane.AddCurve("Torque", torque, Color.Red);
                LineItem myCurve2 = myPane.AddCurve("Power", power, Color.Blue);

                myCurve1.Line.IsOptimizedDraw = true;
                myCurve2.Line.IsOptimizedDraw = true;
                myCurve2.IsY2Axis = true;
                myCurve2.YAxisIndex = 2;
                myPane.Y2Axis.IsVisible = true;
                myPane.Y2Axis.Scale.FontSpec.FontColor = Color.Blue;
                myPane.Y2Axis.Title.FontSpec.FontColor = Color.Blue;
                myPane.Y2Axis.MajorTic.IsOpposite = false;
                myPane.Y2Axis.MinorTic.IsOpposite = false;
                myPane.Y2Axis.Scale.Align = AlignP.Center;
                myPane.Chart.Fill = new Fill(Color.White, Color.FromArgb(255, 255, 166), 90F);
                myPane.Fill = new Fill(Color.FromArgb(250, 250, 255));
                if (init)
                    Form.getZedEngine.AxisChange();


                Form.getZedEngine.GraphPane.CurveList.AddRange(this.getEngineBars());
                Form.getZedEngine.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override void TireTempGraph()
        {
            int minTemp = 0;
            int maxTemp = 200;
            try
            {
                Form.getZedTireTemps.GraphPane.CurveList.Clear();
                GraphPane myPane1 = Form.getZedTireTemps.GraphPane;
                myPane1.Title.Text = "% grip at some temperatures";
                myPane1.XAxis.Title.Text = "Temperature in °C";
                if (Form.getMenuTempChecked)
                    myPane1.XAxis.Title.Text = "Temperature in °F";
                myPane1.YAxis.Title.Text = "% grip";
                List<int> selectedCompounds = new List<int>();
                for (int i = 0; i < Form.getCheckCompounds2.Items.Count; i++)
                {
                    if (Form.getCheckCompounds2.GetItemChecked(i) == true)
                        selectedCompounds.Add(i);
                }

                foreach (int i in selectedCompounds)
                {

                    PointPairList grip = new PointPairList();
                    string selected = Form.getCheckCompounds2.Items[i].ToString();
                    double belowOptimal = Double.Parse(MiscFunctions.getProperty(Form.getCarData.TyreFile, "GripTempPress", 1, selected));
                    double aboveOptimal = Double.Parse(MiscFunctions.getProperty(Form.getCarData.TyreFile, "GripTempPress", 2, selected));
                    double optimaltemp = Double.Parse(MiscFunctions.getProperty(Form.getCarData.TyreFile, "Temperatures", 1, selected));

                    for (int j = minTemp; j < optimaltemp; j++)
                    {
                        double newGrip = (1 - (0.5 * Math.Pow((belowOptimal / (optimaltemp + 273.15)) * (Math.Abs(j - optimaltemp)), 2))) * 100;
                        if (Form.getMenuTempChecked)
                            grip.Add(MiscFunctions.CelsiusToFahrenheit(j), Math.Round(newGrip, 2));
                        else
                            grip.Add(j, Math.Round(newGrip, 2));
                    }
                    for (int j = (int)optimaltemp; j <= maxTemp; j++)
                    {
                        double newGrip = (1 - (0.5 * Math.Pow((aboveOptimal / (optimaltemp + 273.15)) * (Math.Abs(j - optimaltemp)), 2))) * 100;
                        if (Form.getMenuTempChecked)
                            grip.Add(MiscFunctions.CelsiusToFahrenheit(j), Math.Round(newGrip, 2));
                        else
                            grip.Add(j, Math.Round(newGrip, 2));
                    }
                    LineItem myCurve;
                    if (i < colors.Length)
                        myCurve = myPane1.AddCurve(selected, grip, colors[i]);
                    else
                        myCurve = myPane1.AddCurve(selected, grip, Color.Black);
                    myCurve.Line.IsSmooth = true;
                    myCurve.Line.IsOptimizedDraw = true;
                    myCurve.Line.IsAntiAlias = true;
                    myCurve.Symbol.IsVisible = false;
                }
                //myPane1.XAxis.Type = AxisType.Text;
                //myPane1.XAxis.Scale.TextLabels = labels;
                if (Form.getMenuTempChecked)
                {
                    myPane1.XAxis.Scale.Min = MiscFunctions.CelsiusToFahrenheit(minTemp);
                    myPane1.XAxis.Scale.Max = MiscFunctions.CelsiusToFahrenheit(maxTemp);
                }
                else
                {
                    myPane1.XAxis.Scale.Min = minTemp;
                    myPane1.XAxis.Scale.Max = maxTemp;
                }
                myPane1.YAxis.Scale.Max = 101;
                myPane1.Chart.Fill = new Fill(Color.White, Color.FromArgb(255, 255, 166), 90F);
                myPane1.Fill = new Fill(Color.FromArgb(250, 250, 255));
                Form.getZedTireTemps.AxisChange();
                Form.getZedTireTemps.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override void TireThermalDegGraph1()
        {
            Form.getZedTireThermalDeg1.GraphPane.Title.Text = "Not Available!";
        }

        public override void TireThermalDegGraph2()
        {
            Form.getZedTireThermalDeg2.GraphPane.Title.Text = "Not Available!";
        }

        public override void TireWearGraphs()
        {
            try
            {
                Form.getZedTire.GraphPane.CurveList.Clear();
                Form.getZedTire2.GraphPane.CurveList.Clear();
                GraphPane myPane1 = Form.getZedTire.GraphPane;
                GraphPane myPane2 = Form.getZedTire2.GraphPane;
                myPane1.Title.Text = "% grip at a % of wear";
                myPane1.XAxis.Title.Text = "% wear";
                myPane1.YAxis.Title.Text = "% grip";
                myPane2.Title.Text = "Grip at a certain % of wear:";
                myPane2.XAxis.Title.Text = "% wear";
                myPane2.YAxis.Title.Text = "Grip";
                string[] labels = null;

                for (int i = 0; i < Form.getCheckCompounds3.Items.Count; i++)
                {
                    PointPairList grip = new PointPairList();
                    PointPairList wear = new PointPairList();
                    string selected = Form.getCheckCompounds3.Items[i].ToString();
                    bool gripfound = false;
                    foreach (string line in Form.getCarData.TyreFile)
                    {
                        if (line.Contains("WearGrip") && line.Contains("="))
                            gripfound = true;
                    }

                    if (gripfound && (MiscFunctions.getProperty(Form.getCarData.TyreFile, "WearGrip", 1, selected) == "n/a"))
                        labels = new string[] { "0", "6", "13", "19", "25", "31", "38", "44", "50", "56", "63", "69", "75", "81", "88", "94", "100" };
                    else if (gripfound)
                        labels = new string[] { "0", "13", "25", "38", "50", "63", "75", "88", "100" };
                    if (gripfound)
                    {
                        grip.Add(0, 100);
                        for (int j = 1; j < labels.Length; j++)
                        {

                            //Single WearGrip row:
                            if (labels.Length < 10)
                            {
                                grip.Add(Double.Parse(labels[j]), Math.Round(Double.Parse(MiscFunctions.getProperty(Form.getCarData.TyreFile, "WearGrip", j, selected)) * 100, 2));
                            }
                            //Multiple (2) WearGrip rows:
                            else if (j < 9)
                            {
                                grip.Add(Double.Parse(labels[j]), Math.Round(Double.Parse(MiscFunctions.getProperty(Form.getCarData.TyreFile, "WearGrip1", j, selected)) * 100, 2));
                            }
                            else
                            {
                                grip.Add(Double.Parse(labels[j]), Math.Round(Double.Parse(MiscFunctions.getProperty(Form.getCarData.TyreFile, "WearGrip2", j - 8, selected)) * 100, 2));
                            }

                            if (j == 1)
                            {
                                wear.Add(0, Math.Round(grip[0].Y * Double.Parse(MiscFunctions.getProperty(Form.getCarData.TyreFile, "DryLatLong", 1, selected)), 2));
                            }
                            wear.Add(Double.Parse(labels[j]), Math.Round(grip[grip.Count - 1].Y * Double.Parse(MiscFunctions.getProperty(Form.getCarData.TyreFile, "DryLatLong", 1, selected)), 2));
                        }
                        if (Form.getCheckCompounds3.GetItemChecked(i))
                        {
                            LineItem myCurve;
                            if (i < colors.Length)
                                myCurve = myPane1.AddCurve(selected, grip, colors[i]);
                            else
                                myCurve = myPane1.AddCurve(selected, grip, Color.Black);
                            myCurve.Line.IsSmooth = true;
                            myCurve.Line.IsOptimizedDraw = true;
                        }
                        if (Form.getCheckCompounds4.GetItemChecked(i))
                        {
                            LineItem myCurve;
                            if (i < colors.Length)
                                myCurve = myPane2.AddCurve(selected, wear, colors[i]);
                            else
                                myCurve = myPane2.AddCurve(selected, wear, Color.Black);
                            myCurve.Line.IsSmooth = true;
                            myCurve.Line.IsOptimizedDraw = true;
                        }
                    }
                    else
                    {
                        //MessageBox.Show("No tirewear/grip information found.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        break;
                    }
                }
                myPane1.XAxis.Type = AxisType.Text;
                myPane1.XAxis.Scale.TextLabels = labels;
                myPane1.Chart.Fill = new Fill(Color.White, Color.FromArgb(255, 255, 166), 90F);
                myPane1.Fill = new Fill(Color.FromArgb(250, 250, 255));
                myPane2.XAxis.Type = AxisType.Text;
                myPane2.XAxis.Scale.TextLabels = labels;
                myPane2.Chart.Fill = new Fill(Color.White, Color.FromArgb(255, 255, 166), 90F);
                myPane2.Fill = new Fill(Color.FromArgb(250, 250, 255));
                Form.getZedTire.AxisChange();
                Form.getZedTire.Refresh();
                Form.getZedTire2.AxisChange();
                Form.getZedTire2.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override void WearheatGraph()
        {
            try
            {
                Form.getZedWear.GraphPane.CurveList.Clear();
                GraphPane myPane = Form.getZedWear.GraphPane;

                // Set the Titles

                myPane.Title.Text = "WearRate, Heating and Grip of different compounds";
                myPane.XAxis.Title.Text = "";
                myPane.YAxis.Title.Text = "";

                string[] labels = { "WearRate", "Heating", "Grip" };

                double maxheat = 0;
                double maxwear = 0;
                double maxgrip = 0;
                for (int i = 0; i < Form.getCheckCompounds1.CheckedItems.Count; i++)
                {
                    double wear = Double.Parse(MiscFunctions.getProperty(Form.getCarData.TyreFile, "WearRate", 1, Form.getCheckCompounds1.CheckedItems[i].ToString()));
                    double heat = Double.Parse(MiscFunctions.getProperty(Form.getCarData.TyreFile, "Heating", 2, Form.getCheckCompounds1.CheckedItems[i].ToString()));
                    double grip = Double.Parse(MiscFunctions.getProperty(Form.getCarData.TyreFile, "DryLatLong", 1, Form.getCheckCompounds1.CheckedItems[i].ToString()));
                    if (heat > maxheat)
                        maxheat = heat;
                    if (wear > maxwear)
                        maxwear = wear;
                    if (grip > maxgrip)
                        maxgrip = grip;
                }
                double wearscale = maxgrip / maxwear;
                double heatscale = maxgrip / maxheat;
                for (int i = 0; i < Form.getCheckCompounds1.CheckedItems.Count; i++)
                {
                    PointPairList list = new PointPairList();
                    list.Add(i, Math.Round(wearscale * Double.Parse(MiscFunctions.getProperty(Form.getCarData.TyreFile, "WearRate", 1, Form.getCheckCompounds1.CheckedItems[i].ToString())), 2));
                    list.Add(i, Math.Round(heatscale * Double.Parse(MiscFunctions.getProperty(Form.getCarData.TyreFile, "Heating", 2, Form.getCheckCompounds1.CheckedItems[i].ToString())), 2));
                    list.Add(i, Math.Round(Double.Parse(MiscFunctions.getProperty(Form.getCarData.TyreFile, "DryLatLong", 1, Form.getCheckCompounds1.CheckedItems[i].ToString())), 2));
                    BarItem myBar = myPane.AddBar(Form.getCheckCompounds1.CheckedItems[i].ToString(), list, colors[i]);
                    myBar.Bar.Fill = new Fill(colors[i], Color.White, colors[i]);
                }
                myPane.BarSettings.MinClusterGap = 2;
                myPane.XAxis.MajorTic.IsBetweenLabels = true;
                myPane.XAxis.Scale.TextLabels = labels;
                myPane.XAxis.Type = AxisType.Text;

                // Fill the Axis and Pane backgrounds
                myPane.Chart.Fill = new Fill(Color.White, Color.FromArgb(255, 255, 166), 90F);
                myPane.Fill = new Fill(Color.FromArgb(250, 250, 255));

                Form.getZedWear.AxisChange();
                Form.getZedWear.Refresh();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal override List<LineItem> getEngineBars()
        {
            List<LineItem> bars = new List<LineItem>();

            PointPairList limiter = new PointPairList();
            double revlimit = Double.Parse(MiscFunctions.getProperty(Form.getCarData.EngineData.Values, "RevLimitRange", 1))
                * (Double.Parse(MiscFunctions.getProperty(Form.getCarData.EngineData.Values, "RevLimitRange", 2)) - 1)
                + Double.Parse(MiscFunctions.getProperty(Form.getCarData.EngineData.Values, "RevLimitRange", 0));
            limiter.Add(revlimit, -100000);
            limiter.Add(revlimit, 100000);

            bars.Add(new LineItem("Rev limit", limiter, Color.Black, SymbolType.None));

            return bars;
        }
    }
}
