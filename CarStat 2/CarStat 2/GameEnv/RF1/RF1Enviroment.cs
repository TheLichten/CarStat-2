﻿using Microsoft.Win32;
using Misc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarStat_2.GameEnv.RF1
{
    public class RF1Enviroment : GameEnvironmentFunc
    {
        public RF1Enviroment(CarStat2 carStat2) : this(carStat2, "RF1")
        {
            base.Reader = new RF1Reader(this);
        }

        protected RF1Enviroment(CarStat2 carStat2, string game) : base(game)
        {
            base.Data = new DataObjects.CarData();

            base.LabelsSetter = new RF1LabelsSetter(carStat2);
            base.Graphs = new RF1Graphs(carStat2);
            base.SetupCreator = new RF1SetupCreator(carStat2);
        }

        public override void FillWithDefaultValues()
        {
            Dictionary<string, Dictionary<string, string[]>> defaultHDV = DataObjects.DataReader.ListToMultiLevelDict(MiscFunctions.FileToList(new MemoryStream(Properties.Resources.skipbarber)));

            //Fill default values
            DataObjects.DataReader.addDefaultValues(Data.HDVehicleData.Values, defaultHDV);
        }

        public override string FindGameLocation()
        {
            try
            {
                RegistryKey MyReg = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\rFactor", false);
                string rfactorloc = "" + MyReg.GetValue("DataPath");
                if (!Directory.Exists(Path.Combine(rfactorloc, @"GameData\Vehicles")))
                {
                    throw new NullReferenceException();
                }

                return rfactorloc;
            }
            catch (NullReferenceException)
            {
                try
                {
                    using (var hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64))
                    using (var key = hklm.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Steam App 339790", false))
                    {
                        string rfactorloc = "" + key.GetValue("InstallLocation");

                        if (!Directory.Exists(Path.Combine(rfactorloc, @"GameData\Vehicles")))
                        {
                            throw new NullReferenceException();
                        }

                        return rfactorloc;
                    }
                }
                catch (NullReferenceException)
                {
                }
            }

            return "";
        }

        public override string GetSetupFolder()
        {
            throw new NotImplementedException();
        }

        public override string GetVehFolder()
        {
            return @"GameData\Vehicles\";
        }
    }
    
}
