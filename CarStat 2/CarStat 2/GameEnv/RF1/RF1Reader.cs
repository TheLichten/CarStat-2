﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarStat_2;
using CarStat_2.DataObjects;
using Misc;
using System.IO;

namespace CarStat_2.GameEnv.RF1
{
    public class RF1Reader : DataReader
    {
        public RF1Reader(GameEnvironmentFunc gameEnv) : base(gameEnv)
        {
        }

        internal override void readTireFile(bool OnlyTBC)
        {
            this.readTireFile(OnlyTBC, ".tbc");
        }

        protected void readTireFile(bool OnlyTBC, string fileEnding)
        {
            if (OnlyTBC == false)
            {
                string tyreFileName = MiscFunctions.getProperty(gameEnv.Data.HDVehicleData.Values, "general", "TireBrand", 0);
                if (Path.GetExtension(tyreFileName).ToLower() != fileEnding)
                    tyreFileName += fileEnding;

                string tireLoc = DirSearch(gameEnv.Data.VehicleRoot, tyreFileName);
                if (tireLoc == "encrypted")
                {
                    throw new FileNotFoundException("File Encryted");
                }
                gameEnv.Data.Tyrelocation = tireLoc;
                if (gameEnv.Data.Tyrelocation == null)
                    throw new Exception("Tyre file not found. Try another vehicle file.");
            }
            gameEnv.Data.TyreFile = MiscFunctions.FileToListChop(gameEnv.Data.Tyrelocation);

            gameEnv.Data.TireCompounds.Clear();
            for (int i = 0; i < gameEnv.Data.TyreFile.Count; i++) //Bad construction, historically grown. It works though, so /care
            {
                if (gameEnv.Data.TyreFile[i].Contains("[COMPOUND]"))
                {
                    while (gameEnv.Data.TyreFile[i].ToLower().Contains("name=") == false) //Skip to the Tire name
                        i++;
                    string compound = gameEnv.Data.TyreFile[i];
                    compound = compound.Substring(compound.IndexOf('=') + 1).Replace("\"", "");

                    TireCompoundData tire = new TireCompoundData();
                    tire.Compound = compound;

                    //Tire Data is read for one tire set
                    gameEnv.Data.TireCompounds.Add(tire.Compound, tire);


                    i++;
                    string line = gameEnv.Data.TyreFile[i].ToLower();

                    //Switching left right support on
                    if (line.Contains("left:") || line.Contains("right:"))
                        tire.FrontRear = false;
                    else
                        tire.FrontRear = true;
                }
            }
        }
    }
}
