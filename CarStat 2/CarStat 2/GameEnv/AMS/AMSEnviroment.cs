﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarStat_2.GameEnv.AMS
{
    public class AMSEnviroment : RF1.RF1Enviroment
    {
        public AMSEnviroment(CarStat2 carStat2) : base(carStat2, "AMS")
        {
            base.Reader = new RF1.RF1Reader(this);
        }

        public override string FindGameLocation()
        {
            try
            {
                RegistryKey MyReg = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Automobilista", false);
                string amsloc = "" + MyReg.GetValue("DataPath");
                if (!Directory.Exists(Path.Combine(amsloc, @"GameData\Vehicles")))
                {
                    throw new NullReferenceException();
                }

                return amsloc;
            }
            catch (NullReferenceException)
            {
                try
                {
                    using (var hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64))
                    using (var key = hklm.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Steam App 431600", false))
                    {
                        string amsloc = "" + key.GetValue("InstallLocation");

                        if (!Directory.Exists(Path.Combine(amsloc, @"GameData\Vehicles")))
                        {
                            throw new NullReferenceException();
                        }

                        return amsloc;
                    }
                }
                catch (NullReferenceException)
                {
                }
            }

            return "";
        }
    }
}
