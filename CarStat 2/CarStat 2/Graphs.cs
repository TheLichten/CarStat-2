﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZedGraph;
using System.Windows.Forms;
using System.Drawing;
using Misc;

namespace CarStat_2
{
    public abstract class Graphs
    {
        protected CarStat2 Form;
        protected Color[] colors = { Color.Red, Color.Blue, Color.Green, Color.Purple, Color.Orange, Color.Black, Color.Brown, Color.Cyan, Color.Gold, Color.Indigo, Color.Navy, Color.Olive, Color.Peru, Color.RoyalBlue, Color.Snow, Color.Yellow, Color.Azure, Color.Aquamarine, Color.Beige, Color.BurlyWood };
        

        public double heatscale { get; protected set; }
        public double wearscale { get; protected set; }
        public double gripscale { get; protected set; }

        public Graphs(CarStat2 Form)
        {
            this.Form = Form;
        }

        public abstract void BrakeTempsGraph(bool rearBrake);

        //Creates the Thermal degradation 1 graph
        public abstract void TireThermalDegGraph1();

        //Creates the Thermal degradation 2 graph
        public abstract void TireThermalDegGraph2();

        //Creates the Tire temp graph
        public abstract void TireTempGraph();

        //Creates the Tire wear Graphs
        public abstract void TireWearGraphs();

        //Creates the Wear/Heating/Grip graph (General Tire)
        public abstract void WearheatGraph();

        //Creates the Engine Graphs
        public abstract void EngineGraph(bool init);

        //Additional Bars in the engine graph, like revlimiter
        internal abstract List<LineItem> getEngineBars();
        
    }

    public class TireData
    {
        public double Heat { get; set; }
        public double Wear { get; set; }
        public double Grip { get; set; }
        public double Wetgrip { get; set; }
    }
}
