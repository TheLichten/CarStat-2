﻿using CarStat_2.DataObjects;
using CarStat_2.Setup_Manager;
using Misc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZedGraph;
using System.IO;

namespace CarStat_2
{
    public class SetupCreator
    {
        protected CarStat2 carStat;
        protected CarData data;

        protected Setup setup;

        public SetupCreator(CarStat2 carStat)
        {
            this.carStat = carStat;
        }

        public void setSetup(Setup setup)
        {
            this.setup = setup;
            this.refill();
            this.redrawGraphs(null, null);
        }

        public Setup getSetup()
        {
            return setup;
        }

        /// <summary>
        /// Sets up the SetupRows to display anything for this car
        /// </summary>
        public virtual void build()
        {
            data = carStat.gameEnv.Data;

            carStat.tbAeroSpeed.TextChanged += new EventHandler(redrawGraphs);

            //Fuel

            carStat.srFuel.setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, "General", "Fuel"));
            carStat.srFuel.setRealValuesUnit("L");
            carStat.srFuel.setValueFactory(new SetupRow.RoundingFactory(2));
            carStat.srPitStops.setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, "General", "NumPitstops"));

            carStat.srFuel.ValueChangeListener += new EventHandler(this.redrawGraphs);
            carStat.srPitStops.ValueChangeListener += new EventHandler(this.redrawGraphs);

            SetupRow[] pitStopFuel = { carStat.srPitStop1, carStat.srPitStop2, carStat.srPitStop3 };
            for (int i = 0; i < pitStopFuel.Length; i++)
            {
                pitStopFuel[i].setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, "General", "Pitstop" + (i + 1)));
                pitStopFuel[i].setRealValuesUnit("L");
                pitStopFuel[i].setValueFactory(new SetupRow.RoundingFactory(2));
                pitStopFuel[i].ValueChangeListener += new EventHandler(this.redrawGraphs);
            }


            //Driveline
            carStat.btnRescaleSpeedGraph.Click += new EventHandler(rescaleGearingGraph);

            carStat.srFinalDrive.ChangeEnabled = data.Gearing.finalDriveChange;
            carStat.srFinalDrive.setGear(data.Gearing.FinalDrive);
            carStat.srFinalDrive.ValueChangeListener += new EventHandler(this.redrawGraphs);

            GearSetupRow[] srGears = { carStat.srGear1, carStat.srGear2, carStat.srGear3, carStat.srGear4, carStat.srGear5, carStat.srGear6, carStat.srGear7, carStat.srGear8, carStat.srGear9 };
            System.Windows.Forms.Label[] gearLabels = { carStat.lblGear1, carStat.lblGear2, carStat.lblGear3, carStat.lblGear4, carStat.lblGear5, carStat.lblGear6, carStat.lblGear7, carStat.lblGear8, carStat.lblGear9 };

            for (int i = 0; i < srGears.Length && i < gearLabels.Length; i++)
            {
                srGears[i].ChangeEnabled = data.Gearing.gearChange;

                srGears[i].setGear(null);
                srGears[i].setFinalDrive(null);
                srGears[i].setNextGear(null);
                srGears[i].setPrevGear(null);
                srGears[i].Visible = false;

                gearLabels[i].Visible = false;
            }


            for (int i = 0; i < data.Gearing.ForwardGears.Length; i++)
            {
                srGears[i].setGear(data.Gearing.ForwardGears[i]);

                if (i > 0)
                {
                    srGears[i].setPrevGear(srGears[i - 1]);
                    srGears[i - 1].setNextGear(srGears[i]);
                }
                srGears[i].setFinalDrive(carStat.srFinalDrive);
                srGears[i].ValueChangeListener += new EventHandler(this.redrawGraphs);

                srGears[i].Visible = true;
                gearLabels[i].Visible = true;
            }

            if (data.Gearing.ReverseGear != null)
            {
                carStat.srGearR.Visible = true;
                carStat.lblGearR.Visible = true;

                carStat.srGearR.setGear(data.Gearing.ReverseGear);
                carStat.srGearR.setFinalDrive(carStat.srFinalDrive);
                carStat.srGearR.ValueChangeListener += new EventHandler(this.redrawGraphs);
            }
            else
            {
                carStat.srGearR.Visible = false;
                carStat.lblGearR.Visible = false;
            }


            SetupRow[] diffPercentRow = { carStat.srDiffPump, carStat.srDiffPower, carStat.srDiffCoast };
            string[] diffPercentLabel = { "Pump", "Power", "Coast" };
            for (int i = 0; i < diffPercentRow.Length; i++)
            {
                diffPercentRow[i].setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, "DRIVELINE", "Diff" + diffPercentLabel[i]));
                diffPercentRow[i].setRealValuesUnit("%");
                diffPercentRow[i].setValueFactory(new SetupRow.MultiFactory(1, 100));
                diffPercentRow[i].ValueChangeListener += new EventHandler(this.redrawGraphs);
            }

            carStat.srDiffPreload.setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, "DRIVELINE", "DiffPreload"));
            carStat.srDiffPreload.setRealValuesUnit("Nm");
            carStat.srDiffPreload.setValueFactory(new SetupRow.RoundingFactory(2));
            carStat.srDiffPreload.ValueChangeListener += new EventHandler(this.redrawGraphs);

            //Suspension:
            carStat.chkSymmetric.Checked = MiscFunctions.getProperty(data.HDVehicleData.Values, "General", "Symmetric", 0) != "0";
            carStat.chkSymmetric.CheckedChanged += new EventHandler(symmetricChanged);
            carStat.chkSymmetric01.CheckedChanged += new EventHandler(symmetricAssitChanged);
            symmetricChanged(null, null);

            for (int i = 0; i < 4; i++)
            {
                QuatorSuspension quator = new QuatorSuspension(i, carStat);
                quator.setCarSetupOption(data);
                quator.setValueChangeListner(this);
            }
            ThirdSpring frontThird = new ThirdSpring(true, carStat);
            frontThird.setCarSetupOption(data);
            frontThird.setValueChangeListner(this);
            ThirdSpring rearThird = new ThirdSpring(false, carStat);
            rearThird.setCarSetupOption(data);
            rearThird.setValueChangeListner(this);

            string[] frontRear = { "Front", "Rear" }; //Used to make the for work

            SetupRow[] arbs = { carStat.srFARB, carStat.srRARB };
            for (int i = 0; i < arbs.Length; i++)
            {
                arbs[i].setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, "SUSPENSION", frontRear[i] + "AntiSway"));

                if (MiscFunctions.getProperty(data.HDVehicleData.Values, "SUSPENSION", frontRear[i] + "AntiSwayParams", 0) == "1")
                {
                    arbs[i].setRealValuesUnit("N/mm");
                    arbs[i].setValueFactory(new SetupRow.MultiFactory(3, 0.001));
                    arbs[i].ValueChangeListener += new EventHandler(redrawGraphs);
                }
                else
                {
                    arbs[i].setRealValuesUnit("mm");
                    arbs[i].setValueFactory(new SetupRow.MultiFactory(3, 1000));
                    arbs[i].ValueChangeListener += new EventHandler(redrawGraphs);
                }

                if (MiscFunctions.getProperty(data.HDVehicleData.Values, "SUSPENSION", frontRear[i] + "AntiSwayParams", 1) == "1" && arbs[i].getCarSetupOption() != null && arbs[i].getCarSetupOption().AllOptions != null) //Detachable
                {
                    CarSetupOption.SetupValue value = arbs[i].getCarSetupOption().AllOptions[0];

                    value.Text = "";
                    value.Unit = "Detached";
                    value.Value = 0;

                    arbs[i].Detachable = true;
                }
                else
                {
                    arbs[i].Detachable = false;
                }
            }


            SetupRow[] toeIn = { carStat.srFToeIn, carStat.srRToeIn };
            for (int i = 0; i < toeIn.Length; i++)
            {
                toeIn[i].setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, "SUSPENSION", frontRear[i] + "ToeIn"));
                toeIn[i].setRealValuesUnit("°");
                toeIn[i].setValueFactory(new SetupRow.RoundingFactory(2));
                toeIn[i].ValueChangeListener += new EventHandler(redrawGraphs);
            }

            SetupRow[] toeOffset = { carStat.srFToeOffset, carStat.srRToeOffset };
            for (int i = 0; i < toeOffset.Length; i++)
            {
                toeOffset[i].setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, "SUSPENSION", frontRear[i] + "ToeOffset"));
                toeOffset[i].setRealValuesUnit("°");
                toeOffset[i].setValueFactory(new SetupRow.RoundingFactory(2));
                toeOffset[i].ValueChangeListener += new EventHandler(redrawGraphs);

                toeOffset[i].Visible = toeOffset[i].getCarSetupOption() != null;

                if (i == 0)
                    carStat.lblFToeOffset.Visible = toeOffset[i].getCarSetupOption() != null;
                else
                    carStat.lblRToeOffset.Visible = toeOffset[i].getCarSetupOption() != null;
            }


            string[] sidePrefix = { "Left", "Right" };
            SetupRow[] caster = { carStat.srFLCaster, carStat.srFRCaster };
            for (int i = 0; i < caster.Length; i++)
            {
                caster[i].setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, "SUSPENSION", sidePrefix[i] + "Caster"));
                caster[i].setRealValuesUnit("°");
                caster[i].setValueFactory(new SetupRow.RoundingFactory(2));
                caster[i].ValueChangeListener += new EventHandler(redrawGraphs);
            }

            SetupRow[] trackbar = { carStat.srRLTrackbar, carStat.srRRTrackbar };
            for (int i = 0; i < trackbar.Length; i++)
            {
                trackbar[i].setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, "SUSPENSION", sidePrefix[i] + "TrackBar"));
                trackbar[i].setRealValuesUnit("mm");
                trackbar[i].setValueFactory(new SetupRow.MultiFactory(2, 1000));
                trackbar[i].ValueChangeListener += new EventHandler(redrawGraphs);

                trackbar[i].Visible = trackbar[i].getCarSetupOption() != null;
            }
            carStat.lblSetupTrackbar.Visible = carStat.srRLTrackbar.getCarSetupOption() != null && carStat.srRRTrackbar.getCarSetupOption() != null;

            //Chassi adjustments:
            carStat.srWeightLateral.setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, "GENERAL", "CGRight"));
            carStat.srWeightLateral.setValueFactory(new SetupRow.MultiFactory(2, 100));
            carStat.srWeightLong.setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, "GENERAL", "CGRear"));
            carStat.srWeightLong.setValueFactory(new SetupRow.MultiFactory(2, 100));
            carStat.srWeightHeight.setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, "GENERAL", "CGHeight"));
            carStat.srWeightHeight.setValueFactory(new SetupRow.MultiFactory(2, 100));
            carStat.srWeightHeight.setRealValuesUnit("cm");
            carStat.srWedge.setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, "GENERAL", "Wedge"));
            carStat.srWedge.setValueFactory(new SetupRow.RoundingFactory(3));

            carStat.srWeightLateral.ValueChangeListener += new EventHandler(redrawGraphs);
            carStat.srWeightLong.ValueChangeListener += new EventHandler(redrawGraphs);
            carStat.srWeightHeight.ValueChangeListener += new EventHandler(redrawGraphs);
            carStat.srWedge.ValueChangeListener += new EventHandler(redrawGraphs);

            carStat.gbChassisAdj.Visible = false; //momentarely

            SetupRow[] springRubber = { carStat.srSpringRubberFL, carStat.srSpringRubberFR, carStat.srSpringRubberRL, carStat.srSpringRubberRR };
            string[] cornersSections = { "FRONTLEFT", "FRONTRIGHT", "REARLEFT", "REARRIGHT" };
            for (int i = 0; i < springRubber.Length; i++)
            {
                springRubber[i].setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, cornersSections[i], "SpringRubber"));
                springRubber[i].setValueFactory(new SetupRow.MultiFactory(3, 0.001));
                springRubber[i].setRealValuesUnit("N/mm");
                springRubber[i].ValueChangeListener += new EventHandler(redrawGraphs);

                if (!springRubber[i].Visible)
                    carStat.gbSpringRubber.Visible = false;
                else
                    carStat.gbSpringRubber.Visible = true;
            }

            //Wheels
            carStat.srSteeringLock.setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, "CONTROLS", "SteerLock"));
            carStat.srBrakePressure.setValueFactory(new SetupRow.RoundingFactory(2));
            carStat.srBrakePressure.setRealValuesUnit(" deg");
            carStat.srSteeringLock.ValueChangeListener += new EventHandler(redrawGraphs);

            carStat.srBrakePressure.setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, "CONTROLS", "BrakePressure"));
            carStat.srBrakePressure.setValueFactory(new SetupRow.MultiFactory(2, 100));
            carStat.srBrakePressure.setRealValuesUnit("%");
            carStat.srBrakePressure.ValueChangeListener += new EventHandler(redrawGraphs);

            carStat.srBrakeBais.setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, "CONTROLS", "RearBrake"));
            carStat.srBrakeBais.setValueFactory(new SetupRow.MultiFactory(2, 100));
            carStat.srBrakeBais.setRealValuesUnit("%");
            carStat.srBrakeBais.ValueChangeListener += new EventHandler(redrawGraphs);

            carStat.srHandBrakePressure.setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, "CONTROLS", "HandBrakePressure"));
            carStat.srHandBrakePressure.setValueFactory(new SetupRow.MultiFactory(2, 100));
            carStat.srHandBrakePressure.setRealValuesUnit("%");
            carStat.srHandBrakePressure.ValueChangeListener += new EventHandler(redrawGraphs);

            carStat.srHandBrakePressure.Visible = carStat.srHandBrakePressure.getCarSetupOption() != null && (carStat.srHandBrakePressure.getCarSetupOption().NumOfSettings != 1 || carStat.srHandBrakePressure.getCarSetupOption().AllOptions[0].Value != 0);
            carStat.lblHandBrakePressure.Visible = carStat.srHandBrakePressure.Visible;

            carStat.srTorqueSplit.setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, "DRIVELINE", "RearSplit"));
            carStat.srTorqueSplit.setRealValuesUnit("%");
            carStat.srTorqueSplit.setValueFactory(new SetupRow.MultiFactory(2, 100));
            carStat.srTorqueSplit.ValueChangeListener += new EventHandler(redrawGraphs);

            carStat.srTorqueSplit.Visible = MiscFunctions.getProperty(data.HDVehicleData.Values, "DRIVELINE", "WheelDrive", 0).ToLower() == "four";
            carStat.lblTorqueSplit.Visible = carStat.srTorqueSplit.Visible;

            SetupRow[] compounds = { carStat.srFCompound, carStat.srRCompound };
            string[] endsLabels = { "Front", "Rear" };

            for (int i = 0; i < compounds.Length; i++)
            {
                compounds[i].setCarSetupOption(MiscFunctions.getCompoundSetupOption(data.TyreFile));
                CarSetupOption option = compounds[i].getCarSetupOption();

                int compoundSetting;
                if (!int.TryParse(MiscFunctions.getProperty(data.HDVehicleData.Values, "GENERAL", endsLabels[i] + "TireCompoundSetting", 0), out compoundSetting))
                    compoundSetting = 0;

                option.DefaultSetting = compoundSetting;

                compounds[i].ValueChangeListener += new EventHandler(redrawGraphs);
            }

            string compoundRestriction = MiscFunctions.getProperty(data.HDVehicleData.Values, "PITMENU", "CompoundRestrictions", 0);
            if (compoundRestriction == "2" || compoundRestriction == "3")
            {
                carStat.srFCompound.setPartner(carStat.srRCompound);
                carStat.srRCompound.setPartner(carStat.srFCompound);
            }
            else
            {
                carStat.srFCompound.setPartner(null);
                carStat.srRCompound.setPartner(null);
            }

            //Aero
            carStat.srFWing.setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, "FRONTWING", "FW"));
            carStat.srFWing.setValueFactory(new SetupRow.RoundingFactory(2));
            carStat.srFWing.setRealValuesUnit("°");
            carStat.srFWing.ValueChangeListener += new EventHandler(redrawGraphs);

            carStat.srRWing.setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, "REARWING", "RW"));
            carStat.srRWing.setValueFactory(new SetupRow.RoundingFactory(2));
            carStat.srRWing.setRealValuesUnit("°");
            carStat.srRWing.ValueChangeListener += new EventHandler(redrawGraphs);

            carStat.srRadiator.setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, "BODYAERO", "Radiator"));
            carStat.srRadiator.setValueFactory(new SetupRow.MultiFactory(2, 100));
            carStat.srRadiator.setRealValuesUnit("%");
            carStat.srRadiator.ValueChangeListener += new EventHandler(redrawGraphs);

            carStat.srBrakeDucts.setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, "BODYAERO", "BrakeDuct"));
            carStat.srBrakeDucts.setValueFactory(new SetupRow.MultiFactory(2, 100));
            carStat.srBrakeDucts.setRealValuesUnit("%");
            carStat.srBrakeDucts.ValueChangeListener += new EventHandler(redrawGraphs);

            SetupRow[] fenders = { carStat.srLFender, carStat.srRFender };
            bool fenderFlaresConfigurable = false;
            for (int i = 0; i < fenders.Length; i++)
            {
                fenders[i].setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, sidePrefix[i] + "FENDER", "FenderFlare"));
                fenders[i].setValueFactory(new SetupRow.RoundingFactory(2));
                fenders[i].setRealValuesUnit("°");
                fenders[i].ValueChangeListener += new EventHandler(redrawGraphs);

                if (fenders[i].getCarSetupOption() != null && (fenders[i].getCarSetupOption().NumOfSettings != 1 || fenders[i].getCarSetupOption().AllOptions[0].Value != 0))
                    fenderFlaresConfigurable = true;
            }

            carStat.gbFender.Visible = fenderFlaresConfigurable;

            //Engine:
            carStat.srRevlimit.setCarSetupOption(MiscFunctions.getSetupOption(data.EngineData.Values, "RevLimit"));
            carStat.srRevlimit.ValueChangeListener += new EventHandler(redrawGraphs);

            carStat.srBoost.setCarSetupOption(MiscFunctions.getSetupOption(data.EngineData.Values, "EngineBoost"));
            carStat.srBoost.ValueChangeListener += new EventHandler(redrawGraphs);

            carStat.srEngineMixture.setCarSetupOption(MiscFunctions.getSetupOption(data.EngineData.Values, "EngineMixture"));
            carStat.srEngineMixture.ValueChangeListener += new EventHandler(redrawGraphs);

            carStat.srEngineBreaking.setCarSetupOption(MiscFunctions.getSetupOption(data.EngineData.Values, "EngineBrakingMap"));
            carStat.srEngineBreaking.ValueChangeListener += new EventHandler(redrawGraphs);
            carStat.srEngineBreaking.setRealValuesUnit("%");
            carStat.srEngineBreaking.setValueFactory(new SetupRow.MultiFactory(3, 100));
            if (carStat.srEngineBreaking.getCarSetupOption() != null && carStat.srEngineBreaking.getCarSetupOption().AllOptions != null && !carStat.srEngineBreaking.getCarSetupOption().hasSpecialText)
            { //Adjusting the Values
                foreach (CarSetupOption.SetupValue value in carStat.srEngineBreaking.getCarSetupOption().AllOptions)
                {
                    int num = 0;
                    if (Int32.TryParse(value.Text, out num))
                        value.Text = (num - 1) + "";
                }
            }


            //carStat.srTC.setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, "CONTROLS", "TC"));
            carStat.gbAssists.Visible = false;

            //Setting up real values
            setRealValues(carStat.chkRealValues.Checked);

            if (setup == null)
                setToDefault();

            redrawGraphs(null, null);
        }

        #region Read and Write Setup

        /// <summary>
        /// Fills in the Setup
        /// </summary>
        public virtual void refill()
        {
            if (setup == null)
            {
                setToDefault();
                return;
            }

            //The Notes
            string[] notes = new string[0];
            if (setup.SetupValues.ContainsKey("general") && setup.SetupValues["general"].ContainsKey("notes"))
                notes = setup.SetupValues["general"]["notes"];

            string noteText = "";
            foreach (string t in notes)
                noteText = noteText + t;

            //noteText = noteText.Substring(0, noteText.Length - "\n".Length);
            carStat.tbSetupNotes.Text = noteText;

            //General
            setSetupRow(carStat.srFuel, "General", "FuelSetting");
            setSetupRow(carStat.srPitStops, "General", "NumPitstopsSetting");

            SetupRow[] pitStopFuel = { carStat.srPitStop1, carStat.srPitStop2, carStat.srPitStop3 };
            for (int i = 0; i < pitStopFuel.Length; i++)
            {
                setSetupRow(pitStopFuel[i], "General", "Pitstop" + (i + 1) + "Setting");
            }

            //Driveline
            setSetupRow(carStat.srFinalDrive, "DRIVELINE", "FinalDriveSetting");

            GearSetupRow[] gears = { carStat.srGear1, carStat.srGear2, carStat.srGear3, carStat.srGear4, carStat.srGear5, carStat.srGear6, carStat.srGear7, carStat.srGear8, carStat.srGear9 };
            for (int i = 0; i < gears.Length; i++)
            {
                setSetupRow(gears[i], "DRIVELINE", "Gear" + (i + 1) + "Setting");
            }

            setSetupRow(carStat.srGearR, "DRIVELINE", "ReverseSetting");

            SetupRow[] diffRow = { carStat.srDiffPump, carStat.srDiffPower, carStat.srDiffCoast, carStat.srDiffPreload };
            string[] diffLabel = { "Pump", "Power", "Coast", "Preload" };
            for (int i = 0; i < diffRow.Length; i++)
            {
                setSetupRow(diffRow[i], "DRIVELINE", "Diff" + diffLabel[i] + "Setting");
            }

            //Suspension
            string value = MiscFunctions.getProperty(setup.SetupValues, "General", "Symmetric", 0);

            if (value != null)
                carStat.chkSymmetric.Checked = value != "0";
            else
                carStat.chkSymmetric.Checked = MiscFunctions.getProperty(data.HDVehicleData.Values, "General", "Symmetric", 0) != "0";


            for (int i = 0; i < 4; i++)
            {
                new QuatorSuspension(i, carStat).setSetup(setup);
            }

            new ThirdSpring(true, carStat).setSetup(setup);
            new ThirdSpring(false, carStat).setSetup(setup);

            setSetupRow(carStat.srFARB, "SUSPENSION", "FrontAntiSwaySetting");
            setSetupRow(carStat.srRARB, "SUSPENSION", "RearAntiSwaySetting");

            setSetupRow(carStat.srFToeIn, "SUSPENSION", "FrontToeInSetting");
            setSetupRow(carStat.srRToeIn, "SUSPENSION", "RearToeInSetting");

            setSetupRow(carStat.srFToeOffset, "SUSPENSION", "FrontToeOffsetSetting");
            setSetupRow(carStat.srRToeOffset, "SUSPENSION", "FrontToeOffsetSetting");

            setSetupRow(carStat.srFLCaster, "SUSPENSION", "LeftCasterSetting");
            setSetupRow(carStat.srFRCaster, "SUSPENSION", "RightCasterSetting");

            setSetupRow(carStat.srRLTrackbar, "SUSPENSION", "LeftTrackBarSetting");
            setSetupRow(carStat.srRRTrackbar, "SUSPENSION", "RightTrackBarSetting");

            //Chassis
            setSetupRow(carStat.srWeightLateral, "GENERAL", "CGRightSetting");
            setSetupRow(carStat.srWeightLong, "GENERAL", "CGRearSetting");
            setSetupRow(carStat.srWeightHeight, "GENERAL", "CGHeightSetting");
            setSetupRow(carStat.srWedge, "GENERAL", "WedgeSetting");

            SetupRow[] springRubber = { carStat.srSpringRubberFL, carStat.srSpringRubberFR, carStat.srSpringRubberRL, carStat.srSpringRubberRR };
            string[] cornersSections = { "FRONTLEFT", "FRONTRIGHT", "REARLEFT", "REARRIGHT" };
            for (int i = 0; i < springRubber.Length; i++)
            {
                setSetupRow(springRubber[i], cornersSections[i], "SpringRubberSetting");
            }

            //Wheels
            setSetupRow(carStat.srSteeringLock, "CONTROLS", "SteerLockSetting");
            setSetupRow(carStat.srBrakePressure, "CONTROLS", "BrakePressureSetting");
            setSetupRow(carStat.srBrakeBais, "CONTROLS", "RearBrakeSetting");
            setSetupRow(carStat.srHandBrakePressure, "CONTROLS", "HandbrakePressSetting");
            setSetupRow(carStat.srTorqueSplit, "DRIVELINE", "RearSplitSetting");

            setSetupRow(carStat.srFCompound, "GENERAL", "FrontTireCompoundSetting");
            setSetupRow(carStat.srRCompound, "GENERAL", "RearTireCompoundSetting");

            //Aero
            setSetupRow(carStat.srFWing, "FRONTWING", "FWSetting");
            setSetupRow(carStat.srRWing, "REARWING", "RWSetting");
            setSetupRow(carStat.srRadiator, "BODYAERO", "RadiatorSetting");
            setSetupRow(carStat.srBrakeDucts, "BODYAERO", "BrakeDuctSetting");
            setSetupRow(carStat.srLFender, "LEFTFENDER", "FenderFlareSetting");
            setSetupRow(carStat.srRFender, "RIGHTFENDER", "FenderFlareSetting");

            //Engine
            setSetupRow(carStat.srRevlimit, "ENGINE", "RevLimitSetting");
            setSetupRow(carStat.srBoost, "ENGINE", "EngineBoostSetting");
            setSetupRow(carStat.srEngineMixture, "ENGINE", "EngineMixtureSetting");
            setSetupRow(carStat.srEngineBreaking, "ENGINE", "EngineBrakingMapSetting");
        }

        public virtual void saveSetup(string fileloc)
        {

            StreamWriter writer = new StreamWriter(fileloc, false);
            writer.WriteLine("VehicleClassSetting=\"" + data.VEHicleData.Values["classes"][0].Replace(",", "") + "\"");
            writer.WriteLine("UpgradeSetting=" + "(0,0,0,0)"); //TODO: Upgrades
            writer.WriteLine("//VEH=" + (data.MASlocation != null ? Path.Combine(Path.GetDirectoryName(data.MASlocation), Path.GetFileName(data.VEHlocation)) : data.VEHlocation));

            //TODO further upgrades
            writer.WriteLine("//Note: settings commented out if using the default");
            writer.WriteLine("");

            //General:
            writer.WriteLine("[GENERAL]");
            writer.WriteLine("Notes=\"" + carStat.tbSetupNotes.Text.Replace("\n", "") + "\"");
            writer.WriteLine("Symmetric=" + (carStat.chkSymmetric.Checked ? "1" : "0"));
            writeSetupSetting(writer, "CGHeightSetting", carStat.srWeightHeight);
            writeSetupSetting(writer, "CGRightSetting", carStat.srWeightLateral);
            writeSetupSetting(writer, "CGRearSetting", carStat.srWeightLong);
            writeSetupSetting(writer, "WedgeSetting", carStat.srWedge);
            writeSetupSetting(writer, "FrontTireCompoundSetting", carStat.srFCompound);
            writeSetupSetting(writer, "RearTireCompoundSetting", carStat.srRCompound);
            writeSetupSetting(writer, "FuelSetting", carStat.srFuel);
            writeSetupSetting(writer, "NumPitstopsSetting", carStat.srPitStops);
            writeSetupSetting(writer, "Pitstop1Setting", carStat.srPitStop1);
            writeSetupSetting(writer, "Pitstop2Setting", carStat.srPitStop2);
            writeSetupSetting(writer, "Pitstop3Setting", carStat.srPitStop3);
            writer.WriteLine("");

            //Aero
            writer.WriteLine("[LEFTFENDER]");
            writeSetupSetting(writer, "FenderFlareSetting", carStat.srLFender);
            writer.WriteLine("");

            writer.WriteLine("[RIGHTFENDER]");
            writeSetupSetting(writer, "FenderFlareSetting", carStat.srRFender);
            writer.WriteLine("");

            writer.WriteLine("[FRONTWING]");
            writeSetupSetting(writer, "FWSetting", carStat.srFWing);
            writer.WriteLine("");

            writer.WriteLine("[REARWING]");
            writeSetupSetting(writer, "RWSetting", carStat.srRWing);
            writer.WriteLine("");

            writer.WriteLine("[BODYAERO]");
            writeSetupSetting(writer, "RadiatorSetting", carStat.srRadiator);
            writeSetupSetting(writer, "BrakeDuctSetting", carStat.srBrakeDucts);
            writer.WriteLine("");

            //Suspension
            writer.WriteLine("[SUSPENSION]");
            writer.WriteLine("//FrontWheelTrackSetting=0"); //TODO WheelTrackSetupRow
            writer.WriteLine("//RearWheelTrackSetting=0");
            writeSetupSetting(writer, "FrontAntiSwaySetting", carStat.srFARB);
            writeSetupSetting(writer, "RearAntiSwaySetting", carStat.srRARB);
            writeSetupSetting(writer, "FrontToeInSetting", carStat.srFToeIn);
            writeSetupSetting(writer, "FrontToeOffsetSetting", carStat.srFToeOffset);
            writeSetupSetting(writer, "RearToeInSetting", carStat.srRToeIn);
            writeSetupSetting(writer, "RearToeOffsetSetting", carStat.srRToeOffset);
            writeSetupSetting(writer, "LeftCasterSetting", carStat.srFLCaster);
            writeSetupSetting(writer, "RightCasterSetting", carStat.srFRCaster);
            writeSetupSetting(writer, "LeftTrackBarSetting", carStat.srRLTrackbar);
            writeSetupSetting(writer, "RightTrackBarSetting", carStat.srRRTrackbar);

            new ThirdSpring(true, carStat).writeSetup(writer);
            new ThirdSpring(false, carStat).writeSetup(writer);

            //TODO: Chassis Adjustment
            for (int i = 0; i < 12; i++)
            {
                if (i < 10)
                    writer.WriteLine("//ChassisAdj0" + i + "Setting = //1");
                else
                    writer.WriteLine("//ChassisAdj" + i + "Setting = //1");
            }
            writer.WriteLine("");

            //Controls
            writer.WriteLine("[CONTROLS]");
            writeSetupSetting(writer, "SteerLockSetting", carStat.srSteeringLock);
            writeSetupSetting(writer, "RearBrakeSetting", carStat.srBrakeBais);
            writeSetupSetting(writer, "BrakePressureSetting", carStat.srBrakePressure);
            writer.WriteLine("//HandfrontbrakePressSetting=0//0%");
            writeSetupSetting(writer, "HandbrakePressSetting", carStat.srHandBrakePressure);
            writer.WriteLine("//TCSetting=0//1");
            writer.WriteLine("//ABSSetting=0//1");
            writer.WriteLine("");

            //Engine
            writer.WriteLine("[ENGINE]");
            writeSetupSetting(writer, "RevLimitSetting", carStat.srRevlimit);
            writeSetupSetting(writer, "EngineBoostSetting", carStat.srBoost);
            writeSetupSetting(writer, "EngineMixtureSetting", carStat.srEngineMixture);
            writeSetupSetting(writer, "EngineBrakingMapSetting", carStat.srEngineBreaking);
            writer.WriteLine("");

            //DRIVELINE
            writer.WriteLine("[DRIVELINE]");
            writeSetupSetting(writer, "FinalDriveSetting", carStat.srFinalDrive);
            writeSetupSetting(writer, "ReverseSetting", carStat.srGearR);

            GearSetupRow[] gears = { carStat.srGear1, carStat.srGear2, carStat.srGear3, carStat.srGear4, carStat.srGear5, carStat.srGear6, carStat.srGear7, carStat.srGear8, carStat.srGear9 };

            for (int i = 0; i < gears.Length && i < data.Gearing.ForwardGears.Length; i++)
            {
                writeSetupSetting(writer, "Gear" + (i+1) + "Setting", gears[i]);
            }

            writer.WriteLine("//RatioSetSetting=0//N/A");
            writeSetupSetting(writer, "DiffPumpSetting", carStat.srDiffPump);
            writeSetupSetting(writer, "DiffPowerSetting", carStat.srDiffPower);
            writeSetupSetting(writer, "DiffCoastSetting", carStat.srDiffPower);
            writeSetupSetting(writer, "DiffPreloadSetting", carStat.srDiffPower);
            writeSetupSetting(writer, "RearSplitSetting", carStat.srTorqueSplit);
            writer.WriteLine("");

            //Quator Suspensions
            SetupRow[] springRubber = { carStat.srSpringRubberFL, carStat.srSpringRubberFR, carStat.srSpringRubberRL, carStat.srSpringRubberRR };
            for (int i = 0; i < 4 && i < springRubber.Length; i++)
            {
                new QuatorSuspension(i, carStat).writeSetup(writer, springRubber[i]);
            }

            //Final
            writer.WriteLine("[BASIC]");
            writer.WriteLine("Downforce=0.500000");
            writer.WriteLine("Balance=0.500000");
            writer.WriteLine("Ride=0.500000");
            writer.WriteLine("Gearing=0.500000");
            writer.WriteLine("Custom=1");
            writer.WriteLine("");

            //Closing it up
            writer.Flush();
            writer.Close();
            writer.Dispose();

            //Setting up setup
            setup = SetupDatabase.createSetup(fileloc);
        }

#endregion

        #region Default and Real Values

        public void setToDefault()
        {
            setup = null;

            if (MiscFunctions.getProperty(data.HDVehicleData.Values, "General", "Notes", 0) != null)
                carStat.tbSetupNotes.Text = MiscFunctions.getProperty(data.HDVehicleData.Values, "General", "Notes", 0);

            //The Notes
            string[] notes = new string[0];
            if (data.HDVehicleData.Values.ContainsKey("general") && data.HDVehicleData.Values["general"].ContainsKey("notes"))
                notes = data.HDVehicleData.Values["general"]["notes"];

            string noteText = "";
            foreach (string t in notes)
                noteText = noteText + t;

            //noteText = noteText.Substring(0, noteText.Length - "\n".Length);
            carStat.tbSetupNotes.Text = noteText;


            SetupRow[] allSetupRows = { carStat.srFuel, carStat.srPitStops, carStat.srPitStop1, carStat.srPitStop2, carStat.srPitStop3,
                carStat.srGear1, carStat.srGear2, carStat.srGear3, carStat.srGear4, carStat.srGear5, carStat.srGear6, carStat.srGear7, carStat.srGear8, carStat.srGear9,
                carStat.srDiffPump, carStat.srDiffPower, carStat.srDiffCoast, carStat.srDiffPreload,  carStat.srFinalDrive, carStat.srGearR,
                carStat.srFARB, carStat.srFToeIn, carStat.srFToeOffset, carStat.srFLCaster, carStat.srFRCaster, carStat.srRARB, carStat.srRToeIn, carStat.srRToeOffset, carStat.srRLTrackbar, carStat.srRRTrackbar,
                carStat.srWeightLateral, carStat.srWeightLong, carStat.srWeightHeight, carStat.srWedge, carStat.srSpringRubberFL, carStat.srSpringRubberFR, carStat.srSpringRubberRL, carStat.srSpringRubberRR,
                carStat.srSteeringLock, carStat.srBrakePressure, carStat.srBrakeBais, carStat.srHandBrakePressure, carStat.srTorqueSplit, carStat.srFCompound, carStat.srRCompound,
                carStat.srFWing, carStat.srRWing, carStat.srRadiator, carStat.srBrakeDucts, carStat.srLFender, carStat.srRFender,
                carStat.srRevlimit, carStat.srBoost, carStat.srEngineMixture, carStat.srEngineBreaking, carStat.srTC, carStat.srABS
            };

            foreach (SetupRow row in allSetupRows)
                row.goToDefault();

            //Suspension
            carStat.chkSymmetric.Checked = MiscFunctions.getProperty(data.HDVehicleData.Values, "General", "Symmetric", 0) != "0";

            for (int i = 0; i < 4; i++)
            {
                new QuatorSuspension(i, carStat).goToDefault();
            }
            new ThirdSpring(true, carStat).goToDefault();
            new ThirdSpring(false, carStat).goToDefault();


            redrawGraphs(null, null);
        }

        /// <summary>
        /// Sets wheter real values or the text/setting numbers are displayed in the SetupRows
        /// </summary>
        /// <param name="real"></param>
        public void setRealValues(bool real)
        {
            //General
            carStat.srFuel.setDisplayRealValues(!carStat.srFuel.getCarSetupOption().hasSpecialText || real);

            carStat.srPitStops.setDisplayRealValues(true);

            SetupRow[] pitStopFuel = { carStat.srPitStop1, carStat.srPitStop2, carStat.srPitStop3 };
            for (int i = 0; i < pitStopFuel.Length; i++)
            {
                if (pitStopFuel[i].getCarSetupOption() != null)
                    pitStopFuel[i].setDisplayRealValues(!pitStopFuel[i].getCarSetupOption().hasSpecialText || real);
                else
                    pitStopFuel[i].setDisplayRealValues(real);
            }

            //Driveline
            SetupRow[] diffPercentRow = { carStat.srDiffPump, carStat.srDiffPower, carStat.srDiffCoast };
            for (int i = 0; i < diffPercentRow.Length; i++)
            {
                if (diffPercentRow[i].getCarSetupOption() != null)
                    diffPercentRow[i].setDisplayRealValues(!diffPercentRow[i].getCarSetupOption().hasSpecialText || real);
                else
                    diffPercentRow[i].setDisplayRealValues(real);
            }

            carStat.srDiffPreload.setDisplayRealValues(real);

            //Suspension
            for (int i = 0; i < 4; i++)
            {
                new QuatorSuspension(i, carStat).setRealValues(real, data);
            }

            new ThirdSpring(true, carStat).setRealValues(real, data);
            new ThirdSpring(false, carStat).setRealValues(real, data);

            carStat.srFARB.setDisplayRealValues(!carStat.srFARB.getCarSetupOption().hasSpecialText || real);
            carStat.srRARB.setDisplayRealValues(!carStat.srRARB.getCarSetupOption().hasSpecialText || real);

            carStat.srFToeIn.setDisplayRealValues(!carStat.srFToeIn.getCarSetupOption().hasSpecialText || real);
            carStat.srRToeIn.setDisplayRealValues(!carStat.srRToeIn.getCarSetupOption().hasSpecialText || real);

            if (carStat.srFToeOffset.getCarSetupOption() != null)
                carStat.srFToeOffset.setDisplayRealValues(!carStat.srFToeOffset.getCarSetupOption().hasSpecialText || real);
            else
                carStat.srFToeOffset.setDisplayRealValues(real);

            if (carStat.srRToeOffset.getCarSetupOption() != null)
                carStat.srRToeOffset.setDisplayRealValues(!carStat.srRToeOffset.getCarSetupOption().hasSpecialText || real);
            else
                carStat.srRToeOffset.setDisplayRealValues(real);

            carStat.srFLCaster.setDisplayRealValues(!carStat.srFLCaster.getCarSetupOption().hasSpecialText || real);
            carStat.srFRCaster.setDisplayRealValues(!carStat.srFRCaster.getCarSetupOption().hasSpecialText || real);

            if (carStat.srRLTrackbar.getCarSetupOption() != null)
                carStat.srRLTrackbar.setDisplayRealValues(!carStat.srRLTrackbar.getCarSetupOption().hasSpecialText || real);
            else
                carStat.srRLTrackbar.setDisplayRealValues(real);

            if (carStat.srRRTrackbar.getCarSetupOption() != null)
                carStat.srRRTrackbar.setDisplayRealValues(!carStat.srRRTrackbar.getCarSetupOption().hasSpecialText || real);
            else
                carStat.srRRTrackbar.setDisplayRealValues(real);

            //Chassis
            if (carStat.srWeightLateral.getCarSetupOption() != null)
                carStat.srWeightLateral.setDisplayRealValues(!carStat.srWeightLateral.getCarSetupOption().hasSpecialText || real);
            else
                carStat.srWeightLateral.setDisplayRealValues(real);

            if (carStat.srWeightLong.getCarSetupOption() != null)
                carStat.srWeightLong.setDisplayRealValues(!carStat.srWeightLong.getCarSetupOption().hasSpecialText || real);
            else
                carStat.srWeightLong.setDisplayRealValues(real);

            if (carStat.srWeightHeight.getCarSetupOption() != null)
                carStat.srWeightHeight.setDisplayRealValues(!carStat.srWeightHeight.getCarSetupOption().hasSpecialText || real);
            else
                carStat.srWeightHeight.setDisplayRealValues(real);

            if (carStat.srWedge.getCarSetupOption() != null)
                carStat.srWedge.setDisplayRealValues(!carStat.srWedge.getCarSetupOption().hasSpecialText || real);
            else
                carStat.srWedge.setDisplayRealValues(real);

            SetupRow[] springRubber = { carStat.srSpringRubberFL, carStat.srSpringRubberFR, carStat.srSpringRubberRL, carStat.srSpringRubberRR };
            for (int i = 0; i < springRubber.Length; i++)
            {
                if (springRubber[i].getCarSetupOption() != null)
                    springRubber[i].setDisplayRealValues(!springRubber[i].getCarSetupOption().hasSpecialText || real);
                else
                    springRubber[i].setDisplayRealValues(real);
            }

            //Wheels
            carStat.srSteeringLock.setDisplayRealValues(carStat.srSteeringLock.getCarSetupOption() != null && !carStat.srSteeringLock.getCarSetupOption().hasSpecialText); //at the moment, as reading SteeringFraction out of the special values is overly complicated

            carStat.srBrakePressure.setDisplayRealValues((carStat.srBrakePressure.getCarSetupOption() != null && !carStat.srBrakePressure.getCarSetupOption().hasSpecialText) || real);
            carStat.srBrakeBais.setDisplayRealValues((carStat.srBrakeBais.getCarSetupOption() != null && !carStat.srBrakeBais.getCarSetupOption().hasSpecialText) || real);
            carStat.srHandBrakePressure.setDisplayRealValues((carStat.srHandBrakePressure.getCarSetupOption() != null && !carStat.srHandBrakePressure.getCarSetupOption().hasSpecialText) || real);
            carStat.srTorqueSplit.setDisplayRealValues((carStat.srTorqueSplit.getCarSetupOption() != null && !carStat.srTorqueSplit.getCarSetupOption().hasSpecialText) || real);

            carStat.srFCompound.setDisplayRealValues(false);
            carStat.srRCompound.setDisplayRealValues(false);

            //Aero
            carStat.srFWing.setDisplayRealValues(real);
            carStat.srRWing.setDisplayRealValues(real);
            carStat.srRadiator.setDisplayRealValues(real);
            carStat.srBrakeDucts.setDisplayRealValues(real);
            carStat.srLFender.setDisplayRealValues(real);
            carStat.srRFender.setDisplayRealValues(real);

            //Engine
            carStat.srRevlimit.setDisplayRealValues(!(carStat.srRevlimit.getCarSetupOption() != null && carStat.srRevlimit.getCarSetupOption().hasSpecialText && !real));
            carStat.srBoost.setDisplayRealValues(false);
            carStat.srEngineMixture.setDisplayRealValues(real);
            carStat.srEngineBreaking.setDisplayRealValues(real);
        }

#endregion

        #region Eventlisteners

        /// <summary>
        /// Linked to the value change listeners, updates the graphs and certain setuprows
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void redrawGraphs(object sender, EventArgs e)
        {
            //Disabling Fuel Stop Option if necessary
            SetupRow[] pitStopFuel = { carStat.srPitStop1, carStat.srPitStop2, carStat.srPitStop3 };
            System.Windows.Forms.Label[] pitStopFuelLbl = { carStat.lblPitStop1, carStat.lblPitStop2, carStat.lblPitStop3 };
            for (int i = 0; i < pitStopFuel.Length; i++)
            {
                pitStopFuelLbl[i].Visible = carStat.srPitStops.getValue() >= (i + 1) && pitStopFuel[i].getCarSetupOption() != null;
                pitStopFuel[i].Visible = carStat.srPitStops.getValue() >= (i + 1) && pitStopFuel[i].getCarSetupOption() != null;
            }

            //Updating the Gear Ratios, because maybe the FinalDrive ratio changed
            GearSetupRow[] gears = { carStat.srGear1, carStat.srGear2, carStat.srGear3, carStat.srGear4, carStat.srGear5, carStat.srGear6, carStat.srGear7, carStat.srGear8, carStat.srGear9 };
            for (int i = 0; i < gears.Length; i++)
            {
                gears[i].writeValue();
            }
            carStat.srGearR.writeValue();

            //Gearing RPM to Speed Graph
            GraphPane gearingPane = carStat.zedGearboxGraph.GraphPane;
            gearingPane.Title.Text = "RPM/Speed";
            gearingPane.XAxis.Title.Text = "Speed (kmph)";
            gearingPane.YAxis.Title.Text = "RPM";

            gearingPane.CurveList.Clear();

            double revlimit = carStat.srRevlimit.getValue();
            double upshiftAlgo;
            if (!Double.TryParse(MiscFunctions.getProperty(data.HDVehicleData.Values, "CONTROLS", "UpshiftAlgorithm", 0), out upshiftAlgo))
                upshiftAlgo = 1;

            double shiftRPM = revlimit * upshiftAlgo;

            double tireCircumfrence = 0;
            string compoundName = data.TireCompounds.First().Key; //TODO tire compound setup row
            if (MiscFunctions.getProperty(data.HDVehicleData.Values, "DRIVELINE", "WheelDrive", 0).ToLower() == "front")
                tireCircumfrence = Double.Parse(MiscFunctions.getProperty(data.TyreFile, "Radius", 1, false, compoundName));
            else
                tireCircumfrence = Double.Parse(MiscFunctions.getProperty(data.TyreFile, "Radius", 1, true, compoundName));

            tireCircumfrence = tireCircumfrence * 2 * Math.PI;

            PointPairList rpmList = new PointPairList();

            rpmList.Add(0, 0);

            for (int i = 0; i < gears.Length; i++)
            {
                double ratio = gears[i].getFactoryValue();
                if (ratio == -1)
                    break;

                double speed = ((shiftRPM / ratio) * 60.0 * tireCircumfrence) / 1000.0; //wheelrpm, then making it wheelrph, making it meters per hour, then kmph

                rpmList.Add(Math.Round(speed, 2), Math.Round(shiftRPM, 2));

                if (i + 1 < gears.Length && gears[i + 1].getFactoryValue() != -1)
                {
                    double upperRatio = gears[i + 1].getFactoryValue();

                    double rpmPostShift = ((speed * 1000.0) / tireCircumfrence / 60.0) * upperRatio; //meters per hour, then making it wheelrph, then wheelrpm, lastly engine rpm
                    rpmList.Add(Math.Round(speed, 2), Math.Round(rpmPostShift, 2));
                }
                else
                {
                    carStat.lblTopSpeed.Text = Math.Round(speed, 2) + " kmph";
                }
            }

            LineItem rpmCurve = new LineItem("RPM", rpmList, System.Drawing.Color.Red, SymbolType.Default);

            gearingPane.CurveList.Add(rpmCurve);

            rpmCurve.Line.IsOptimizedDraw = true;

            gearingPane.YAxis.Scale.FontSpec.FontColor = System.Drawing.Color.Red;
            gearingPane.YAxis.Title.FontSpec.FontColor = System.Drawing.Color.Red;

            gearingPane.Chart.Fill = new Fill(System.Drawing.Color.White, System.Drawing.Color.FromArgb(255, 255, 166), 90F);

            carStat.zedGearboxGraph.AxisChange();
            gearingPane.Fill.Color = System.Drawing.SystemColors.ButtonFace;
            gearingPane.Border.IsVisible = false;
            carStat.zedGearboxGraph.MasterPane.Border.IsVisible = false;
            carStat.zedGearboxGraph.Refresh();

            //Aero:
            double aeroConstant = 1.584 * 0.5; //rf2 Aero constant and the 1/2 to make the aero calculation work
            double aeroSpeed = 0;

            double frontLiftFactor = 0;
            double rearLiftFactor = 0;
            double dragFactor = 0;

            if (!Double.TryParse(carStat.tbAeroSpeed.Text, out aeroSpeed))
                aeroSpeed = 0;

            //Making km/h to m/s
            aeroSpeed = aeroSpeed / 3.6;

            //Lift

            double frontWingFactor = 0;
            if (MiscFunctions.getProperty(data.HDVehicleData.Values, "FRONTWING", "FWLiftParams", 0) != null)
                frontWingFactor = Double.Parse(MiscFunctions.getProperty(data.HDVehicleData.Values, "FRONTWING", "FWLiftParams", 0)) + carStat.srFWing.getValue() * Double.Parse(MiscFunctions.getProperty(data.HDVehicleData.Values, "FRONTWING", "FWLiftParams", 1));


            double rearWingFactor = 0;
            if (MiscFunctions.getProperty(data.HDVehicleData.Values, "REARWING", "RWLiftParams", 0) != null)
                rearWingFactor = Double.Parse(MiscFunctions.getProperty(data.HDVehicleData.Values, "REARWING", "RWLiftParams", 0)) + carStat.srRWing.getValue() * Double.Parse(MiscFunctions.getProperty(data.HDVehicleData.Values, "REARWING", "RWLiftParams", 1));

            double diffuserFactor = 0;

            if(!Double.TryParse(MiscFunctions.getProperty(data.HDVehicleData.Values, "DIFFUSER", "DiffuserBasePlus", 0), out diffuserFactor))
                diffuserFactor = 0;

            frontLiftFactor = frontWingFactor * (1 - getRearForceDistribution("FRONTWING", "FW")) + rearWingFactor * (1 - getRearForceDistribution("REARWING", "RW")) + diffuserFactor * (1 - getRearForceDistribution("DIFFUSER", "Diffuser"));
            rearLiftFactor = frontWingFactor * getRearForceDistribution("FRONTWING", "FW") + rearWingFactor * getRearForceDistribution("REARWING", "RW") + diffuserFactor * getRearForceDistribution("DIFFUSER", "Diffuser");

            //Drag

            if (MiscFunctions.getProperty(data.HDVehicleData.Values, "FRONTWING", "FWDragParams", 0) != null)
                dragFactor += Double.Parse(MiscFunctions.getProperty(data.HDVehicleData.Values, "FRONTWING", "FWDragParams", 0)) + carStat.srFWing.getValue() * Double.Parse(MiscFunctions.getProperty(data.HDVehicleData.Values, "FRONTWING", "FWDragParams", 1));
            if (MiscFunctions.getProperty(data.HDVehicleData.Values, "REARWING", "RWDragParams", 0) != null)
                dragFactor += Double.Parse(MiscFunctions.getProperty(data.HDVehicleData.Values, "REARWING", "RWDragParams", 0)) + carStat.srRWing.getValue() * Double.Parse(MiscFunctions.getProperty(data.HDVehicleData.Values, "REARWING", "RWDragParams", 1));

            //Ducts

            double brakeDuctFactor = 0;
            if (Double.TryParse(MiscFunctions.getProperty(data.HDVehicleData.Values, "BODYAERO", "BrakeDuctLift", 0), out brakeDuctFactor))
                brakeDuctFactor = brakeDuctFactor * carStat.srBrakeDucts.getValue();
            else
                brakeDuctFactor = 0;

            double radiatorFactor = Double.Parse(MiscFunctions.getProperty(data.HDVehicleData.Values, "BODYAERO", "RadiatorLift", 0)) * carStat.srRadiator.getValue();
            double bodyRearSplit = getRearForceDistribution("BODYAERO", "BodyAero");
            if (bodyRearSplit == -1)
                bodyRearSplit = getRearForceDistribution("BODYAERO", "Body");

            frontLiftFactor += brakeDuctFactor * (1 - bodyRearSplit) + radiatorFactor * (1 - bodyRearSplit);
            rearLiftFactor += brakeDuctFactor * bodyRearSplit + radiatorFactor * bodyRearSplit;
            dragFactor += Double.Parse(MiscFunctions.getProperty(data.HDVehicleData.Values, "BODYAERO", "RadiatorDrag", 0)) * carStat.srRadiator.getValue()
                + Double.Parse(MiscFunctions.getProperty(data.HDVehicleData.Values, "BODYAERO", "BodyDragBase", 0));

            double brakeDuctDrag = 0;
            if (Double.TryParse(MiscFunctions.getProperty(data.HDVehicleData.Values, "BODYAERO", "BrakeDuctDrag", 0), out brakeDuctDrag))
                dragFactor += brakeDuctDrag * carStat.srBrakeDucts.getValue();

            //Fenders

            if (data.HDVehicleData.Values.ContainsKey("LEFTFENDER".ToLower()) && MiscFunctions.getProperty(data.HDVehicleData.Values, "LEFTFENDER", "FenderLiftParams", 0) != null)
            {
                double fenderFactor = Double.Parse(MiscFunctions.getProperty(data.HDVehicleData.Values, "LEFTFENDER", "FenderLiftParams", 0)) + Double.Parse(MiscFunctions.getProperty(data.HDVehicleData.Values, "LEFTFENDER", "FenderLiftParams", 1)) *  (carStat.srLFender.getValue() != -1 ? carStat.srLFender.getValue() : 0);

                frontLiftFactor += fenderFactor * (1 - getRearForceDistribution("LEFTFENDER", "Fender"));
                rearLiftFactor += fenderFactor * getRearForceDistribution("LEFTFENDER", "Fender");

                if (MiscFunctions.getProperty(data.HDVehicleData.Values, "LEFTFENDER", "FenderDragParams", 0) != null)
                    dragFactor += Double.Parse(MiscFunctions.getProperty(data.HDVehicleData.Values, "LEFTFENDER", "FenderDragParams", 0)) + Double.Parse(MiscFunctions.getProperty(data.HDVehicleData.Values, "LEFTFENDER", "FenderDragParams", 1)) * (carStat.srLFender.getValue() != -1 ? carStat.srLFender.getValue() : 0);
            }

            if (data.HDVehicleData.Values.ContainsKey("RIGHTFENDER".ToLower()) && MiscFunctions.getProperty(data.HDVehicleData.Values, "RIGHTFENDER", "FenderLiftParams", 0) != null)
            {
                double fenderFactor = Double.Parse(MiscFunctions.getProperty(data.HDVehicleData.Values, "RIGHTFENDER", "FenderLiftParams", 0)) + Double.Parse(MiscFunctions.getProperty(data.HDVehicleData.Values, "RIGHTFENDER", "FenderLiftParams", 1)) * (carStat.srRFender.getValue() != -1 ? carStat.srRFender.getValue() : 0);

                frontLiftFactor += fenderFactor * (1 - getRearForceDistribution("RIGHTFENDER", "Fender"));
                rearLiftFactor += fenderFactor * getRearForceDistribution("RIGHTFENDER", "Fender");

                if (MiscFunctions.getProperty(data.HDVehicleData.Values, "RIGHTFENDER", "FenderDragParams", 0) != null)
                    dragFactor += Double.Parse(MiscFunctions.getProperty(data.HDVehicleData.Values, "RIGHTFENDER", "FenderDragParams", 0)) + Double.Parse(MiscFunctions.getProperty(data.HDVehicleData.Values, "RIGHTFENDER", "FenderDragParams", 1)) * (carStat.srRFender.getValue() != -1 ? carStat.srRFender.getValue() : 0);
            }

            //Write

            carStat.setLabelText("lblFrontLift", Math.Round(frontLiftFactor*(-1), 3) + "");
            carStat.setLabelText("lblRearLift", Math.Round(rearLiftFactor * (-1), 3) + "");
            carStat.setLabelText("lblAeroDragFactor", Math.Round(dragFactor, 3) + "");
            
            carStat.setLabelText("lblFrontDownforce", Math.Round((-1) * frontLiftFactor * aeroConstant * Math.Pow(aeroSpeed, 2), 0) + "N");
            carStat.setLabelText("lblRearDownforce", Math.Round((-1) * rearLiftFactor * aeroConstant * Math.Pow(aeroSpeed, 2), 0) + "N");
            carStat.setLabelText("lblAeroDragForce", Math.Round(dragFactor * aeroConstant * Math.Pow(aeroSpeed, 2), 0) + "N"); //TODO Test if this is correct
        }

        public void symmetricAssitChanged(object sender, EventArgs e)
        {
            if (sender.Equals(carStat.chkSymmetric01))
                carStat.chkSymmetric.Checked = carStat.chkSymmetric01.Checked;
        }

        public void rescaleGearingGraph(object sender, EventArgs e)
        {
            carStat.zedGearboxGraph.AxisChange();
            carStat.zedGearboxGraph.Refresh();
            carStat.zedGearboxGraph.ZoomOutAll(carStat.zedGearboxGraph.GraphPane);
        }

        /// <summary>
        /// This methode is called when the Symmetric (left to right symmetric) from the Suspension setup pages is checked or unchecked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void symmetricChanged(object sender, EventArgs e)
        {
            QuatorSuspension frontLeft = new QuatorSuspension(0, carStat);
            QuatorSuspension frontRight = new QuatorSuspension(1, carStat);
            QuatorSuspension rearLeft = new QuatorSuspension(2, carStat);
            QuatorSuspension rearRight = new QuatorSuspension(3, carStat);

            frontLeft.setPartners(carStat.chkSymmetric.Checked, frontRight, data);
            frontRight.setPartners(carStat.chkSymmetric.Checked, frontLeft, data);
            rearLeft.setPartners(carStat.chkSymmetric.Checked, rearRight, data);
            rearRight.setPartners(carStat.chkSymmetric.Checked, rearLeft, data);

            if (carStat.chkSymmetric.Checked)
            {
                carStat.srFLCaster.setPartner(carStat.srFRCaster);
                carStat.srFRCaster.setPartner(carStat.srFLCaster);

                carStat.srRLTrackbar.setPartner(carStat.srRRTrackbar);
                carStat.srRRTrackbar.setPartner(carStat.srRLTrackbar);
            }
            else
            {
                carStat.srFLCaster.setPartner(null);
                carStat.srFRCaster.setPartner(null);

                carStat.srRLTrackbar.setPartner(null);
                carStat.srRRTrackbar.setPartner(null);
            }

            carStat.chkSymmetric01.Checked = carStat.chkSymmetric.Checked;
        }

        #endregion

        #region Assist Functions

        private void setSetupRow(SetupRow setupRow, string section, string property)
        {
            setSetupRow(setupRow, setup, section, property);
        }

        public static void setSetupRow(SetupRow setupRow, Setup setup, string section, string property)
        {
            string value = MiscFunctions.getProperty(setup.SetupValues, section, property, 0);

            if (value != null)
            {
                setupRow.setSettingTo(Int32.Parse(value));
            }
            else
            {
                setupRow.goToDefault();
            }
        }

        public static void writeSetupSetting(StreamWriter writer, string property, SetupRow row)
        {
            int setting = row.getSetting();

            if (setting == -1)
                setting = 0;

            if (row.getCarSetupOption() != null && setting >= row.getCarSetupOption().NumOfSettings)
                setting = row.getCarSetupOption().NumOfSettings - 1;

            string text = property + "=" + setting + "//" + row.getText();

            if (row.getCarSetupOption() == null || (row.getCarSetupOption() != null && (setting == row.getCarSetupOption().DefaultSetting ||
                (row.getCarSetupOption().DefaultSetting >= row.getCarSetupOption().NumOfSettings && (row.getCarSetupOption().NumOfSettings-1) == setting))))
                text = "//" + text;

            writer.WriteLine(text);
        }

        public double getRearForceDistribution(string section, string prefix)
        {
            if (!data.HDVehicleData.Values.ContainsKey(section.ToLower()))
                return -1;
            if (!data.HDVehicleData.Values[section.ToLower()].ContainsKey(prefix.ToLower() + "forcedistrib"))
                return -1;

            string text = data.HDVehicleData.Values[section.ToLower()][prefix.ToLower() + "forcedistrib"][0];
            text = text.ToLower();

            if (text.Contains("front_subbody") && !text.Contains("rear_subbody"))
                return 0;
            else if (!text.Contains("front_subbody") && text.Contains("rear_subbody"))
                return 1;
            else if (!text.Contains("front_subbody") && !text.Contains("rear_subbody"))
                return 0.5;

            string reduced = text.Substring(text.IndexOf("front_subbody") + "front_subbody".Length, text.IndexOf("rear_subbody") - text.IndexOf("front_subbody") - "front_subbody".Length);
            reduced = reduced.Replace(":", "");

            string[] values = reduced.Split(',');

            double rearSplit = 0;

            if (Double.TryParse(values[values.Length - 1], out rearSplit))
                return rearSplit;
            else
                return 0.5;
        }

        #endregion

        #region Classes

        //The Third Spring
        private class ThirdSpring
        {
            public string Side { get; private set; }
            public System.Windows.Forms.Label Label { get; private set; }
            public SetupRow Spring { get; private set; }
            public SetupRow SlowBump { get; private set; }
            public SetupRow SlowRebound { get; private set; }
            public SetupRow FastBump { get; private set; }
            public SetupRow FastRebound { get; private set; }
            public SetupRow Packers { get; private set; }

            public ThirdSpring(bool front, CarStat2 carStat)
            {
                if (front)
                {
                    Side = "Front3rd";
                    Label = carStat.lblFThirdSpring;
                    Spring = carStat.srFSpring;
                    SlowBump = carStat.srFSlowBump;
                    SlowRebound = carStat.srFSlowRebound;
                    FastBump = carStat.srFFastBump;
                    FastRebound = carStat.srFFastRebound;
                    Packers = carStat.srFPacker;
                }
                else
                {
                    Side = "Rear3rd";
                    Label = carStat.lblRThirdSpring;
                    Spring = carStat.srRSpring;
                    SlowBump = carStat.srRSlowBump;
                    SlowRebound = carStat.srRSlowRebound;
                    FastBump = carStat.srRFastBump;
                    FastRebound = carStat.srRFastRebound;
                    Packers = carStat.srRPacker;
                }
            }

            public virtual void setCarSetupOption(CarData data)
            {
                bool thirdSpringNotPresent = MiscFunctions.getProperty(data.HDVehicleData.Values, "SUSPENSION", Side + "SpringRange", 0) == null
                    || MiscFunctions.getProperty(data.HDVehicleData.Values, "SUSPENSION", Side + "SlowBumpRange", 0) == null;

                Label.Visible = !thirdSpringNotPresent;
                Spring.Visible = !thirdSpringNotPresent;
                SlowBump.Visible = !thirdSpringNotPresent;
                SlowRebound.Visible = !thirdSpringNotPresent;
                FastBump.Visible = !thirdSpringNotPresent;
                FastRebound.Visible = !thirdSpringNotPresent;
                Packers.Visible = !thirdSpringNotPresent;


                if (thirdSpringNotPresent)
                {
                    //No third spring
                    Spring.setCarSetupOption(null);
                    SlowBump.setCarSetupOption(null);
                    SlowRebound.setCarSetupOption(null);
                    FastBump.setCarSetupOption(null);
                    FastRebound.setCarSetupOption(null);
                    Packers.setCarSetupOption(null);
                    return;
                }


                Spring.setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, "SUSPENSION", Side + "Spring"));
                SlowBump.setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, "SUSPENSION", Side + "SlowBump"));
                SlowRebound.setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, "SUSPENSION", Side + "SlowRebound"));
                FastBump.setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, "SUSPENSION", Side + "FastBump"));
                FastRebound.setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, "SUSPENSION", Side + "FastRebound"));
                Packers.setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, "SUSPENSION", Side + "Packer"));

                Spring.setRealValuesUnit("N/mm");
                SlowBump.setRealValuesUnit("N/m/s");
                SlowRebound.setRealValuesUnit("N/m/s");
                FastBump.setRealValuesUnit("N/m/s");
                FastRebound.setRealValuesUnit("N/m/s");
                Packers.setRealValuesUnit("mm");

                Spring.setValueFactory(new SetupRow.MultiFactory(3, 0.001));
                SlowBump.setValueFactory(new SetupRow.RoundingFactory(2));
                SlowRebound.setValueFactory(new SetupRow.RoundingFactory(2));
                FastBump.setValueFactory(new SetupRow.RoundingFactory(2));
                FastRebound.setValueFactory(new SetupRow.RoundingFactory(2));
                Packers.setValueFactory(new SetupRow.MultiFactory(2, 1000));

                if (MiscFunctions.getProperty(data.HDVehicleData.Values, "SUSPENSION", "ApplySlowToFastDampers", 0) != "0")
                {
                    SlowBump.setPartner(FastBump);
                    FastBump.setPartner(SlowBump);

                    SlowRebound.setPartner(FastRebound);
                    FastRebound.setPartner(SlowRebound);
                }
            }

            public void setValueChangeListner(SetupCreator creator)
            {
                Spring.ValueChangeListener += new EventHandler(creator.redrawGraphs);
                SlowBump.ValueChangeListener += new EventHandler(creator.redrawGraphs);
                SlowRebound.ValueChangeListener += new EventHandler(creator.redrawGraphs);
                FastBump.ValueChangeListener += new EventHandler(creator.redrawGraphs);
                FastRebound.ValueChangeListener += new EventHandler(creator.redrawGraphs);
                Packers.ValueChangeListener += new EventHandler(creator.redrawGraphs);
            }

            public virtual void setSetup(Setup setup)
            {
                setSetupRow(Spring, setup, "SUSPENSION", Side + "SpringSetting");
                setSetupRow(SlowBump, setup, "SUSPENSION", Side + "SlowBumpSetting");
                setSetupRow(SlowRebound, setup, "SUSPENSION", Side + "SlowReboundSetting");
                setSetupRow(FastBump, setup, "SUSPENSION", Side + "FastBumpSetting");
                setSetupRow(FastRebound, setup, "SUSPENSION", Side + "FastReboundSetting");
                setSetupRow(Packers, setup, "SUSPENSION", Side + "PackerSetting");
            }

            public virtual void writeSetup(StreamWriter writer)
            {
                writeSetupSetting(writer, Side + "PackerSetting", Packers);
                writeSetupSetting(writer, Side + "SpringSetting", Spring);
                writeSetupSetting(writer, Side + "SlowBumpSetting", SlowBump);
                writeSetupSetting(writer, Side + "FastBumpSetting", FastBump);
                writeSetupSetting(writer, Side + "SlowReboundSetting", SlowRebound);
                writeSetupSetting(writer, Side + "FastReboundSetting", FastRebound);
            }

            public void goToDefault()
            {
                Spring.goToDefault();
                SlowBump.goToDefault();
                SlowRebound.goToDefault();
                FastBump.goToDefault();
                FastRebound.goToDefault();
                Packers.goToDefault();
            }

            public void setRealValues(bool real, CarData data)
            {
                if (Spring.getCarSetupOption() != null && !Spring.getCarSetupOption().hasSpecialText)
                    Spring.setDisplayRealValues(true);
                else
                    Spring.setDisplayRealValues(real);

                SlowBump.setDisplayRealValues(real);
                SlowRebound.setDisplayRealValues(real);
                FastBump.setDisplayRealValues(real);
                FastRebound.setDisplayRealValues(real);

                if (Packers.getCarSetupOption() != null && !Packers.getCarSetupOption().hasSpecialText)
                    Packers.setDisplayRealValues(true);
                else
                    Packers.setDisplayRealValues(real);
            }
        }

        //A SUSPENSION corner
        private class QuatorSuspension
        {
            public string Section { get; private set; }
            public SetupRow Camber { get; private set; }
            public SetupRow Spring { get; private set; }
            public SetupRow SlowBump { get; private set; }
            public SetupRow SlowRebound { get; private set; }
            public SetupRow FastBump { get; private set; }
            public SetupRow FastRebound { get; private set; }
            public SetupRow Packers { get; private set; }
            public SetupRow RideHeight { get; private set; }

            public SetupRow Pressure { get; private set; }
            public SetupRow Disc { get; private set; }

            public QuatorSuspension(int i, CarStat2 carStat)
            {
                switch (i)
                {
                    case 0:
                        Section = "FRONTLEFT";
                        Camber = carStat.srFLCamber;
                        Spring = carStat.srFLSpring;
                        SlowBump = carStat.srFLSlowBump;
                        SlowRebound = carStat.srFLSlowRebound;
                        FastBump = carStat.srFLFastBump;
                        FastRebound = carStat.srFLFastRebound;
                        Packers = carStat.srFLPacker;
                        RideHeight = carStat.srFLRideHeight;

                        Pressure = carStat.srFLPressure;
                        Disc = carStat.srFLDisc;
                        break;

                    case 1:
                        Section = "FRONTRIGHT";
                        Camber = carStat.srFRCamber;
                        Spring = carStat.srFRSpring;
                        SlowBump = carStat.srFRSlowBump;
                        SlowRebound = carStat.srFRSlowRebound;
                        FastBump = carStat.srFRFastBump;
                        FastRebound = carStat.srFRFastRebound;
                        Packers = carStat.srFRPacker;
                        RideHeight = carStat.srFRRideHeight;

                        Pressure = carStat.srFRPressure;
                        Disc = carStat.srFRDisc;
                        break;

                    case 2:
                        Section = "REARLEFT";
                        Camber = carStat.srRLCamber;
                        Spring = carStat.srRLSpring;
                        SlowBump = carStat.srRLSlowBump;
                        SlowRebound = carStat.srRLSlowRebound;
                        FastBump = carStat.srRLFastBump;
                        FastRebound = carStat.srRLFastRebound;
                        Packers = carStat.srRLPacker;
                        RideHeight = carStat.srRLRideHeight;

                        Pressure = carStat.srRLPressure;
                        Disc = carStat.srRLDisc;
                        break;

                    case 3:
                        Section = "REARRIGHT";
                        Camber = carStat.srRRCamber;
                        Spring = carStat.srRRSpring;
                        SlowBump = carStat.srRRSlowBump;
                        SlowRebound = carStat.srRRSlowRebound;
                        FastBump = carStat.srRRFastBump;
                        FastRebound = carStat.srRRFastRebound;
                        Packers = carStat.srRRPacker;
                        RideHeight = carStat.srRRRideHeight;

                        Pressure = carStat.srRRPressure;
                        Disc = carStat.srRRDisc;
                        break;
                }

            }

            public virtual void setCarSetupOption(CarData data)
            {
                Camber.setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, Section, "Camber"));
                Spring.setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, Section, "Spring"));
                SlowBump.setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, Section, "SlowBump"));
                SlowRebound.setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, Section, "SlowRebound"));
                FastBump.setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, Section, "FastBump"));
                FastRebound.setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, Section, "FastRebound"));
                Packers.setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, Section, "Packer"));
                RideHeight.setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, Section, "RideHeight"));

                Camber.setRealValuesUnit("°");
                Spring.setRealValuesUnit("N/mm");
                SlowBump.setRealValuesUnit("N/m/s");
                SlowRebound.setRealValuesUnit("N/m/s");
                FastBump.setRealValuesUnit("N/m/s");
                FastRebound.setRealValuesUnit("N/m/s");
                Packers.setRealValuesUnit("mm");
                RideHeight.setRealValuesUnit("mm");

                Camber.setValueFactory(new SetupRow.RoundingFactory(2));
                Spring.setValueFactory(new SetupRow.MultiFactory(3, 0.001));
                SlowBump.setValueFactory(new SetupRow.RoundingFactory(2));
                SlowRebound.setValueFactory(new SetupRow.RoundingFactory(2));
                FastBump.setValueFactory(new SetupRow.RoundingFactory(2));
                FastRebound.setValueFactory(new SetupRow.RoundingFactory(2));
                Packers.setValueFactory(new SetupRow.MultiFactory(2, 1000));
                RideHeight.setValueFactory(new SetupRow.MultiFactory(2, 1000));



                Pressure.setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, Section, "Pressure"));
                Disc.setCarSetupOption(MiscFunctions.getSetupOption(data.HDVehicleData.Values, Section, "BrakeDisc"));

                Pressure.setRealValuesUnit("kPa");
                Disc.setRealValuesUnit("mm");

                Pressure.setValueFactory(new SetupRow.RoundingFactory(2));
                Disc.setValueFactory(new SetupRow.MultiFactory(2, 1000));
            }

            public void setValueChangeListner(SetupCreator creator)
            {
                Camber.ValueChangeListener += new EventHandler(creator.redrawGraphs);
                Spring.ValueChangeListener += new EventHandler(creator.redrawGraphs);
                SlowBump.ValueChangeListener += new EventHandler(creator.redrawGraphs);
                SlowRebound.ValueChangeListener += new EventHandler(creator.redrawGraphs);
                FastBump.ValueChangeListener += new EventHandler(creator.redrawGraphs);
                FastRebound.ValueChangeListener += new EventHandler(creator.redrawGraphs);
                Packers.ValueChangeListener += new EventHandler(creator.redrawGraphs);
                RideHeight.ValueChangeListener += new EventHandler(creator.redrawGraphs);

                Pressure.ValueChangeListener += new EventHandler(creator.redrawGraphs);
                Disc.ValueChangeListener += new EventHandler(creator.redrawGraphs);
            }

            public virtual void setSetup(Setup setup)
            {
                setSetupRow(Camber, setup, Section, "CamberSetting");
                setSetupRow(Spring, setup, Section, "SpringSetting");
                setSetupRow(SlowBump, setup, Section, "SlowBumpSetting");
                setSetupRow(SlowRebound, setup, Section, "SlowReboundSetting");
                setSetupRow(FastBump, setup, Section, "FastBumpSetting");
                setSetupRow(FastRebound, setup, Section, "FastReboundSetting");
                setSetupRow(Packers, setup, Section, "PackerSetting");
                setSetupRow(RideHeight, setup, Section, "RideHeightSetting");

                setSetupRow(Pressure, setup, Section, "PressureSetting");
                setSetupRow(Disc, setup, Section, "BrakeDiscSetting");
            }

            public virtual void writeSetup(StreamWriter writer, SetupRow springrubber)
            {
                writer.WriteLine("["+Section+"]");
                writeSetupSetting(writer, "CamberSetting", Camber);
                writeSetupSetting(writer, "PressureSetting", Pressure);
                writeSetupSetting(writer, "PackerSetting", Packers);
                writeSetupSetting(writer, "SpringSetting", Spring);
                writeSetupSetting(writer, "SpringRubberSetting", springrubber);
                writeSetupSetting(writer, "RideHeightSetting", RideHeight);
                writeSetupSetting(writer, "SlowBumpSetting", SlowBump);
                writeSetupSetting(writer, "FastBumpSetting", FastBump);
                writeSetupSetting(writer, "SlowReboundSetting", SlowRebound);
                writeSetupSetting(writer, "FastReboundSetting", FastRebound);
                writeSetupSetting(writer, "BrakeDiscSetting", Disc);
                writer.WriteLine("//BrakePadSetting=0//1");
                writer.WriteLine("");
            }

            public void goToDefault()
            {
                Camber.goToDefault();
                Spring.goToDefault();
                SlowBump.goToDefault();
                SlowRebound.goToDefault();
                FastBump.goToDefault();
                FastRebound.goToDefault();
                Packers.goToDefault();
                RideHeight.goToDefault();

                Pressure.goToDefault();
                Disc.goToDefault();
            }

            public void setRealValues(bool real, CarData data)
            {
                if (Camber.getCarSetupOption() != null)
                    Camber.setDisplayRealValues(!Camber.getCarSetupOption().hasSpecialText || real);

                if (Spring.getCarSetupOption() != null)
                    Spring.setDisplayRealValues(!Spring.getCarSetupOption().hasSpecialText || real);

                SlowBump.setDisplayRealValues(real);
                SlowRebound.setDisplayRealValues(real);
                FastBump.setDisplayRealValues(real);
                FastRebound.setDisplayRealValues(real);

                if (Packers.getCarSetupOption() != null)
                    Packers.setDisplayRealValues(!Packers.getCarSetupOption().hasSpecialText || real);

                if (RideHeight.getCarSetupOption() != null)
                    RideHeight.setDisplayRealValues(!RideHeight.getCarSetupOption().hasSpecialText || real);



                if (Pressure.getCarSetupOption() != null)
                    Pressure.setDisplayRealValues(!Pressure.getCarSetupOption().hasSpecialText || real);

                if (Disc.getCarSetupOption() != null)
                    Disc.setDisplayRealValues(!Disc.getCarSetupOption().hasSpecialText || real);
            }

            public void setPartners(bool symmetric, QuatorSuspension quator, CarData data)
            {
                if (MiscFunctions.getProperty(data.HDVehicleData.Values, "SUSPENSION", "ApplySlowToFastDampers", 0) != "0")
                {
                    //SlowBump.setPartner(FastBump);
                    //FastBump.setPartner(SlowBump);

                    SlowRebound.setPartner(FastRebound);
                    FastRebound.setPartner(SlowRebound);
                }

                if (symmetric)
                {
                    Camber.setPartner(quator.Camber);
                    Spring.setPartner(quator.Spring);
                    Packers.setPartner(quator.Packers);
                    RideHeight.setPartner(quator.RideHeight);

                    SlowBump.setPartner(quator.SlowBump);
                    FastBump.setPartner(quator.FastBump);

                    Pressure.setPartner(quator.Pressure);
                    Disc.setPartner(quator.Disc);

                    //Slow applies to Fast
                    if (MiscFunctions.getProperty(data.HDVehicleData.Values, "SUSPENSION", "ApplySlowToFastDampers", 0) != "0")
                    {
                        //FastBump.setPartner(quator.SlowBump);

                        FastRebound.setPartner(quator.SlowRebound);
                    }
                    else
                    {
                        //SlowBump.setPartner(quator.SlowBump);
                        //FastBump.setPartner(quator.FastBump);

                        SlowRebound.setPartner(quator.SlowRebound);
                        FastRebound.setPartner(quator.FastRebound);
                    }
                }
                else
                {
                    Camber.setPartner(null);
                    Spring.setPartner(null);
                    Packers.setPartner(null);
                    RideHeight.setPartner(null);

                    SlowBump.setPartner(null);
                    FastBump.setPartner(null);

                    Pressure.setPartner(null);
                    Disc.setPartner(null);

                    //Slow does not applies to Fast
                    if (MiscFunctions.getProperty(data.HDVehicleData.Values, "SUSPENSION", "ApplySlowToFastDampers", 0) == "0")
                    {
                        //SlowBump.setPartner(null);
                        //FastBump.setPartner(null);

                        SlowRebound.setPartner(null);
                        FastRebound.setPartner(null);
                    }
                }
            }
        }

        #endregion
    }
}
