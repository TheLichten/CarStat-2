﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace CarStat_2.VehicleSelector
{
    public partial class Loading : Form
    {
        public Loading()
        {
            InitializeComponent();
            this.TopMost = true;
            this.Show();
        }
    }
}
