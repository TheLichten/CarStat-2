﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using Misc;
using CarStat_2.Windows;

namespace CarStat_2.VehicleSelector
{
    public partial class VehSelector : Form
    {
        List<Vehicle> vehfiles = new List<Vehicle>();
        private List<Vehicle> vehselected = new List<Vehicle>();
        private string gameLoc = "";
        private string appName = null;

        private CarStat2 master;

        //Initialization of the vehicle selector with a lot of registry-stuff
        public VehSelector(CarStat2 master)
        {
            this.master = master;
            this.Owner = master;
            this.appName = "CarStat";
            InitializeComponent();
            this.Visible = true;


            gameEnvWidget1.Prep(master, this);
            gameEnvWidget1.Change += new EventHandler(GameEnvChange_Listener);

            GameEnvChange_Listener(null, null);
        }

        //Run the search process
        private void Run()
        {
            treeView1.Nodes.Clear();
            vehfiles.Clear();
            Cursor.Current = Cursors.WaitCursor;

            if (gameLoc != null)
            {
                string vehFolder = gameLoc + @"\" + master.gameEnv.GetVehFolder();

                if (master.gameEnv.GAME == "GTR")
                    findVEH(vehFolder, ".car");
                else
                    findVEH(vehFolder);


            }
            makeTree();
            Cursor.Current = Cursors.Default;
        }

        //Make the treeview of the vehicles
        private void makeTree()
        {
            for (int i = 0; i < vehfiles.Count; i++)
            {
                if (vehfiles[i].Locatie.Contains("Vehicles"))
                    vehfiles[i].Locatie = vehfiles[i].Locatie.Substring(vehfiles[i].Locatie.LastIndexOf("Vehicles") + 9);

                if (vehfiles[i].Locatie.Contains("GameData\\Teams"))
                    vehfiles[i].Locatie = vehfiles[i].Locatie.Substring(vehfiles[i].Locatie.LastIndexOf("GameData\\Teams") + "GameData\\Teams".Length +1);
            }

            //Filter
            string[] searchWords = tbSearch.Text.ToLower().Split(' ');
            if (searchWords.Length == 1 && searchWords[0].Trim() == "")
                searchWords = new string[0];


            HiddenCheckBoxTreeNode start = new HiddenCheckBoxTreeNode("Cars");
            treeView1.Nodes.Add(start);
            for (int i = 0; i < vehfiles.Count; i++)
            {
                bool pass = true;
                for (int j = 0; j < searchWords.Length; j++)
                {
                    if (!vehfiles[i].Locatie.ToLower().Contains(searchWords[j].Trim()))
                    {
                        pass = false;
                        break;
                    }
                }

                if (pass)
                    nodeAdd(start, vehfiles[i].Locatie);
            }
            hideCheckboxesInTreeview(treeView1.Nodes[0]);
            start.Expand();
        }

        //Adds a new node to the treeview
        private TreeNode nodeAdd(TreeNode parent, string text)
        {
            int i = 0;
            bool exists = false;
            foreach (TreeNode noot in parent.Nodes)
            {
                if (noot.Text == (text.Split('\\'))[0])
                {
                    exists = true;
                    break;
                }
                i++;
            }
            TreeNode veh = new TreeNode();
            if (exists)
            {
                if (text.Contains('\\'))
                {
                    veh = nodeAdd(parent.Nodes[i], text.Substring(text.IndexOf('\\') + 1));
                }
            }
            else
            {

                if (text.Contains('\\'))
                {
                    parent.Nodes.Add(new HiddenCheckBoxTreeNode((text.Split('\\'))[0]));
                    veh = nodeAdd(parent.Nodes[i], text.Substring(text.IndexOf('\\') + 1));
                }
                else
                {
                    parent.Nodes.Add(new TreeNode((text.Split('\\'))[0]));
                }
            }
            return parent.Nodes[parent.Nodes.Count - 1];
        }


        private void findVEH(string dir)
        {
            this.findVEH(dir, ".veh");
        }

        //Finds the vehicle files in the given dir
        private void findVEH(string dir, string fileEnding)
        {
            try
            {
                foreach (string f in Directory.GetFiles(dir))
                {
                    try
                    {
                        if (Path.GetExtension(f).ToLower() == fileEnding)
                        {
                            vehfiles.Add(new Vehicle(f, ""));
                        }
                        if (Path.GetExtension(f).ToLower() == ".mas")
                        {
                            List<string> vehs = MiscFunctions.getVehiclesFromMas(f, fileEnding);
                            foreach (string veh in vehs)
                            {
                                vehfiles.Add(new Vehicle(Path.Combine(dir, veh), f));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                    
                }
                foreach (string d in Directory.GetDirectories(dir))
                {
                    findVEH(d, fileEnding);
                }
            }
            catch (System.Exception excpt)
            {
                Console.WriteLine(excpt.Message);
            }
        }

        private void hideCheckboxesInTreeview(TreeNode node)
        {
            /*
            if (node.Text.ToLower().EndsWith(".veh") == false) //So it's NOT a vehicle file
            {
                for (int i = 0; i < node.Nodes.Count; i++)
                    hideCheckboxesInTreeview(node.Nodes[i]);

                HiddenCheckBoxTreeNode hiddenNode = new HiddenCheckBoxTreeNode(node.Text, node.Nodes.);
                node.Parent.Nodes.Insert(node.Index, "
                node.Index
            }
            */
        }

        //Stores or doesn't store the location of the startupdir
        private void checkRemember_CheckedChanged(object sender, EventArgs e)
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey("SOFTWARE", true);
            RegistryKey newkey = key.CreateSubKey(appName);
        }

        //Opens the selected vehicle file by creating the path to it and closing the vehselector
        private void btnOpenVEH_Click(object sender, EventArgs e)
        {
            btnOpenVEH.Text = "Loading...";
            readCheckedNodes(treeView1.Nodes[0]);
            if (vehselected.Count == 0)
            {
                btnOpenVEH.Text = "Open checked car(s)";
                MessageBox.Show("Please select/check one or more cars to open.", "No car selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            this.Close();
        }

        private void readCheckedNodes(TreeNode node)
        {
            if (node.Checked)
            {
                string file = node.FullPath;
                file = file.Substring("Cars\\".Length);
                if (Path.GetExtension(file).ToLower() == ".veh" || Path.GetExtension(file).ToLower() == ".car")
                {
                    foreach (Vehicle vehi in vehfiles)
                    {

                        if (file == vehi.Locatie)
                        {
                            string folder = @"\" + master.gameEnv.GetVehFolder();

                            vehselected.Add(new Vehicle(gameLoc + folder + file, vehi.MasFile));
                        }
                    }
                }
            }
            foreach (TreeNode subNode in node.Nodes)
                readCheckedNodes(subNode);

        }

        //Guess!
        private void btnCollapse_Click(object sender, EventArgs e)
        {
            treeView1.CollapseAll();
        }

        //Guess again!
        private void btnExpand_Click(object sender, EventArgs e)
        {
            treeView1.ExpandAll();
        }

        //Adds vehicle files to the vehicles list to be executed.
        private void treeView1_NodeMouseDoubleClick(object sender, EventArgs e)
        {
            try
            {
                string file = treeView1.SelectedNode.FullPath;

                if (Path.GetExtension(file).ToLower() == ".veh")
                {
                    treeView1.SelectedNode.Checked = true;
                    btnOpenVEH_Click(null, null);
                }
            }
            catch { }
        }

        public List<Vehicle> getCheckedVehicles()
        {
            return vehselected;
        }

        private StringBuilder readRecursive(StringBuilder theList, TreeNode selectedNode)
        {
            foreach (TreeNode node in selectedNode.Nodes)
            {
                string branch = node.FullPath;
                string selectedBranch = gameLoc + @"\Installed\Vehicles\" + branch.Substring(branch.IndexOf("Start\\") + 6);
                if (node.FullPath.ToLower().EndsWith(".veh"))
                {
                    List<string> vehicleFile = MiscFunctions.FileToList(selectedBranch);
                    string locWithoutVeh = selectedBranch.Substring(0, selectedBranch.LastIndexOf(@"\"));


                    theList.AppendLine(locWithoutVeh.Substring(locWithoutVeh.LastIndexOf(@"\") + 1) + @"\" + MiscFunctions.getProperty(vehicleFile, "Description", 1));
                }
                else
                    readRecursive(theList, node);
            }
            return theList;
        }

        private void tbSearch_TextChanged(object sender, EventArgs e)
        {
            treeView1.BeginUpdate();
            treeView1.Nodes.Clear();
            makeTree();
            treeView1.EndUpdate();
        }

        private void treeView1_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            try
            {
                string file = treeView1.SelectedNode.FullPath;

                if (Path.GetExtension(file).ToLower() == ".veh")
                {
                    treeView1.SelectedNode.Checked = true;
                    btnOpenVEH_Click(null, null);
                }
            }
            catch { }

        }

        private void GameEnvChange_Listener(object ob, EventArgs e)
        {
            if (master.gameEnv != null)
                gameLoc = master.gameEnv.GetGameLocation();
            else
                gameLoc = null;

            Run();
        }

        
    }

    [Serializable]
    public class Vehicle
    {
        public string Locatie { get; set; }
        public string MasFile { get; set; }

        public Vehicle(string locatie, string masFile)
        {
            this.Locatie = locatie;
            this.MasFile = masFile;
        }
    }
}
