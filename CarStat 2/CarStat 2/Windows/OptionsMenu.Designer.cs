﻿namespace CarStat_2.Windows
{
    partial class OptionsMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OptionsMenu));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkOpenOnlyNonEncrypted = new System.Windows.Forms.CheckBox();
            this.grTemperatur = new System.Windows.Forms.GroupBox();
            this.rdF = new System.Windows.Forms.RadioButton();
            this.rdC = new System.Windows.Forms.RadioButton();
            this.grTorque = new System.Windows.Forms.GroupBox();
            this.rdFTLB = new System.Windows.Forms.RadioButton();
            this.rdNM = new System.Windows.Forms.RadioButton();
            this.grPower = new System.Windows.Forms.GroupBox();
            this.rdKW = new System.Windows.Forms.RadioButton();
            this.rdHP = new System.Windows.Forms.RadioButton();
            this.btnResetGame = new System.Windows.Forms.Button();
            this.gameEnvWidget1 = new CarStat_2.Windows.GameEnvWidget();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.grTemperatur.SuspendLayout();
            this.grTorque.SuspendLayout();
            this.grPower.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkOpenOnlyNonEncrypted);
            this.groupBox1.Location = new System.Drawing.Point(13, 167);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(210, 119);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Settings";
            // 
            // chkOpenOnlyNonEncrypted
            // 
            this.chkOpenOnlyNonEncrypted.AutoSize = true;
            this.chkOpenOnlyNonEncrypted.Location = new System.Drawing.Point(19, 17);
            this.chkOpenOnlyNonEncrypted.Margin = new System.Windows.Forms.Padding(2);
            this.chkOpenOnlyNonEncrypted.Name = "chkOpenOnlyNonEncrypted";
            this.chkOpenOnlyNonEncrypted.Size = new System.Drawing.Size(179, 17);
            this.chkOpenOnlyNonEncrypted.TabIndex = 0;
            this.chkOpenOnlyNonEncrypted.Text = "Open Only Non Encrypted Mods";
            this.chkOpenOnlyNonEncrypted.UseVisualStyleBackColor = true;
            this.chkOpenOnlyNonEncrypted.CheckedChanged += new System.EventHandler(this.chkOpenOnlyNonEncrypted_CheckedChanged);
            // 
            // grTemperatur
            // 
            this.grTemperatur.Controls.Add(this.rdF);
            this.grTemperatur.Controls.Add(this.rdC);
            this.grTemperatur.Location = new System.Drawing.Point(13, 116);
            this.grTemperatur.Margin = new System.Windows.Forms.Padding(2);
            this.grTemperatur.Name = "grTemperatur";
            this.grTemperatur.Padding = new System.Windows.Forms.Padding(2);
            this.grTemperatur.Size = new System.Drawing.Size(210, 47);
            this.grTemperatur.TabIndex = 4;
            this.grTemperatur.TabStop = false;
            this.grTemperatur.Text = "Temperatur";
            // 
            // rdF
            // 
            this.rdF.AutoSize = true;
            this.rdF.Location = new System.Drawing.Point(107, 17);
            this.rdF.Margin = new System.Windows.Forms.Padding(2);
            this.rdF.Name = "rdF";
            this.rdF.Size = new System.Drawing.Size(35, 17);
            this.rdF.TabIndex = 2;
            this.rdF.TabStop = true;
            this.rdF.Text = "°F";
            this.rdF.UseVisualStyleBackColor = true;
            // 
            // rdC
            // 
            this.rdC.AutoSize = true;
            this.rdC.Location = new System.Drawing.Point(19, 17);
            this.rdC.Margin = new System.Windows.Forms.Padding(2);
            this.rdC.Name = "rdC";
            this.rdC.Size = new System.Drawing.Size(36, 17);
            this.rdC.TabIndex = 1;
            this.rdC.TabStop = true;
            this.rdC.Text = "°C";
            this.rdC.UseVisualStyleBackColor = true;
            this.rdC.CheckedChanged += new System.EventHandler(this.rdC_CheckedChanged);
            // 
            // grTorque
            // 
            this.grTorque.Controls.Add(this.rdFTLB);
            this.grTorque.Controls.Add(this.rdNM);
            this.grTorque.Location = new System.Drawing.Point(13, 64);
            this.grTorque.Margin = new System.Windows.Forms.Padding(2);
            this.grTorque.Name = "grTorque";
            this.grTorque.Padding = new System.Windows.Forms.Padding(2);
            this.grTorque.Size = new System.Drawing.Size(210, 47);
            this.grTorque.TabIndex = 3;
            this.grTorque.TabStop = false;
            this.grTorque.Text = "Torque";
            // 
            // rdFTLB
            // 
            this.rdFTLB.AutoSize = true;
            this.rdFTLB.Location = new System.Drawing.Point(107, 17);
            this.rdFTLB.Margin = new System.Windows.Forms.Padding(2);
            this.rdFTLB.Name = "rdFTLB";
            this.rdFTLB.Size = new System.Drawing.Size(42, 17);
            this.rdFTLB.TabIndex = 2;
            this.rdFTLB.TabStop = true;
            this.rdFTLB.Text = "ft-lb";
            this.rdFTLB.UseVisualStyleBackColor = true;
            // 
            // rdNM
            // 
            this.rdNM.AutoSize = true;
            this.rdNM.Location = new System.Drawing.Point(19, 17);
            this.rdNM.Margin = new System.Windows.Forms.Padding(2);
            this.rdNM.Name = "rdNM";
            this.rdNM.Size = new System.Drawing.Size(39, 17);
            this.rdNM.TabIndex = 1;
            this.rdNM.TabStop = true;
            this.rdNM.Text = "nm";
            this.rdNM.UseVisualStyleBackColor = true;
            this.rdNM.CheckedChanged += new System.EventHandler(this.rdNM_CheckedChanged);
            // 
            // grPower
            // 
            this.grPower.Controls.Add(this.rdKW);
            this.grPower.Controls.Add(this.rdHP);
            this.grPower.Location = new System.Drawing.Point(13, 12);
            this.grPower.Margin = new System.Windows.Forms.Padding(2);
            this.grPower.Name = "grPower";
            this.grPower.Padding = new System.Windows.Forms.Padding(2);
            this.grPower.Size = new System.Drawing.Size(210, 47);
            this.grPower.TabIndex = 0;
            this.grPower.TabStop = false;
            this.grPower.Text = "Power";
            // 
            // rdKW
            // 
            this.rdKW.AutoSize = true;
            this.rdKW.Location = new System.Drawing.Point(107, 17);
            this.rdKW.Margin = new System.Windows.Forms.Padding(2);
            this.rdKW.Name = "rdKW";
            this.rdKW.Size = new System.Drawing.Size(68, 17);
            this.rdKW.TabIndex = 2;
            this.rdKW.TabStop = true;
            this.rdKW.Text = "Kilo Watt";
            this.rdKW.UseVisualStyleBackColor = true;
            // 
            // rdHP
            // 
            this.rdHP.AutoSize = true;
            this.rdHP.Location = new System.Drawing.Point(19, 17);
            this.rdHP.Margin = new System.Windows.Forms.Padding(2);
            this.rdHP.Name = "rdHP";
            this.rdHP.Size = new System.Drawing.Size(86, 17);
            this.rdHP.TabIndex = 1;
            this.rdHP.TabStop = true;
            this.rdHP.Text = "Horse Power";
            this.rdHP.UseVisualStyleBackColor = true;
            this.rdHP.CheckedChanged += new System.EventHandler(this.rdHP_CheckedChanged);
            // 
            // btnResetGame
            // 
            this.btnResetGame.Location = new System.Drawing.Point(6, 203);
            this.btnResetGame.Name = "btnResetGame";
            this.btnResetGame.Size = new System.Drawing.Size(130, 22);
            this.btnResetGame.TabIndex = 7;
            this.btnResetGame.Text = "Reset Default Game";
            this.btnResetGame.UseVisualStyleBackColor = true;
            this.btnResetGame.Click += new System.EventHandler(this.btnResetGame_Click);
            // 
            // gameEnvWidget1
            // 
            this.gameEnvWidget1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gameEnvWidget1.BackColor = System.Drawing.Color.Transparent;
            this.gameEnvWidget1.Change = null;
            this.gameEnvWidget1.Location = new System.Drawing.Point(6, 19);
            this.gameEnvWidget1.Name = "gameEnvWidget1";
            this.gameEnvWidget1.Size = new System.Drawing.Size(130, 178);
            this.gameEnvWidget1.TabIndex = 6;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.gameEnvWidget1);
            this.groupBox2.Controls.Add(this.btnResetGame);
            this.groupBox2.Location = new System.Drawing.Point(460, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(142, 236);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Game Selected";
            // 
            // OptionsMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(614, 296);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.grTemperatur);
            this.Controls.Add(this.grTorque);
            this.Controls.Add(this.grPower);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "OptionsMenu";
            this.Text = "Options Menu";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grTemperatur.ResumeLayout(false);
            this.grTemperatur.PerformLayout();
            this.grTorque.ResumeLayout(false);
            this.grTorque.PerformLayout();
            this.grPower.ResumeLayout(false);
            this.grPower.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox grPower;
        private System.Windows.Forms.RadioButton rdKW;
        private System.Windows.Forms.RadioButton rdHP;
        private System.Windows.Forms.GroupBox grTorque;
        private System.Windows.Forms.RadioButton rdFTLB;
        private System.Windows.Forms.RadioButton rdNM;
        private System.Windows.Forms.GroupBox grTemperatur;
        private System.Windows.Forms.RadioButton rdF;
        private System.Windows.Forms.RadioButton rdC;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chkOpenOnlyNonEncrypted;
        private GameEnvWidget gameEnvWidget1;
        private System.Windows.Forms.Button btnResetGame;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}