﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CarStat_2.Windows
{
    public partial class Help : Form
    {
        public Help(Form owner)
        {
            InitializeComponent();
            this.Visible = true;
            this.Owner = owner;
            this.webBrowser1.Url = new Uri("https://gitlab.com/TheLichten/CarStat-2/wikis/Handbook");
            this.webBrowser1.Update();
        }
    }
}
