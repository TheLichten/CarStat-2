﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarStat_2.Windows
{
    public partial class GameFolderSelector : Form
    {
        private CarStat2 carStat;
        private string gameName;

        public string gameFolder;

        private bool disableHandle = false;

        public GameFolderSelector(CarStat2 carStat)
        {
            InitializeComponent();

            this.Owner = carStat;
            this.carStat = carStat;

            if (carStat.gameEnv == null)
            {
                MessageBox.Show("You need to select a game first, before you can set the game folder!", "No Game Loaded!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
                return;
            }

            string myDocuments = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            string setupFolder = "";

            cbProfile.Items.Clear();
            cbProfile.Enabled = true;

            switch (carStat.gameEnv.GAME)
            {
                case "RF1":
                    gameName = "rFactor 1";
                    setupFolder = Path.Combine(myDocuments, @"rFactor");
                    break;

                case "RF2":
                    gameName = "rFactor 2";
                    break;

                case "GTR":
                    gameName = "GTR/GTL";
                    break;

                case "AMS":
                    gameName = "Automobilista";
                    setupFolder = Path.Combine(myDocuments, @"Automobilista");
                    break;

                default:
                    gameName = "Unknown";
                    break;
            }

            if (carStat.gameEnv.GAME == "RF2")
            {
                cbProfile.Items.Add("player");
                cbProfile.Enabled = false;
                cbProfile.SelectedIndex = 0;
            }
            else if (setupFolder != "")
            {
                setupFolder = Path.Combine(setupFolder, @"userdata");

                if (Directory.Exists(setupFolder))
                {
                    List<string> list = new List<string>(Directory.GetDirectories(setupFolder));

                    for (int i = 0; i < list.Count; i++)
                    {
                        if (list[i].Contains("LOG") || list[i].Contains("CONTROLLER"))
                        {
                            list.RemoveAt(i);
                            i--;
                        }
                        else
                        {
                            list[i] = Path.GetFileName(list[i]); //Profile Name
                        }
                    }

                    cbProfile.Items.AddRange(list.ToArray());
                    cbProfile.SelectedIndex = 0;
                }
            }


            btnFindGame.Text = "Find " + gameName + " Folder";
            chkRemeber.Text = "Remeber " + gameName + " Location";
            btnSetLoc.Text = "Select " + gameName + " Folder";

            gameFolder = carStat.gameEnv.GetGameLocation();

            this.Show();
        }

        private void btnSetGameLoc_Click(object sender, EventArgs e)
        {
            string value = "";
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.ShowNewFolderButton = false;
            dialog.SelectedPath = carStat.gameEnv.GetGameLocation();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                value = dialog.SelectedPath;
                if (!isValid(value))
                {
                    MessageBox.Show("You have to select the " + gameName + " folder to find the gamedata (Usally in steamapps/common/).", "Not in the right folder", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //gameFolder = null;
                    return;
                }
                else
                {
                    gameFolder = value;

                    try
                    {
                        disableHandle = true;
                        if (carStat.gameEnv.GetGameLocation() == gameFolder)
                        {
                            chkRemeber.Checked = true;
                        }
                        else
                        {
                            chkRemeber.Checked = false;
                        }
                        disableHandle = false;
                    }
                    catch (Exception)
                    {
                        disableHandle = false;
                    }
                }
            }
        }

        private void chkRemeber_CheckedChanged(object sender, EventArgs e)
        {
            if (disableHandle)
                return;

            if (chkRemeber.Checked)
            {
                carStat.gameEnv.StoreGameLocation(gameFolder);
            }
            else
            {
                carStat.gameEnv.StoreGameLocation("");
            }
        }

        private void btnFindGame_Click(object sender, EventArgs e)
        {
            string value = carStat.gameEnv.FindGameLocation();

            if (!isValid(value))
            {
                MessageBox.Show(gameName + " folder not found in the registry.", "Unable to find your " + gameName + " Install", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            else
            {
                gameFolder = value;

                chkRemeber.Checked = true;
                chkRemeber_CheckedChanged(null, null);
            }
        }

        private bool isValid(string path)
        {
            if (path == "" || path == null || carStat.gameEnv == null)
                return false;

            return Directory.Exists(Path.Combine(path, carStat.gameEnv.GetVehFolder()));
        }

        private void cbProfile_SelectedIndexChanged(object sender, EventArgs e)
        {
            carStat.gameEnv.Profile = cbProfile.Text;
        }
    }
}
