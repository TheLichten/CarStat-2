﻿using Misc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarStat_2.Windows
{
    public partial class OptionsMenu : Form
    {
        private CarStat2 carStat2;

        public OptionsMenu(CarStat2 owner)
        {
            this.carStat2 = owner;
            //Setup
            InitializeComponent();
            MiscFunctions.makeParentChildWindow(carStat2, this);

            this.Show();

            gameEnvWidget1.Prep(carStat2, this);
            gameEnvWidget1.Change += new EventHandler(GameEnvChange_Listener);

            //Build-up
            loadRegistrySettings();
        }

        private void GameEnvChange_Listener(object sender, EventArgs e)
        {
            loadRegistrySettings();
        }

        private void loadRegistrySettings()
        {
            if (carStat2.gameEnv != null && carStat2.gameEnv.GAME == "RF2")
            {
                chkOpenOnlyNonEncrypted.Visible = true;

                if (carStat2.gameEnv.LoadReadEncrytedFiles() == false)
                {
                    chkOpenOnlyNonEncrypted.Checked = true;
                }
                else
                {
                    chkOpenOnlyNonEncrypted.Checked = false;
                }
            }
            else
            {
                chkOpenOnlyNonEncrypted.Visible = false;
            }


            if (GameEnvironmentFunc.LoadMenuSetting("Power") == MiscFunctions.Units.kW)
            {
                rdKW.Checked = true;
            }
            else
            {
                rdHP.Checked = true;
            }
            if (GameEnvironmentFunc.LoadMenuSetting("Torque") == MiscFunctions.Units.ftlb)
            {
                rdFTLB.Checked = true;
            }
            else
            {
                rdNM.Checked = true;
            }
            if (GameEnvironmentFunc.LoadMenuSetting("Temperature") == MiscFunctions.Units.F)
            {
                rdF.Checked = true;
            }
            else
            {
                rdC.Checked = true;
            }
        }

        //Actionslisteners

        private void rdHP_CheckedChanged(object sender, EventArgs e)
        {
            if (rdHP.Checked)
                GameEnvironmentFunc.RememberMenuSetting(MiscFunctions.Units.HP, "Power");
            else
                GameEnvironmentFunc.RememberMenuSetting(MiscFunctions.Units.kW, "Power");

            carStat2.redrawView();
        }

        private void rdNM_CheckedChanged(object sender, EventArgs e)
        {
            if (rdNM.Checked)
                GameEnvironmentFunc.RememberMenuSetting(MiscFunctions.Units.Nm, "Torque");
            else
                GameEnvironmentFunc.RememberMenuSetting(MiscFunctions.Units.ftlb, "Torque");

            carStat2.redrawView();
        }

        private void rdC_CheckedChanged(object sender, EventArgs e)
        {
            if (rdC.Checked)
                GameEnvironmentFunc.RememberMenuSetting(MiscFunctions.Units.C, "Temperature");
            else
                GameEnvironmentFunc.RememberMenuSetting(MiscFunctions.Units.F, "Temperature");

            carStat2.redrawView();
        }

        private void chkOpenOnlyNonEncrypted_CheckedChanged(object sender, EventArgs e)
        {
            if (chkOpenOnlyNonEncrypted.Checked) //All is good
            {
                carStat2.gameEnv.RememberReadEncryptedFiles(false); //Don't read encrypted files
            }
            else //What are you doing man!
            {
                DialogResult result = MessageBox.Show("Are you sure want to read encrypted files?\n\nISI and/or the mod creator may/does not like this, this is on you!", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    carStat2.gameEnv.RememberReadEncryptedFiles(true); //Read encrypted files
                }
                else
                {
                    chkOpenOnlyNonEncrypted.Checked = true;
                }
            }
        }

        private void btnResetGame_Click(object sender, EventArgs e)
        {
            carStat2.gameEnv = null;
            carStat2.makevisible(false, false);

            GameEnvironmentFunc.StoreAutoStartSetting("");
            gameEnvWidget1.Prep(carStat2, this);
        }
    }
}
