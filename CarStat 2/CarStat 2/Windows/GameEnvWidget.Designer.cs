﻿namespace CarStat_2.Windows
{
    partial class GameEnvWidget
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnSelectFolder = new System.Windows.Forms.Button();
            this.btnGame = new System.Windows.Forms.Button();
            this.toolTip_GameEnv = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // btnSelectFolder
            // 
            this.btnSelectFolder.Location = new System.Drawing.Point(0, 136);
            this.btnSelectFolder.Name = "btnSelectFolder";
            this.btnSelectFolder.Size = new System.Drawing.Size(130, 42);
            this.btnSelectFolder.TabIndex = 14;
            this.btnSelectFolder.Text = "Select Game Folder";
            this.btnSelectFolder.UseVisualStyleBackColor = true;
            this.btnSelectFolder.Click += new System.EventHandler(this.btnSelectFolder_Click);
            // 
            // btnGame
            // 
            this.btnGame.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnGame.Location = new System.Drawing.Point(0, 0);
            this.btnGame.Name = "btnGame";
            this.btnGame.Size = new System.Drawing.Size(130, 130);
            this.btnGame.TabIndex = 13;
            this.toolTip_GameEnv.SetToolTip(this.btnGame, "Press to Select the Game");
            this.btnGame.UseVisualStyleBackColor = false;
            this.btnGame.Click += new System.EventHandler(this.btnGame_Click);
            // 
            // GameEnvWidget
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.btnSelectFolder);
            this.Controls.Add(this.btnGame);
            this.Name = "GameEnvWidget";
            this.Size = new System.Drawing.Size(130, 178);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSelectFolder;
        private System.Windows.Forms.Button btnGame;
        private System.Windows.Forms.ToolTip toolTip_GameEnv;
    }
}
