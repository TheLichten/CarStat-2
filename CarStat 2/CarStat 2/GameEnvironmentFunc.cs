﻿using Microsoft.Win32;
using Misc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarStat_2
{
    public abstract class GameEnvironmentFunc
    {
        public string GAME { private set; get; }

        public SetLabels LabelsSetter { protected set; get; }
        public Graphs Graphs { protected set; get; }
        public SetupCreator SetupCreator { protected set; get; }
        public DataObjects.DataReader Reader { protected set; get; }
        public DataObjects.CarData Data { internal set; get; }

        public string Profile { set; get; }


        protected GameEnvironmentFunc(string game)
        {
            GAME = game;
        }

        //Store if CarStat 2 should read encrypted files or not
        public virtual void RememberReadEncryptedFiles(bool readIt)
        {
            try
            {
                RegistryKey key = Registry.CurrentUser.OpenSubKey("SOFTWARE", true);
                RegistryKey carKey = key.CreateSubKey("CarStat");
                carKey.SetValue("ReadEncrypted", readIt);
            }
            catch { }
        }

        public virtual bool LoadReadEncrytedFiles()
        {
            try
            {
                RegistryKey MyReg = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\CarStat", true);
                if (MyReg.GetValue("ReadEncrypted").ToString() == true.ToString())
                {
                    return true;
                }
            }
            catch { }
            return false;
        }

        //Store the preferred menusetting 
        public static void RememberMenuSetting(MiscFunctions.Units unit, string setting)
        {
            try
            {
                RegistryKey key = Registry.CurrentUser.OpenSubKey("SOFTWARE", true);
                RegistryKey carKey = key.CreateSubKey("CarStat");
                carKey.SetValue(setting, unit);
            }
            catch { }
        }

        //Get the stored menusetting 
        public static MiscFunctions.Units LoadMenuSetting(string setting)
        {
            try
            {
                RegistryKey MyReg = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\CarStat", true);
                if (MyReg.GetValue(setting).ToString() != "bladiebla")
                {
                    return (MiscFunctions.Units)Enum.Parse(typeof(MiscFunctions.Units), MyReg.GetValue(setting).ToString());
                }
            }
            catch { }
            return MiscFunctions.Units.Empty;
        }

        //Get the stored autostart Game 
        public static string LoadAutoStartSetting()
        {
            try
            {
                RegistryKey MyReg = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\CarStat", true);
                return MyReg.GetValue("AutoStart").ToString();
            }
            catch { }
            return null;
        }

        public static void StoreAutoStartSetting(string game)
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey("SOFTWARE", true);
            RegistryKey carKey = key.CreateSubKey("CarStat");
            carKey.SetValue("AutoStart", game);
        }

        public virtual string GetGameLocation()
        {
            try
            {
                RegistryKey MyReg = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\" + "CarStat", true);
                string rfactorloc = MyReg.GetValue("Startupdir-" + GAME).ToString();

                return rfactorloc;
            }
            catch (NullReferenceException)
            {
                return null;
            }
        }

        public virtual void StoreGameLocation(string loc)
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey("SOFTWARE", true);
            RegistryKey newkey = key.CreateSubKey("CarStat");
            newkey.SetValue("Startupdir-"+GAME, loc);
        }

        public abstract string FindGameLocation();

        public abstract void FillWithDefaultValues();

        public abstract string GetVehFolder();

        public abstract string GetSetupFolder();
    }
}
