﻿namespace CarStat_2.Setup_Manager
{
    partial class SetupManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetupManager));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnMoveSetup = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnAddSetup = new System.Windows.Forms.Button();
            this.btnClearEmptyFolder = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnRenameSetup = new System.Windows.Forms.Button();
            this.btnLoadSetup = new System.Windows.Forms.Button();
            this.btnCopyToClipboard = new System.Windows.Forms.Button();
            this.btnDeleteSetup = new System.Windows.Forms.Button();
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.gameEnvWidget1 = new CarStat_2.Windows.GameEnvWidget();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView1.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(9, 46);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(2);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersWidth = 16;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.ShowEditingIcon = false;
            this.dataGridView1.Size = new System.Drawing.Size(621, 428);
            this.dataGridView1.TabIndex = 4;
            // 
            // btnMoveSetup
            // 
            this.btnMoveSetup.Location = new System.Drawing.Point(5, 17);
            this.btnMoveSetup.Margin = new System.Windows.Forms.Padding(2);
            this.btnMoveSetup.Name = "btnMoveSetup";
            this.btnMoveSetup.Size = new System.Drawing.Size(130, 25);
            this.btnMoveSetup.TabIndex = 5;
            this.btnMoveSetup.Text = "Move";
            this.btnMoveSetup.UseVisualStyleBackColor = true;
            this.btnMoveSetup.Click += new System.EventHandler(this.btnMoveSetup_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.gameEnvWidget1);
            this.groupBox1.Controls.Add(this.btnRefresh);
            this.groupBox1.Controls.Add(this.btnAddSetup);
            this.groupBox1.Controls.Add(this.btnClearEmptyFolder);
            this.groupBox1.Location = new System.Drawing.Point(634, 10);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(143, 293);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "General Settings";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(5, 265);
            this.btnRefresh.Margin = new System.Windows.Forms.Padding(2);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(130, 24);
            this.btnRefresh.TabIndex = 12;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnAddSetup
            // 
            this.btnAddSetup.Location = new System.Drawing.Point(5, 237);
            this.btnAddSetup.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddSetup.Name = "btnAddSetup";
            this.btnAddSetup.Size = new System.Drawing.Size(130, 24);
            this.btnAddSetup.TabIndex = 11;
            this.btnAddSetup.Text = "Add Setup";
            this.btnAddSetup.UseVisualStyleBackColor = true;
            this.btnAddSetup.Click += new System.EventHandler(this.btnAddSetup_Click);
            // 
            // btnClearEmptyFolder
            // 
            this.btnClearEmptyFolder.Location = new System.Drawing.Point(5, 209);
            this.btnClearEmptyFolder.Margin = new System.Windows.Forms.Padding(2);
            this.btnClearEmptyFolder.Name = "btnClearEmptyFolder";
            this.btnClearEmptyFolder.Size = new System.Drawing.Size(130, 24);
            this.btnClearEmptyFolder.TabIndex = 9;
            this.btnClearEmptyFolder.Text = "Delete Empty Folders";
            this.btnClearEmptyFolder.UseVisualStyleBackColor = true;
            this.btnClearEmptyFolder.Click += new System.EventHandler(this.btnClearEmptyFolder_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnRenameSetup);
            this.groupBox2.Controls.Add(this.btnLoadSetup);
            this.groupBox2.Controls.Add(this.btnCopyToClipboard);
            this.groupBox2.Controls.Add(this.btnDeleteSetup);
            this.groupBox2.Controls.Add(this.btnMoveSetup);
            this.groupBox2.Location = new System.Drawing.Point(634, 307);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(143, 167);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Edit Setup";
            // 
            // btnRenameSetup
            // 
            this.btnRenameSetup.Location = new System.Drawing.Point(5, 107);
            this.btnRenameSetup.Margin = new System.Windows.Forms.Padding(2);
            this.btnRenameSetup.Name = "btnRenameSetup";
            this.btnRenameSetup.Size = new System.Drawing.Size(130, 25);
            this.btnRenameSetup.TabIndex = 9;
            this.btnRenameSetup.Text = "Rename Setup";
            this.btnRenameSetup.UseVisualStyleBackColor = true;
            this.btnRenameSetup.Click += new System.EventHandler(this.btnRenameSetup_Click);
            // 
            // btnLoadSetup
            // 
            this.btnLoadSetup.Location = new System.Drawing.Point(5, 137);
            this.btnLoadSetup.Margin = new System.Windows.Forms.Padding(2);
            this.btnLoadSetup.Name = "btnLoadSetup";
            this.btnLoadSetup.Size = new System.Drawing.Size(130, 25);
            this.btnLoadSetup.TabIndex = 8;
            this.btnLoadSetup.Text = "Load Setup";
            this.btnLoadSetup.UseVisualStyleBackColor = true;
            this.btnLoadSetup.Click += new System.EventHandler(this.btnLoadSetup_Click);
            // 
            // btnCopyToClipboard
            // 
            this.btnCopyToClipboard.Location = new System.Drawing.Point(5, 77);
            this.btnCopyToClipboard.Margin = new System.Windows.Forms.Padding(2);
            this.btnCopyToClipboard.Name = "btnCopyToClipboard";
            this.btnCopyToClipboard.Size = new System.Drawing.Size(130, 25);
            this.btnCopyToClipboard.TabIndex = 7;
            this.btnCopyToClipboard.Text = "Copy To Clipboard";
            this.btnCopyToClipboard.UseVisualStyleBackColor = true;
            this.btnCopyToClipboard.Click += new System.EventHandler(this.btnCopyToClipboard_Click);
            // 
            // btnDeleteSetup
            // 
            this.btnDeleteSetup.Location = new System.Drawing.Point(5, 47);
            this.btnDeleteSetup.Margin = new System.Windows.Forms.Padding(2);
            this.btnDeleteSetup.Name = "btnDeleteSetup";
            this.btnDeleteSetup.Size = new System.Drawing.Size(130, 25);
            this.btnDeleteSetup.TabIndex = 6;
            this.btnDeleteSetup.Text = "Delete";
            this.btnDeleteSetup.UseVisualStyleBackColor = true;
            this.btnDeleteSetup.Click += new System.EventHandler(this.btnDeleteSetup_Click);
            // 
            // tbSearch
            // 
            this.tbSearch.Location = new System.Drawing.Point(10, 10);
            this.tbSearch.Margin = new System.Windows.Forms.Padding(2);
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.Size = new System.Drawing.Size(621, 20);
            this.tbSearch.TabIndex = 8;
            this.tbSearch.TextChanged += new System.EventHandler(this.tbSearch_TextChanged);
            // 
            // gameEnvWidget1
            // 
            this.gameEnvWidget1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gameEnvWidget1.Change = null;
            this.gameEnvWidget1.Location = new System.Drawing.Point(5, 18);
            this.gameEnvWidget1.Name = "gameEnvWidget1";
            this.gameEnvWidget1.Size = new System.Drawing.Size(130, 178);
            this.gameEnvWidget1.TabIndex = 13;
            // 
            // SetupManager
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(783, 486);
            this.Controls.Add(this.tbSearch);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dataGridView1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "SetupManager";
            this.Text = "Setup Manager";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnMoveSetup;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnDeleteSetup;
        private System.Windows.Forms.Button btnLoadSetup;
        private System.Windows.Forms.Button btnCopyToClipboard;
        private System.Windows.Forms.Button btnClearEmptyFolder;
        private System.Windows.Forms.Button btnAddSetup;
        private System.Windows.Forms.TextBox tbSearch;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Button btnRenameSetup;
        private Windows.GameEnvWidget gameEnvWidget1;
    }
}