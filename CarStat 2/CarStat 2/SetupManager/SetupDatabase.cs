﻿using Misc;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarStat_2.Setup_Manager
{
    internal class SetupDatabase
    {
        private string rFactor2Loc;
        
        private const string SETUP_NAME_COLUMN = "Name";
        private const string SETUP_TRACK_COLUMN = "Track";
        private const string SETUP_VEHICLE_COLUMN = "Vehicle";
        private const string SETUP_DATE_COLUMN = "Date";
        private const string SETUP_ID_COLUMN = "ID";

        public DataTable db { get; private set; }
        public List<Setup> listOfSetups { get; private set; }

        public SetupDatabase(string rFactor2Location)
        {
            rFactor2Loc = rFactor2Location;
            db = new DataTable();
            listOfSetups = new List<Setup>();
            this.build();
        }

        public void setRfactor2Location(string rFactor2Location)
        {
            rFactor2Loc = rFactor2Location;
            this.build();
        }

        public void build()
        {
            db.Clear();
            listOfSetups.Clear();

            //Set up columns
            DataColumn idCol = new DataColumn(SETUP_ID_COLUMN, typeof(Int32));

            db.Columns.Add(idCol);
            db.Columns.Add(SETUP_NAME_COLUMN);
            db.Columns.Add(SETUP_TRACK_COLUMN);
            db.Columns.Add(SETUP_VEHICLE_COLUMN);
            db.Columns.Add(SETUP_DATE_COLUMN);
            
            db.PrimaryKey = new DataColumn[] { idCol };

            //Search for setups
            if (rFactor2Loc == "" || rFactor2Loc == null)
                return;

            recursiveAddingOfSetups(rFactor2Loc);

            string tempGarLoc = Path.Combine(Path.GetDirectoryName(rFactor2Loc), "tempGarage.svm");
            if (File.Exists(tempGarLoc))
                this.addSetup(tempGarLoc);
        }

        private void recursiveAddingOfSetups(string dir)
        {
            try
            {
                string[] files = Directory.GetFiles(dir);

                foreach (string f in files)
                {
                    this.addSetup(f);
                }

                //Adds the Directorys
                string[] dirs = Directory.GetDirectories(dir);

                foreach (string d in dirs)
                {
                    recursiveAddingOfSetups(d);
                }
            }
            catch
            {
                MessageBox.Show("An error has occured");
            }
        }

        public bool addSetup(string file)
        {
            if (Path.GetExtension(file).ToLower() == ".svm")
            {
                Setup setup = createSetup(file);

                DateTime date = File.GetLastWriteTime(setup.FileLocation);

                //Adding the setup to the db
                listOfSetups.Add(setup);
                DataRow row = db.NewRow();
                row[SETUP_NAME_COLUMN] = setup.Name;
                row[SETUP_TRACK_COLUMN] = setup.Track;
                row[SETUP_VEHICLE_COLUMN] = setup.VehicleClass;
                row[SETUP_DATE_COLUMN] = date.Year + "." + (date.Month < 10 ? "0"+date.Month : ""+date.Month) + "." + (date.Day < 10 ? "0" + date.Day : "" + date.Day);
                row[SETUP_ID_COLUMN] = listOfSetups.Count - 1;
                db.Rows.Add(row);
                return true;
            }
            else
            {
                return false;
            }
        }

        public void setRowFilter(string[] words)
        {
            string expression = "";
            for (int i = 0; i < words.Length; i++)
            {
                expression = expression + "(" +
                       SETUP_NAME_COLUMN + " LIKE " + "'%{" + i + "}%'" + " OR " +
                       SETUP_TRACK_COLUMN + " LIKE " + "'%{" + i + "}%'" + " OR " +
                       SETUP_VEHICLE_COLUMN + " LIKE " + "'%{" + i + "}%'" + " OR " +
                       SETUP_DATE_COLUMN + " LIKE " + "'%{" + i + "}%'" +
                       ")" + " AND ";
            }
            expression = expression.Substring(0, expression.Length - (" AND ".Length));

            db.DefaultView.RowFilter = string.Format(expression, words);

        }

        public static Setup createSetup(string file)
        {
            Setup setup = new Setup();
            setup.FileLocation = file;
            setup.Name = Path.GetFileNameWithoutExtension(file);
            setup.Track = Path.GetFileName(Path.GetDirectoryName(file));

            List<string> list = MiscFunctions.FileToList(file);
            setup.SetupValues = DataObjects.DataReader.ListToMultiLevelDict(list);

            Dictionary<string, string[]> dict = DataObjects.DataReader.ListToDict(list);
            setup.VehicleClass = MiscFunctions.getProperty(dict, "VehicleClassSetting", 0);

            return setup;
        }
    }
}
