using Misc;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarStat_2.Setup_Manager
{
    public partial class SetupManager : Form
    {
        private string setupFolder;
        private CarStat2 master;

        private SetupDatabase database;

        public SetupManager(CarStat2 master)
        {
            InitializeComponent();
            MiscFunctions.makeParentChildWindow(master, this);
            this.master = master;

            gameEnvWidget1.Prep(master, this);
            gameEnvWidget1.Change += new EventHandler(GameEnvChange_Listener);

            GameEnvChange_Listener(null, null);

            this.DragEnter += new DragEventHandler(dragEnter);
            this.DragOver += new DragEventHandler(dragOver);
            this.DragLeave += new EventHandler(dragLeave);
            this.DragDrop += new DragEventHandler(dragAndDrop);

            dataGridView1.Font = new Font(dataGridView1.Font.FontFamily, 7.8f);
            dataGridView1.SelectionChanged += new EventHandler(this.selectionChanged);

            this.Show();
            this.reloadTheTable();
        }

        public void reloadTheTable()
        {
            database = new SetupDatabase(setupFolder);

            dataGridView1.ClearSelection();
            dataGridView1.DataSource = database.db;

            dataGridView1.Columns[0].Visible = false; //makes the id column invisible
            dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

            selectionChanged(null, null);

            bool isInitialized = setupFolder != "";
            btnAddSetup.Enabled = isInitialized;
            btnClearEmptyFolder.Enabled = isInitialized;
            btnRefresh.Enabled = isInitialized;

            tbSearch_TextChanged(null, null);
        }

        //Darg and drop

        private void dragEnter(object sender, DragEventArgs e)
        {
            if (GetFileNames(e) != null)
                e.Effect = DragDropEffects.Copy;
        }

        private void dragOver(object sender, DragEventArgs e)
        {

        }

        private void dragLeave(object sender, EventArgs e)
        {

        }

        private void dragAndDrop(object sender, DragEventArgs e)
        {
            string[] files = GetFileNames(e);

            if (files == null)
                return;

            List<Setup> list = new List<Setup>();

            for (int i = 0; i < files.Length; i++)
            {
                list.Add(SetupDatabase.createSetup(files[i]));
            }

            new MoveSetupDialog(this, list, setupFolder, false);
        }

        private string[] GetFileNames(DragEventArgs e)
        {
            string[] arr = (string[])e.Data.GetData("FileName");

            if (arr == null)
                return null;

            List<string> list = new List<string>();
            for (int i = 0; i < arr.Length; i++)
            {
                if (Path.GetExtension(arr[i]).ToLower() == ".svm")
                {
                    list.Add(Path.GetFullPath(arr[i]));
                }

            }

            string[] name = new string[list.Count];
            for (int i = 0; i < name.Length; i++)
            {
                name[i] = list[i];
            }

            if (name.Length == 0)
                return null;

            return name;
        }

        private void GameEnvChange_Listener(object sender, EventArgs e)
        {
            if (master.gameEnv != null && master.gameEnv.GetGameLocation() != "")
            {
                string gameFolder = master.gameEnv.GetGameLocation();
                string myDocuments = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

                switch (master.gameEnv.GAME)
                {
                    case "RF2":
                        setupFolder = Path.Combine(gameFolder, @"UserData\player\Settings");
                        break;

                    case "RF1":
                        setupFolder = null;
                        MessageBox.Show("This game is sadly not supported in the SetupManager", "This game is not supported", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        break;

                    case "GTR":
                        setupFolder = null;
                        MessageBox.Show("This game is sadly not supported in the SetupManager", "This game is not supported", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        break;

                    case "AMS":
                        setupFolder = Path.Combine(myDocuments, @"Automobilista\userdata");
                        if (!Directory.Exists(setupFolder))
                            break;

                        List<string> list = new List<string>(Directory.GetDirectories(setupFolder));

                        for (int i = 0; i < list.Count; i++)
                        {
                            if (list[i].Contains("LOG") || list[i].Contains("CONTROLLER"))
                            {
                                list.RemoveAt(i);
                                i--;
                            }
                            else
                            {
                                list[i] = Path.GetFileName(list[i]); //Profile Name
                            }
                        }

                        if (list.Count == 0)
                        {
                            setupFolder = null;
                            break;
                        }

                        if (list.Count > 1 && (master.gameEnv.Profile != "" && master.gameEnv.Profile != null))
                        {
                            setupFolder = Path.Combine(setupFolder, master.gameEnv.Profile);
                        }
                        else
                        {
                            setupFolder = Path.Combine(setupFolder, list[0]);
                        }

                        setupFolder = Path.Combine(setupFolder, @"Settings");
                        break;

                    default:
                        setupFolder = null;
                        MessageBox.Show("This game is sadly not supported in the SetupManager", "This game is not supported", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        break;
                }


            }
            else
            {
                setupFolder = null;
            }

            if (setupFolder == null || !Directory.Exists(setupFolder))
                setupFolder = "";

            this.reloadTheTable();
        }

        private void selectionChanged(object sender, EventArgs e)
        {
            bool selected = dataGridView1.SelectedRows.Count > 0;

            btnMoveSetup.Enabled = selected;
            btnDeleteSetup.Enabled = selected;
            btnCopyToClipboard.Enabled = selected;

            btnRenameSetup.Enabled = dataGridView1.SelectedRows.Count == 1;
            btnLoadSetup.Enabled = (dataGridView1.SelectedRows.Count == 1 && master.getCarData.HDVehicleData.Values != null);
        }

        private void btnClearEmptyFolder_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("This action will delete empty folders in your UserData\\player\\Settings." + "\n" +
                "No setups or any other file will be lost." + "\n" +
                "This is usefull to delete folders for tracks that are not installed anymore." + "\n" +
                "The Folders will be created again when you start rf2 with the track installed", "Deleting Empty Folders", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (result == DialogResult.Yes)
            {
                List<string> folders = new List<string>();
                folders.Add(setupFolder);

                for (int i = 0; i < folders.Count; i++)
                {
                    string value = folders[i];

                    string[] subDirs = Directory.GetDirectories(value);
                    string[] files = Directory.GetFiles(value);

                    if (subDirs.Length == 0 && files.Length == 0)
                    {
                        //This is delete
                        Directory.Delete(value);

                        //deletes the now empty super Folders
                        while (value != Path.GetDirectoryName(setupFolder))
                        {
                            value = Path.GetDirectoryName(value);

                            if (Directory.GetDirectories(value).Length == 0 && Directory.GetFiles(value).Length == 0)
                            {
                                Directory.Delete(value);
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                    else if (subDirs.Length > 0)
                    {
                        //Adds sub folders
                        foreach (string d in subDirs)
                            folders.Add(d);
                    }
                }
            }
        }

        private void btnAddSetup_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Filter = master.gameEnv.GAME + " Setup Files (*.svm)|*.svm";
            fileDialog.RestoreDirectory = true;
            fileDialog.Multiselect = true;

            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                string[] files = fileDialog.FileNames;

                List<Setup> setups = new List<Setup>();
                for (int i = 0; i < files.Length; i++)
                {
                    setups.Add(SetupDatabase.createSetup(files[i]));
                }

                new MoveSetupDialog(this, setups, setupFolder, false);
            }
        }

        private void btnMoveSetup_Click(object sender, EventArgs e)
        {
            List<Setup> setups = getSelected();

            if (setups == null)
                return;

            new MoveSetupDialog(this, setups, setupFolder, true);
        }

        private void btnDeleteSetup_Click(object sender, EventArgs e)
        {
            List<Setup> setups = getSelected();

            if (setups == null)
                return;

            DialogResult result = MessageBox.Show("Warning! This action will delete the selected " + setups.Count + " Setups!" + "\n" +
                "If you delete them, they are gone for good", "Warning! Deleting Setups", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);

            if (result == DialogResult.OK)
            {
                foreach (Setup set in setups)
                {
                    File.Delete(set.FileLocation);
                }
                this.reloadTheTable();
            }

        }

        private void btnCopyToClipboard_Click(object sender, EventArgs e)
        {
            List<Setup> setups = getSelected();

            if (setups == null)
                return;

            string[] paths = new string[setups.Count];

            int i = 0;
            foreach (Setup set in setups)
            {
                paths[i] = set.FileLocation;
                i++;
            }

            Clipboard.SetDataObject(new DataObject(DataFormats.FileDrop, paths), true);
        }

        private void btnLoadSetup_Click(object sender, EventArgs e)
        {
            List<Setup> setups = getSelected();

            if (setups == null || master.getCarData.HDVehicleData.Values == null)
                return;

            if (setups.Count != 1)
                return;

            Setup set = setups[0];

            string text = master.getCarData.VEHicleData.Values["classes"][0].Replace(",", "");
            bool match = text.Contains(set.VehicleClass);

            if (!match)
            {
                DialogResult result = MessageBox.Show("Warning! The selected Setup is not for the loaded vehicle!" + "\n"
                    + "CarStat can try to load this setup onto this car, but it will very likely create a useless setup." + "\n"
                    + "To do this select okay." + "\n"
                    + "If you want to load the setup properly, press cancel and load a car of the class " + set.VehicleClass,
                    "Car and Setup Mismatch", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);

                match = result == DialogResult.OK;
            }

            if (match)
            {
                master.gameEnv.SetupCreator.setSetup(set);
                this.Dispose();
            }

        }

        private List<Setup> getSelected()
        {
            DataGridViewSelectedRowCollection rows = dataGridView1.SelectedRows;

            //Avoids crash because of nothing selected
            if (rows == null || rows.Count == 0)
                return null;

            List<Setup> setupsSelected = new List<Setup>();
            for (int i = 0; i < rows.Count; i++)
            {
                object value = rows[i].Cells[0].Value;

                if (value.GetType() == typeof(int))
                {
                    int id = (int)value;
                    setupsSelected.Add(database.listOfSetups[id]);
                }
            }

            return setupsSelected;
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            this.reloadTheTable();
        }

        private void tbSearch_TextChanged(object sender, EventArgs e)
        {
            string text = tbSearch.Text.Trim().Replace("'", " ").Replace("&", " ");
            if (text == "")
            {
                database.db.DefaultView.RowFilter = string.Empty;
                return;
            }


            string[] words = text.Split(' ');
            database.setRowFilter(words);
        }

        private void btnRenameSetup_Click(object sender, EventArgs e)
        {
            List<Setup> setups = getSelected();

            if (setups == null)
                return;

            if (setups.Count != 1)
                return;

            Setup set = setups[0];

            new RenameSetup(this, set).setMode(false);
        }
    }
}
