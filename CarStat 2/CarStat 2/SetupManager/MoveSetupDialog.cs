﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarStat_2.Setup_Manager
{
    public partial class MoveSetupDialog : Form
    {
        private List<Setup> setups;
        private string setupFolder;
        private SetupManager master;

        //Used for save setup out of the setupCreator
        private CarStat2 carStat;

        private string[] dirs; 

        public MoveSetupDialog(SetupManager master, List<Setup> setups, string setupFolder, bool allowCut)
        {
            InitializeComponent();
            this.setups = setups;
            this.setupFolder = setupFolder;
            this.master = master;

            this.Owner = master;
            this.chkCopy.Visible = allowCut;
            this.chkCopy.Enabled = allowCut;

            this.tbSearch_TextChanged(null, null);
            this.Show();

            btnMove.Enabled = false;

            if (!allowCut)
            {
                btnMove.Text = "Add";
                this.Text = "Add Setup";
            }
        }

        public MoveSetupDialog(CarStat2 master, Setup setup, string setupFolder)
        {
            InitializeComponent();
            List<Setup> set = new List<Setup>();
            set.Add(setup);

            this.setups = set;
            this.setupFolder = setupFolder;
            this.carStat = master;

            this.Owner = carStat;

            this.chkCopy.Visible = false;
            this.chkCopy.Enabled = false;

            this.tbSearch_TextChanged(null, null);
            this.Show();

            btnMove.Enabled = false;

            btnMove.Text = "Save";
            this.Text = "Save Setup";
        }

        private void tbSearch_TextChanged(object sender, EventArgs e)
        {
            string[] dirs = Directory.GetDirectories(setupFolder);

            //Limiting the number of displayed
            string search = tbSearch.Text.Trim();
            if (search != "")
            {
                List<string> list = new List<string>();

                foreach (string d in dirs)
                {
                    if (Path.GetFileName(d).ToLower().Contains(search.ToLower()))
                    {
                        list.Add(d);
                    } 
                }

                dirs = new string[list.Count];
                for (int i = 0; i < dirs.Length; i++)
                    dirs[i] = list[i];
            }

            //Setting up the list for the display
            string[] names = new string[dirs.Length];
            for (int i = 0; i < dirs.Length; i++)
                names[i] = Path.GetFileName(dirs[i]);

            //Displaying
            listBox1.Items.Clear();
            foreach (string n in names)
                listBox1.Items.Add(n);

            btnMove.Enabled = false;
            this.dirs = dirs;
        }

        private void btnMove_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex < 0 || dirs.Length == 0)
                return;

            string directory = dirs[listBox1.SelectedIndex];

            foreach (Setup set in setups)
            {
                string destiny = Path.Combine(directory, set.Name + ".svm");
                bool process = true;

                if (File.Exists(destiny) && carStat == null)
                {
                    int i = 1;
                    string option = destiny;
                    while (File.Exists(option))
                    {
                        option = Path.Combine(directory, set.Name + i + ".svm");
                        i++;
                    }

                    DialogResult result = MessageBox.Show("While tranfering setup " + set.Name +
                        " an conflict has occured, as there is already a " + set.Name + " in the destinitation folder." + "\n\n\n" +
                        "You can eighter overright the setup in the desination by selection yes," + "\n" +
                        "you can let the new setup be renamed as " + Path.GetFileNameWithoutExtension(option) + " by selecting no" + "\n" +
                        "or you can skip this setup by selecting cancel", "Setup File Conflict", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

                    if (result == DialogResult.Yes)
                    {
                        File.Delete(destiny);
                    }
                    else if (result == DialogResult.No)
                    {
                        destiny = option;
                    }
                    else
                    {
                        process = false;
                    }
                }
                else if (File.Exists(destiny) && carStat != null)
                {
                    //As the file has a random name anyway, we just extend it. This function should never be called, but could be potentialy
                    int i = 1;
                    string option = destiny;
                    while (File.Exists(option))
                    {
                        option = Path.Combine(directory, set.Name + i + ".svm");
                        i++;
                    }

                    destiny = option;
                }

                try
                {
                    if (process)
                    {
                        if (carStat != null)
                        {
                            //Importing the setup
                            File.Move(set.FileLocation, destiny);
                            new RenameSetup(carStat, SetupDatabase.createSetup(destiny)).setMode(true);
                        }
                        else
                        {
                            //Standard
                            if (chkCopy.Checked)
                                File.Copy(set.FileLocation, destiny);
                            else
                                File.Move(set.FileLocation, destiny);
                        }
                    }
                        
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Something went wrong with your setup transfer of setup"+ set.Name +".\n"+ex.StackTrace, "Setup transfer error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

                
            }

            this.Dispose();

            if (master != null)
                this.master.reloadTheTable();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnMove.Enabled = true;
        }

        private void chkCopy_CheckedChanged(object sender, EventArgs e)
        {
            if (chkCopy.Checked)
                btnMove.Text = "Copy";
            else
                btnMove.Text = "Move";
        }
    }
}
