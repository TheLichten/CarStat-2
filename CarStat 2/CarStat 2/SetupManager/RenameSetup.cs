﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace CarStat_2.Setup_Manager
{
    public partial class RenameSetup : Form
    {

        private Setup setup;
        private bool mode = false;

        public RenameSetup(Form master, Setup setup)
        {
            InitializeComponent();

            tbName.KeyPress += new KeyPressEventHandler(enterPressed);

            this.Owner = master;
            this.setup = setup;
            tbName.Text = setup.Name;

            if (!File.Exists(setup.FileLocation))
                throw new ArgumentException("Setup File " + setup.FileLocation + " does not exist");

            this.Show();
        }

        public void setMode(bool mode)
        {
            if (mode)
            {
                this.Text = "Give your Setup a Name";
                tbName.Text = "";
                this.FormClosed += new FormClosedEventHandler(deletingNotImportedSetup);
                mode = true;
            }
            else
            {
                this.Text = "Rename Setup";
                mode = false;
            }
        }

        private void deletingNotImportedSetup(object sender, FormClosedEventArgs args)
        {
            File.Delete(setup.FileLocation);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            //If there is even change
            if (tbName.Text != setup.Name || mode)
            {
                string dir = Path.GetDirectoryName(setup.FileLocation);
                string filename = tbName.Text + ".svm";

                string nextFileLoc = Path.Combine(dir, filename);

                //Conflict
                if (File.Exists(nextFileLoc))
                {
                    DialogResult result = MessageBox.Show("The name " + tbName.Text + " is already used by another setup in the same folder.\nDo you want to override that setup?",
                        "Naming Conflict! Override?", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);

                    if (result == DialogResult.OK)
                    {
                        try
                        {
                            File.Delete(nextFileLoc);
                        }
                        catch (Exception)
                        {
                            MessageBox.Show("File could not be overwritten!\n\nEnsure no other programm is reading this file at the same time as this program or choose another name", "Operation Overwritte Failed!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                    }
                    else
                    {
                        return;
                    }
                }

                //Writing

                try
                {
                    File.Move(setup.FileLocation, nextFileLoc);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("File could not be renamed!\n" + ex.Message + "\n" + ex.StackTrace, "Renaming Failed!", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    try
                    {
                        if (File.Exists(nextFileLoc))
                            File.Delete(nextFileLoc);
                    }
                    catch
                    {
                    }

                    return;
                }
            }

            if (this.Owner is SetupManager)
                ((SetupManager)this.Owner).reloadTheTable();

            this.Visible = false;
            this.Dispose();
            
        }

        private void enterPressed(object sender, KeyPressEventArgs e)
        {
            if (btnSave.Enabled == false)
                return;

            if (e.KeyChar == '\r')
            {
                btnSave_Click(null, null);
            }
        }

        private void tbName_TextChanged(object sender, EventArgs e)
        {
            if (tbName.Text.Replace(" ", "").Length == 0)
                btnSave.Enabled = false;
            else
                btnSave.Enabled = true;
        }
    }
}
