﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Collections;
using Misc;

namespace CarStat_2.DataObjects
{
    public class CarData
    {
        public bool corrected = false;

        private string vehlocation = null;
        private string vehicleRoot = null;
        private string maslocation = null;

        private string iconPath = null;

        public EngineData EngineData { get; private set; }
        public VEHicleData VEHicleData { get; private set; }
        public HDVehicleData HDVehicleData { get; private set; }
        public Dictionary<string, TireCompoundData> TireCompounds { get; private set; }
        public Gearing Gearing { get; internal set; }

        private string hdvlocation = null;
        private string engineLocation = null;

        
        private string tyrelocation = null;
        private string upgradelocation = null;
        
        public List<string> TyreFile { get; set; }
        public List<string> UpgradeFile { get; set; }
        public EngineUpgrade TheEngineUpgrade { get; set; }

        public CarData()
        {
            this.Reset();
        }

        public void Reset()
        {
            corrected = false;
            vehlocation = null;
            maslocation = null;
            vehicleRoot = null;

            VEHicleData = new VEHicleData();
            EngineData = new EngineData();
            HDVehicleData = new HDVehicleData();
            TireCompounds = new Dictionary<string, TireCompoundData>();
            Gearing = new Gearing();

            this.SetIcon(null);

            vehicleRoot = null;
            hdvlocation = null;
            engineLocation = null;
            tyrelocation = null;
            
            TyreFile = null;
            upgradelocation = null;
            UpgradeFile = null;
            TheEngineUpgrade = new EngineUpgrade(1,1);
        }

        public void SetIcon(string iconPath)
        {
            if (this.iconPath != null)
            {
                try
                {
                    System.IO.File.Delete(this.iconPath);
                }
                catch
                {

                }
                
            }
            this.iconPath = iconPath;
        }

        public Image GetIcon()
        {
            try
            {
                return System.Drawing.Image.FromFile(iconPath);
            }
            catch
            {
                return null;
            }
        }

        public string VehicleRoot
        {
            get { return vehicleRoot; }
            set { vehicleRoot = value; }
        }

        
        public string VEHlocation
        {
            get { return vehlocation; }
            set
            {
                vehlocation = value;
                vehicleRoot = value.Substring(0, (vehlocation).LastIndexOf(@"\"));
            }
        }


        public string MASlocation
        {
            get { return maslocation; }
            set { maslocation = value; }
        }

        public string HDVlocation
        {
            get { return hdvlocation; }
            set { hdvlocation = value; }
        }
        public string EngineFileLocation
        {
            get { return engineLocation; }
            set { engineLocation = value; }
        }
        public string Tyrelocation
        {
            get { return tyrelocation; }
            set { tyrelocation = value; }
        }
        public string Upgradelocation
        {
            get { return upgradelocation; }
            set { upgradelocation = value; }
        }
        
    }

    public class EngineUpgrade
    {
        public double GeneralTorqueMult { get; set; }
        public double GeneralPowerMult { get; set; }

        public Dictionary<string, string[]> dict { get; set; }

        public EngineUpgrade(double generalTorqueMult, double generalPowerMult)
        {
            GeneralTorqueMult = generalTorqueMult;
            GeneralPowerMult = generalPowerMult;
            dict = new Dictionary<string, string[]>();
        }
    }



    public class FormSizeData
    {
        public int WindowWidth { get; set; }
        public int WindowHeight { get; set; }
        public int ExpandSize { get; set; }
        public int WindowHeightDefault { get; set; }
        public int WindowWidthDefault { get; set; }
        public Point UpgradeStartPos { get; set; }
    }

    public class Gearing
    {
        public Gear[] ForwardGears;
        public Gear ReverseGear;
        public Gear FinalDrive;

        public bool gearChange;
        public bool finalDriveChange;

        public string gearFileLoc;
        public Dictionary<string, Dictionary<string, string[]>> gearFileValue;

        public class Gear
        {
            public int number; //Which gear this is. 0 is final drive, -1 reverse
            public Ratio[] ratios;
            public int offset; //Used to translate the settings over to the postion in the ratios array
            public int defaultSetting = 0;
        }

        public class Ratio
        {
            public Ratio Bevel;
            public int value;
            public int basis;

            public double getDecimal()
            {
                double dec = ((double)value)/ ((double)basis);

                if (Bevel != null)
                    dec = dec * Bevel.getDecimal();

                return dec;
            }

            public bool equals(Ratio ratio)
            {
                if (ratio == null)
                    return false;

                if (Bevel == null ^ ratio.Bevel == null)
                    return false;

                if (Bevel != null)
                {
                    if (!Bevel.equals(ratio.Bevel))
                        return false;
                }

                return value == ratio.value && basis == ratio.basis;
            }
        }
    }

    public class TireCompoundData
    {
        public string Compound { get; set; }

        public bool FrontRear { get; set; }

        public List<string> TGMFile1 { get; set; }
        public List<string> TGMFile2 { get; set; }

        public Dictionary<string, Dictionary<string, string[]>> TGMValues1 { get; set; }
        public Dictionary<string, Dictionary<string, string[]>> TGMValues2 { get; set; }
    }

    public class HDVehicleData
    {
        public List<string> HDVfile { get; set; }

        public Dictionary<string, Dictionary<string, string[]>> Values { get; set; }
    }

    //The Data inside the vehicle File
    public class VEHicleData
    {
        public List<string> VEHfile { get; set; }

        public Dictionary<string, string[]> Values { get; set; }

        
    }

    //Contains the engine data
    public class EngineData
    {
        private double maxtorque = 0;
        private double maxpower = 0;
        private double torquerpm = 0;
        private double powerrpm = 0;
        public double GeneralPowerMult { get; set; }
        public double GeneralTorqueMult { get; set; }

        public List<string> EngineFile { get; set; }

        public Dictionary<string, string[]> Values { get; set; }

        public double fuelAirMixtureTorqueMulti(double fuel)
        {
            double tableStart = Double.Parse(MiscFunctions.getProperty(Values, "FuelAirMixtureTable", 0));
            double tableStep = Double.Parse(MiscFunctions.getProperty(Values, "FuelAirMixtureTable", 1));

            double tablePostion = (fuel - tableStart) / tableStep;

            //Value is exactly a value out of the table
            if (((double)((int)tablePostion)) == tablePostion)
            {
                return Double.Parse(MiscFunctions.getProperty(Values, "FuelAirMixtureEffects", ((int) tablePostion), 0));
            }

            double tablePer = Double.Parse(MiscFunctions.getProperty(Values, "FuelAirMixtureEffects", ((int)tablePostion), 0));
            double tablePost = Double.Parse(MiscFunctions.getProperty(Values, "FuelAirMixtureEffects", ((int)tablePostion)+1, 0));

            return MiscFunctions.calcValueBetweenTwoValues(tablePer, tablePost, tablePostion - ((double)((int)tablePostion)));
        }

        public double Maxtorque
        {
            get { return maxtorque; }
            set { maxtorque = Math.Round(value, 1); }
        }
        public double Maxpower
        {
            get { return maxpower; }
            set { maxpower = value; }
        }
        public double Torquerpm
        {
            get { return torquerpm; }
            set { torquerpm = Math.Round(value, 0); }
        }
        public double Powerrpm
        {
            get { return powerrpm; }
            set { powerrpm = Math.Round(value, 0); }
        }

        public void resetMax()
        {

            maxtorque = 0;
            maxpower = 0;
            torquerpm = 0;
            powerrpm = 0;
        }
    }
}
