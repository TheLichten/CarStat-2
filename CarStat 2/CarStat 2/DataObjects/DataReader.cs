﻿using Misc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarStat_2.DataObjects
{
    public class DataReader
    {
        protected string tempFolder;

        protected GameEnvironmentFunc gameEnv;

        public DataReader(GameEnvironmentFunc gameEnv)
        {
            this.gameEnv = gameEnv;

            this.tempFolder = MiscFunctions.tempFolder;
        }

        #region Main functions

        //Loads all the files that are found in the VEHicle and HDV file
        public virtual void loadFiles(bool newfile)
        {
            if (gameEnv.Data.VEHlocation != null)
                this.tempFolder = Path.Combine(MiscFunctions.tempFolder, Path.GetFileNameWithoutExtension(gameEnv.Data.VEHlocation));

            readFilePreparation();
            Application.DoEvents();


            //Read VEHicle file:
            if (gameEnv.Data.MASlocation != "" && gameEnv.Data.MASlocation != null)
            {
                //If the user checked the "only non-encrypted"-option -> check if the file is not encrypted. If it is, it's game over
                if (gameEnv.LoadReadEncrytedFiles() && MiscFunctions.CheckIfEncrypted(gameEnv.Data.MASlocation, Path.GetFileName(gameEnv.Data.VEHlocation)) == true) //the file is encrypted
                {
                    throw new FileNotFoundException("File Encryted");
                }
                MiscFunctions.ExtractMAS(gameEnv.Data.MASlocation, Path.GetFileName(gameEnv.Data.VEHlocation), tempFolder);
                gameEnv.Data.VEHicleData.VEHfile = MiscFunctions.FileToList(Path.Combine(tempFolder, Path.GetFileName(gameEnv.Data.VEHlocation)));
            }
            else //Vehicle-file not in MAS-file
            {
                gameEnv.Data.VEHicleData.VEHfile = MiscFunctions.FileToList(gameEnv.Data.VEHlocation);
            }
            gameEnv.Data.VEHicleData.Values = ListToDict(gameEnv.Data.VEHicleData.VEHfile);


            Thread iconThread = new Thread(this.readIcon);
            iconThread.Name = "IconThread-" + Thread.CurrentThread.Name;
            iconThread.Start();

            this.findHDV();

            if (gameEnv.Data.HDVlocation == null)
            {
                switch (gameEnv.GAME)
                {
                    case "AMS":
                        throw new Exception("HDV file not found. HDV files of Base Game are hidden, and therefore you can not open them (but you can open the TBC and Engine Files).\nIf this is not base game content, then try another vehicle file");
                    case "RF2":
                        throw new Exception("HDV file not found. DLC content can not be opened.\nIf this is not dlc content, then try another vehicle file");
                    default:
                        throw new Exception("HDV file not found. Try another vehicle file.");
                }
            }
                
            gameEnv.Data.HDVehicleData.HDVfile = MiscFunctions.FileToList(gameEnv.Data.HDVlocation);
            gameEnv.Data.HDVehicleData.Values = ListToMultiLevelDict(gameEnv.Data.HDVehicleData.HDVfile);
            readEngineMultipliers();

            //Read Upgrade file:
            string upgradeFileName = MiscFunctions.getProperty(gameEnv.Data.VEHicleData.Values, "Upgrades", 0);
            if (upgradeFileName != "n/a" && upgradeFileName != null)
            {
                if (!upgradeFileName.ToLower().Contains(".ini"))
                    upgradeFileName = upgradeFileName + ".ini";

                gameEnv.Data.Upgradelocation = DirSearch(gameEnv.Data.VehicleRoot, upgradeFileName);
                if (gameEnv.Data.Upgradelocation != null)
                {
                    if (File.Exists(gameEnv.Data.Upgradelocation))
                    {
                        gameEnv.Data.UpgradeFile = MiscFunctions.FileToList(gameEnv.Data.Upgradelocation);
                    }

                    //throw new Exception("Upgrade file not found. Try another vehicle file.");
                }
            }
        }

        protected virtual void findHDV()
        {
            //Read HDV file:
            string hdvFileName = MiscFunctions.getProperty(gameEnv.Data.VEHicleData.Values, "hdvehicle", 0);
            if (hdvFileName != null && !hdvFileName.ToLower().Contains(".hdv"))
                hdvFileName = hdvFileName + ".hdv";


            string hdvfile = DirSearch(gameEnv.Data.VehicleRoot, hdvFileName);
            if (hdvfile == "encrypted")
            {
                throw new FileNotFoundException("File Encryted");
            }
            gameEnv.Data.HDVlocation = hdvfile;
        }

        protected virtual void readEngineMultipliers()
        {
            gameEnv.Data.EngineData.GeneralTorqueMult = 1;
            gameEnv.Data.EngineData.GeneralPowerMult = 1;

            string temp = MiscFunctions.getProperty(gameEnv.Data.HDVehicleData.Values, "engine", "GeneralTorqueMult", 0);
            if (temp != null && temp != "n/a")
                gameEnv.Data.EngineData.GeneralTorqueMult = Double.Parse(temp);
            temp = MiscFunctions.getProperty(gameEnv.Data.HDVehicleData.Values, "engine", "GeneralPowerMult", 0);
            if (temp != null && temp != "n/a")
                gameEnv.Data.EngineData.GeneralPowerMult = Double.Parse(temp);
        }



        //Read the engine file(s)
        internal virtual void readEngineFile()
        {
            string engineFileName = MiscFunctions.getProperty(gameEnv.Data.HDVehicleData.Values, "engine", "Normal", 0);
            if (Path.GetExtension(engineFileName).ToLower() != ".ini")
                engineFileName += ".ini";
            string engineFileLoc = DirSearch(gameEnv.Data.VehicleRoot, engineFileName);
            if (engineFileLoc == "encrypted")
            {
                throw new FileNotFoundException("File Encryted");
            }

            readEngineFile(engineFileLoc);
        }

        internal virtual void readEngineFile(string engineFileLoc)
        {
            gameEnv.Data.EngineFileLocation = engineFileLoc;
            if (gameEnv.Data.EngineFileLocation == null)
                throw new Exception("Engine file not found. Try another vehicle file.");
            gameEnv.Data.EngineData.EngineFile = MiscFunctions.FileToList(gameEnv.Data.EngineFileLocation);

            //Fix Woochoo bug
            gameEnv.Data.EngineData.EngineFile[0] = gameEnv.Data.EngineData.EngineFile[0].Replace(Path.GetFileName(gameEnv.Data.EngineFileLocation), "");

            gameEnv.Data.EngineData.Values = ListToDict(gameEnv.Data.EngineData.EngineFile);
        } 

        internal void readTireFile()
        {
            try
            {
                readTireFile(false);
            }
            catch (Exception)
            {

            }
            
        }

        //Read the tire file
        internal virtual void readTireFile(bool OnlyTBC)
        {

            if (OnlyTBC == false)
            {
                string tyreFileName = MiscFunctions.getProperty(gameEnv.Data.HDVehicleData.Values, "general", "TireBrand", 0);
                if (Path.GetExtension(tyreFileName).ToLower() != ".tbc")
                    tyreFileName += ".tbc";

                string tireLoc = DirSearch(gameEnv.Data.VehicleRoot, tyreFileName);
                if (tireLoc == "encrypted")
                {
                    throw new Exception("File Encryted");
                }
                gameEnv.Data.Tyrelocation = tireLoc;
                if (gameEnv.Data.Tyrelocation == null)
                    throw new Exception("Tyre file not found. Try another vehicle file.");
            }
            gameEnv.Data.TyreFile = MiscFunctions.FileToListChop(gameEnv.Data.Tyrelocation);

            //Read the TGM files and place them in gameEnv.Data.TGMfiles
            gameEnv.Data.TireCompounds.Clear();
            List<Thread> tireReadingThreads = new List<Thread>();
            for (int i = 0; i < gameEnv.Data.TyreFile.Count; i++) //Bad construction, historically grown. It works though, so /care
            {
                if (gameEnv.Data.TyreFile[i].Contains("[COMPOUND]"))
                {
                    while (gameEnv.Data.TyreFile[i].ToLower().Contains("name=") == false) //Skip to the Tire name
                        i++;
                    string compound = gameEnv.Data.TyreFile[i];
                    compound = compound.Substring(compound.IndexOf('=') + 1).Replace("\"", "");

                    TireCompoundData tire = new TireCompoundData();
                    tire.Compound = compound;

                    //TireDataReader
                    TireDataReader reader = new TireDataReader(gameEnv.Data, i, tire, !gameEnv.LoadReadEncrytedFiles(), tempFolder);
                    Thread thread = new Thread(reader.readData);
                    thread.Name = "TireDataReading-" + tire.Compound;
                    thread.Start();
                    tireReadingThreads.Add(thread);

                    //Tire Data is read for one tire set
                    gameEnv.Data.TireCompounds.Add(tire.Compound, tire);
                }
            }
            foreach (Thread thread in tireReadingThreads)
                thread.Join();
        }

        //Is there for async reading
        protected class TireDataReader
        {
            private CarData data;
            private int startpoint;
            private TireCompoundData tire;
            private bool onlyNonEncrypted;
            private string tempFolder;

            protected TireDataReader(bool onlyNonEncrypted, string tempFolder)
            {

            }

            public TireDataReader(CarData data, int startpoint, TireCompoundData tire, bool onlyNonEncrypted, string tempFolder)
            {
                this.data = data;
                this.startpoint = startpoint;
                this.tire = tire;
                this.onlyNonEncrypted = onlyNonEncrypted;
                this.tempFolder = tempFolder;
            }

            public virtual void readData()
            {
                int i = startpoint;
                while ((data.TyreFile[i + 1].Contains("[COMPOUND]") == false) && ((i + 2) < data.TyreFile.Count))
                {
                    string line = data.TyreFile[i].ToLower().Trim();
                    if (line.Contains("front:") || line.Contains("rear:") || line.Contains("all:") || line.Contains("left:") || line.Contains("right:"))
                    {
                        while (!data.TyreFile[i].ToLower().Contains("tgm=")) //Skip to the TGM-section
                        {
                            i++;
                            if (!data.TyreFile[i].Contains("[COMPOUND]") && i < data.TyreFile.Count)
                                break;
                        } 

                        string fileloctemp = null;
                        string tgmLocation = data.TyreFile[i].Substring(data.TyreFile[i].IndexOf("=") + 1).Replace("\"", "");
                        if (tgmLocation.ToLower().EndsWith(".tgm") == false) //Add the TGM-extension
                            tgmLocation += ".TGM";

                        fileloctemp = DirSearch(data.VehicleRoot, tgmLocation, onlyNonEncrypted, tempFolder);
                        if (fileloctemp == "encrypted")
                        {
                            throw new FileNotFoundException("File Encryted");
                        }

                        if (fileloctemp != null)
                        {
                            List<string> listTGM = MiscFunctions.FileToListTGM(fileloctemp);
                            Dictionary<string, Dictionary<string, string[]>> dictTGM = ListToMultiLevelDict(listTGM);

                            //Adding missing values:
                            //dictTGM = addDefaultValues(dictTGM, CarStat2.defaultTGM);

                            //Ordering it in correctly
                            if (line.Contains("front:") || line.Contains("all:") || line.Contains("right:"))
                            {
                                tire.TGMFile1 = listTGM;
                                tire.TGMValues1 = dictTGM;
                            }


                            if (line.Contains("rear:") || line.Contains("all:") || line.Contains("left:"))
                            {
                                tire.TGMFile2 = listTGM;
                                tire.TGMValues2 = dictTGM;
                            }

                            //Switching left right support on
                            if (line.Contains("left:") || line.Contains("right:"))
                                tire.FrontRear = false;
                            else
                                tire.FrontRear = true;
                        }
                    }
                    i++;
                }

                if (tire.TGMFile2 == null)
                {
                    tire.TGMFile2 = tire.TGMFile1;
                    tire.TGMValues2 = tire.TGMValues1;
                }
                    
            }
        }

        protected virtual void readIcon()
        {
            string iconPath = Path.Combine(Path.GetDirectoryName(gameEnv.Data.VEHlocation), Path.GetFileNameWithoutExtension(gameEnv.Data.VEHlocation) + "icon.dds");
            bool temp = false;

            if (gameEnv.Data.MASlocation != "" && gameEnv.Data.MASlocation != null)
            {
                //Reading the icon
                MiscFunctions.ExtractMAS(gameEnv.Data.MASlocation, Path.GetFileName(iconPath), tempFolder);
                iconPath = Path.Combine(tempFolder, Path.GetFileName(iconPath));
                temp = true;
            }


            try
            {
                string bmpFileLoc = Path.GetTempPath() + Guid.NewGuid().ToString() + ".bmp";
                MiscFunctions.ConvertDDSToBMP(iconPath, bmpFileLoc);
                gameEnv.Data.SetIcon(bmpFileLoc);
            }
            catch
            {
                gameEnv.Data.SetIcon(null);
            }

            try
            {
                if (temp)
                {
                    File.Delete(iconPath);
                }
            }
            catch
            {

            }
        }

        internal virtual void readGearRatios()
        {
            gameEnv.Data.Gearing = new Gearing();
            bool loadGearFile = false;

            int numberOfGears = Int32.Parse(MiscFunctions.getProperty(gameEnv.Data.HDVehicleData.Values, "DRIVELINE", "ForwardGears", 0));
            gameEnv.Data.Gearing.ForwardGears = new Gearing.Gear[numberOfGears];

            //HDV values for the forward gears
            for (int i = 0; i < gameEnv.Data.Gearing.ForwardGears.Length; i++)
            {
                if (MiscFunctions.getProperty(gameEnv.Data.HDVehicleData.Values, "DRIVELINE", "Gear" + (i + 1) + "Range", 0) != null && MiscFunctions.getProperty(gameEnv.Data.HDVehicleData.Values, "DRIVELINE", "Gear" + (i + 1) + "Special", 0) != null)
                {
                    Gearing.Gear gear = new Gearing.Gear();
                    gear.number = i + 1;
                    gear.offset = Int32.Parse(MiscFunctions.getProperty(gameEnv.Data.HDVehicleData.Values, "DRIVELINE", "Gear" + (i + 1) + "Range", 0));
                    gear.ratios = new Gearing.Ratio[Int32.Parse(MiscFunctions.getProperty(gameEnv.Data.HDVehicleData.Values, "DRIVELINE", "Gear" + (i + 1) + "Range", 2))];

                    for (int j = 0; j < gear.ratios.Length; j++)
                    {
                        gear.ratios[j] = getRatio("Gear" + (i + 1) + "Special", j, 0);
                    }
                    gameEnv.Data.Gearing.ForwardGears[i] = gear;
                }
                else
                {
                    loadGearFile = true;
                }
            }
            //FinalDrive
            if (MiscFunctions.getProperty(gameEnv.Data.HDVehicleData.Values, "DRIVELINE", "FinalDriveRange", 0) != null)
            {
                Gearing.Gear gear = new Gearing.Gear();
                gear.number = 0;
                gear.offset = 0;
                gear.ratios = new Gearing.Ratio[Int32.Parse(MiscFunctions.getProperty(gameEnv.Data.HDVehicleData.Values, "DRIVELINE", "FinalDriveRange", 2))];

                for (int j = 0; j < gear.ratios.Length; j++)
                {
                    gear.ratios[j] = getRatio("FinalDriveSpecial", j, 2);
                    gear.ratios[j].Bevel = getRatio("FinalDriveSpecial", j, 1);
                }
                gameEnv.Data.Gearing.FinalDrive = gear;
            }
            else
            {
                loadGearFile = true;
            }

            //Reverse
            if (MiscFunctions.getProperty(gameEnv.Data.HDVehicleData.Values, "DRIVELINE", "ReverseRange", 0) != null)
            {
                Gearing.Gear gear = new Gearing.Gear();
                gear.number = -1;
                gear.offset = 0;
                gear.ratios = new Gearing.Ratio[Int32.Parse(MiscFunctions.getProperty(gameEnv.Data.HDVehicleData.Values, "DRIVELINE", "ReverseRange", 2))];

                for (int j = 0; j < gear.ratios.Length; j++)
                {
                    gear.ratios[j] = getRatio("ReverseSpecial", j, 0);
                }
                gameEnv.Data.Gearing.ReverseGear = gear;
            }
            else
            {
                loadGearFile = true;
            }

            //In case of lacking the gears, using the gearing file
            if (loadGearFile)
            {
                string gearFile = MiscFunctions.getProperty(gameEnv.Data.HDVehicleData.Values, "DRIVELINE", "GearFile", 0);
                if (!gearFile.ToLower().Contains(".ini"))
                    gearFile = gearFile + ".ini";
                gameEnv.Data.Gearing.gearFileLoc = DirSearch(gameEnv.Data.VEHlocation, gearFile);

                if (gameEnv.Data.Gearing.gearFileLoc != null)
                {
                    gameEnv.Data.Gearing.gearFileValue = ListToMultiLevelDict(MiscFunctions.FileToList(gameEnv.Data.Gearing.gearFileLoc));

                    //Gear ratios
                    if (gameEnv.Data.Gearing.gearFileValue.ContainsKey("GEAR_RATIOS".ToLower()) && gameEnv.Data.Gearing.gearFileValue["GEAR_RATIOS".ToLower()].ContainsKey("ratio"))
                    {
                        //All gears use the same ratios
                        Gearing.Ratio[] ratios = new Gearing.Ratio[gameEnv.Data.Gearing.gearFileValue["GEAR_RATIOS".ToLower()]["ratio"].Length];
                        for (int i = 0; i < ratios.Length; i++)
                        {
                            ratios[i] = getRatio("ratio", i, 10);
                        }

                        //Forward gears
                        for (int i = 0; i < gameEnv.Data.Gearing.ForwardGears.Length; i++)
                        {
                            Gearing.Gear gear = gameEnv.Data.Gearing.ForwardGears[i];
                            if (gear == null) //Only not set up gears will be filled
                            {
                                gear = new Gearing.Gear();
                                gear.number = i + 1;
                                gear.offset = 0;
                                gear.ratios = ratios;
                                gameEnv.Data.Gearing.ForwardGears[i] = gear;
                            }
                        }

                        //Reverse
                        if (gameEnv.Data.Gearing.ReverseGear == null)
                        {
                            Gearing.Gear gear = new Gearing.Gear();
                            gear.number = -1;
                            gear.offset = 0;
                            gear.ratios = ratios;
                            gameEnv.Data.Gearing.ReverseGear = gear;
                        }
                    }

                    //FinalDrive
                    if (gameEnv.Data.Gearing.FinalDrive == null)
                    {
                        if (gameEnv.Data.Gearing.gearFileValue.ContainsKey("FINAL_DRIVE".ToLower()) && gameEnv.Data.Gearing.gearFileValue["FINAL_DRIVE".ToLower()].ContainsKey("ratio"))
                        {
                            Gearing.Ratio bevel = getRatio("bevel", 0, 12);
                            Gearing.Ratio[] ratios = new Gearing.Ratio[gameEnv.Data.Gearing.gearFileValue["FINAL_DRIVE".ToLower()]["ratio"].Length];

                            for (int i = 0; i < ratios.Length; i++)
                            {
                                ratios[i] = getRatio("ratio", i, 11);
                                ratios[i].Bevel = bevel;
                            }

                            Gearing.Gear gear = new Gearing.Gear();
                            gear.number = 0;
                            gear.offset = 0;
                            gear.ratios = ratios;
                            gameEnv.Data.Gearing.FinalDrive = gear;
                        }
                    }
                }
            }

            //Gear Settings
            gameEnv.Data.Gearing.gearChange = MiscFunctions.getProperty(gameEnv.Data.HDVehicleData.Values, "DRIVELINE", "AllowGearingChanges", 0) != "0";
            gameEnv.Data.Gearing.finalDriveChange = MiscFunctions.getProperty(gameEnv.Data.HDVehicleData.Values, "DRIVELINE", "AllowFinalDriveChanges", 0) != "0";

            //Setting default settings
            Int32.TryParse(MiscFunctions.getProperty(gameEnv.Data.HDVehicleData.Values, "DRIVELINE", "FinalDriveSetting", 0), out gameEnv.Data.Gearing.FinalDrive.defaultSetting);

            Int32.TryParse(MiscFunctions.getProperty(gameEnv.Data.HDVehicleData.Values, "DRIVELINE", "ReverseSetting", 0), out gameEnv.Data.Gearing.ReverseGear.defaultSetting);

            for (int i = 0; i < gameEnv.Data.Gearing.ForwardGears.Length; i++)
            {
                Int32.TryParse(MiscFunctions.getProperty(gameEnv.Data.HDVehicleData.Values, "DRIVELINE", "Gear" + (i + 1) + "Setting", 0), out gameEnv.Data.Gearing.ForwardGears[i].defaultSetting);
            }

            //Removing reverse if it is unaviable
            if (MiscFunctions.getProperty(gameEnv.Data.HDVehicleData.Values, "DRIVELINE", "AllowReverseAndNeutral", 0) == "0")
                gameEnv.Data.Gearing.ReverseGear = null;

        }

        protected virtual Gearing.Ratio getRatio(string property, int segment, int finalDrive)
        {
            Gearing.Ratio ratio = new Gearing.Ratio();

            int offset = 3;
            if (finalDrive == 2)
                offset = 5;
            else if (finalDrive >= 10)
                offset = 0;

            string groupKey = "DRIVELINE";
            if (finalDrive == 10)
                groupKey = "GEAR_RATIOS";
            else if (finalDrive == 11 || finalDrive == 12)
                groupKey = "FINAL_DRIVE";

            if (finalDrive < 10)
            {
                int realSegment = -1;
                Int32.TryParse(MiscFunctions.getProperty(gameEnv.Data.HDVehicleData.Values, groupKey, property, segment, 0), out realSegment);

                //Needs to find the right segment
                if (realSegment != segment)
                {
                    if (gameEnv.Data.HDVehicleData.Values.ContainsKey(groupKey.ToLower()))
                    {
                        if (gameEnv.Data.HDVehicleData.Values[groupKey.ToLower()].ContainsKey(property.ToLower()))
                        {
                            string[] allSegments = gameEnv.Data.HDVehicleData.Values[groupKey.ToLower()][property.ToLower()];
                            if (allSegments != null || segment < allSegments.Length)
                            {
                                for (int i = 0; i < allSegments.Length; i++)
                                {
                                    int readSegment = -1;
                                    Int32.TryParse(allSegments[i].Split(',')[0], out readSegment);

                                    if (readSegment == segment)
                                    {
                                        segment = i;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            Dictionary<string, Dictionary<string, string[]>> dict = gameEnv.Data.HDVehicleData.Values;

            if (finalDrive >= 10)
                dict = gameEnv.Data.Gearing.gearFileValue;

            string basis = MiscFunctions.getProperty(dict, groupKey, property, segment, offset);
            string value = MiscFunctions.getProperty(dict, groupKey, property, segment, offset + 1);

            if (basis == null || value == null)
            {
                if (finalDrive >= 10)
                    finalDrive -= 10;
                //filler
                switch (finalDrive)
                {
                    case 0:
                        basis = "14";
                        value = "32";
                        break;

                    case 1:
                        basis = "1";
                        value = "1";
                        break;

                    case 2:
                        basis = "9";
                        value = "31";
                        break;

                }
            }

            ratio.basis = Int32.Parse(basis);
            ratio.value = Int32.Parse(value);
            return ratio;
        }

        public virtual void rereadForUpgrading()
        {
            readFilePreparation(); //Makes the rfactor2.bms and tempfolder
            readEngineFile();
            readTireFile(false);
            readGearRatios();
            readFileCleanup();  //Cleans up all the files
        }

        #endregion

        #region Misc Functions

        public static Dictionary<string, Dictionary<string, string[]>> ListToMultiLevelDict(List<string> list)
        {
            Dictionary<string, Dictionary<string, string[]>> dict = new Dictionary<string, Dictionary<string, string[]>>();
            int start = CycleTillNext(list, 0, '[') + 1;
            int end = CycleTillNext(list, start, '[');


            while (start < list.Count)
            {
                string title = list[start - 1].Split(']')[0].Replace("[", "").ToLower().Trim();
                
                if (title != "node")
                {
                    Dictionary<string, string[]> temp = new Dictionary<string, string[]>();
                    if (start != end)
                        temp = ListToDict(list, start, end);

                    if (dict.ContainsKey(title))
                    {
                        dict[title] = temp;
                    }
                    else
                    {
                        dict.Add(title, temp);
                    }
                }

                start = end + 1;
                end = CycleTillNext(list, start, '[');
            }

            return dict;
        }

        private static int CycleTillNext(List<string> list, int start, char c)
        {
            for (int i = start; i < list.Count; i++)
            {
                if (System.Text.RegularExpressions.Regex.Split(list[i], "//")[0].Contains(c))
                {
                    return i;
                }
            }

            return list.Count;
        }

        public static Dictionary<string, string[]> ListToDict(List<string> list)
        {
            return ListToDict(list, 0, list.Count);
        }

        public static Dictionary<string, string[]> ListToDict(List<string> list, int startIndex, int endIndex)
        {
            if (startIndex >= endIndex || endIndex > list.Count || startIndex < 0 || list == null)
            {
                throw new ArgumentException("Illegal Arguments used while creating a Dictionary");
            }

            Dictionary<string, string[]> dict = new Dictionary<string, string[]>(endIndex - startIndex);

            for (int i = startIndex; i < endIndex; i++)
            {
                try
                {
                    string line = list.ElementAt(i);
                    line = line.Replace("\t", " ").Trim();
                    line = line.Replace("https://", "https:/ /").Replace("http://", "http:/ /");
                    line = line.Replace("\\\\", "//"); //Thanks FSR
                    line = System.Text.RegularExpressions.Regex.Split(line, "//")[0].Trim(); 
                    
                    line = line.Replace("https:/ /", "https://").Replace("http:/ /", "http://");

                    if (line.Contains("="))
                    {
                        string key = line.Split('=')[0].Replace("*", "").ToLower();
                        string value = line.Substring(line.IndexOf('=')+1);

                        value = prepareValue(value);

                        if (dict.ContainsKey(key))
                        {
                            string[] old = dict[key];
                            string[] arr = new string[old.Length+1];

                            for (int iter = 0; iter < old.Length; iter++)
                                arr[iter] = old[iter];

                            arr[arr.Length - 1] = value;
                            dict[key] = arr;
                        }
                        else
                        {
                            dict.Add(key, new string[] { value });
                        }
                    }
                    else if (line.ElementAt(0) == '[')
                    {
                        dict.Add(line.Trim().ToLower(), new string[] { "[TITLE]=" + i });
                    }
                    else if (line.Contains(":"))
                    {
                        dict.Add(line.Substring(0, line.IndexOf(':')).Trim().ToLower(), new string[] { ":SUBCATIGORY:=" + i });
                    }
                }
                catch
                {
                    //Just continue
                }

            }

            return dict;
        }

        public static string prepareValue(string value)
        {
            value = value.Trim();

            //For Notes
            bool isNote = false;
            if (value.IndexOf("\"(") == 0 && value.Contains(","))
            {
                value = value.Substring(value.IndexOf("\"(") + 1);
                isNote = true;
            }

            if (value.LastIndexOf(")\"") == (value.Length - 2) && value.Contains(")\"") && value.Contains(","))
            {
                value = value.Substring(0, value.LastIndexOf(")\""));
                isNote = true;
            }


            //For (values) comments, like fucking MAK Corp
            if (!isNote)
            {
                int level = 0;

                for (int i = 0; i < value.Length; i++)
                {
                    int nextOpenIndex = value.IndexOf('(', i);
                    int nextCloseIndex = value.IndexOf(')', i);

                    if (nextOpenIndex != -1 && nextCloseIndex != -1)
                    {
                        if (nextOpenIndex < nextCloseIndex)
                        {
                            i = nextOpenIndex;
                            level++;
                        }
                        else if (nextCloseIndex < nextOpenIndex)
                        {
                            i = nextCloseIndex;
                            level--;
                        }

                        //Checking if we are back to level 0, then cutting it down
                        if (level == 0)
                        {
                            value = value.Substring(0, i + 1);
                        }
                    }
                    else
                    {
                        //All parenthesis are proccessed, without violating the level
                        break;
                    }
                }
            }
            



            //For too (((values)))
            while (value.Contains("(") && ((value.Contains("\"") && value.IndexOf("(") < value.IndexOf("\"")) || !value.Contains("\"")))
                value = value.Substring(value.IndexOf("(") + 1);

            while (value.Contains(")") && ((value.Contains("\"") && value.LastIndexOf(")") > value.LastIndexOf("\"")) || !value.Contains("\"")))
                value = value.Substring(0, value.LastIndexOf(")"));
            
            value = value.Replace("\"", "").Replace(";", ",").Trim();

            return value;
        }

        public static Dictionary<string, Dictionary<string, string[]>> addDefaultValues(Dictionary<string, Dictionary<string, string[]>> dict, Dictionary<string, Dictionary<string, string[]>> defaults)
        {
            Dictionary<string, Dictionary<string, string[]>>.Enumerator enumerator = defaults.GetEnumerator();

            while (enumerator.MoveNext())
            {
                KeyValuePair<string, Dictionary<string, string[]>> pair = enumerator.Current;

                if (!dict.ContainsKey(pair.Key))
                {
                    dict.Add(pair.Key, pair.Value);
                }
                else
                {
                    dict[pair.Key] = addDefaultValues(dict[pair.Key], pair.Value);
                }
            }

            return dict;
        }

        public static Dictionary<string, string[]> addDefaultValues(Dictionary<string, string[]> dict, Dictionary<string, string[]> defaults)
        {
            Dictionary<string, string[]>.Enumerator enumerator = defaults.GetEnumerator();

            while (enumerator.MoveNext())
            {
                KeyValuePair<string, string[]> pair = enumerator.Current;

                if (!dict.ContainsKey(pair.Key))
                {
                    dict.Add(pair.Key, pair.Value);
                }
            }

            return dict;
        }

        protected string DirSearch(string startdir, string filename)
        {
            return DirSearch(startdir, filename, !gameEnv.LoadReadEncrytedFiles(), tempFolder);
        }

        //Searches a dir for a file from the inside to the outside
        public static string DirSearch(string startdir, string filename, bool onlyNonEncrypted, string tempfolder)
        {
            string fileloctemp = null;
            if (filename == null || filename == "")
                throw new ArgumentException("The Filename can not be null");
            if (filename.Contains(@"\"))
                filename = filename.Substring(filename.LastIndexOf(@"\") + 1);
            try
            {
                //Testing inital folder
                if (fileloctemp == null)
                {
                    fileloctemp = DirSearchIn(startdir, filename, onlyNonEncrypted, tempfolder);
                }

                string dir = startdir.Substring(0, startdir.LastIndexOf(@"\"));

                //Testing higher folders (Until exiting the rFactor 2 folder or finding the file)
                for (int i = 0; fileloctemp == null && i < 5; i++)
                {
                    fileloctemp = DirSearchIn(dir, filename, onlyNonEncrypted, tempfolder);
                    dir = dir.Substring(0, dir.LastIndexOf(@"\"));
                }

                return fileloctemp;
            }
            catch (Exception excpt)
            {
                Console.WriteLine(excpt.Message);
                return null;
            }
        }

        //Searches in a dir for a file from the outside to the inside
        private static string DirSearchIn(string startdir, string filename, bool onlyNonEncrypted, string tempFolder)
        {
            if (filename.Contains(@"\"))
                filename = filename.Substring(filename.LastIndexOf(@"\") + 1);
            try
            {
                foreach (string f in Directory.GetFiles(startdir))
                {
                    try
                    {
                        //Finding the file in the folder directly
                        if ((Path.GetFileNameWithoutExtension(f).ToLower() == (Path.Combine(startdir, filename).ToLower())) || (f.ToLower() == (Path.Combine(startdir, filename)).ToLower()))
                        {
                            return f;
                        }
                        //The File f is a mas file
                        if (Path.GetExtension(f).ToLower() == ".MAS".ToLower()) //If it's a MAS-files we have to check if the file is in the MAS-archive
                        {
                            string realFileName = null;
                            if ((realFileName = MiscFunctions.checkFileInMAS(f, filename)) != null)
                            {
                                //If the user checked the "only non-encrypted"-option -> check if the file is not encrypted. If it is, it's game over
                                if (onlyNonEncrypted && MiscFunctions.CheckIfEncrypted(f, realFileName) == true) //the file is encrypted
                                {
                                    //MessageBox.Show("Sorry, this car has encrypted data, can't read it.", "Encrypted file", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    return "encrypted";
                                }
                                MiscFunctions.ExtractMAS(f, realFileName, tempFolder);
                                return Path.Combine(tempFolder, realFileName);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }

                //Only reached when all Files were checked
                string path = null;
                string[] dirs = Directory.GetDirectories(@startdir);
                for (int i = 0; i < dirs.Length && path == null; i++)
                {
                    path = DirSearchIn(dirs[i], filename, onlyNonEncrypted, tempFolder);
                }

                return path;
            }
            catch (System.Exception excpt)
            {
                Console.WriteLine(excpt.Message);
            }
            return null;
        }
        #endregion

        protected virtual void readFilePreparation()
        {
            if (!Directory.Exists(MiscFunctions.tempFolder))
            {
                DirectoryInfo di = Directory.CreateDirectory(MiscFunctions.tempFolder);
                di.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
            }


            Directory.CreateDirectory(tempFolder);
        }

        public static void readFileCleanup()
        {
            try
            {

                if (Directory.Exists(MiscFunctions.tempFolder))
                    Directory.Delete(MiscFunctions.tempFolder, true);
            }
            catch { }
        }
    }
}
