﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Misc;
using static CarStat_2.DataObjects.Gearing;

namespace CarStat_2.DataObjects
{
    public class GearSetupRow : SetupRow
    {
        private Gear gear;

        private GearSetupRow finalDrive;
        private GearSetupRow nextGear;
        private GearSetupRow prevGear;

        public GearSetupRow() :base()
        {
            
        }

        public void setGear(Gear gear)
        {
            this.gear = gear;
            setCarSetupOption(new GearSetupOption(gear));
        }

        public void setFinalDrive(GearSetupRow row)
        {
            finalDrive = row;
        }

        public void setNextGear(GearSetupRow row)
        {
            nextGear = row;
        }

        public void setPrevGear(GearSetupRow row)
        {
            prevGear = row;
        }

        public Gear getGear()
        {
            return gear;
        }

        public Ratio getRatio()
        {
            if (gear == null || this.getSetting() == -1)
                return null;

            return gear.ratios[this.getSetting()];
        }

        public override double getValue()
        {
            Ratio ratio = getRatio();
            if (ratio == null)
                return -1;

            return ratio.getDecimal();
        }

        public override double getFactoryValue()
        {
            double ratio = getValue();
            if (ratio == -1)
                return -1;
            if (finalDrive == null)
                return -1;

            return ratio * finalDrive.getValue();
        }

        protected override void disableOptions()
        {
            base.disableOptions();

            int setting = this.getSetting();

            if (setting == -1 || gear == null || setting >= gear.ratios.Length)
                return;

            if (!ChangeEnabled)
                return;

            if (setting > 0 && prevGear != null && prevGear.getRatio() != null)
            {
                if (prevGear.getRatio().equals(gear.ratios[setting - 1]))
                {
                    base.minus.Enabled = false;
                    prevGear.plus.Enabled = false;
                }
                else if (!prevGear.plus.Enabled)
                {
                    prevGear.disableOptions();
                }
            }

            if (setting + 1 < gear.ratios.Length && nextGear != null && nextGear.getRatio() != null)
            {
                if (nextGear.getRatio().equals(gear.ratios[setting + 1]))
                {
                    base.plus.Enabled = false;
                    nextGear.minus.Enabled = false;
                }
                else if (!nextGear.minus.Enabled)
                {
                    nextGear.disableOptions();
                }
            }
        }

        public override string getText()
        {
            string text = "";

            if (gear == null)
                return "";

            text = Math.Round(getValue(), 2)+"";
            if (finalDrive != null)
            {
                text = text + " (" + Math.Round(getFactoryValue(), 2) + ")";
            }

            return text;
        }

        private class GearSetupOption : CarSetupOption
        {
            internal GearSetupOption(Gear gear)
            {
                if (gear == null)
                {
                    NumOfSettings = 1;
                    DefaultSetting = 0;
                    return;
                }

                NumOfSettings = gear.ratios.Length;
                DefaultSetting = gear.defaultSetting;
            }
        }
    }
}
