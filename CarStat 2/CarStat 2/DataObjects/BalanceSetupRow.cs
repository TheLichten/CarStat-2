﻿using Misc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarStat_2.DataObjects
{
    public class BalanceSetupRow : SetupRow
    {
        public BalanceSetupRow() : base()
        {

        }

        public override string getText()
        {
            string standard = base.getText();

            if (!base.displayRealValues)
                return standard;

            double value = getFactoryValue();

            return (100 - value) + realValuesUnit + " : " + value + realValuesUnit;
        }
    }
}
