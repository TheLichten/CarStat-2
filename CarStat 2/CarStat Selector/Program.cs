﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using Misc;
using System.IO;
using System.Diagnostics;

namespace CarStat_Selector
{
    static class Program
    {
        public static VersionNumber VERSION = new VersionNumber(1, 0, 0, 0);

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            string[] version = Application.ProductVersion.Split('.');
            VERSION = new VersionNumber(Int32.Parse(version[0]), Int32.Parse(version[1]), Int32.Parse(version[2]), Int32.Parse(version[3]));

            updateManagement();

            new CarStat_2.CarStat2(VERSION).Show();

            Application.Run();

        }

        private static void updateManagement()
        {
            try
            {
                WebClient client = new WebClient();
                Stopwatch watch = new Stopwatch();
                System.Threading.Tasks.Task<Stream> task = client.OpenReadTaskAsync("https://gitlab.com/TheLichten/CarStat-2/wikis/Version.md");

                watch.Start();

                while (!task.IsCompleted && !task.IsFaulted)
                {
                    Thread.Sleep(100);

                    if (watch.ElapsedMilliseconds > 1750)
                    {
                        watch.Stop();

                        return;
                    }
                }

                watch.Stop();

                if (task.IsFaulted)
                    throw task.Exception;

                Stream stream = task.Result;

                StreamReader reader = new StreamReader(stream);
                string line = reader.ReadLine();
                string link = reader.ReadLine();

                reader.Close();
                stream.Close();
                client.Dispose();
                string version = line.Split(':')[1].Trim();

                VersionNumber avaible = new VersionNumber(version);


                if (avaible.compareTo(VERSION) == 1) //Newer version avaible
                {
                    DialogResult result = MessageBox.Show("A new Version of CarStat is avaible.\nCurrent Version:\t" + VERSION.ToString() + "\nNew Version:\t" + avaible.ToString() + "\n\nDo you want to download the update?", "Update Avaible", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

                    if (result == DialogResult.OK)
                    {
                        System.Diagnostics.Process.Start(link.Trim());
                    }
                }
            }
            catch
            {

            }
        }

    }
}
