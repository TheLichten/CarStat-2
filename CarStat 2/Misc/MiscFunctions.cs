﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using MAS2Extract;
using Microsoft.Win32;
using System.Drawing;
using System.Windows.Forms;
using FreeImageAPI;

namespace Misc
{
    static public class MiscFunctions
    {
        public static string tempFolder = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), @"temp");
        public enum Units { Nm, HP, ftlb, C, F, kW, Empty };

        //Store if CarStat 1 or 2 should autostart... or not.
        static public void RememberAutostart(string version)
        {
            try
            {
                RegistryKey key = Registry.CurrentUser.OpenSubKey("SOFTWARE", true);
                RegistryKey carKey = key.CreateSubKey("CarStat");
                carKey.SetValue("AutoStart", version);
            }
            catch { }
        }

        static public void preparePictureConversion()
        {
            if (!File.Exists("FreeImage.dll"))
                File.WriteAllBytes("FreeImage.dll", Misc.Properties.Resources.FreeImage);
            if (!File.Exists("FreeImage.h"))
                File.WriteAllText("FreeImage.h", Misc.Properties.Resources.FreeImageH);
            if (!File.Exists("FreeImage.lib"))
                File.WriteAllBytes("FreeImage.lib", Misc.Properties.Resources.FreeImageLIB);
        }

        static public void deletePicutreConversionKit()
        {
            if (File.Exists("FreeImage.dll"))
            {
                try
                {
                    File.Delete("FreeImage.dll");
                }
                catch
                {

                }
            }
            if (File.Exists("FreeImage.h"))
                File.Delete("FreeImage.h");
            if (File.Exists("FreeImage.lib"))
                File.Delete("FreeImage.lib");
        }

        static public string ConvertDDSToBMP(string ddsFileToConvert, string savePath)
        {
            FIBITMAP bitmap = FreeImage.Load(FREE_IMAGE_FORMAT.FIF_DDS, ddsFileToConvert, FREE_IMAGE_LOAD_FLAGS.DEFAULT);
            if (FreeImage.Save(FREE_IMAGE_FORMAT.FIF_BMP, bitmap, savePath, FREE_IMAGE_SAVE_FLAGS.DEFAULT))
                return savePath;

            return null;
        }

        //Extract a given file out of an rFactor 2 MAS2-file.
        static public void ExtractMAS(string MasFile, string fileToUnpack, string destination)
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "quickbms.exe";
            startInfo.Arguments = "-Y -f \"" + fileToUnpack + "\" rfactor2.bms \"" + MasFile + "\" \"" + destination + "\"";
            process.StartInfo = startInfo;
            process.Start();

            try
            {
                process.PriorityBoostEnabled = true;
            }
            catch
            {

            }

            try
            {
                process.WaitForExit();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show("Error while extracting files.\nFailed to unpack: " + fileToUnpack + "\n" + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        //Checks if a file in the given MAS-file is encrypted (if the MAS-file is encrypted) or not.
        static public bool CheckIfEncrypted(string MasFile, string fileToUnpack)
        {
            MAS2Reader reader = new MAS2Reader(MasFile);
            foreach (MAS2File file in reader.Files)
            {
                if (Path.GetFileName(file.Filename).ToLower() == fileToUnpack.ToLower())
                {
                    try
                    {
                        reader.ExtractFile(file, Path.Combine(tempFolder, fileToUnpack));

                        StreamReader filereader = new StreamReader(Path.Combine(tempFolder, fileToUnpack));
                        string line = filereader.ReadLine();
                        while (line == "")
                        {
                            line = filereader.ReadLine();
                        }
                        byte[] bytes = new byte[line.Length * sizeof(char)];
                        System.Buffer.BlockCopy(line.ToCharArray(), 0, bytes, 0, bytes.Length);
                        foreach (byte bit in bytes)
                        {
                            if (bit > 250)
                            {
                                filereader.Close();
                                return true;
                            }
                        }
                        filereader.Close();
                        break;
                    }
                    catch
                    {
                        return true; //Error while unpacking, which means it is encrypted.
                    }
                }
            }
            return false; //Looks like it's not encrypted.
        }


        //Gets a list of files in a MAS2-archive with the given extension
        static public List<string> getVehiclesFromMas(string MASlocatie, string extension)
        {
            List<string> vehfiles = new List<string>();
            MAS2Reader reader = new MAS2Reader(MASlocatie);
            foreach (MAS2File file in reader.Files)
            {
                if (Path.GetExtension(file.Filename).ToLower() == extension)
                {
                    vehfiles.Add(file.Filename);
                }
            }
            return vehfiles;
        }

        //Returns a bool saying if a given file is in the given MAS-archive... or not
        static public string checkFileInMAS(string MASlocatie, string filename)
        {
            MAS2Reader reader = new MAS2Reader(MASlocatie);
            foreach (MAS2File file in reader.Files)
            {
                if (Path.GetFileNameWithoutExtension(file.Filename).ToLower() == Path.GetFileNameWithoutExtension(filename).ToLower() &&
                    Path.GetExtension(file.Filename).ToLower() == Path.GetExtension(filename).ToLower())
                {
                    return file.Filename;
                }
            }
            return null;
        }

        //Trys to find a file with this prefix
        static public string checkFilePrefixInMAS(string MASlocatie, string filename)
        {
            MAS2Reader reader = new MAS2Reader(MASlocatie);
            foreach (MAS2File file in reader.Files)
            {
                if (Path.GetFileNameWithoutExtension(file.Filename).ToLower().Contains(Path.GetFileNameWithoutExtension(filename).ToLower()))
                {
                    return file.Filename;
                }
            }
            return null;
        }

        //Calculates the value of a number in between value 1 (at distance 0) value 2 (at distance 1)
        public static double calcValueBetweenTwoValues(double value1, double value2, double distance)
        {
            double dif = value2 - value1;
            return value1 + (dif * distance);
        }

        //Table has a fixed distance of 0 (first value) and 1 (last value)
        public static double calcValueInTable(double[] table, double distance)
        {
            double stepsize = 1.0 / (table.Length-1);
            double position = distance / stepsize;

            if (((double)((int)position)) == position)
            {
                return table[(int)position];
            }

            return calcValueBetweenTwoValues(table[(int)position], table[((int)position)+1], distance% stepsize);
        }

        public static void makeParentChildWindow(Form Parent, Form Child)
        {
            Child.Owner = Parent;
            Parent.Enabled = false;

            ChildParent couple = new ChildParent(Parent, Child);
            Child.FormClosing += couple.ChildParentDecouple_FormClosing;

            if (Child.Visible == false)
            {
                Parent.Enabled = true;
            }
        }

        private class ChildParent
        {
            private Form Child;
            private Form Parent;

            public ChildParent(Form Parent, Form Child)
            {
                this.Parent = Parent;
                this.Child = Child;
            }

            public void ChildParentDecouple_FormClosing(object sender, FormClosingEventArgs e)
            {
                Parent.Enabled = true;
            }
        }
        

        //Makes a List of strings from the given file (memory = faster)
        static public List<string> FileToList(string fileloc)
        {
            List<string> filelist = new List<string>();
            StreamReader reader = new StreamReader(fileloc);
            string line;
            while ((line = reader.ReadLine()) != null)
            {
                if (!line.Trim().StartsWith("//") && !line.Equals(""))
                {
                    line = line.Replace("https://", "https:/ /").Replace("http://", "http:/ /");
                    line = Regex.Split(line, "//")[0].Trim();
                    line = line.Replace("https:/ /", "https://").Replace("http:/ /", "http://");
                    filelist.Add(line);
                }
            }
            reader.Close();
            return filelist;
        }

        static public List<string> FileToList(Stream data)
        {
            List<string> filelist = new List<string>();
            StreamReader reader = new StreamReader(data);
            string line;
            while ((line = reader.ReadLine()) != null)
            {
                if (!line.Trim().StartsWith("//") && !line.Equals(""))
                {
                    if (line.Contains("//"))
                        line = line.Substring(0, line.IndexOf("//"));
                    filelist.Add(line);
                }
            }
            reader.Close();
            return filelist;
        }

        //Makes a List of strings from the given file (memory = faster) without removing anything.
        static public List<string> FileToList(string fileloc, bool dontremove)
        {
            List<string> filelist = new List<string>();
            StreamReader reader = new StreamReader(fileloc);
            string line;
            while ((line = reader.ReadLine()) != null)
            {
                if (dontremove || (!line.Trim().StartsWith("//") && !line.Equals("")))
                    filelist.Add(line);
            }
            reader.Close();
            return filelist;
        }

        //Makes a List of strings from the given TGM-file. Stop at [LookupData] to save memory space
        static public List<string> FileToListTGM(string fileloc)
        {
            List<string> filelist = new List<string>();
            StreamReader reader = new StreamReader(fileloc);
            string line;
            while ((line = reader.ReadLine()) != null)
            {
                if (!line.Trim().StartsWith("//") && !line.Equals(""))
                {
                    if (line.Contains("//"))
                        line = line.Substring(0, line.IndexOf("//"));
                    filelist.Add(line);
                }
                if (line.Trim().ToLower() == "[lookupdata]" || line.Trim().ToLower() == "[lookupv2]")
                    break;
            }
            reader.Close();
            return filelist;
        }

        //Makes a List of strings from the given TGM-file. Stop at [LookupData] to save memory space
        static public List<string> FileToListTGM(Stream data)
        {
            List<string> filelist = new List<string>();
            StreamReader reader = new StreamReader(data);
            string line;
            while ((line = reader.ReadLine()) != null)
            {
                if (!line.Trim().StartsWith("//") && !line.Equals(""))
                {
                    if (line.Contains("//"))
                        line = line.Substring(0, line.IndexOf("//"));
                    filelist.Add(line);
                }
                if (line.Trim().ToLower() == "[lookupdata]" || line.Trim().ToLower() == "[lookupv2]")
                    break;
            }
            reader.Close();
            return filelist;
        }

        //Makes a file of a List of strings 
        static public void ListToFile(List<string> file, string fileloc)
        {
            StreamWriter writer = new StreamWriter(fileloc);
            for (int i = 0; i < file.Count; i++)
            {
                writer.WriteLine(file[i]);
            }
            writer.Close();
        }

        //Makes a List of strings from the given file but only starts from the first compound
        static public List<string> FileToListChop(string fileloc)
        {
            List<string> filelist = new List<string>();
            StreamReader reader = new StreamReader(fileloc);
            string line;
            while (!((line = reader.ReadLine()).Contains("[COMPOUND]")))
            {
            }
            while (line != null)
            {
                if (line.Contains("//"))
                    line = line.Substring(0, line.IndexOf("//"));
                if (!line.Trim().StartsWith("//") && !line.Equals(""))
                    filelist.Add(line.Trim());

                line = reader.ReadLine();
            }
            reader.Close();
            return filelist;
        }

        //Function to convert seconds to hours:minutes
        static public string getTime(double time)
        {
            int hours = (int)time / 3600;
            int minutes = (int)(((time - (hours * 3600)) / 60));
            string timestring = hours + " hours, " + minutes + " minutes";
            return timestring;
        }

        //Calculates the engine power, given a torquenumber at a specific rpm
        static public int torqueToHP(double torque, double rpm)
        {
            int power;
            power = (int)(torque * rpm / 9549);
            return power;
        }

        //Calculates the HP, given a unit in kW
        static public double kwToHP(double kw)
        {
            return Math.Round(kw * 1.3410, 0);
        }

        //Calculates the Lb, given a unit in Nm
        static public double nmToLb(double nm)
        {
            return Math.Round(nm * 0.7375621, 0);
        }

        //Calculates the PSI, given a unit in kpa
        static public double kpaToPSI(double kpa)
        {
            return Math.Round(kpa * 0.145037738, 1);
        }

        //Calculates the temp in F, given a unit in C
        static public double CelsiusToFahrenheit(double celsius)
        {
            return Math.Round((celsius * ((9.0 / 5.0))) + 32, 0);
        }

        //Calculates the temp in °C, given a unit in Kelvin
        static public double KelvinToCelsius(double kelvin)
        {
            return kelvin - 273.15;
        }

        //Changes meters into inches (mostly for tire rim diameter)
        static public double MetersToInches(double meters)
        {
            return meters * 39.3701d;
        }

        //Gets the amount of turbo's the car has
        static public int getTurboAmount(List<string> file)
        {
            int turboAmount = 0;
            foreach (string line in file)
            {
                if (line.Split('=')[0].Equals("TurboInertia"))
                {
                    turboAmount++;
                }
            }
            return turboAmount;
        }

        public static CarSetupOption getSetupOption(Dictionary<string, Dictionary<string, string[]>> dict, string sectionKey, string key)
        {
            sectionKey = sectionKey.ToLower();
            if (!dict.ContainsKey(sectionKey))
                return null;

            return getSetupOption(dict[sectionKey], key);
        }

        //Reads the SetupOption under the specific key
        public static CarSetupOption getSetupOption(Dictionary<string, string[]> dict, string key)
        {
            CarSetupOption setup = new CarSetupOption();
            setup.Caption = getProperty(dict, key + "Caption", 0);

            if (!dict.ContainsKey((key + "Range").ToLower()))
                return null;

            //Set the values of the range
            setup.StartValue = Double.Parse(getProperty(dict, key + "Range", 0));
            setup.StepSize = Double.Parse(getProperty(dict, key + "Range", 1));
            setup.NumOfSettings = (int) Double.Parse(getProperty(dict, key + "Range", 2));

            if (setup.NumOfSettings == 0) //Some modders use 0 range, and they should be taken into a dark room and beaten
                setup.NumOfSettings = 1;

            setup.AllOptions = new CarSetupOption.SetupValue[setup.NumOfSettings];

            //Generate Settings
            for (int i = 0; i < setup.NumOfSettings; i++)
            {
                CarSetupOption.SetupValue value = new CarSetupOption.SetupValue();
                value.Text = "" + (i + 1);
                value.Value = setup.StartValue + (setup.StepSize * i);
                setup.AllOptions[i] = value;
            }

            setup.hasSpecialText = false;

            //Applies special values
            if (dict.ContainsKey((key + "Special").ToLower()))
            {
                string[] specialValues = dict[(key + "Special").ToLower()];

                for (int i = 0; i < specialValues.Length; i++)
                {
                    string[] special = specialValues[i].Split(',');
                    int setting = (int) Double.Parse(special[0]);


                    if (setting >= 0 && setting < setup.AllOptions.Length)
                    {
                        CarSetupOption.SetupValue value = setup.AllOptions[setting];



                        if (special.Length>2 && special[2] != "" && special[2] != null)
                        {
                            value.Unit = special[2];
                            value.Unit = value.Unit.Replace("�", "°");

                            if (special[1] == null)
                                special[1] = "";

                            value.Text = special[1];

                            setup.hasSpecialText = true;
                        }
                        else if (special.Length > 1)
                        {
                            if (special[1] != "" && special[1] != null)
                            {
                                value.Text = special[1];

                                setup.hasSpecialText = true;
                            }
                        }

                        double read = 0;
                        if (special.Length > 3 && special[3] != "" && special[3] != null && Double.TryParse(special[3], out read))
                            value.Value = read;
                    }
                }
            }

            //Sets the default setting
            string defaultSetting = getProperty(dict, key + "Setting", 0);
            if (defaultSetting != null)
                setup.DefaultSetting = (int)Double.Parse(defaultSetting);
            else
                setup.DefaultSetting = 0;

            return setup;
        }

        public static CarSetupOption getCompoundSetupOption(List<string> tyreFile)
        {
            CarSetupOption option = new CarSetupOption();
            option.StartValue = 0;
            option.StepSize = 0;
            option.hasSpecialText = true;
            option.Caption = null;

            List<CarSetupOption.SetupValue> setupValues = new List<CarSetupOption.SetupValue>();

            for (int i = 0; i < tyreFile.Count; i++) //Bad construction, historically grown. It works though, so /care
            {
                if (tyreFile[i].Contains("[COMPOUND]"))
                {
                    while (tyreFile[i].ToLower().Contains("name=") == false) //Skip to the Tire name
                        i++;
                    string compound = tyreFile[i];
                    compound = compound.Substring(compound.IndexOf('=') + 1).Replace("\"", "");

                    CarSetupOption.SetupValue setupValue = new CarSetupOption.SetupValue();
                    setupValue.Text = "";
                    setupValue.Unit = compound;
                    setupValue.Value = 0;

                    setupValues.Add(setupValue);
                }
            }

            option.AllOptions = setupValues.ToArray();
            option.NumOfSettings = setupValues.Count;

            return option;
        }

        //Searches a simple dictionary for a certain value
        public static string getProperty(Dictionary<string, string[]> dict, string property, int value)
        {
            return getProperty(dict, property, 0, value);
        }

        //Searches a simple dictionary for a certain value
        public static string getProperty(Dictionary<string, string[]> dict, string property, int segment, int value)
        {
            property = property.ToLower();

            if (dict == null)
                return null;

            string[] result = null;
            dict.TryGetValue(property, out result);

            if (result == null)
                return null;

            //Segments, aka when the file has multible lines with the same key
            if (segment >= result.Length || segment < 0)
                return null;

            string requested = result[segment].Trim();

            requested = Regex.Split(requested, "//")[0].Replace("\t", "").Trim(); //removing the comment, if neccessary


            //Values, when one key has multiple values
            string[] resultInValues = requested.Split(',');
            if (value >= resultInValues.Length || value < 0)
                return null;

            requested = resultInValues[value].Trim();

            return requested;
        }

        //Searches a multilevel dictionary for a certain value
        public static string getProperty(Dictionary<string, Dictionary<string, string[]>> dict, string groupKey, string property, int value)
        {
            return getProperty(dict, groupKey, property, 0, value);
        }

        //Searches a multilevel dictionary for a certain value
        public static string getProperty(Dictionary<string, Dictionary<string, string[]>> dict, string groupKey, string property, int segment, int value)
        {
            groupKey = groupKey.ToLower();
            property = property.ToLower();

            if (dict == null)
                return null;

            Dictionary<string, string[]> group = null;
            dict.TryGetValue(groupKey, out group);

            

            return getProperty(group, property, segment, value);
        }


        //Gets the value of the given property in the given file on the given location (= value)
        static public string getProperty(List<string> file, string property, int value)
        {
            string prop = null;
            value = value - 1;
            foreach (string line in file)
            {
                if (line.Split('=')[0].ToLower().Equals(property.ToLower()) || line.Split(new string[] { "*=" }, StringSplitOptions.None)[0].ToLower().Equals(property.ToLower()))
                {
                    prop = Regex.Split(line, "//")[0];
                    prop = prop.Split('=')[1];
                    prop = prop.Replace("\"", "");
                    prop = prop.Replace("(", "");
                    prop = prop.Replace(")", "");
                    string[] props = prop.Split(',');
                    if (props.Length > value)
                        prop = props[value];
                    else
                        prop = "";
                    prop = prop.Trim().Trim();
                    break;
                }
            }
            if ((prop == null) || (prop == ""))
                prop = "n/a";
            return prop;
        }

        //Gets the value of the given property in the given file on the given location (= value), with optional bool teaminf
        static public string getProperty(List<string> file, string property, int value, bool teaminf)
        {
            string prop = null;
            value = value - 1;
            foreach (string line in file)
            {
                if (line.Split('=')[0].Equals(property))
                {
                    prop = Regex.Split(line, "//")[0];
                    prop = prop.Split('=')[1];
                    prop = prop.Replace("\"", "");
                    prop = prop.Replace("(", "");
                    prop = prop.Replace(")", "");
                    prop = prop.Trim();
                    break;
                }
            }
            if ((prop == null) || (prop == ""))
                prop = "n/a";
            return prop;
        }

        //Gets the value of the given property in the given file on the given location (= value) with a start-field
        static public string getProperty(List<string> file, string property, int value, string start)
        {
            string prop = null;
            value = value - 1;
            int i;
            for (i = 0; i < file.Count; i++)
            {
                if (file[i].Contains("Name=\"" + start + "\"") || file[i].Contains("Name=" + start))
                {
                    break;
                }
            }
            for (int j = i; j < file.Count; j++)
            {
                string line = file[j];
                if (line.Split('=')[0].Equals(property))
                {
                    prop = Regex.Split(line, "//")[0];
                    prop = prop.Split('=')[1];
                    prop = prop.Replace("\"", "");
                    prop = prop.Replace("(", "");
                    prop = prop.Replace(")", "");
                    prop = prop.Split(',')[value];
                    prop = prop.Trim();
                    break;
                }

            }
            if ((prop == null) || (prop == ""))
                prop = "n/a";
            return prop;
        }

        //Gets the value of the given property in the given file on the given location (= value) with a start-field
        static public string getProperty(List<string> file, string property, int value, bool rear, string start)
        {
            string prop = null;
            value = value - 1;
            int i;
            for (i = 0; i < file.Count; i++)
            {
                if (file[i].Contains("Name=\"" + start + "\"") || file[i].Contains("Name=" + start))
                {
                    break;
                }
            }
            if (rear)
            {
                for (; i < file.Count; i++)
                {
                    if (file[i].ToLower().Contains("rear:") || file[i].ToLower().Contains("left:") || file[i].ToLower().Contains("all:"))
                    {
                        break;
                    }
                }
            }
            else
            {
                for (; i < file.Count; i++)
                {
                    if (file[i].ToLower().Contains("front:") || file[i].ToLower().Contains("right:") || file[i].ToLower().Contains("all:"))
                    {
                        break;
                    }
                }
            }
            for (int j = i; j < file.Count; j++)
            {
                string line = file[j];
                if (line.Split('=')[0].Equals(property))
                {
                    prop = Regex.Split(line, "//")[0];
                    prop = prop.Split('=')[1];
                    prop = prop.Replace("\"", "");
                    prop = prop.Replace("(", "");
                    prop = prop.Replace(")", "");
                    prop = prop.Split(',')[value];
                    prop = prop.Trim();
                    break;
                }

            }
            if ((prop == null) || (prop == ""))
                prop = "n/a";
            return prop;
        }

        //Gets the value of the given property in the given file on the given location (= value) with a start-field, meant for reading the brakes
        static public string getPropertyBrakes(List<string> file, string property, int location, string start)
        {
            string prop = null;
            location = location - 1;
            int i;
            for (i = 0; i < file.Count; i++)
            {
                if (file[i].Contains(start))
                {
                    break;
                }
            }
            for (int j = i; j < file.Count; j++)
            {
                string line = file[j];
                if (line.Split('=')[0].Equals(property))
                {
                    prop = Regex.Split(line, "//")[0];
                    prop = prop.Split('=')[1];
                    prop = prop.Replace("\"", "");
                    prop = prop.Replace("(", "");
                    prop = prop.Replace(")", "");
                    if (location >= prop.Length)
                        prop = "n/a";
                    else
                        prop = prop.Split(',')[location];
                    prop = prop.Trim();
                    break;
                }

            }
            if ((prop == null) || (prop == ""))
                prop = "n/a";
            return prop;
        }

        static public void changeParameter(Dictionary<string, Dictionary<string,string[]>> dict, string groupKey, string propertyKey, string value, int segment)
        {
            groupKey = groupKey.ToLower().Trim();

            Dictionary<string, string[]> group = null;
            dict.TryGetValue(groupKey, out group);

            if (group == null)
                return;

            changeParameter(group, propertyKey, value, segment);
        }

        static public void changeParameter(Dictionary<string, string[]> dict, string propertyKey, string value, int segment)
        {

            propertyKey = propertyKey.ToLower();

            int changeTyp = 0;
            switch (propertyKey[propertyKey.Length - 1])
            {
                case '*': changeTyp = 2; propertyKey = propertyKey.Replace("*", ""); break;
                case '+': changeTyp = 1; propertyKey = propertyKey.Replace("+", ""); break;
                case '-': changeTyp = -1; propertyKey = propertyKey.Replace("-", ""); break;
            }

            propertyKey = propertyKey.Trim();

            if (dict.ContainsKey(propertyKey))
            {
                //Incerasing the number of segments, if necessary
                if (dict[propertyKey].Length <= segment)
                {
                    string[] longer = new string[segment + 1];

                    for (int i = 0; i < dict[propertyKey].Length; i++)
                        longer[i] = dict[propertyKey][i];

                    dict[propertyKey] = longer;
                }

                if (changeTyp != 0)
                {
                    //Values are getting edited

                    string[] oldValues = dict[propertyKey][segment].Split(',');
                    string[] newValues = value.Split(',');
                    value = "";

                    for (int i = 0; i < oldValues.Length && i < newValues.Length; i++)
                    {
                        double number = getNumberFromString(oldValues[i]);
                        double change = getNumberFromString(newValues[i]);

                        if (changeTyp == 2)
                        {
                            number = number * change;
                        }
                        else
                        {
                            number = number + (change * changeTyp);
                        }

                        value = value + "" + number;

                        if (i + 1 != oldValues.Length && i + 1 != newValues.Length)
                            value = value + ",";
                    }
                }


                dict[propertyKey][segment] = value;
            }
            else
            {
                //Key does not exist, so it is added
                dict.Add(propertyKey, new string[] { value });
            }
        }

        static public void changeParameter(string section, string newParameter, List<string> file)
        {
            newParameter = newParameter.Trim();
            section = section.Trim();

            if (newParameter.Trim() == "")
                return;
            int i = 0;

            while (file[i].ToLower().Contains(section.ToLower()) == false) //Skipping to the Section
            {
                i++;
            }

            i++;
            string parameter = newParameter.Substring(0, newParameter.IndexOf("="));
            if (newParameter.Contains("-="))
                parameter = newParameter.Substring(0, newParameter.IndexOf("-="));
            if (newParameter.Contains("+="))
                parameter = newParameter.Substring(0, newParameter.IndexOf("+="));
            if (newParameter.Contains("*="))
                parameter = newParameter.Substring(0, newParameter.IndexOf("*="));
            while (file[i].Contains(parameter) == false)  //Skipping to the parameter:
            {
                if ((file[i].Contains("[") && file[i].Contains("]")) || (i == (file.Count - 1))) //This means we are at the next session or the end of the file without finding the parameter. So we'll add a new one!
                {
                    file.Insert(i, newParameter);
                    return;
                }
                i++;
            }

            if (newParameter.Contains("-=") || newParameter.Contains("+=") || newParameter.Contains("*="))
            {
                string[] splittedOrigineel = file[i].Split(new char[] { ',' });
                string[] splittedNew = newParameter.Split(new char[] { ',' });
                if (splittedOrigineel.Length >= 1)
                {
                    for (int j = 0; j < splittedOrigineel.Length; j++)
                    {
                        double original = getNumberFromString(splittedOrigineel[j]);
                        double calculation = getNumberFromString(splittedNew[j]);
                        double result = 0;
                        if (newParameter.Contains("-="))
                            result = original - calculation;
                        else if (newParameter.Contains("+="))
                            result = original + calculation;
                        else if (newParameter.Contains("*="))
                            result = original * calculation;

                        splittedNew[j] = splittedOrigineel[j].Replace(original.ToString(), result.ToString());
                    }
                    file[i] = "";
                    foreach (string part in splittedNew)
                    {
                        file[i] += part + ",";
                    }
                    file[i] = file[i].TrimEnd(new char[] { ',' });
                }
            }
            else
            {
                file[i] = newParameter;
            }
        }


        static private double getNumberFromString(string theString)
        {
            string numbers = string.Empty;
            if (theString.Contains("="))
                theString = theString.Substring(theString.IndexOf("="));
            for (int i = 0; i < theString.Length; i++)
            {
                if (Char.IsDigit(theString[i]) || (theString[i]) == '.')
                    numbers += theString[i];
            }

            if (numbers.Length > 0)
                return double.Parse(numbers);
            else
                return 0;
        }

        public static double[] getNumbersFromString(string line, char spliterator)
        {
            string[] values = line.Split(spliterator);
            double[] numbers = new double[values.Length];

            for (int i = 0; i < values.Length; i++)
            {
                try
                {
                    numbers[i] = Double.Parse(values[i]);
                }
                catch
                {
                    numbers[i] = 0;
                }
            }

            return numbers;
        }

        //Returns a location for a new CarStat-window
        static public Point getLocation(int number, int amount)
        {
            while (number > 3)
            {
                number -= 4;
            }
            switch (number)
            {
                case 0: return new Point(0, 0);
                case 1: return new Point(Screen.PrimaryScreen.WorkingArea.Width - 718, 0);
                case 2: return new Point(0, Screen.PrimaryScreen.WorkingArea.Height - 380);
                case 3: return new Point(Screen.PrimaryScreen.WorkingArea.Width - 718, Screen.PrimaryScreen.WorkingArea.Height - 380);
            }
            return new Point(0, 0); ;
        }


    }
}
