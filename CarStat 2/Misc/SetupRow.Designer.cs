﻿namespace Misc
{
    partial class SetupRow
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.minus = new System.Windows.Forms.Button();
            this.valueDisplay = new System.Windows.Forms.Label();
            this.plus = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // minus
            // 
            this.minus.BackColor = System.Drawing.SystemColors.ControlLight;
            this.minus.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.minus.Location = new System.Drawing.Point(0, 0);
            this.minus.Name = "minus";
            this.minus.Size = new System.Drawing.Size(23, 23);
            this.minus.TabIndex = 1;
            this.minus.Text = "-";
            this.minus.UseVisualStyleBackColor = false;
            // 
            // valueDisplay
            // 
            this.valueDisplay.BackColor = System.Drawing.Color.White;
            this.valueDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.valueDisplay.Location = new System.Drawing.Point(28, 0);
            this.valueDisplay.Name = "valueDisplay";
            this.valueDisplay.Size = new System.Drawing.Size(110, 23);
            this.valueDisplay.TabIndex = 3;
            this.valueDisplay.Text = "Sample Text";
            this.valueDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // plus
            // 
            this.plus.BackColor = System.Drawing.SystemColors.ControlLight;
            this.plus.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.plus.Location = new System.Drawing.Point(144, 0);
            this.plus.Name = "plus";
            this.plus.Size = new System.Drawing.Size(23, 23);
            this.plus.TabIndex = 4;
            this.plus.Text = "+";
            this.plus.UseVisualStyleBackColor = false;
            // 
            // SetupRow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.plus);
            this.Controls.Add(this.valueDisplay);
            this.Controls.Add(this.minus);
            this.Name = "SetupRow";
            this.Size = new System.Drawing.Size(167, 23);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label valueDisplay;
        protected System.Windows.Forms.Button minus;
        protected System.Windows.Forms.Button plus;
    }
}
