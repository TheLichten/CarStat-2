﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Misc
{
    public partial class About : Form
    {
        private string newVersionLink;

        public About(Form owner, VersionNumber current)
        {
            InitializeComponent();

            lblMain.Text = "CarStat " + current.Major + "." + current.Minor;
            lblVersion.Text = "Version " + current.ToString();

            this.Owner = owner;

            //Reads the newest version
            bool newer = false;

            try
            {
                WebClient client = new WebClient();
                Stream stream = client.OpenRead("https://gitlab.com/TheLichten/CarStat-2/wikis/Version.md");
                StreamReader reader = new StreamReader(stream);
                string line = reader.ReadLine();
                newVersionLink = reader.ReadLine().Trim();

                reader.Close();
                stream.Close();
                string version = line.Split(':')[1].Trim();

                VersionNumber avaible = new VersionNumber(version);

                newer = (avaible.compareTo(current) == 1);

                this.lblNewVersionNumber.Text = avaible.ToString();
            }
            catch
            {

            }


            this.Visible = true;

            this.lblNewVersionLabel.Visible = newer;
            this.lblNewVersionNumber.Visible = newer;
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://forum.studio-397.com/index.php?threads/carstat-2-1-the-return.58106/");
        }

        private void linkMail_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("Mailto:lichtenrider@hotmail.de");
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("Mailto:bartstroobants@gmail.com");
        }

        private void lblNewVersionNumber_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (newVersionLink != null)
                System.Diagnostics.Process.Start(newVersionLink);
        }
    }
}
