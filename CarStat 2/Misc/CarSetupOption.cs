﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Misc
{
    public class CarSetupOption
    {
        public string Caption { get; internal protected set; }
        internal double StartValue { get; set; }
        internal double StepSize { get; set; }
        public int NumOfSettings { get; internal protected set; }
        public int DefaultSetting { get; set; }
        public SetupValue[] AllOptions { get; internal protected set; }

        public bool hasSpecialText { get; internal protected set; }


        public class SetupValue
        {
            public string Text { get; set; }
            public string Unit { get; set; }
            public double Value { get; set; }
        }

    }
}
