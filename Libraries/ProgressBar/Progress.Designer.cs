﻿namespace Update_Movies
{
    partial class Progress
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            this.number = new System.Windows.Forms.Label();
            this.moviename = new System.Windows.Forms.Label();
            this.Stop = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(39, 22);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(403, 23);
            this.progressBar1.Step = 1;
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(43, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Busy updating movie: ";
            // 
            // number
            // 
            this.number.AutoSize = true;
            this.number.Location = new System.Drawing.Point(43, 94);
            this.number.Name = "number";
            this.number.Size = new System.Drawing.Size(22, 13);
            this.number.TabIndex = 2;
            this.number.Text = "# /";
            // 
            // moviename
            // 
            this.moviename.AutoSize = true;
            this.moviename.Location = new System.Drawing.Point(151, 72);
            this.moviename.Name = "moviename";
            this.moviename.Size = new System.Drawing.Size(51, 13);
            this.moviename.TabIndex = 3;
            this.moviename.Text = "movietitle";
            // 
            // Stop
            // 
            this.Stop.Location = new System.Drawing.Point(332, 94);
            this.Stop.Name = "Stop";
            this.Stop.Size = new System.Drawing.Size(110, 23);
            this.Stop.TabIndex = 4;
            this.Stop.Text = "Stop updating";
            this.Stop.UseVisualStyleBackColor = true;
            this.Stop.Click += new System.EventHandler(this.Stop_Click);
            // 
            // Progress
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(490, 126);
            this.Controls.Add(this.Stop);
            this.Controls.Add(this.moviename);
            this.Controls.Add(this.number);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.progressBar1);
            this.Name = "Progress";
            this.Text = "Progress";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label number;
        public System.Windows.Forms.Label moviename;
        private System.Windows.Forms.Button Stop;
    }
}